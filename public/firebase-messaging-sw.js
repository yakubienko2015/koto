/*
Give the service worker access to Firebase Messaging.
Note that you can only use Firebase Messaging here, other Firebase libraries are not available in the service worker.
*/
importScripts('https://www.gstatic.com/firebasejs/7.18.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.18.0/firebase-messaging.js');

/*
Initialize the Firebase app in the service worker by passing in the messagingSenderId.
* New configuration for app@pulseservice.com
*/
firebase.initializeApp({
  apiKey: "AIzaSyBQoeky-I6yfAySe1IkNjlTSdRBOfic8qw",
  authDomain: "koto-78f7c.firebaseapp.com",
  databaseURL: "https://koto-78f7c.firebaseio.com",
  projectId: "koto-78f7c",
  storageBucket: "koto-78f7c.appspot.com",
  messagingSenderId: "740544526241",
  appId: "1:740544526241:web:86dbb6ee2508a51ce8bde0",
  measurementId: "G-RWH2VQEVKR"
});

/*
Retrieve an instance of Firebase Messaging so that it can handle background messages.
*/
const messaging = firebase.messaging();
messaging.setBackgroundMessageHandler(function(payload) {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  // Customize notification here
  const notificationTitle = 'Background Message Title';
  const notificationOptions = {
    body: 'Background Message body.',
    icon: '/firebase-logo.png'
  };

  return self.registration.showNotification(notificationTitle,
      notificationOptions);
});
