self.addEventListener('push', function(event) {
  event.waitUntil(
    sendNotification(event.data)
  );
});
function sendNotification (data) {
  try {
    data = data.json();
  } catch (e) {
    return self.registration.showNotification(data.text());
  }
  return self.registration.showNotification(data.title, data);
}
