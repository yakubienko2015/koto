<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| App Routes
|--------------------------------------------------------------------------
*/

Route::group(['middleware' => 'guest:app'], function () {
    Route::get('login', 'AuthController@showLoginForm')
        ->name('loginForm');
    Route::post('login', 'AuthController@login')
        ->name('login');
});

Route::group(['middleware' => 'auth:app'], function () {
    Route::post('logout', 'AuthController@logout')
        ->name('logout');
    Route::post('ping', 'AuthController@ping')
        ->name('ping');
    Route::get('user', 'AuthController@user')
        ->name('user');
    Route::put('send-rest-time', 'AuthController@sendRestTime')
        ->name('sendRestTime');

    Route::get('orders', 'HomeController@orders')
        ->name('orders');
    Route::get('orders/work-places', 'CookController@orderWorkPlaces');
    Route::get('orders/work-products', 'CookController@orderWorkProducts');
    Route::put('orders/{order}/make-ready', 'CookController@makeOrderReady');
    Route::put(
        'orders/{order}/make-ready-product/{orderProduct}',
        'CookController@makeOrderProductReady'
    );

    Route::get('/', 'HomeController@index');

    Route::group(['middleware' => 'can:module-courier'], function () {
        Route::get('courier', 'HomeController@courier')
            ->name('courier');
        Route::put('courier/logout', 'HomeController@logoutCourier')
            ->name('logoutCourier');
        Route::put('courier/set-delivered', 'HomeController@setOrderDelivered')
            ->name('setOrderDelivered');
        Route::get('courier-cabinet', 'HomeController@courierCabinet')
            ->name('courierCabinet');
        Route::put('courier/start-rest', 'HomeController@startCourierRest');
        Route::put('courier/stop-rest', 'HomeController@stopCourierRest');
        Route::put('courier/send-message', 'HomeController@sendMessage')
            ->name('sendMessage');
    });

    Route::group(['middleware' => 'can:module-cook'], function () {
        Route::get('cook/{terminal}/{workPlace}', 'CookController@cook')
            ->name('cook');

        Route::get('cook/{terminal}', 'CookController@issucook')
            ->name('issucook');

        Route::get('cook', 'CookController@cookSelect')
            ->name('cookSelect');
    });


    Route::group(['middleware' => 'can:module-admin'], function () {
        Route::get('stats/{terminal}', 'AdminController@stats')
            ->name('stats');
        Route::get('admin/{terminal}', 'AdminController@admin')
            ->name('admin');
        Route::put(
            'admin/change-terminal-status/{terminal}',
            'AdminController@changeTerminalStatus'
        );
        Route::get(
            'admin/terminal-stats/{terminal}',
            'AdminController@getTerminalStats'
        );
    });

    Route::group(['middleware' => 'can:module-operator'], function () {
        Route::get('operator', 'HomeController@operator')
            ->name('operator');
        Route::get('operator/terminals', 'HomeController@terminals');
        Route::post('operator/view/{order}', 'HomeController@view');
        Route::post('operator/to-order/{order}', 'HomeController@toOrder');
    });

    Route::group(['middleware' => 'can:module-logist'], function () {
        Route::get('logist', 'LogistController@logist')
            ->name('logist');
        Route::put('logist/login', 'LogistController@loginCourier')
            ->name('logist.login');
        Route::get('logist/couriers', 'LogistController@couriers')
            ->name('logist.couriers');
        Route::put('logist/set-courier', 'LogistController@setCourier')
            ->name('logist.setCourier');
    });
});
