<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SiteController@index')
    ->name('index');
Route::get('test', 'SiteController@test')
    ->name('test');

// Opinions
Route::get('r/{opinion:slug}', 'SiteController@opinion')
    ->name('opinion');
Route::post('r/{opinion:slug}', 'SiteController@opinionSave')
    ->name('opinion.save');
Route::get('qr/{promo:qr_code}', 'SiteController@qrCode')
    ->name('qr');
Route::get('qro', 'SiteController@qrOpinion')
    ->name('qrOpinion');

// Application QR code redirect
Route::get('{type}', 'SiteController@qrStore')
    ->where('type', 'ios|android')
    ->name('qrStore');
Route::post('{type}', 'SiteController@qrStoreSave')
    ->where('type', 'ios|android')
    ->name('qrStoreSave');

// Payments
Route::post('liqpay-status', 'SiteController@liqpayStatus')
    ->name('liqpayStatus');

Auth::routes();

// Telegram bot
Route::post('telegram/webhook', 'SiteController@telegramWebhook');

Route::middleware('auth')->group(function () {
    Route::get('account', 'AccountController@index')
        ->name('account');
    Route::put('account', 'AccountController@update')
        ->name('updateAccount');

    Route::post('subscriptions', 'PushController@update');
});

Route::prefix('ajax')->name('ajax.')->group(function () {
    Route::get('lang', 'AjaxController@getLangTexts');
    Route::any('basket/add', 'BasketController@add')
        ->name('basket.add');
    Route::any('basket/repeat/{order}', 'BasketController@repeat')
        ->name('basket.repeat');
    Route::any('basket/remove', 'BasketController@remove')
        ->name('basket.remove');
    Route::any('order', 'BasketController@order')
        ->name('basket.order');
    Route::get('order-track/{order}', 'AjaxController@orderTrack')
        ->name('basket.orderTrack');
    Route::any('basket', 'BasketController@basket')
        ->name('basket');
    Route::get('streets.json', 'AjaxController@streets');
    Route::post('callback', 'AjaxController@callback')
        ->name('callback');
    Route::get('find-bowl', 'AjaxController@findBowl');
    Route::get('how-many-kotcoins', 'AjaxController@howManyKotcoins')
        ->name('howManyKotcoins');
    Route::put('game-score', 'AjaxController@gameScore')
        ->name('gameScore');
    Route::get('valid-promo', 'AjaxController@validPromo')
        ->name('validPromo');
});
