<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| City Web Routes
|--------------------------------------------------------------------------
*/

$cities = DB::table('cities')->pluck('slug')->implode('|');
Route::prefix('{city:slug}')->where(['city' => $cities ? $cities : '0'])->group(function () {
    Route::get('/', 'SiteController@index')->name('cityIndex');

    Route::get('streets.json', 'AjaxController@streets');

    Route::get('qro', 'SiteController@qrOpinion')->name('qrOpinionFull');

    Route::get('payment-response/{order?}', 'SiteController@paymentResponse')
        ->name('paymentResponse');

    Route::get('search', 'SiteController@search')->name('search');
    Route::get('basket', 'SiteController@basket')->name('basket');
    Route::get('news', 'SiteController@posts')->name('posts');
    Route::get('sitemap', 'SiteController@sitemap')->name('sitemap');

    Route::any('success', 'SiteController@success')->name('success');
    Route::any('track/{order}', 'SiteController@track')->name('track');
    Route::get('play/{order}', 'SiteController@play')->name('play');

    $posts = DB::table('posts')->pluck('slug')->implode('|');
    Route::get('news/{post:slug}', 'SiteController@post')->name('post')
        ->where('post', $posts ? $posts : '0');

    $categories = DB::table('categories')->pluck('slug')->implode('|');
    Route::get('{category:slug}', 'SiteController@category')->name('category')
        ->where('category', $categories ? $categories : '0');

    $products = DB::table('products')->pluck('slug')->implode('|');
    Route::get('{category:slug}/{product:slug}', 'SiteController@product')
        ->name('product')
        ->where('category', $categories ? $categories : '0')
        ->where('product', $products ? $products : '0');

    $pages = DB::table('pages')->pluck('slug')->implode('|');
    Route::get('{page:slug}', 'SiteController@page')->name('page')
        ->where('page', $pages ? $pages : '0');
});
