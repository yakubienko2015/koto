<?php

// Dashboard
Route::get('/', 'DashboardController@dashboard')
    ->name('dashboard');

Route::resource('users', 'UserController')
    ->only('index')
    ->middleware('can:view-users');
Route::resource('users', 'UserController')
    ->except(['show', 'index'])
    ->middleware('can:manage-users');
Route::get('users/iiko', 'UserController@iiko')
    ->name('users.iiko')
    ->middleware('can:manage-users');

Route::resource('customers', 'CustomerController')
    ->only('index')
    ->middleware('can:view-customers');
Route::resource('customers', 'CustomerController')
    ->except(['index', 'show'])
    ->middleware('can:manage-customers');

Route::resource('birthdays', 'BirthdaysController')
    ->only('index')
    ->middleware('can:view-birthdays');

Route::resource('cities', 'CityController')
    ->only('index')
    ->middleware('can:view-cities');
Route::resource('cities', 'CityController')
    ->except(['index', 'show'])
    ->middleware('can:manage-cities');
Route::get('cities/iiko', 'CityController@iiko')
    ->name('cities.iiko')
    ->middleware('can:manage-cities');

Route::resource('roles', 'RoleController')
    ->only('index')
    ->middleware('can:view-roles');
Route::resource('roles', 'RoleController')
    ->except(['index', 'show'])
    ->middleware('can:manage-roles');
Route::post('permissions/store', 'RoleController@storePermission')
    ->name('permissions.store')
    ->middleware('can:manage-permissions');

Route::resource('terminals', 'TerminalController')
    ->only('index')
    ->middleware('can:view-terminals');
Route::resource('terminals', 'TerminalController')
    ->except(['index', 'show'])
    ->middleware('can:manage-terminals');
Route::get('terminals/iiko', 'TerminalController@iiko')
    ->name('terminals.iiko')
    ->middleware('can:manage-terminals');

Route::resource('categories', 'CategoryController')
    ->only('index')
    ->middleware('can:view-categories');
Route::resource('categories', 'CategoryController')
    ->except(['index', 'show'])
    ->middleware('can:manage-categories');
Route::get('categories/iiko', 'CategoryController@iiko')
    ->name('categories.iiko')
    ->middleware('can:manage-categories');

Route::resource('product-groups', 'ProductGroupController')
    ->only('index')
    ->middleware('can:view-product-groups');
Route::resource('product-groups', 'ProductGroupController')
    ->except(['index', 'show'])
    ->middleware('can:manage-product-groups');
Route::get('product-groups/iiko', 'ProductGroupController@iiko')
    ->name('product-groups.iiko')
    ->middleware('can:manage-product-groups');

Route::resource('products', 'ProductController')
    ->only('index')
    ->middleware('can:view-products');
Route::resource('products', 'ProductController')
    ->except(['index', 'show'])
    ->middleware('can:manage-products');
Route::post('products/{product}/remove-image', 'ProductController@removeImage')
    ->name('products.removeImage')
    ->middleware('can:manage-products');
Route::get('products/iiko', 'ProductController@iiko')
    ->name('products.iiko')
    ->middleware('can:manage-products');

Route::resource('achievements', 'AchievementController')
    ->only('index')
    ->middleware('can:view-achievements');
Route::resource('achievements', 'AchievementController')
    ->except(['index', 'show'])
    ->middleware('can:manage-achievements');
Route::post('achievements/{achievement}/remove-image', 'AchievementController@removeImage')
    ->name('achievements.removeImage')
    ->middleware('can:manage-achievements');

Route::resource('pages', 'PageController')
    ->only('index')
    ->middleware('can:view-pages');
Route::resource('pages', 'PageController')
    ->except(['index', 'show'])
    ->middleware('can:manage-pages');
Route::post('pages/{page}/remove-image', 'PageController@removeImage')
    ->name('pages.removeImage')
    ->middleware('can:manage-pages');

Route::resource('posts', 'PostController')
    ->only('index')
    ->middleware('can:view-posts');
Route::resource('posts', 'PostController')
    ->except(['index', 'show'])
    ->middleware('can:manage-posts');
Route::post('posts/{post}/remove-image', 'PostController@removeImage')
    ->name('posts.removeImage')
    ->middleware('can:manage-posts');

Route::resource('promos', 'PromoController')
    ->only('index')
    ->middleware('can:view-promos');
Route::get('promos/random', 'PromoController@randomQr')
    ->name('promos.random')
    ->middleware('can:manage-promos');
Route::resource('promos', 'PromoController')
    ->except(['index', 'show'])
    ->middleware('can:manage-promos');

Route::get('orders/game', 'OrderController@gameStats')
    ->name('orders.game')
    ->middleware('can:manage-orders');
Route::get('orders/iiko', 'OrderController@iiko')
    ->name('orders.iiko')
    ->middleware('can:manage-orders');
Route::resource('orders', 'OrderController')
    ->only(['index', 'show'])
    ->middleware('can:view-orders');
Route::resource('orders', 'OrderController')
    ->except(['index', 'show'])
    ->middleware('can:manage-orders');
Route::post('orders/sendtoiiko/{order}', 'OrderController@sendToIiko')
    ->name('orders.sendToIiko')
    ->middleware('can:manage-orders');

Route::get('settings', 'SettingsController@index')
    ->name('settings.index')
    ->middleware('can:view-settings');
Route::put('settings', 'SettingsController@update')
    ->name('settings.update')
    ->middleware('can:manage-settings');

Route::resource('slides', 'SlideController')
    ->only(['index'])
    ->middleware('can:view-slides');
Route::resource('slides', 'SlideController')
    ->except(['index', 'show'])
    ->middleware('can:manage-slides');
Route::post('slides/{slide}/remove-image', 'SlideController@removeImage')
    ->name('slides.removeImage')
    ->middleware('can:manage-slides');

Route::get('opinions', 'OpinionController@index')
    ->name('opinions.index')
    ->middleware('can:view-opinions');

Route::get('market-bonuses', 'MarketBonusController@index')
    ->name('marketBonuses.index')
    ->middleware('can:view-market-bonuses');

Route::resource('bowl-ingredients', 'BowlIngredientController')
    ->only('index')
    ->middleware('can:view-bowl-ingredients');
Route::resource('bowl-ingredients', 'BowlIngredientController')
    ->except(['index', 'show'])
    ->middleware('can:manage-bowl-ingredients');

Route::get('translations', 'SettingsController@translations')
    ->name('settings.translations')
    ->middleware('can:manage-translations');

Route::post('upload-image', 'DashboardController@uploadImage');

Route::resource('marks', 'MarkController')
    ->only('index')
    ->middleware('can:view-marks');
Route::resource('marks', 'MarkController')
    ->except(['index', 'show'])
    ->middleware('can:manage-marks');

// Telegram bot
Route::resource('telegram/groups', 'TelegramGroupController')
    ->only('index')
    ->middleware('can:view-telegram-groups');
Route::resource('telegram/groups', 'TelegramGroupController')
    ->except(['index', 'show'])
    ->middleware('can:manage-telegram-groups');
Route::post(
    'telegram/groups/{group}/send-message',
    'TelegramGroupController@sendMessage'
)->name('groups.sendMessage')
    ->middleware('can:manage-telegram-groups');

Route::resource('telegram/tasks', 'TelegramTaskController')
    ->only('index')
    ->middleware('can:view-telegram-tasks');
Route::resource('telegram/tasks', 'TelegramTaskController')
    ->except(['index', 'show'])
    ->middleware('can:manage-telegram-tasks');
Route::get('telegram/export', 'TelegramTaskController@export')
    ->name('tasks.export')
    ->middleware('can:view-telegram-tasks');

// Artisan
Route::get('artisan', 'DashboardController@artisan')
    ->name('artisan')
    ->middleware('can:run-commands');

Route::resource('callbacks', 'CallbackController')
    ->only('index')
    ->middleware('can:view-callbacks');

Route::resource('notifications', 'NotificationsController')
    ->only(['create', 'store', 'index'])
    ->middleware('can:manage-notifications');
