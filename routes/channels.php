<?php

use Illuminate\Support\Facades\Broadcast;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('app-orders.{city}', function ($user, $city) {
    $user->ping();
    return ['id' => $user->id, 'name' => $user->name];
});
// Broadcast::channel('app-operator.{city}', function ($user, $city) {
//     Timer::firstOrCreate([
//         'channel' => "presence-app-operator.{$city}",
//         'user_id' => $user->id,
//         'stopped_at' => null,
//     ]);
//     return ['id' => $user->id, 'name' => $user->name];
// });
Broadcast::channel('app-admin.{city}', function ($user, $city) {
    return ['id' => $user->id, 'name' => $user->name];
});
Broadcast::channel('app-city-order.{city}.{order}', function ($user, $city, $order) {
    return true;
});
Broadcast::channel('app-logist', function ($user) {
    return ['id' => $user->id, 'name' => $user->name];
});
