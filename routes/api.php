<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Api authentication
Route::group(['namespace' => 'Auth'], function () {
    Route::any('login', 'LoginController@login');
    Route::post('register', 'RegisterController@register');
    Route::post('get-sms', 'SmsController@get');
    Route::post('check-sms', 'SmsController@check');
    Route::put('reset-password', 'PasswordController@sendCodeForResetCustomerPassword');
    Route::post('reset-password', 'PasswordController@resetCustomerPassword');
    Route::post('logout', 'LoginController@logout')->middleware('auth:api');
});

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('orders/create', 'OrderController@create');
    Route::get('orders', 'OrderController@transactions');
    Route::get('orders/track', 'OrderController@track');
    Route::post('orders/calculate', 'OrderController@basket');
    Route::put('orders/cancel/{order}', 'OrderController@cancel');
    Route::get('orders/{order}', 'OrderController@transaction');
    Route::get('promo', 'OrderController@promo');
    Route::get('profile', 'ProfileController@index');
    Route::put('profile', 'ProfileController@update');
    Route::delete('profile', 'ProfileController@destroy');
    Route::get('achievements', 'GeneralInformationController@achievements');
    Route::get('bonuses', 'GeneralInformationController@bonuses');
    Route::post('opinion/{order}', 'ServiceRatingController@add');
    Route::get('notifications', 'GeneralInformationController@notifications');
    Route::put('notifications/set-token', 'GeneralInformationController@setDeviceToken');
    Route::put('notifications/{notification}', 'GeneralInformationController@readNotification');
    Route::delete('notifications/{notification}', 'GeneralInformationController@removeNotification');
    Route::get('pages/{page}', 'GeneralInformationController@pages');
});

Route::get('info', 'GeneralInformationController@info');
Route::get('bowls/ingredients', 'BowlConstructorController@getIngredientsTree');
Route::get('bowls/find', 'BowlConstructorController@findBowl');
Route::get('catalog', 'CatalogController@index');
Route::get('catalog/{category:slug}', 'CatalogController@category');
Route::get('product/{product:id}', 'CatalogController@product');

Route::get('cities', 'GeneralInformationController@cities');
Route::get('streets', 'GeneralInformationController@streets');
