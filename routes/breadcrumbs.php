<?php

Breadcrumbs::for('admin.dashboard', function ($trail) {
    $trail->push('Администрирование', route('admin.dashboard'));
});

Breadcrumbs::for('admin.users.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Работники', route('admin.users.index'));
});
Breadcrumbs::for('admin.users.create', function ($trail) {
    $trail->parent('admin.users.index');
    $trail->push('Создание');
});
Breadcrumbs::for('admin.users.edit', function ($trail) {
    $trail->parent('admin.users.index');
    $trail->push('Редактирование');
});
Breadcrumbs::for('admin.customers.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Клиенти', route('admin.customers.index'));
});
Breadcrumbs::for('admin.customers.create', function ($trail) {
    $trail->parent('admin.customers.index');
    $trail->push('Создание');
});
Breadcrumbs::for('admin.customers.edit', function ($trail) {
    $trail->parent('admin.customers.index');
    $trail->push('Редактирование');
});

Breadcrumbs::for('admin.birthdays.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Именинники', route('admin.birthdays.index'));
});

Breadcrumbs::for('admin.cities.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Города', route('admin.cities.index'));
});
Breadcrumbs::for('admin.cities.create', function ($trail) {
    $trail->parent('admin.cities.index');
    $trail->push('Создание');
});
Breadcrumbs::for('admin.cities.edit', function ($trail) {
    $trail->parent('admin.cities.index');
    $trail->push('Редактирование');
});

Breadcrumbs::for('admin.terminals.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Точки выдачи', route('admin.terminals.index'));
});
Breadcrumbs::for('admin.terminals.create', function ($trail) {
    $trail->parent('admin.terminals.index');
    $trail->push('Создание');
});
Breadcrumbs::for('admin.terminals.edit', function ($trail) {
    $trail->parent('admin.terminals.index');
    $trail->push('Редактирование');
});

Breadcrumbs::for('admin.roles.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Роли', route('admin.roles.index'));
});
Breadcrumbs::for('admin.roles.create', function ($trail) {
    $trail->parent('admin.roles.index');
    $trail->push('Создание');
});
Breadcrumbs::for('admin.roles.edit', function ($trail) {
    $trail->parent('admin.roles.index');
    $trail->push('Редактирование');
});

Breadcrumbs::for('admin.categories.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Категории', route('admin.categories.index'));
});
Breadcrumbs::for('admin.categories.create', function ($trail) {
    $trail->parent('admin.categories.index');
    $trail->push('Создание');
});
Breadcrumbs::for('admin.categories.edit', function ($trail) {
    $trail->parent('admin.categories.index');
    $trail->push('Редактирование');
});

Breadcrumbs::for('admin.products.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Товары', route('admin.products.index'));
});
Breadcrumbs::for('admin.products.create', function ($trail) {
    $trail->parent('admin.products.index');
    $trail->push('Создание');
});
Breadcrumbs::for('admin.products.edit', function ($trail) {
    $trail->parent('admin.products.index');
    $trail->push('Редактирование');
});

Breadcrumbs::for('admin.achievements.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Ачивки', route('admin.achievements.index'));
});
Breadcrumbs::for('admin.achievements.create', function ($trail) {
    $trail->parent('admin.achievements.index');
    $trail->push('Создание');
});
Breadcrumbs::for('admin.achievements.edit', function ($trail) {
    $trail->parent('admin.achievements.index');
    $trail->push('Редактирование');
});

Breadcrumbs::for('admin.pages.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Страницы', route('admin.pages.index'));
});
Breadcrumbs::for('admin.pages.create', function ($trail) {
    $trail->parent('admin.pages.index');
    $trail->push('Создание');
});
Breadcrumbs::for('admin.pages.edit', function ($trail) {
    $trail->parent('admin.pages.index');
    $trail->push('Редактирование');
});

Breadcrumbs::for('admin.promos.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Промо', route('admin.promos.index'));
});
Breadcrumbs::for('admin.promos.create', function ($trail) {
    $trail->parent('admin.promos.index');
    $trail->push('Создание');
});
Breadcrumbs::for('admin.promos.edit', function ($trail) {
    $trail->parent('admin.promos.index');
    $trail->push('Редактирование');
});

Breadcrumbs::for('admin.posts.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Блог', route('admin.posts.index'));
});
Breadcrumbs::for('admin.posts.create', function ($trail) {
    $trail->parent('admin.posts.index');
    $trail->push('Создание');
});
Breadcrumbs::for('admin.posts.edit', function ($trail) {
    $trail->parent('admin.posts.index');
    $trail->push('Редактирование');
});

Breadcrumbs::for('admin.bowl-ingredients.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Конструктор боулов', route('admin.bowl-ingredients.index'));
});
Breadcrumbs::for('admin.bowl-ingredients.create', function ($trail) {
    $trail->parent('admin.bowl-ingredients.index');
    $trail->push('Создание');
});
Breadcrumbs::for('admin.bowl-ingredients.edit', function ($trail) {
    $trail->parent('admin.bowl-ingredients.index');
    $trail->push('Редактирование');
});

Breadcrumbs::for('admin.groups.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Групы', route('admin.groups.index'));
});
Breadcrumbs::for('admin.groups.create', function ($trail) {
    $trail->parent('admin.groups.index');
    $trail->push('Создание');
});
Breadcrumbs::for('admin.groups.edit', function ($trail) {
    $trail->parent('admin.groups.index');
    $trail->push('Редактирование');
});

Breadcrumbs::for('admin.product-groups.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Групы', route('admin.product-groups.index'));
});
Breadcrumbs::for('admin.product-groups.create', function ($trail) {
    $trail->parent('admin.product-groups.index');
    $trail->push('Создание');
});
Breadcrumbs::for('admin.product-groups.edit', function ($trail) {
    $trail->parent('admin.product-groups.index');
    $trail->push('Редактирование');
});

Breadcrumbs::for('admin.tasks.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Задачи', route('admin.tasks.index'));
});
Breadcrumbs::for('admin.tasks.create', function ($trail) {
    $trail->parent('admin.tasks.index');
    $trail->push('Создание');
});
Breadcrumbs::for('admin.tasks.edit', function ($trail) {
    $trail->parent('admin.tasks.index');
    $trail->push('Редактирование');
});

Breadcrumbs::for('admin.marks.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Метки', route('admin.marks.index'));
});
Breadcrumbs::for('admin.marks.create', function ($trail) {
    $trail->parent('admin.marks.index');
    $trail->push('Создание');
});
Breadcrumbs::for('admin.marks.edit', function ($trail) {
    $trail->parent('admin.marks.index');
    $trail->push('Редактирование');
});

Breadcrumbs::for('admin.slides.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Слайд-шоу', route('admin.slides.index'));
});
Breadcrumbs::for('admin.slides.create', function ($trail) {
    $trail->parent('admin.slides.index');
    $trail->push('Создание');
});
Breadcrumbs::for('admin.slides.edit', function ($trail) {
    $trail->parent('admin.slides.index');
    $trail->push('Редактирование');
});

Breadcrumbs::for('admin.orders.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Заказы', route('admin.orders.index'));
});
Breadcrumbs::for('admin.orders.create', function ($trail) {
    $trail->parent('admin.orders.index');
    $trail->push('Создание');
});
Breadcrumbs::for('admin.orders.show', function ($trail) {
    $trail->parent('admin.orders.index');
    $trail->push('Просмотр');
});
Breadcrumbs::for('admin.orders.edit', function ($trail) {
    $trail->parent('admin.orders.index');
    $trail->push('Редактирование');
});

Breadcrumbs::for('admin.settings.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Настройки', route('admin.settings.index'));
});

Breadcrumbs::for('admin.opinions.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Отзывы', route('admin.opinions.index'));
});

Breadcrumbs::for('admin.marketBonuses.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('QR приложение', route('admin.marketBonuses.index'));
});

Breadcrumbs::for('admin.callbacks.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Обратные вызовы', route('admin.callbacks.index'));
});

Breadcrumbs::for('admin.notifications.create', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Создание уведомления');
});
