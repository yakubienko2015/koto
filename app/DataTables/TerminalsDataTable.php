<?php

namespace App\DataTables;

use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Services\DataTable;

class TerminalsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('name', function ($item){
                return $item->name;
            })
            ->addColumn('action', 'admin.partials.actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Terminal $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        return app('city')->terminals();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('terminals-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters(config('datatables-html.parameters'))
                    ->buttons(
                        Button::make('create')->action("window.location = '".route('admin.terminals.create')."';"),
                        Button::make('reset'),
                        Button::make([
                            'text' => __('admin.sync_with_iiko'),
                        ])->action("window.location = '".route('admin.terminals.iiko')."';")
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'id', 'title' => 'ID', 'searchable' => false, 'width' => '10px'],
            ['data' => 'name', 'title' => 'Название', 'orderable' => false],
            ['data' => 'action', 'title' => '<i class="icon fas fa-pencil-ruler"></i>', 'orderable' => false, 'width' => '10px'],
        ];
    }
}
