<?php

namespace App\DataTables;

use App\Models\Product;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Services\DataTable;

class ProductsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('name', function ($item){
                return $item->name;
            })
            ->editColumn('price', function ($item){
                return "{$item->price} грн";
            })
            ->editColumn('category.name', function ($item){
                return $item->category->name ?? '-';
            })
            ->editColumn('slug', function ($item){
                return route('product', [app('city'), $item->category ?? '-', $item->slug]);
            })
            ->editColumn('is_active', function ($item){
                return $item->is_active
                    ? '<i class="icon fas fa-check"></i>'
                    : '<i class="icon fas fa-times"></i>';
            })
            ->addColumn('action', 'admin.partials.actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Product $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Product $model)
    {
        return $model->newQuery()->with('category', 'meta');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('products-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters(config('datatables-html.parameters'))
                    ->buttons(
                        Button::make([
                            'extend' => 'create',
                            'text' => '<i class="fa fa-plus"></i> Создать',
                        ])->action("window.location = '".route('admin.products.create')."';"),
                        Button::make([
                            'extend' => 'reset',
                            'text' => '<i class="fa fa-undo"></i> Обновить',
                        ]),
                        Button::make([
                            'text' => '<i class="fa fa-download"></i> ' . __('admin.sync_with_iiko'),
                            'disabled' => true,
                        ])->action(app()->getLocale() != 'ru'
                            ? 'alert("Включите русский язык")'
                            : "confirm('для товаров обновятся следующие поля: название, категория, группа, цена, ккал, БЖУ, порядок, доступность, изображение (если его нет в карточке)')?window.location = '".route('admin.products.iiko')."':'';")
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'id', 'title' => 'ID', 'searchable' => false, 'width' => '10px'],
            ['data' => 'name', 'title' => 'Название', 'orderable' => false],
            ['data' => 'category.name', 'title' => 'Категория', 'orderable' => false],
            ['data' => 'price', 'title' => 'Цена'],
            ['data' => 'slug', 'title' => 'URL'],
            ['data' => 'priority', 'title' => 'Порядок'],
            ['data' => 'is_active', 'title' => '<i class="icon fas fa-laptop"></i>', 'orderable' => false, 'width' => '10px'],
            ['data' => 'action', 'title' => '<i class="icon fas fa-pencil-ruler"></i>', 'orderable' => false, 'width' => '10px'],
        ];
    }
}
