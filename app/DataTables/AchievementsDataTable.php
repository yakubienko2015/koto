<?php

namespace App\DataTables;

use App\Models\Achievement;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Services\DataTable;

class AchievementsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $categories = collect(app('iiko')
            ->OrganizationsApi()
            ->getGuestCategories(app('city')->organization_uuid) ?? [])
            ->pluck('name', 'id');

        return datatables()
            ->eloquent($query)
            ->editColumn('name', function ($item){
                return $item->name;
            })
            ->editColumn('has_category', function ($item) use ($categories) {
                return $categories[$item->has_category_uuid] ?? '-';
            })
            ->editColumn('get_category', function ($item) use ($categories) {
                return $categories[$item->get_category_uuid] ?? '-';
            })
            ->editColumn('is_active', function ($item){
                return $item->is_active
                    ? '<i class="icon fas fa-check"></i>'
                    : '<i class="icon fas fa-times"></i>';
            })
            ->addColumn('action', 'admin.partials.actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Achievement $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Achievement $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('achievements-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters(config('datatables-html.parameters'))
                    ->buttons(
                        Button::make([
                            'extend' => 'create',
                            'text' => '<i class="fa fa-plus"></i> Создать',
                        ])->action("window.location = '".route('admin.achievements.create')."';"),
                        Button::make([
                            'extend' => 'reset',
                            'text' => '<i class="fa fa-undo"></i> Обновить',
                        ])
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'id', 'title' => 'ID', 'searchable' => false, 'width' => '10px'],
            ['data' => 'name', 'title' => 'Название', 'orderable' => false],
            ['data' => 'active_from', 'title' => 'Дата начала', 'orderable' => false],
            ['data' => 'active_to', 'title' => 'Дата конца', 'orderable' => false],
            ['data' => 'has_category', 'title' => 'Категория гостей для ачивки'],
            ['data' => 'get_category', 'title' => 'Категория гостей прохождения'],
            ['data' => 'is_active', 'title' => '<i class="icon fas fa-laptop"></i>', 'orderable' => false, 'width' => '10px'],
            ['data' => 'action', 'title' => '<i class="icon fas fa-pencil-ruler"></i>', 'orderable' => false, 'width' => '10px'],
        ];
    }
}
