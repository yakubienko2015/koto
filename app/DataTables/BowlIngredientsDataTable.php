<?php

namespace App\DataTables;

use App\Models\BowlIngredient;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Services\DataTable;

class BowlIngredientsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('name', function ($item){
                return $item->name;
            })
            ->addColumn('action', 'admin.partials.actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\BowlIngredient $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(BowlIngredient $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('bowl-ingredients-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters(config('datatables-html.parameters'))
                    ->buttons(
                        Button::make('create')->action("window.location = '".route('admin.bowl-ingredients.create')."';"),
                        Button::make('reset')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'id', 'title' => 'ID', 'searchable' => false, 'width' => '10px'],
            ['data' => 'name', 'title' => 'Название', 'orderable' => false],
            ['data' => 'step', 'title' => 'Шаг', 'orderable' => true],
            ['data' => 'action', 'title' => '<i class="icon fas fa-pencil-ruler"></i>', 'orderable' => false, 'width' => '10px'],
        ];
    }
}
