<?php

namespace App\DataTables;

use App\Models\City;
use App\Models\Promo;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Services\DataTable;

class PromosDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('is_active', function ($item){
                return $item->is_active
                    ? '<i class="icon fas fa-check"></i>'
                    : '<i class="icon fas fa-times"></i>';
            })
            ->editColumn('qr_code', function ($item){
                return $item->qr_code ? route('qr', $item->qr_code) : '-';
            })
            ->addColumn('action', 'admin.partials.actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Promo $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Promo $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('promos-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters(config('datatables-html.parameters'))
                    ->buttons(
                        Button::make([
                            'extend' => 'create',
                            'text' => '<i class="fa fa-plus"></i> Создать',
                        ])->action("window.location = '".route('admin.promos.create')."';"),
                        Button::make([
                            'extend' => 'reset',
                            'text' => '<i class="fa fa-undo"></i> Обновить',
                        ]),
                        Button::make([
                            'text' => '<i class="fa fa-download"></i> ' . __('QR generate'),
                            'disabled' => true,
                        ])->action("window.location = '".route('admin.promos.random')."';")
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'code', 'title' => 'Код', 'orderable' => false],
            ['data' => 'qr_code', 'title' => 'QR код', 'orderable' => false],
            ['data' => 'is_active', 'title' => '<i class="icon fas fa-laptop"></i>', 'orderable' => false, 'width' => '10px'],
            ['data' => 'action', 'title' => '<i class="icon fas fa-pencil-ruler"></i>', 'orderable' => false, 'width' => '10px'],
        ];
    }
}
