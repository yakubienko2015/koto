<?php

namespace App\DataTables;

use App\Models\Category;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Services\DataTable;

class CategoriesDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('name', function ($item){
                return $item->name;
            })
            ->editColumn('slug', function ($item){
                return route('category', [app('city'), $item->slug]);
            })
            ->editColumn('is_active', function ($item){
                return $item->is_active
                    ? '<i class="icon fas fa-check"></i>'
                    : '<i class="icon fas fa-times"></i>';
            })
            ->addColumn('action', 'admin.partials.actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Category $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Category $model)
    {
        return $model->newQuery()->with('meta');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('categories-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters(config('datatables-html.parameters'))
                    ->buttons(
                        Button::make([
                            'extend' => 'create',
                            'text' => '<i class="fa fa-plus"></i> Создать',
                        ])->action("window.location = '".route('admin.categories.create')."';"),
                        Button::make([
                            'extend' => 'reset',
                            'text' => '<i class="fa fa-undo"></i> Обновить',
                        ]),
                        Button::make([
                            'text' => '<i class="fa fa-download"></i> ' . __('admin.sync_with_iiko'),
                            'disabled' => true,
                        ])->action(app()->getLocale() != 'ru'
                            ? 'alert("Включите русский язык")'
                            : "window.location = '".route('admin.categories.iiko')."';")
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'id', 'title' => 'ID', 'searchable' => false, 'width' => '10px'],
            ['data' => 'name', 'title' => 'Название'],
            ['data' => 'slug', 'title' => 'URL'],
            ['data' => 'priority', 'title' => 'Порядок'],
            ['data' => 'is_active', 'title' => '<i class="icon fas fa-laptop"></i>', 'orderable' => false, 'width' => '10px'],
            ['data' => 'action', 'title' => '<i class="icon fas fa-pencil-ruler"></i>', 'orderable' => false, 'width' => '10px'],
        ];
    }
}
