<?php

namespace App\DataTables;

use Spatie\Permission\Models\Role;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Services\DataTable;

class RolesDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('title', function ($role){
                return __("admin.roles.{$role->name}");
            })
            ->addColumn('action', 'admin.partials.actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Role $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Role $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('roles-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters(config('datatables-html.parameters'))
                    ->buttons(
                        Button::make('create')->action("window.location = '".route('admin.roles.create')."';"),
                        Button::make('reset')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'id', 'title' => 'ID', 'searchable' => false, 'width' => '10px'],
            ['data' => 'name', 'title' => 'Машинное имя'],
            ['data' => 'title', 'title' => 'Название'],
            ['data' => 'action', 'title' => '<i class="icon fas fa-pencil-ruler"></i>', 'width' => '10px'],
        ];
    }
}
