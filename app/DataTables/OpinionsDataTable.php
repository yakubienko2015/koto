<?php

namespace App\DataTables;

use App\Models\Opinion;
use Yajra\DataTables\Services\DataTable;

class OpinionsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('filled_at', function ($item){
                return $item->filled_at;
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Opinion $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Opinion $model)
    {
        return $model->newQuery()
            ->whereNotNull('filled_at')
            ->orderBy('filled_at', 'desc')
            ->with('order');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('opinions-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters(config('datatables-html.parameters'))
                    ->buttons([
                        'excel', 'csv', 'reset'
                    ])
                    ->orderBy(0);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return array_merge([
            ['data' => 'order.id', 'title' => 'Заказ', 'orderable' => false],
            ['data' => 'order.phone', 'title' => 'Телефон', 'orderable' => false],
            ['data' => 'order.customer_name', 'title' => 'Имя', 'orderable' => false],
            ['data' => 'filled_at', 'title' => 'Дата', 'orderable' => false],
            ['data' => 'comment', 'title' => 'Коментарий', 'orderable' => false],
        ], array_map(function ($e) {
            return [
                'data' => "marks.{$e}",
                'title' => __($e),
            ];
        }, config('shop.opinion_marks')));
    }
}
