<?php

namespace App\DataTables;

use App\Models\Task;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Services\DataTable;

class TasksDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('next_time', function ($item){
                return $item->next_time;
            })
            ->addColumn('action', 'admin.partials.actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Task $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Task $model)
    {
        return $model->newQuery()->with('group');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('tasks-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters(config('datatables-html.parameters'))
                    ->buttons(
                        Button::make([
                            'extend' => 'create',
                            'text' => '<i class="fa fa-plus"></i> Создать',
                        ])->action("window.location = '".route('admin.tasks.create')."';"),
                        Button::make([
                            'extend' => 'reset',
                            'text' => '<i class="fa fa-undo"></i> Обновить',
                        ]),
                        Button::make([
                            'text' => 'Отчет за сегодня',
                        ])->action("window.location = '".route('admin.tasks.export', ['date' => now()->format('Y-m-d')])."';"),
                        Button::make([
                            'text' => 'Скачать отчет',
                        ])->action("window.location = '".route('admin.tasks.export')."';")
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'id', 'title' => 'ID', 'searchable' => false, 'width' => '10px'],
            ['data' => 'title', 'title' => 'Название', 'orderable' => false],
            ['data' => 'group.name', 'title' => 'Группа', 'orderable' => false],
            ['data' => 'next_time', 'title' => 'Следущая дата/время', 'orderable' => false],
            ['data' => 'action', 'title' => '<i class="icon fas fa-pencil-ruler"></i>', 'orderable' => false, 'width' => '10px'],
        ];
    }
}
