<?php

namespace App\DataTables;

use App\Models\Callback;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Services\DataTable;

class CallbacksDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Callback $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Callback $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('callbacks-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters(config('datatables-html.parameters'))
                    ->buttons(
                        Button::make('reset')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'id', 'title' => 'ID', 'searchable' => false, 'width' => '10px'],
            ['data' => 'phone', 'title' => 'Номер телефона', 'orderable' => false],
            ['data' => 'name', 'title' => 'Имя', 'orderable' => false],
            ['data' => 'comment', 'title' => 'Комментарий', 'orderable' => false],
            ['data' => 'created_at', 'title' => 'Дата/Время', 'orderable' => true],
        ];
    }
}
