<?php

namespace App\DataTables;

use App\Models\User;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Services\DataTable;

class UsersDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('pin', function ($item) {
                return $item->getActivePin()->id ?? '-';
            })
            ->addColumn('is_active', function ($item) {
                $urls = $item
                    ->timers()
                    ->whereNull('stopped_at')
                    ->pluck('channel')
                    ->implode(', ');
                return $urls ? "<b title='{$urls}'>+</b>" : '<b>-</b>';
                return $item->timers()->whereNull('stopped_at')->pluck('channel')->implode(', ');
            })
            ->addColumn('action', 'admin.partials.actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        return $model->newQuery()->whereDoesntHave('roles', function ($query) {
            $query->whereName('customer');
        });
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('users-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters(config('datatables-html.parameters'))
                    ->buttons(
                        Button::make([
                            'extend' => 'create',
                            'text' => '<i class="fa fa-plus"></i> Создать',
                        ])->action("window.location = '".route('admin.users.create')."';"),
                        Button::make('export'),
                        Button::make([
                            'extend' => 'reset',
                            'text' => '<i class="fa fa-undo"></i> Обновить',
                        ]),
                        Button::make([
                            'text' => __('admin.sync_with_iiko'),
                        ])->action("confirm('Обновить только пен для существующего работника, иначе создаст нового с автоматически подобранной ролью.')?window.location = '".route('admin.users.iiko')."':'';")
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'id', 'title' => 'ID', 'searchable' => false, 'width' => '10px'],
            ['data' => 'name', 'title' => 'Имя'],
            ['data' => 'phone', 'title' => 'Номер телефона'],
            ['data' => 'role_names', 'title' => 'Роль', 'searchable' => false],
            ['data' => 'telegram_id', 'title' => 'Telegram id'],
            ['data' => 'is_active', 'title' => 'Онлайн', 'width' => '10px', 'searchable' => false, 'sortable' => false],
            ['data' => 'action', 'title' => '<i class="icon fas fa-pencil-ruler"></i>', 'width' => '10px', 'searchable' => false, 'sortable' => false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'users_' . date('YmdHis');
    }
}
