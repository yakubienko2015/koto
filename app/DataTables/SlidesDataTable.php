<?php

namespace App\DataTables;

use App\Models\Slide;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Services\DataTable;

class SlidesDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('title', function ($item){
                return $item->title;
            })
            ->editColumn('is_active', function ($item){
                return $item->is_active
                    ? '<i class="icon fas fa-check"></i>'
                    : '<i class="icon fas fa-times"></i>';
            })
            ->addColumn('action', 'admin.partials.actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Slide $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Slide $model)
    {
        return $model->whereCityId(null)
            ->orWhere('city_id', app('city')->id);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('slides-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters(config('datatables-html.parameters'))
                    ->buttons(
                        Button::make('create')->action("window.location = '".route('admin.slides.create')."';"),
                        Button::make('reset')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'id', 'title' => 'ID', 'searchable' => false, 'width' => '10px'],
            ['data' => 'title', 'title' => 'Заголовок', 'orderable' => false],
            ['data' => 'priority', 'title' => 'Порядок', 'searchable' => false],
            ['data' => 'is_active', 'title' => '<i class="icon fas fa-laptop"></i>', 'searchable' => false, 'width' => '10px'],
            ['data' => 'action', 'title' => '<i class="icon fas fa-pencil-ruler"></i>', 'orderable' => false, 'width' => '10px'],
        ];
    }
}
