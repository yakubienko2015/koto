<?php

namespace App\DataTables;

use App\Models\Order;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Services\DataTable;

class OrdersDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('ordered_at', function ($item){
                return $item->ordered_at;
            })
            ->editColumn('status', function ($item){
                return config('shop.order_statuses')[$item->status] ?? '-';
            })
            ->editColumn('terminal.name', function ($item){
                return $item->terminal->name ?? '-';
            })
            ->addColumn('action', 'admin.partials.actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Order $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Order $model)
    {
        return app('city')->orders()->with('terminal');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('orders-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters(config('datatables-html.parameters'))
                    ->orderBy(0)
                    ->buttons(
                        Button::make('create')->action("window.location = '".route('admin.orders.create')."';"),
                        Button::make('reset'),
                        Button::make([
                            'text' => 'Сверить с iiko',
                        ])->action("window.location = '".route('admin.orders.iiko')."';")//,
                        // Button::make([
                        //     'text' => 'Скачать статистику по игре',
                        // ])->action("window.location = '".route('admin.orders.game')."';")
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'id', 'title' => 'ID', 'width' => '10px'],
            ['data' => 'number', 'title' => 'Номер', 'orderable' => false],
            ['data' => 'terminal.name', 'title' => 'Терминал', 'orderable' => false],
            ['data' => 'customer_name', 'title' => 'Имя', 'orderable' => false],
            ['data' => 'phone', 'title' => 'Телефон', 'orderable' => false],
            ['data' => 'sum', 'title' => 'Сумма', 'orderable' => false],
            ['data' => 'ordered_at', 'title' => 'Дата/время'],
            ['data' => 'status', 'title' => 'Статус', 'orderable' => false],
            ['data' => 'action', 'title' => '<i class="icon fas fa-pencil-ruler"></i>', 'orderable' => false, 'width' => '10px'],
        ];
    }
}
