<?php

namespace App\DataTables;

use App\Models\MarketBonus;
use Yajra\DataTables\Services\DataTable;

class MarketBonusesDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('activated_at', function ($item){
                return $item->activated_at ?? 'Не начислено';
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\MarketBonus $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(MarketBonus $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('MarketBonuses-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters(config('datatables-html.parameters'))
                    ->buttons([
                        'excel', 'csv', 'reset'
                    ])
                    ->orderBy(0);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'id', 'title' => 'ID', 'searchable' => false, 'width' => '10px'],
            ['data' => 'type', 'title' => 'Тип устройства'],
            ['data' => 'phone', 'title' => 'Номер телефона'],
            ['data' => 'activated_at', 'title' => 'Дата начисления'],
            ['data' => 'created_at', 'title' => 'Дата регистрации'],
        ];
    }
}
