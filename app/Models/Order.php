<?php

namespace App\Models;

use App\Events\NewOrder;
use App\Events\UpdateOrder;
use App\Events\UpdateTerminalStats;
use App\Notifications\PreOrderNotification;
use App\Notifications\WinGameNotification;
use App\Traits\HasOrderIiko;
use App\Traits\TimestampSerializable;
use Auth;
use Carbon\Carbon;
use Hash;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Order extends Model
{
    use TimestampSerializable, HasOrderIiko;

    public $usedPromo = null;

    /**
     * @var array
     */
    protected $fillable = [
        'uuid',
        'city_id',
        'terminal_id',
        'user_id',
        'ordered_at',
        'cook_at',
        'phone',
        'customer_name',
        'payment_type',
        'sum',
        'data',
        'status',
        'number',
    ];

    /**
     * @var array
     */
    public $casts = [
        'data' => 'array',
        'city_id' => 'integer',
        'terminal_id' => 'integer',
        'user_id' => 'integer',
        'sum' => 'double',
        'status' => 'integer',
        'ordered_at' => 'datetime',
        'cook_at' => 'datetime',
        'created_at' => 'datetime',
    ];

    /**
     * @var array
     */
    protected $attributes = [
    ];

    protected $dates = [
        'ordered_at',
        'cook_at',
    ];

    protected $appends = [
        'status_name',
        'is_pre_order',
        'time_left',
        'time_cook_left',
        'sauce_cost',
        'free_sauces',
    ];

    protected $dispatchesEvents = [
        'created' => NewOrder::class,
    ];

    const ORDER_STATUS_NO_EXISTS = 0;
    const ORDER_STATUS_NEW = 1;
    const ORDER_STATUS_OPERATE = 11;
    const ORDER_STATUS_IN_PROGRESS = 2;
    const ORDER_STATUS_READY = 3;
    const ORDER_STATUS_WAITING = 4;
    const ORDER_STATUS_ON_WAY = 5;
    const ORDER_STATUS_CANCELLED = 6;
    const ORDER_STATUS_DELIVERED = 7;
    const ORDER_STATUS_UNCONFIRMED = 8;
    const ORDER_STATUS_CLOSED = 9;

    const ORDER_PAYMENT_LIQPAY = 'liqpay';
    const ORDER_PAYMENT_CASH = 'cash';
    const ORDER_PAYMENT_KOTCOINS = 'kotcoins';

    const ORDER_PROMO_COUPON_PRODUCT_ID = 132;

    const BOWL_MODIFIER_CATEGORY_ID = 6;

    /**
     * @var array
     */
    public $basket = null;

    protected static function booted()
    {
        static::updated(function ($order) {
            GLOBAL $nestingOrderLevel;
            $nestingOrderLevel++;
            if ($nestingOrderLevel > 2) {
                logger()->channel('orderStatuses')->info("ORDER #{$order->number}/{$order->id} STATUS BROKEN");
                return;
            }
            if ($order->wasChanged('status')) {
                logger()->channel('orderStatuses')->info("ORDER #{$order->number}/{$order->id} STATUS CHANGE TO " . config("shop.order_statuses.{$order->status}", $order->status) . ' BY ' . (auth()->user()->name ?? 'underfined') . ' ON ' . url()->current());
                if ($order->status >= Order::ORDER_STATUS_IN_PROGRESS
                    && $order->status <= Order::ORDER_STATUS_CLOSED) {
                    if ($order->terminal) {
                        event(new UpdateTerminalStats($order->terminal));
                    }
                }
                if ($order->status == Order::ORDER_STATUS_IN_PROGRESS) {
                    try {
                        $order->updateWorkPlaces();
                    } catch (\Throwable $th) {
                        logger('error on update work place', [$th->getMessage()]);
                    }
                    $orderData = $order->data;
                    $timeForDelivery = $order->city->getDeliveryTime();
                    $orderData['time_for_delivery'] = $timeForDelivery;
                    $orderData['time_for_cook'] = $timeForDelivery == 59 ? 25 : 15;
                    if (! $order->wasChanged('cook_at')) {
                        // Got recursion on $orderData['pre_order'] == true (update)
                        $order->update([
                            'data' => $orderData,
                            'cook_at' => $orderData['pre_order'] ? null : now(),
                        ]);
                    }
                }
                if ($order->status == Order::ORDER_STATUS_CLOSED
                        && $order->user_id) {
                    $order->createOpinionLink();
                }
                if ($order->status == Order::ORDER_STATUS_CLOSED) {
                    // Got recursion because update too
                    $order->sendGameScore(true);
                }
                if ($order->status == Order::ORDER_STATUS_ON_WAY) {
                    $order->autoAssignCourier();
                }
                event(new UpdateOrder($order));
                logger()->channel('orderStatuses')->info("ORDER #{$order->number}/{$order->id} STATUS CHANGED");
            }
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function terminal()
    {
        return $this->belongsTo('App\Models\Terminal');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function opinion()
    {
        return $this->hasOne('App\Models\Opinion');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions()
    {
        return $this->hasMany('App\Models\Transaction');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\Models\User')
            ->withPivot('role', 'created_at');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function operators()
    {
        return $this->users()->wherePivot('role', 'operator');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function couriers()
    {
        return $this->users()->wherePivot('role', 'courier');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany('App\Models\Product')
            ->using('App\Models\OrderProduct')
            ->withPivot([
                'amount',
                'sum',
                'modifiers',
                'cooked_at',
                'sub_product_id',
                'id',
            ])
            ->where('sub_product_id', null);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function subProducts()
    {
        return $this->belongsToMany('App\Models\Product', 'order_product', 'order_id', 'sub_product_id')
            ->using('App\Models\OrderProduct')
            ->withPivot([
                'amount',
                'sum',
                'modifiers',
                'cooked_at',
                'product_id',
                'id',
            ]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function workPlaces()
    {
        return $this->belongsToMany('App\Models\WorkPlace')
            ->using('App\Models\OrderWorkPlace')
            ->withPivot([
                'id',
                'completed_at',
                'user_id',
                'product_id',
                'created_at',
            ]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function orderWorkPlaces()
    {
        return $this->hasMany('App\Models\OrderWorkPlace');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return Tag::whereHas('products', function($q) {
            $q->whereIn('id', $this->products->pluck('id'));
        });
    }

    /**
     * @return string|null
     */
    public function getAddressAttribute()
    {
        $address = $this->data['address'] ?? null;
        if ($address) {
            $address = implode(', ', [
                $address['city'] ?? '',
                $address['street'] ?? '',
                $address['home'] ?? null,
                $address['floor'] ?? null,
                $address['doorphone'] ?? null,
                $address['apartment'] ?? null,
            ]);
        }
        return $address;
    }

    /**
     * @return string|null
     */
    public function getDishesAttribute()
    {
        return implode(', ', $this->products()
            ->withPivot('amount')
            ->select('order_product.amount', 'name')
            ->get()
            ->map(function ($val) {
                return "{$val->name} x{$val->amount}";
            })->toArray()
        );
    }

    /**
     * @return float
     */
    public function getBonusesAttribute()
    {
        return $this->transactions()->whereType(Transaction::TYPE_KOTCOINS)->sum('sum');
        // if ($this->payment_type === self::ORDER_PAYMENT_KOTCOINS) {
        //     return -$this->sum;
        // }
        // return in_array($this->status, [
        //     self::ORDER_STATUS_DELIVERED,
        //     self::ORDER_STATUS_CLOSED,
        // ]) ? $this->sum * 0.1 : 0;
    }

    /**
     * @return string
     */
    public function getStatusNameAttribute()
    {
        return __('shop.order_statuses')[$this->status] ?? __('Unknown');
    }

    /**
     * @return bool
     */
    public function getIsPreOrderAttribute()
    {
        return (bool) ($this->data['pre_order'] ?? false);
    }

    /**
     * @return integer
     */
    public function getTimeLeftAttribute()
    {
        if ($this->ordered_at < now()) {
            return -$this->ordered_at->diffInSeconds(now());
        }
        return $this->ordered_at->diffInSeconds(now());
    }

    /**
     * @return integer
     */
    public function getTimeCookLeftAttribute()
    {
        $time = ($this->cook_at ?? now())->addMinutes($this->data['time_for_cook'] ?? 15);
        if ($time < now()) {
            return -$time->diffInSeconds(now());
        }
        return $time->diffInSeconds(now());
    }

    /**
     * @return float
     */
    public function getSauceCostAttribute()
    {
        return Product::find(config('shop.add_sauce_product_id'))->price ?? 0;
    }

    /**
     * @return float
     */
    public function getFreeSaucesAttribute()
    {
        $freeSauces = $this->products->map(function ($product) {
            return $product->getSubProductsForWork()
                ->whereIn('product_group_id', config('shop.rolls_group_ids', []))
                ->count()
                *
                $product->pivot->amount;
        })->sum();
        return ceil($freeSauces * config('shop.free_sauce_coff'));
    }

    public function addLoadedItem($product, $amount, $modifierProduct = null, $discount = 0)
    {
        $sum = $modifierProduct ? ($product->price + $modifierProduct->price) : $product->price;
        $discountSum = $discount > 0 ? round($sum * $discount / 100, 1) : 0;

        $this->sum = $this->sum + ($sum * $amount - $discountSum);
        $orderItem = new OrderProduct([
            'amount' => $amount,
            'product_id' => $product->id,
            'sum' => $sum * $amount - $discountSum,
            'modifiers' => $modifierProduct->id ?? 0,
        ]);
        $product->pivot = $orderItem;
        $this->products[$product->id . '-' . ($modifierProduct->id ?? 0)] = $product;
    }

    public function editLoadedItem($product, $promo = null)
    {
        if ($product && $promo) {
            $discountSum = $promo->discount_sum;
            $product->pivot->sum -= $discountSum;
            $this->sum -= $discountSum;
        }
    }

    public function loadFromCookies()
    {
        $items = json_decode(request()->cookie(config('shop.basket_cookie_name')), true);
        $requestPromo = request()->cookie('promo');
        $this->sum = 0;

        foreach ($items ?? [] as $key => $amount) {
            $keys = explode('-', $key);
            $product = Product::where('id', $keys[0])->first();
            $modifierProduct = Product::where('id', $keys[1])->first();
            if ($product && $product->category_id != config('shop.presents_category_id')) {
                $this->addLoadedItem($product, $amount, $modifierProduct);
            }
        }

        if ($requestPromo) {
            $promo = Promo::whereCode($requestPromo)
                ->whereIsActive(true)
                ->where('min_basket_sum', '<=', $this->sum)
                ->first();

            if ($promo && $promo->product) {
                if ($promo->product->category_id != config('shop.presents_category_id')) {
                    $this->editLoadedItem(
                        $this->products["{$promo->product_id}-0"] ?? null,
                        $promo
                    );
                } else {
                    $this->addLoadedItem($promo->product, 1, null, $promo->discount);
                }
                $this->usedPromo = $promo;
            }
        }

        $this->city()->associate(app('city'));
        $deliveryTime = app('city')->getDeliveryTime();
        $this->ordered_at = now()->addMinutes($deliveryTime > 0 ? $deliveryTime : 122);
        $this->status = 0;
    }

    public function loadFromRequest()
    {
        $items = collect(request()->get('items', []));
        $requestPromo = request()->get('promo');
        $this->sum = 0;

        foreach (Product::whereIn('id', $items->pluck('id'))->get() as $product) {
            $item = $items->where('id', $product->id)->first();
            $amount = $item['count'] ?? 1;

            $modifierProduct = isset($item['double']) && $item['double']
                ? $product->doubleProduct
                : null;

            if ($product->category_id != config('shop.presents_category_id')) {
                $this->addLoadedItem($product, $amount, $modifierProduct);
            }
        }

        if ($requestPromo) {
            $promo = Promo::whereCode($requestPromo)
                ->whereIsActive(true)
                ->where('min_basket_sum', '<=', $this->sum)
                ->first();

            if ($promo && $promo->product) {
                if ($promo->product->category_id != config('shop.presents_category_id')) {
                    $this->editLoadedItem(
                        $this->products["{$promo->product_id}-0"] ?? null,
                        $promo
                    );
                } else {
                    $this->addLoadedItem($promo->product, 1, null, $promo->discount);
                }
                $this->usedPromo = $promo;
            }
        }

        $this->city()->associate(request()->get('city_id', 1));
        $deliveryTime = app('city')->getDeliveryTime();
        $this->ordered_at = now()->addMinutes($deliveryTime > 0 ? $deliveryTime : 122);
        $this->status = 0;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function scopeRecommends($query)
    {
        return Product::active()
            ->whereNotIn('id', $this->products->pluck('id'))
            ->whereHas('tags', function($q) {
                $q->whereIn('name', $this->tags()->pluck('name'));
            })
            ->limit(3);
    }

    /**
     * @param array $data
     * @param \App\Models\User $user
     * @return array
     */
    public static function getPaymentItemsForUser($data, $user)
    {
        $sumForPay = $data['order']['fullSum'];
        $paymentItems = [];
        $isMobile = $data['source'] === 'mobile';
        if ($data['use_bonuses'] ?? false) {
            $bonuses = $user->getIikoBonuses();
            if ($bonuses >= $sumForPay) {
                $bonuses = $sumForPay;
                $sumForPay = 0;
            } else {
                $sumForPay = $sumForPay - $bonuses;
            }
            $paymentItems[] = [
                'paymentType' => ['code' => 'BON'],
                'sum' => $bonuses,
                'isProcessedExternally' => false,
                'additionalData' => '{"searchScope":"PHONE","credential":"'.$data['phone'].'"}',
            ];
        }
        if ($data['payment'] === 'liqpay' && $sumForPay) {
            $paymentItems[] = [
                'paymentType' => ['code' => $isMobile ? 'FLPC' : 'CARD'],
                'sum' => $sumForPay,
                'isProcessedExternally' => false,
            ];
        }
        if ($data['payment'] === 'cash' && $sumForPay) {
            $paymentItems[] = [
                'paymentType' => ['code' => 'CASH'],
                'sum' => $sumForPay,
                'isProcessedExternally' => false,
            ];
        }
        return $paymentItems;
    }

    /**
     * @param array $data
     * @return array
     */
    public static function getOrderRequest($data)
    {
        $isMobile = $data['source'] === 'mobile';
        $liqpayPaymentItem = isset($data['liqpay']) ? [
            'paymentType' => ['code' => $isMobile ? 'FLPC' : 'CARD'],
            'sum' => $data['liqpay']['amount'],
            'isProcessedExternally' => true,
            'isExternal' => $data['liqpay']['status'] == 'success',
            'additionalData' => $data['liqpay']['status'] == 'success'
                ? 'Оплата успешна #' . $data['liqpay']['payment_id']
                : 'Платеж требует проверки #' . $data['liqpay']['payment_id'],
        ] : null;
        if ($liqpayPaymentItem) {
            $findedLiqpayPayment = false;
            foreach ($data['order']['paymentItems'] ?? [] as $key => $paymentItem) {
                if ($paymentItem['paymentType']['code'] === 'CARD' || $paymentItem['paymentType']['code'] === 'FLPC') {
                    $data['order']['paymentItems'][$key] = $liqpayPaymentItem;
                    $findedLiqpayPayment = true;
                }
            }
            if (! $findedLiqpayPayment) {
                $data['order']['paymentItems'][] = $liqpayPaymentItem;
            }
        }
        return [
            'organization' => app('city')->organization_uuid,
            'customer' => [
                'name' => $data['name'],
                'phone' => $data['phone'],
            ],
            'order' => [
                'date' => is_object($data['order']['date'])
                    ? $data['order']['date']->toDateTimeString()
                    : $data['order']['date'],
                'items' => $data['order']['items'],
                'phone' => $data['phone'],
                'customerName' => $data['name'],
                'address' => $data['address'],
                'personsCount' => $data['persons'] ?? 1,
                'comment' => $data['comment'] ?? '',
                'paymentItems' => $data['order']['paymentItems'],
            ],
            'coupon' => $data['promo'] ?? null,
        ];
    }

    public function makeOrderCustomer($iikoCustomerId, $autoAuthIfNew)
    {
        $user = User::where('phone', $this->phone);
        if ($iikoCustomerId) {
            $user = $user->orWhere('uuid', $iikoCustomerId)->first();
        } else {
            $user = $user->first();
        }

        if (! $user) {
            $user = User::create([
                'uuid', $iikoCustomerId,
                'name' => $this->customer_name,
                'phone' => $this->phone,
                'password' => Hash::make(uniqid()),
            ])->assignRole('customer');

            if ($autoAuthIfNew) {
                Auth::login($user);
            }
        }

        $this->update([
            'user_id' => $user->id,
        ]);
    }

    public static function makeOrder($data)
    {
        if ($data['address']['home'] == 'fin') {
            abort(response()->json([
                'errors' => [request()->fingerprint()],
                'message' => 'fingerprint',
            ], 422));
        }
        if (! $data['pre_order']) {
            $data['order']['date'] = app('order')->ordered_at;
        }
        $paymentItems = self::getPaymentItemsForUser($data, request()->user());
        $data['order']['paymentItems'] = $paymentItems;
        $data['liqpayResultUrl'] = $data['liqpay_result_url'] ?? false;
        $orderRequest = self::getOrderRequest($data);
        if (config('shop.make_order_with_iiko')) {
            $orderRequest = self::fixIikoModifiers($orderRequest);
            $problems = app('iiko')->OrdersApi()->checkCreate($orderRequest);
            if (isset($problems['problem'])) {
                abort(response()->json([
                    'errors' => [$problems['problem']],
                    'message' => 'Ошибка в iiko',
                ], 422));
            }
        }
        $order = app('order');
        $order->data = $data;
        $order->phone = $data['phone'];
        $order->payment_type = $data['payment'] ?? Order::ORDER_PAYMENT_CASH;
        $order->ordered_at = $data['order']['date'];
        $order->customer_name = $data['name'];
        $order->status = self::ORDER_STATUS_NEW;
        $order->user_id = auth()->id();
        $order->save();
        $order->attachProducts(app('order')->products);

        $liqpayItem = collect($paymentItems)
            ->whereIn('paymentType.code', ['CARD', 'FLPC'])
            ->first();
        if (config('shop.make_order_with_iiko') && is_null($liqpayItem)) {
            $iikoOrder = app('iiko')->OrdersApi()->addOrder($orderRequest);
            $order->update([
                'uuid' => $iikoOrder['orderId'],
                'sum' => $iikoOrder['sum'],
            ]);
        }

        $promo = Promo::whereCode($data['promo'] ?? '')
            ->whereIsActive(true)
            ->first();
        if ($promo) {
            $promo->useInOrder($order);
            if (config('shop.make_order_with_iiko') && is_null($liqpayItem)) {
                $promo->setCategoryToOrderCustomer($iikoOrder);
            }
        }

        if ($order->products->contains(config('shop.market_bonus_product_id'))) {
            MarketBonus::activateByUser(auth()->user(), true);
        }

        // Uncomment this if needed autoadding customer to db
        // $order->makeOrderCustomer($iikoOrder['customerId'] ?? null, ! auth()->user());

        return $order;
    }

    /**
     * @param Product $product
     * @param integer $amount
     * @param integer $modifiers
     *
     * @return Product
     */
    public function addProduct(Product $product, $amount = 1, $modifiers = 0): Product
    {
        $sum = $product->price * $amount;
        $orderItem = new OrderProduct([
            'amount' => $amount,
            'product_id' => $product->id,
            'sum' => $sum,
            'modifiers' => $modifiers,
        ]);
        $product->pivot = $orderItem;
        $this->products["{$product->id}-{$modifiers}"] = $product;
        $this->sum = $this->sum + $sum;

        return $product;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public static function calculateSouces($data)
    {
        $order = app('order');
        if ($order->free_sauces) {
            $freeSauseProduct = Product::find(config('shop.free_sauce_product_id'));
            $sum = $freeSauseProduct->price * $order->free_sauces;
            $orderItem = new OrderProduct([
                'amount' => $order->free_sauces,
                'product_id' => $freeSauseProduct->id,
                'sum' => $sum,
                'modifiers' => 0,
            ]);
            $freeSauseProduct->pivot = $orderItem;
            $order->products["{$freeSauseProduct->id}-0"] = $freeSauseProduct;
            $order->sum = $order->sum + $sum;

            $data['order']['items'][] = [
                'id' => $freeSauseProduct->uuid,
                'code' => $freeSauseProduct->id,
                'name' => $freeSauseProduct->name,
                'amount' => $order->free_sauces,
                'sum' => $sum,
                'modifiers' => 0.
            ];
        }
        if ($data['sauces'] ?? 0) {
            $addSauseProduct = Product::find(config('shop.add_sauce_product_id'));
            $sum = $addSauseProduct->price * $data['sauces'];
            $orderItem = new OrderProduct([
                'amount' => $data['sauces'],
                'product_id' => $addSauseProduct->id,
                'sum' => $sum,
                'modifiers' => 0,
            ]);
            $addSauseProduct->pivot = $orderItem;
            $order->products["{$addSauseProduct->id}-0"] = $addSauseProduct;
            $order->sum = $order->sum + $sum;

            $data['order']['items'][] = [
                'id' => $addSauseProduct->uuid,
                'code' => $addSauseProduct->id,
                'name' => $addSauseProduct->name,
                'amount' => $data['sauces'],
                'sum' => $sum,
                'modifiers' => 0.
            ];
        }
        $data['fullSum'] = app('order')->sum;
        return $data;
    }

    /**
     * Undocumented function
     *
     * @param [type] $score
     *
     * @return void
     */
    public function setGameScore($score)
    {
        $orderData = $this->data;
        $orderData['game_score'] = $score;
        if ($score > 41) {
            $orderData['game_score_sended'] = false;
        }
        $this->update([
            'data' => $orderData,
        ]);
        event(new UpdateOrder($this));
        if ($score >= 42) {
            $this->sendWinGameNotify();
        }
    }

    /**
     * Undocumented function
     *
     * @param [type] $score
     *
     * @return void
     */
    public function sendGameScore($first = false)
    {
        $score = $this->data['game_score'] ?? 0;
        if (config('shop.make_order_with_iiko')
            && config('shop.win_cutomer_category_uuid')
            && $this->uuid
            && $score > 41) {

            $orderData = $this->data;
            if (! $orderData) {
                logger('on set customer game win category / there no order data');
                return;
            }

            if ($first) {
                $orderData['game_score_sended'] = false;
                $this->update(['data' => $orderData]);
                return;
            }

            try {
                $this->setCustomerGameWinCategory();
                $orderData['game_score_sended'] = true;
                $this->update(['data' => $orderData]);
            } catch (\Throwable $th) {
                logger('iiko error on set customer game win category', [$th->getMessage(), "#{$this->id}", $this->phone]);
                $orderData['game_score_sended'] = false;
                $this->update(['data' => $orderData]);
            }
        }
    }

    /**
     *
     */
    public function setCustomerGameWinCategory()
    {
        $iikoCustomer = app('iiko')
            ->CustomersApi()
            ->getCustomerByPhone($this->city->organization_uuid, $this->phone);
        app('iiko')->CustomersApi()->addCategory(
            app('city')->organization_uuid,
            $iikoCustomer['id'],
            config('shop.win_cutomer_category_uuid')
        );
    }

    public function setGuestGameWinCategory($iikoOrder)
    {
        app('iiko')->CustomersApi()->addCategory(
            app('city')->organization_uuid,
            $iikoOrder['guests'][0]['id'],
            config('shop.win_cutomer_category_uuid')
        );
    }

    public function setExternalCustomerGameWinCategory($iikoOrder)
    {
        app('iiko')->CustomersApi()->addCategory(
            app('city')->organization_uuid,
            $iikoOrder['customer']['externalId'],
            config('shop.win_cutomer_category_uuid')
        );
    }

    public function setCustomerPromoCategory($iikoOrder)
    {
        app('iiko')->CustomersApi()->addCategory(
            app('city')->organization_uuid,
            $iikoOrder['customer']['externalId'],
            config('shop.win_cutomer_category_uuid')
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function createOpinionLink()
    {
        $opinion = Opinion::firstOrCreate([
            'order_id' => $this->id,
        ], [
            'slug' => substr(md5(uniqid(mt_rand(), true)), 0, 8),
        ]);

        return $opinion;
    }

    /**
     * @return response|null
     */
    public function getLiqpayForm($sum = 0)
    {
        $sum = $sum ? $sum : $this->sum;
        if (config('services.liqpay.public_key')) {
            $publicKey = config('services.liqpays.'.app('city')->id.'.s.public_key', config('services.liqpay.public_key'));
            $privateKey = config('services.liqpays.'.app('city')->id.'.s.private_key', config('services.liqpay.private_key'));

            return (new \LiqPay($publicKey, $privateKey))->cnb_form([
                'action' => 'pay',
                'amount' => $sum,
                'currency' => 'UAH',
                'description' => 'Покупка',
                'order_id' => substr(md5(rand(0, 10000)), 0, 5) . $this->id,
                'result_url' => route('paymentResponse', app('city')->slug),
                'server_url' => route('liqpayStatus'),
                'version' => '3',
                'language' => app()->getLocale(),
            ]);
        }
        return null;
    }

    /**
     * @return array|null
     */
    public function getLiqpayApiForm($sum = 0, $resultUrl = null)
    {
        $resultUrl = $resultUrl ?: route('paymentResponse', app('city')->slug);

        $sum = $sum ? $sum : $this->sum;
        if (config('services.liqpay.public_key')) {
            $publicKey = config('services.liqpays.'.app('city')->id.'.m.public_key', config('services.liqpay.public_key'));
            $privateKey = config('services.liqpays.'.app('city')->id.'.m.private_key', config('services.liqpay.private_key'));

            $params = [
                'action' => 'pay',
                'amount' => $sum,
                'currency' => 'UAH',
                'description' => 'Покупка',
                'order_id' => substr(md5(rand(0, 10000)), 0, 5) . $this->id,
                'result_url' => $resultUrl,
                'server_url' => route('liqpayStatus'),
                'version' => '3',
                'language' => app()->getLocale(),
            ];

            return [
                'form' => (new \LiqPay($publicKey, $privateKey))->cnb_form($params),
            ];
        }
        return null;
    }

    public function getBasketFiller()
    {
        $basketFill = json_encode([
            'street' => $this->data['address']['street'] ?? '',
            'home' => $this->data['address']['home'] ?? '',
            'floor' => $this->data['address']['floor'] ?? '',
            'doorphone' => $this->data['address']['doorphone'] ?? '',
            'apartment' => $this->data['address']['apartment'] ?? '',
        ]);

        return '<script>localStorage.setItem("phone:' . $this->phone . '", \'' . $basketFill . '\');</script>';
    }

    public function updateWorkPlaces()
    {
        $workPlaces = WorkPlace::whereTerminalId($this->terminal_id)
            ->pluck('id')->flip()->map(function ($item) {
                return 0;
            });

        $productGroups = $this->subProducts->groupBy('product_group_id');
        foreach ($productGroups as $productGroupId => $products) {
            $eWorkPlaces = $workPlaces->map(function ($zero, $workPlaceId)
                    use ($productGroupId, $products, $workPlaces) {

                $actives = Timer::activeCookOnTerminalWorkPlaceProductGroupById(
                        $this->terminal_id,
                        $workPlaceId,
                        $productGroupId
                    )->whereNull('stopped_at')
                    ->count();
                $actives = $actives
                    ? $products->where('pivot.cooked_at', '=', null)->count()
                    : 0;
                $workPlaces[$workPlaceId] += $actives;
                return $actives;
            });
        }
        $noActiveWorkPlacesIds = $workPlaces->reject()->keys();

        $workPlaces->filter()->map(function ($item, $workPlaceId) {
            $orderWorkPlace = OrderWorkPlace::where('order_id', $this->id)
                ->where('work_place_id', $workPlaceId)
                ->first();
            if (! $orderWorkPlace) {
                try {
                OrderWorkPlace::create([
                    'order_id' => $this->id,
                    'work_place_id' => $workPlaceId,
                    'completed_at' => null,
                ]);
                } catch (\Throwable $th) {
                    logger('updateWorkPlaces:4_2', [$this->id, $workPlaceId, $th->getMessage()]);
                }
            } else {
                $orderWorkPlace->update([
                    'completed_at' => null,
                ]);
            }
        });
        OrderWorkPlace::where('order_id', $this->id)
            ->whereNull('completed_at')
            ->whereIn('work_place_id', $noActiveWorkPlacesIds)
            ->update([
                'completed_at' => now(),
            ]);
    }

    public function autoAssignCourier()
    {
        if (config('shop.make_order_with_iiko')) {
            $courierInfo = $this->getIikoInfo()['courierInfo'] ?? [];
            $courier = User::whereUuid($courierInfo['courierId'] ?? null)->first();
            if ($courier) {
                $this->couriers()->detach();
                $this->couriers()->attach($courier->id, ['role' => 'courier']);
                $courier->pin->update([
                    'courier_status' => Pin::COURIER_STATUS_ON_WAY,
                ]);
            }
        }
    }

    public function adminAssignCourier($courier)
    {
        if ($courier) {
            $this->couriers()->detach();
            $this->couriers()->attach($courier->id, ['role' => 'courier']);
            $courier->pin->update([
                'courier_status' => Pin::COURIER_STATUS_ON_WAY,
            ]);
        }
    }

    public function assignCourier($courier)
    {
        if (config('shop.make_order_with_iiko')) {
            $iikoResponce = app('iiko')->OrdersApi()->assignCourier(
                app('city')->organization_uuid, [
                    'courierId' => $courier->uuid,
                    'orderId' => $this->uuid,
                ]);
        }
        $this->couriers()->detach();
        $this->couriers()->attach($courier->id, ['role' => 'courier']);
        $this->update([
            'status' => Order::ORDER_STATUS_ON_WAY,
        ]);
        return $iikoResponce ?? null;
    }

    public function setDelivered(User $courier)
    {
        if (config('shop.make_order_with_iiko')) {
            $iikoResponce = app('iiko')->OrdersApi()->setOrderDelivered(
                app('city')->organization_uuid, [
                    'courierId' => $courier->uuid,
                    'orderId' => $this->uuid,
                    'delivered' => true,
                    'actualDeliveryTime' => now()->format('Y-m-d H:i:s'),
                ]);
        }

        $this->update([
            'status' => Order::ORDER_STATUS_DELIVERED,
        ]);

        return $iikoResponce ?? null;
    }

    /**
     * Undocumented function
     *
     * @param [type] $score
     *
     * @return void
     */
    public function cancelOnIiko()
    {
        if ($this->uuid) {
            app('iiko')->OrdersApi()->addOrderProblem(
                app('city')->organization_uuid, [
                    'orderId' => $this->uuid,
                    'problemText' => 'Отмена предзаказа через моб. пуш.'
                ]);
        }
    }

    /**
     * @return bool if successfuly sended then true else false
     */
    public function sendPreOrderNotify()
    {
        $customer = $this->user;
        if ($customer) {
            $notificationsCount = $customer->notifications()
                ->where('type', 'App\Notifications\PreOrderNotification')
                ->where('data', 'like', '%"order_id":' . $this->id . '%')->count();

            if ($notificationsCount === 0) {
                $this->notifyPreOrder();
                return true;
            }
        }
        return false;
    }

    /**
     * @return bool if successfuly sended then true else false
     */
    public function sendSecondPreOrderNotify()
    {
        $customer = $this->user;
        if ($customer) {
            $notificationsCount = $customer->notifications()
                ->where('type', 'App\Notifications\PreOrderNotification')
                ->where('data', 'like', '%"order_id":' . $this->id . '%')
                ->whereNull('read_at')
                ->count();

            if ($notificationsCount > 0) {
                $this->notifyPreOrder();
                $operatorGoup = Group::find(config('shop.operators_group_id'));
                if ($operatorGoup) {
                    $operatorGoup->sendMessage('Не подтверждено заказ №' . $this->number);
                }
            }
            $customer->notifications()
                ->where('type', 'App\Notifications\PreOrderNotification')
                ->where('data', 'like', '%"order_id":' . $this->id . '%')
                ->delete();
            return true;
        }
        return false;
    }

    public function notifyPreOrder()
    {
        return $this->user->notify(new PreOrderNotification([
            'title' => __('Confirm pre order'),
            'body' => __('Confirm pre order #:num', ['num' => $this->number]),
            'order_id' => $this->id,
        ]));
    }

    /**
     * @return bool if successfuly sended then true else false
     */
    public function sendWinGameNotify()
    {
        $customer = $this->user;
        if ($customer) {
            $notificationsCount = $customer->notifications()
                ->where('type', 'App\Notifications\WinGameNotification')
                ->where('data', 'like', '%"order_id":' . $this->id . '%')->count();

            if ($notificationsCount === 0) {
                $this->notifyWin();
                return true;
            }
        }
        return false;
    }

    public function notifyWin()
    {
        return $this->user->notify(new WinGameNotification([
            'title' => __('Win game'),
            'body' => __('You win game'),
            'order_id' => $this->id,
            'code' => '',
        ]));
    }

    public function attachProducts($products)
    {
        foreach ($products as $product) {
            $this->products()->attach([
                $product->id => $product->pivot->toArray(),
            ]);
            foreach ($product->getSubProductsForWork() as $subProduct) {
                for ($i = 0; $i < $product->pivot->amount; $i++) {
                    $this->products()->attach([
                        $product->id => [
                            'sub_product_id' => $subProduct->id,
                        ] + $product->pivot->toArray(),
                    ]);
                }
            }
        }
    }
}
