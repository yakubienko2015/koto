<?php

namespace App\Models;

use App\Library\HasTranslations;
use App\Traits\TimestampSerializable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic;

class Achievement extends Model
{
    use HasTranslations, TimestampSerializable;

    public const TYPE_LOYALTY = 'loyalty';
    public const TYPE_FREQUENCY = 'frequency';
    public const TYPE_SUM = 'sum';

    /**
     * @var array
     */
    protected $fillable = [
        'image',
        'name',
        'bonuses',
        'description',
        'active_from',
        'active_to',
        'is_active',
        'has_category_uuid',
        'get_category_uuid',
        'type',
        'frequency',
        'sum',
    ];

    /**
     * @var array
     */
    public $casts = [
        'bonuses' => 'integer',
        'active_from' => 'date:Y-m-d',
        'active_to' => 'date:Y-m-d',
        'is_active' => 'boolean',
        'frequency' => 'integer',
        'sum' => 'float',
    ];

    /**
     * @var array
     */
    public $translatable = ['name', 'description'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function scopeActive($query)
    {
        return $query->whereIsActive(true)
            ->where(function ($query) {
                $query->whereDate('active_from', '<=', date('Y-m-d'))
                    ->orWhereNull('active_from');
            })
            ->where(function ($query) {
                $query->whereDate('active_to', '>=', date('Y-m-d'))
                    ->orWhereNull('active_to');
            });
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getProgressAttribute()
    {
        $userCategories = request()->user()->getIikoCategories();
        if ($userCategories->contains($this->get_category_uuid)) {
            return 100;
        }

        if ($this->type === 'sum') {
            $sum = $this->getCurrentUserReport()->sum('orderSum');
            return $this->sum > $sum
                ? $sum / $this->sum * 100
                : 100;
        } elseif ($this->type === 'frequency') {
            $count = $this->getCurrentUserReport()->count();
            return $this->frequency > $count
                ? $count / $this->frequency * 100
                : 100;
        }
        return 0;
    }

    /**
     * Undocumented function
     *
     * @param User $user
     *
     * @return collect
     */
    public function getTransactionsForUser(User $user)
    {
        try {
            $userinfo = $user->getIikoCustomerByPhone();
            $transactions = app('iiko')
                ->OrganizationsApi()
                ->getTransactionsReport(
                    app('city')->organization_uuid,
                    $userinfo['id'] ?? '',
                    $this->active_from ?? now(),
                    $this->active_to ?? now()
                );
        } catch (\Throwable $th) {
            logger('error iiko transactions for achieves', [$th->getMessage()]);
            $transactions = [];
        }
        return collect($transactions);
    }

    /**
     * Undocumented function
     *
     * @return collect
     */
    public function getCurrentUserReport()
    {
        return $this->getTransactionsForUser(
            request()->user()
        )->where('transactionType', 'CloseOrder');
    }

    /**
     * Undocumented function
     *
     * @param [type] $value
     *
     * @return void
     */
    public function getImageAttribute($value)
    {
        $imageUrl = $value ? asset($value) : null;
        return $imageUrl ? ($imageUrl . '?' . $this->updated_at->getTimestamp()) : null;
    }

    /**
     * @param \Illuminate\Http\UploadedFile $file
     *
     * @return array
     */
    public function saveImage(UploadedFile $file)
    {
        $ext = $file->guessExtension();
        $fileName = "{$this->id}_m.${ext}";

        $resizedImage = ImageManagerStatic::make($file->getRealPath())
            // ->resize(600, 365)
            ->encode($ext);
        Storage::disk('public')->put("achievements/{$fileName}", $resizedImage);
        $this->update([
            'image' => Storage::url("achievements/{$fileName}"),
        ]);
    }
}
