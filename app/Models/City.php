<?php

namespace App\Models;

use App\Library\HasTranslations;
use Cache;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class City extends Model
{
    use Sluggable, HasTranslations;

    /**
     * @var array
     */
    protected $fillable = [
        'slug',
        'name',
        'name_r',
        'uuid',
        'organization_uuid',
        'is_active',
        'work_time',
        'payments',
    ];

    /**
     * @var array
     */
    public $translatable = ['name', 'name_r'];

    /**
     * @var array
     */
    public $casts = [
        'is_active' => 'boolean',
        'work_time' => 'array',
        'payments' => 'array',
    ];

    /**
     * @var array
     */
    protected $attributes = [
        'is_active' => true,
        'payments' => '["cash", "kotcoins"]',
    ];

    /**
     * @var array
     */
    protected $workTime = [];

    /**
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug'
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function terminals()
    {
        return $this->hasMany('App\Models\Terminal');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pages()
    {
        return $this->hasMany('App\Models\Page');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany('App\Models\Post');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return Product::query();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }

    public function getPayments()
    {
        return array_filter($this->payments, function ($e) {
            if (in_array($e, array_keys(config('shop.payments')))) {
                return $e;
            }
        });
    }

    /**
     * @return mixed
     */
    public function getPosts()
    {
        if (! isset($this->activePosts)) {
            $categories = Post::orderBy('priority', 'desc')
                ->with('meta')
                ->get()
                ->where('meta.is_active', 1);

            $this->activePosts = $categories;
        }

        return $this->activePosts;
    }

    /**
     * @return mixed
     */
    public function getPages()
    {
        if (! isset($this->activePages)) {
            $categories = Page::orderBy('priority', 'desc')
                ->with('meta')
                ->get()
                ->where('meta.is_active', 1);

            $this->activePages = $categories;
        }

        return $this->activePages;
    }

    /**
     * @return mixed
     */
    public function getPagesOnHeader()
    {
        return $this->getPages()->filter(function ($value) {
            return in_array('header', $value->places ?? []);
        });
    }

    /**
     * @return mixed
     */
    public function getPagesOnFooterGeo()
    {
        return $this->getPages()->filter(function ($value) {
            return in_array('foot_geo', $value->places ?? []);
        });
    }

    /**
     * @return mixed
     */
    public function getPagesOnFooter()
    {
        return $this->getPages()->filter(function ($value) {
            return in_array('footer', $value->places ?? []);
        });
    }

    /**
     * @return mixed
     */
    public function getPagesOnFooterClient()
    {
        return $this->getPages()->filter(function ($value) {
            return in_array('foot_client', $value->places ?? []);
        });
    }

    /**
     * @return mixed
     */
    public function getCategories()
    {
        if (! isset($this->activeCategories)) {
            $categories = Category::orderBy('priority', 'desc')
                ->with('meta')
                ->get()
                ->where('meta.is_active', 1);

            $this->activeCategories = $categories;
        }

        return $this->activeCategories;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function getWorkTime($dateTime)
    {
        if (! $this->workTime) {
            $this->workTime = $this->work_time[$dateTime->weekday()]
                ?? (config('shop.default_work_times')[$dateTime->weekday()]);
            $this->workTime['from'] = Carbon::createFromTimeString(
                $this->workTime['from'] ?? '08:00');
            $this->workTime['to'] = Carbon::createFromTimeString(
                $this->workTime['to'] ?? '22:00');
            $this->workTime['closed'] = (bool) ($this->workTime['closed'] ?? 0);
        }
        return $this->workTime;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function getTodayWorkTime()
    {
        return $this->getWorkTime(now());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function getTodayWorkTimeJs()
    {
        $workTime = $this->getTodayWorkTime();
        $workTime['from'] = $workTime['from']->timestamp;
        $workTime['to'] = $workTime['to']->timestamp;
        return $workTime;
    }

    public function getRecommendTime()
    {
        $workTime = $this->getTodayWorkTime();
        $time = now()->addHours(2);
        if ($time < $workTime['to']) {
            return $time;
        }
        $workTime = $this->getWorkTime(now()->addDay());
        return $workTime['from'];
    }

    public function getIndexUrl()
    {
        return app('city')->id == 1
            ? route('index')
            : route('cityIndex', app('city'));
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public static function loadFromIiko()
    {
        $iiko = \App::get('iiko');

        $organizations = $iiko->OrganizationsApi()->getList();

        $newCities = [];
        // dd('Інформація про клієнта +380634026531', $iiko->CustomersApi()->getCustomerByPhone(app('city')->organization_uuid, '+380634026531'),
        // 'Інформація про метрики', $iiko->CustomersApi()->getCountersByGuests(app('city')->organization_uuid, [
        //     'guestIds' => ['b61ff53d-cdc9-11ea-8217-06967b0fe362'],
        //     'periods' => [0,1,2,3,4,5],
        //     'metrics' => [1,2],
        // ]),
        // 'Інформація про транзакціям', $iiko->OrganizationsApi()->getTransactionsReport(app('city')->organization_uuid, 'b61ff53d-cdc9-11ea-8217-06967b0fe362','2019-08-29','2020-08-28'));
        // dd($iiko->CustomersApi()->getCountersByGuests(app('city')->organization_uuid, [
        //     'guestIds' => ['829be4a9-abda-11ea-8216-06967b0fe362'],
        //     'periods' => [0,1,2,3,4,5],
        //     'metrics' => [1,2],
        // ]),
        // $iiko->OrganizationsApi()->getGuestCategories(app('city')->organization_uuid),
        // $iiko->OrdersApi()->getManualConditionInfos(app('city')->organization_uuid),
        // $iiko->OrganizationsApi()->getOrganizationCorporateNutritions(app('city')->organization_uuid),
        // $iiko->OrganizationsApi()->getPrograms(app('city')->organization_uuid),
        // $iiko->DeliverySettingsApi()->getDeliveryDiscounts(app('city')->organization_uuid));

        foreach ($organizations as $organization) {
            $cities = $iiko->CitiesApi()->getCities($organization['id']);
            $newCities[] = self::updateOrCreate([
                'organization_uuid' => $organization['id'],
            ], [
                'uuid' => isset($cities[0]) ? $cities[0]['id'] : null,
                'name' => isset($cities[0]) ? $cities[0]['name'] : $organization['name'],
                'is_active' => $organization['isActive'],
            ]);
        }

        return collect($newCities);
    }

    /**
     *
     */
    public function getStreets()
    {
        $lang = app()->getLocale();
        $code = "web_streets_{$this->id}_${lang}";
        if (Cache::has($code)){
            $shortStreets = Cache::get($code);
        } else {
            $iiko = \App::get('iiko');

            try {
                $cities = collect($iiko->CitiesApi()
                    ->getCitiesWithStreets($this->organization_uuid)
                );
                $city = $cities->where('city.name', '=', $this->name)
                    ->first() ?? $cities->first();
                $streets = collect($city['streets'] ?? []);
                $shortStreets = $streets->pluck('name');

                Cache::put($code, $shortStreets, 60 * 60 * 3);
            } catch (\Throwable $th) {
                $shortStreets = collect([]);
            }
        }
        return $shortStreets;
    }

    /**
     * @return int
     */
    public function getDeliveryTime()
    {
        $terminal = $this->terminals->first();
        if (is_null($terminal)) {
            return 0;
        }
        if ($terminal->status == Terminal::STATUS_LOW
            || $terminal->status == Terminal::STATUS_MIDDLE) {
            return 39;
        } elseif ($terminal->status == Terminal::STATUS_HIGHT) {
            return 59;
        }
        return 0;
    }

    /**
     * @return string
     */
    public function getDeliveryTimeString()
    {
        return Carbon::create(0, 1, 1, 0, $this->getDeliveryTime(), 0)->format('i:s');
    }

    /**
     * @param string $key
     * @param mixed $default
     * @return mixed
     */
    public function config($key, $default = null)
    {
        if ($default === null) {
            $default = config("shop.citied.*.$key");
        }
        return config("shop.citied.{$this->id}.$key", $default);
    }
}
