<?php

namespace App\Models;

use Auth;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, HasRoles, HasApiTokens;

    /**
     * @const array
     */
    public const SEXES = [
        'unknown',
        'male',
        'female',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'phone',
        'password',
        'birthday',
        'next_birthday',
        'uuid',
        'sex',
        'is_active',

        'city_id',
        'referrer_id',
        'ref_code',
        'device_token',

        'telegram_id',
        'logout',
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'logout'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'birthday' => 'date:Y-m-d',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'birthday',
    ];

    /**
     * @var array
     */
    protected $appends = [
        'role_names',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo('App\Models\City')->withDefault(function ($city) {
            return app('city');
        });
    }

    /**
     * Specifies the user's FCM token
     *
     * @return string
     */
    public function routeNotificationForFcm()
    {
        return $this->device_token;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function referrer()
    {
        return $this->belongsTo('App\Models\User', 'referrer_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function terminals()
    {
        return $this->belongsToMany('App\Models\Terminal');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions()
    {
        return $this->hasMany('App\Models\Transaction');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function holidays()
    {
        return $this->hasMany('App\Models\UserHoliday');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function phoneOrders()
    {
        return $this->hasMany('App\Models\Order', 'phone', 'phone');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function workOrders()
    {
        return $this->belongsToMany('App\Models\Order')->withPivot('role', 'created_at');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function timers()
    {
        return $this->hasMany('App\Models\Timer');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function pin()
    {
        return $this->hasOne('App\Models\Pin');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tasks()
    {
        return $this->belongsToMany('App\Models\Task')
            ->using('App\Models\TaskUser')
            ->withPivot([
                'id',
                'completed_at',
                'justification',
                'created_at',
            ]);
    }

    /**
     * @return string
     */
    public function adminlte_image()
    {
        return 'https://picsum.photos/300/300';
    }

    /**
     * @return string
     */
    public function adminlte_desc()
    {
        return 'Хороший хлопець';
    }

    /**
     * @return string
     */
    public function getRoleNamesAttribute()
    {
        return $this->getRoleNames()->map(function ($name) {
            return __("admin.roles.${name}");
        })->implode(', ');
    }

    /**
     * @return string
     */
    public function getPhoneNameAttribute()
    {
        return "{$this->phone} {$this->name}";
    }

    /**
     * @return float
     */
    public function getKotcoinsAttribute()
    {
        return $this->transactions()->whereType(Transaction::TYPE_KOTCOINS)->sum('sum');
        // return $this->getIikoBonuses();
    }

    /**
     * @return float
     */
    public function getIikoBonuses($globaled = true)
    {
        // return 5;
        if (config('global.current_customer_balance') !== null && $globaled) {
            return config('global.current_customer_balance');
        }
        $iiko = \App::get('iiko');
        $city = \App::get('city');
        try {
            $customer = $iiko->CustomersApi()->getCustomerByPhone($city->organization_uuid, $this->phone);
        } catch (\Throwable $throw) {
            return 0;
        }
        $wallets = collect($customer['walletBalances'] ?? []);
        $balance = $wallets->where('wallet.programType', 'Bonus')->sum('balance');
        if ($globaled) {
            config(['global.current_customer_balance' => $balance]);
        }
        return $balance;

    }

    /**
     * @return float
     */
    public static function getIikoBonusesByPhone($phone)
    {
        $iiko = \App::get('iiko');
        $city = \App::get('city');
        try {
            $customer = $iiko->CustomersApi()->getCustomerByPhone($city->organization_uuid, $phone);
        } catch (\Throwable $throw) {
            return 0;
        }
        $wallets = collect($customer['walletBalances'] ?? []);
        return $wallets->where('wallet.programType', 'Bonus')->sum('balance');
    }

    /**
     * @return float
     */
    public function getIikoOrders()
    {
        $iiko = \App::get('iiko');
        $city = \App::get('city');
        try {
            $deliveryHistory = $iiko->OrdersApi()->getDeliveryHistoryByCustomerId($city->organization_uuid, $this->uuid);
            $orders = $deliveryHistory['customersDeliveryHistory'][0]['deliveryHistory'];
        } catch (\Throwable $throw) {
            return collect([]);
        }
        return collect($orders);
    }

    /**
     * @return float
     */
    public function getIikoTransactions()
    {
        $iiko = \App::get('iiko');
        $city = \App::get('city');
        try {
            $transactions = $iiko->OrganizationsApi()->getTransactionsReport(
                $city->organization_uuid,
                $this->uuid,
                now()->subDays(360)->format('Y-m-d'),
                now()->format('Y-m-d')
            );
        } catch (\Throwable $throw) {
            return collect([]);
        }
        return collect($transactions)->where('transactionType', 'RefillWalletFromOrder');
    }

    /**
     * @return float
     */
    public function getIikoCustomer()
    {
        $iiko = \App::get('iiko');
        $city = $this->city ?? \App::get('city');
        try {
            $customer = $iiko->CustomersApi()->getCustomerById($city->organization_uuid, $this->uuid);
        } catch (\Throwable $throw) {
            return collect([]);
        }
        return collect($customer);
    }

    /**
     * @return float
     */
    public function getIikoCustomerByPhone()
    {
        $iiko = \App::get('iiko');
        $city = $this->city ?? \App::get('city');
        try {
            $customer = $iiko->CustomersApi()->getCustomerByPhone($city->organization_uuid, $this->phone);
        } catch (\Throwable $throw) {
            return collect([]);
        }
        return collect($customer);
    }

    /**
     * @return float
     */
    public function getIikoCustomerCategories()
    {
        $city = $this->city ?? \App::get('city');
        try {
            $customer = app('iiko')->CustomersApi()->getCategoriesByGuests($city->organization_uuid, [
                'guestIds' => [$this->uuid],
            ]);
        } catch (\Throwable $throw) {
            return collect([]);
        }
        return collect($customer);
    }

    /**
     * @return float
     */
    public function saveIikoCustomer($data = [])
    {
        $iiko = \App::get('iiko');
        $city = $this->city ?? \App::get('city');
        $customer = $iiko->CustomersApi()->createOrUpdate($city->organization_uuid, $data);
        return collect($customer);
    }

    public function getIikoCategories()
    {
        if (config('global.user_categories') !== null) {
            return config('global.user_categories');
        }
        $userInfo = $this->getIikoCustomerByPhone();
        $userCategories = collect($userInfo['categories'] ?? [])->pluck('id');
        config(['global.user_categories' => $userCategories]);
        return $userCategories;
    }

    /**
     * @return Collect
     */
    public static function getRolesForSelect()
    {
        return Role::pluck('id', 'name')->map(function ($item, $key) {
            return __("admin.roles.{$key}");
        });
    }

    /**
     * @return array
     */
    public static function getPermissionsForSelect()
    {
        return Permission::pluck('id', 'name')->map(function ($item, $key) {
            return __("admin.permissions.{$key}");
        });
    }

    /**
     * @return array
     */
    public static function getSexesForSelect()
    {
        return collect(self::SEXES)->flip()->map(function ($item, $key) {
            return __("admin.sexes.{$key}");
        });
    }

    public function getHomePath()
    {
        $terminal = $this->terminals()->first() ?? app('city')->terminals->first();

        switch (true) {
            case $this->hasRole('operator'):
                return route('app.operator');
            case $this->hasRole('admin'):
                return route('app.admin', $terminal);
            case $this->hasRole('manager'):
                return route('app.admin', $terminal);
            // case $this->hasRole('stats'):
            //     return route('app.stats');
            case $this->hasRole('cook'):
                return route('app.cookSelect');
            case $this->hasRole('issucook'):
                return route('app.cookSelect');
            case $this->hasRole('courier'):
                return route('app.courier');
            case $this->hasRole('logist'):
                return route('app.logist');
        }
        Auth::guard('app')->logout();
        return route('app.login');
    }

    public function generatePin($forever = false)
    {
        $this->pin()->delete();

        return $this->pin()->create([
            'id' => Pin::generateID(),
            'expired_at' => $forever ? null : now()->addHours(8),
        ]);
    }

    public function getActivePin()
    {
        return $this->pin()->active()->first();
    }

    public function getThisMonthSalary($channel = null)
    {
        if ($channel) {
            $totalDuration = $this->timers()->whereChannel($channel)
                ->where('started_at', '>', now()->startOfMonth())->sum('duration');
        } else {
            $totalDuration = $this->timers()
                ->where('started_at', '>', now()->startOfMonth())->sum('duration');
        }
        $totalOrdersSum = $this->workOrders()
            ->wherePivot('created_at', '>', now()->startOfMonth())
            ->whereIn('status', [
                Order::ORDER_STATUS_DELIVERED,
                Order::ORDER_STATUS_CLOSED,
            ])->sum('sum');
        $minutePrice = config('shop.default_operator_price');
        $price = $totalDuration / 60 * $minutePrice + $totalOrdersSum * 0.01;
        return number_format($price, 2, '.', '');
    }

    public function getWelcomeComments()
    {
        // $roleName = $this->getRoleNames()->first();
        // $cityId = app('city')->id;
        return [
            'Cегодня ' . now()->locale('ru')->translatedFormat('l'),
            now()->format('d.m.Y'),
            // 'У тебя уже ' . __('shop.price', [
            //     $this->getThisMonthSalary("presence-app-{$roleName}.{$cityId}"),
            // ]),
            // 'Ти на 3-м месте по продуктивности',
            // trans_choice('{0} Ти крутое|{1} Ти крутой|{2} Ти крутая', [
            //     array_search($this->sex, self::SEXES),
            // ]),
            'Хорошего тебе дня.',
        ];
    }

    public function ping($stop = false)
    {
        $timer = Timer::firstOrCreate([
            'channel' => request()->header('referer'),
            'user_id' => $this->id,
            'stopped_at' => null,
        ]);
        $timer->update([
            'updated_at' => now(),
        ]);
        if ($stop) {
            $timer->stop();
        }
        return $timer;
    }

    public function getGroups()
    {
        return Group::role($this->getRoleNames())
            ->where(function ($query) {
                $query->whereCityId($this->city_id)->orWhere('city_id', null);
            })
            ->where(function ($query) {
                $query->whereIn('terminal_id', $this->terminals->pluck('id'))
                    ->orWhere('terminal_id', null);
            });
    }

    public function getTasks()
    {
        $tasks = collect([]);
        foreach ($this->getGroups()->get() as $group) {
            $tasks = $tasks->merge($group->tasks()->whereDate('date', now())->get());
        }
        return $tasks;
    }

    /**
     * @return Ccollection
     */
    public static function loadEmployeesPinsFromIiko()
    {
        $users = app('iiko')->SettingsApi()
            ->getEmployees(app('city')->organization_uuid)['users'] ?? [];
        // $iiko = app('iiko');
        // dd($users, collect($users)->where('cellPhone')->where('pinCode')->where('daleted', false));
            // dd(app('iiko')->OrdersApi()->get('orders/get_courier_orders',
            // app('iiko')->OrdersApi()->withToken([
            //     'organization' => app('city')->organization_uuid,
            //     'courier' => '3fc737fd-8771-4127-9b8b-f6be29b6147c',
            //     'order' => '7d5220a4-41ac-4ea7-9b00-8938f76113f6',
            // ])));
            // dd(app('iiko')->OrdersApi()->post('orders/assignCourier', [
            //     'courierId' => '3fc737fd-8771-4127-9b8b-f6be29b6147c',
            //     'orderId' => '7d5220a4-41ac-4ea7-9b00-8938f76113f6',
            //     'organizationId' => app('city')->organization_uuid,
            //     'requestTimeout' => 500,
            // ], app('iiko')->OrdersApi()->withToken([
            //     'organization' => app('city')->organization_uuid,
            //     'courier' => '3fc737fd-8771-4127-9b8b-f6be29b6147c',
            //     'order' => '7d5220a4-41ac-4ea7-9b00-8938f76113f6',
            // ])));
        $updatedUsers = [];

        foreach (collect($users)->where('cellPhone')->where('pinCode')->where('deleted', false) as $user) {
            $userByUuid = User::whereUuid($user['id'])->first();
            $userByPhone = User::wherePhone($user['cellPhone'])->first();

            try {
                if ($userByUuid) {
                    $userByUuid->pin()->updateOrCreate([
                        'user_id' => $userByUuid->id,
                    ], [
                        'id' => $user['pinCode'],
                    ]);
                    $userByUuid->update([
                        'phone' => $user['cellPhone'],
                    ]);
                    $updatedUsers[] = $userByUuid;
                    $userByUuid->syncRoles(config('shop.iiko_roles')[$user['mainRole']['code']]);
                } else if ($userByPhone) {
                    $userByPhone->pin()->updateOrCreate([
                        'user_id' => $userByPhone->id,
                    ], [
                        'id' => $user['pinCode'],
                    ]);
                    $userByPhone->update(['uuid' => $user['id']]);
                    $updatedUsers[] = $userByPhone;
                    $userByPhone->syncRoles(config('shop.iiko_roles')[$user['mainRole']['code']]);
                } else {
                    if (isset(config('shop.iiko_roles')[$user['mainRole']['code']])) {
                        $name = trim("{$user['firstName']} {$user['middleName']} {$user['lastName']}");
                        $updatedUser = User::create([
                            'name' => $name ? $name : $user['displayName'],
                            'email' => $user['email'],
                            'uuid' => $user['id'],
                            'phone' => $user['cellPhone'],
                            'password' => bcrypt('1LhEWej8W1yI'),
                        ])->assignRole(config('shop.iiko_roles')[$user['mainRole']['code']]);
                        $updatedUser->pin()->create([
                            'id' => $user['pinCode'],
                        ]);
                        $updatedUsers[] = $updatedUser;
                    }
                }
            } catch (\Throwable $throw) {
                logger('Error on upload users from iiko', [$throw->getMessage(), $user['cellPhone']]);
                if (strpos($throw->getMessage(), 'Duplicate entry')) {
                    if (isset($userByUuid)) {
                        $userByUuid->delete();
                    }
                    if (isset($userByPhone)) {
                        $userByPhone->delete();
                    }
                }
            }
        }

        return collect($updatedUsers);
    }

    /**
     * @return void
     */
    public function updateNextBirthday()
    {
        $birthdayThisYear = $this->birthday->setYear(now()->year);
        $this->update([
            'next_birthday' => $birthdayThisYear > now()
                ? $birthdayThisYear
                : $birthdayThisYear->setYear(now()->year + 1),
        ]);
    }

    /**
     * @return bool if successfuly sended then true else false
     */
    public function sendBirthdayNotify()
    {
        $bonuses = $this->getIikoBonuses(false);
        $birthday = $this->birthday->format('Y-m-d');
        $operatorGoup = Group::find(config('shop.operators_group_id'));
        if ($operatorGoup) {
            $operatorGoup->sendMessage("Именинник: {$this->name} {$this->phone} {$birthday} - Коткойны {$bonuses}");
        }
    }
}
