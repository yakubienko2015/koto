<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Timer extends Model
{
    public const CREATED_AT = 'started_at';

    /**
     * @var array
     */
    protected $fillable = [
        'user_id', 'channel', 'duration', 'started_at', 'stopped_at', 'updated_at'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'started_at',
        'stopped_at',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function stop()
    {
        $this->update([
            'stopped_at' => now(),
            'duration' => ($this->updated_at ?: now())->diffInSeconds($this->started_at),
        ]);
    }

    public function scopeActiveCookOnTerminal($query, $terminal)
    {
        return $query->where('stopped_at', null)
            ->where('channel', 'like', "%/app/cook/{$terminal->id}%");
    }

    public function scopeActiveCookOnTerminalWorkPlace($query, $terminal, $workPlace)
    {
        return $query->where('stopped_at', null)
            ->where('channel', 'like', "%/app/cook/{$terminal->id}/{$workPlace->id}%");
    }

    public function scopeActiveCookOnTerminalWorkPlaceProductGroupById($query, $terminalId, $workPlaceId, $productGroupId)
    {
        return $query->where('stopped_at', null)
            ->where('channel', 'like', "%/app/cook/{$terminalId}/{$workPlaceId}%groups[]={$productGroupId}%");
    }

    public function scopeActiveCookOnTerminalProductGroup($query, $terminal, $productGroup)
    {
        return $query->where('stopped_at', null)
            ->where('channel', 'like', "%/app/cook/{$terminal->id}/%groups[]={$productGroup->id}%");
    }
}
