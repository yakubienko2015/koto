<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserHoliday extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'date',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'date' => 'date:Y-m-d',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
