<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductGroup extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'uuid',
        'is_active',
    ];
    /**
     * @var array
     */
    public $casts = [
        'is_active' => 'boolean',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function workPlaces()
    {
        return $this->belongsToMany('App\Models\WorkPlace');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public static function loadFromIiko()
    {
        $menu = app('iiko')
            ->NomenclatureApi()
            ->getMenu(app('city')->organization_uuid);

        $groups = $menu['productCategories'] ?? [];

        $newGroups = [];
        foreach ($groups as $group) {
            $createdGroup = self::updateOrCreate([
                'uuid' => $group['id'],
            ], [
                'name' => $group['name'],
            ]);
            $newGroups[] = $createdGroup;
        }

        return collect($newGroups);
    }
}
