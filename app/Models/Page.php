<?php

namespace App\Models;

use App\Library\HasMeta;
use App\Library\HasTranslations;
use App\Traits\TimestampSerializable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

class Page extends Model
{
    use HasMeta, HasTranslations, TimestampSerializable;

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'city_id',
        'slug',
        'content',
        'places',
        'is_active',
        'priority',
        'image',
    ];

    /**
     * @var array
     */
    public $translatable = ['name', 'content'];

    /**
     * @var array
     */
    public $casts = [
        'is_active' => 'boolean',
        'city_id' => 'integer',
        'priority' => 'integer',
        'places' => 'array',
    ];

    /**
     * @var array
     */
    protected $attributes = [
        'is_active' => true,
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }

    /**
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeMenu($query)
    {
        return $query->whereCityId(null)
            ->orWhere('city_id', app('city')->id)
            ->whereIsActive(1)
            ->orderBy('priority');
    }

    /**
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeInCity($query, int $cityId = null)
    {
        $cityId = $cityId ? $cityId : app('city')->id;
        return $query->whereCityId(null)
            ->orWhere('city_id', $cityId);
    }

    /**
     * Scope a query to only include OnHeader Pages.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOnHeader($query)
    {
        return $query->where('places', 'like', '%header%');
    }

    /**
     * Scope a query to only include OnFooter Pages.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOnFooter($query)
    {
        return $query->where('places', 'like', '%footer%');
    }

    /**
     * Scope a query to only include OnFooter Pages.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOnFooterGeo($query)
    {
        return $query->where('places', 'like', '%foot_geo%');
    }

    /**
     * Scope a query to only include OnFooter Pages.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOnFooterClient($query)
    {
        return $query->where('places', 'like', '%foot_client%');
    }

    /**
     * Scope a query to only include site Pages.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOnSite($query)
    {
        return $query->where('places', 'like', '%site%');
    }

    /**
     * Scope a query to only include api Pages.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOnApi($query)
    {
        return $query->where('places', 'like', '%api%');
    }

    /**
     * Undocumented function
     *
     * @param [type] $value
     *
     * @return void
     */
    public function getImageAttribute($value)
    {
        $imageUrl = $value ? asset($value) : null;
        return $imageUrl ? ($imageUrl . '?' . $this->updated_at->getTimestamp()) : null;
    }

    /**
     * @param \Illuminate\Http\UploadedFile $file
     * @return array
     */
    public function saveImage(UploadedFile $file)
    {
        $ext = $file->guessExtension();
        $fileName = "{$this->id}.${ext}";

        $savedName = $file->storePubliclyAs('public/pages', $fileName);
        $savedName = str_replace('public/', 'storage/', $savedName);
        $this->update([
            'image' => asset($savedName),
        ]);
    }
}
