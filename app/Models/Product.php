<?php

namespace App\Models;

use App\Library\HasCitizens;
use App\Library\HasMeta;
use App\Library\HasTranslations;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

class Product extends Model implements Searchable
{
    use Sluggable, HasMeta, HasTranslations, HasCitizens;

    public const MEASURE_UNITS = ['kg', 'l', 'pcs', 'port'];

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'uuid',
        'category_id',
        'price',
        'old_price',
        'improved_product_id',
        'amount',
        'image',
        'mob_image',
        'mark',
        'sharpness',
        'ingredients',
        'work_ingredients',
        'weight',
        'description',
        'measure_unit',
        'time_for_cook',

        'carbohydrate_amount',
        'energy_amount',
        'fat_amount',
        'fiber_amount',

        'is_active',
        'is_top',
        'is_new',
        'is_rec',
        'product_group_id',
        'priority',
    ];

    /**
     * @var array
     */
    public $casts = [
        'is_active' => 'boolean',
        'category_id' => 'integer',
        'improved_product_id' => 'integer',
        'product_group_id' => 'integer',
        'priority' => 'integer',
        'amount' => 'integer',
        'is_top' => 'boolean',
        'is_new' => 'boolean',
        'is_rec' => 'boolean',
        'work_ingredients' => 'array',
    ];

    protected $appends = [
        'full_ingredients',
        'webp_image_small',
        'webp_image_medium',
    ];

    /**
     * @var array
     */
    public $translatable = ['name', 'ingredients', 'description'];

    /**
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug'
        ];
    }

    public function getSearchResult(): SearchResult
    {
        return new \Spatie\Searchable\SearchResult(
           $this,
           $this->name
        );
    }

    protected static function booted()
    {
        static::saved(function ($product) {
            if ($product->wasChanged('price', 'old_price')) {
                $product->prices()->updateOrCreate([
                    'city_id' => app('city')->id,
                ], [
                    'price' => $product
                        ->getChanges()['price'] ?? $product->price,
                    'old_price' => $product
                        ->getChanges()['old_price'] ?? $product->old_price,
                ]);
            }
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function marks()
    {
        return $this->belongsToMany('App\Models\Mark');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function bowlIngredients()
    {
        return $this->belongsToMany('App\Models\BowlIngredient');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function subproducts()
    {
        return $this->belongsToMany('App\Models\Product', 'product_product', 'product_id', 'subproduct_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orderables()
    {
        return $this->hasMany('App\Models\OrderProduct');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function prices()
    {
        return $this->hasMany('App\Models\ProductPrice');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function group()
    {
        return $this->belongsTo('App\Models\ProductGroup');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function improvedProduct()
    {
        return $this->belongsTo('App\Models\Product');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function improvedInProduct()
    {
        return $this->hasOne('App\Models\Product', 'improved_product_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function scopeActive($query)
    {
        return $query->whereIsActive(true)->has('improvedInProduct', '<', 1);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function scopeTop($query)
    {
        return $query->whereIsActive(true)->whereIsTop(true)->orderBy('priority');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function scopeActiveCategory($query)
    {
        return $query->whereIn('category_id', Cache::remember(
            'activeCategories',
            86400,
            function () {
                return Category::active()->pluck('id');
            })
        );
    }

    public function getRecommendsAttribute()
    {
        $tags = $this->tags->pluck('name');
        return self::whereIsActive(true)
            ->whereCategoryId($this->category_id)
            ->where('id', '<>', $this->id)
            ->withCount(['tags' => function (Builder $q) use ($tags) {
                $q->whereIn('name', $tags);
            }])
            ->whereHas('tags')
            ->orderBy('tags_count', 'desc')
            ->limit(3)
            ->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function scopeRecommends($query)
    {
        $tags = $this->tags->pluck('name');
        return $query->active()
            ->whereCategoryId($this->category_id)
            ->where('id', '<>', $this->id)
            ->withCount(['tags' => function (Builder $q) use ($tags) {
                $q->whereIn('name', $tags);
            }])
            ->whereHas('tags')
            ->orderBy('tags_count', 'desc')
            ->limit(3);
    }

    public function getPriceAttribute($value)
    {
        $price = $this->prices->where('city_id', app('city')->id)->first();
        return (float) (is_null($price) ? $value : $price->price);
    }

    public function getOldPriceAttribute($value)
    {
        $price = $this->prices->where('city_id', app('city')->id)->first();
        return (float) (is_null($price) ? $value : $price->old_price);
    }

    public function getImprovedPriceAttribute()
    {
        return $this->improvedProduct->price ?? null;
    }

    public function getImprovedOldPriceAttribute()
    {
        return $this->improvedProduct->old_price ?? null;
    }

    public function getFullIngredientsAttribute()
    {
        return $this->ingredients . $this->subproducts->pluck('name')->implode(', ');
    }

    public function getImageAttribute($value)
    {
        $imageUrl = $value ? asset($value) : null;
        if (! $imageUrl && $this->improvedInProduct) {
            $newImage = $this->improvedInProduct->image;
            $imageUrl = $newImage ? asset($newImage) : null;
        }
        if (! $imageUrl && $this->category_id == config('shop.bowl_constructors_category_id')) {
            $imageUrl = asset($value ? $value : 'images/site/prod-01.png');
        }
        return $imageUrl ? ($imageUrl . '?' . $this->updated_at->getTimestamp()) : null;
    }

    public function getWebpImageSmallAttribute($value)
    {
        return str_replace('.png', '_small.webp', $this->image);
    }

    public function getWebpImageMediumAttribute($value)
    {
        return str_replace('.png', '_medium.webp', $this->image);
    }

    public function getMobImageAttribute($value)
    {
        $imageUrl = $value ? asset($value) : null;
        return $imageUrl ? ($imageUrl . '?' . $this->updated_at->getTimestamp()) : null;
    }

    public function getDoubleProductAttribute()
    {
        $iiko = \App::get('iiko');
        $city = \App::get('city');

        $menu = $iiko->NomenclatureApi()->getMenu($city->organization_uuid);

        $products = collect($menu['products'] ?? []);
        $iikoProduct = $products->where('id', $this->uuid)->first();
        if ($iikoProduct) {
            $iikoModifier = collect($iikoProduct['modifiers'])
                ->where('required', false)
                ->first();
            if ($iikoModifier) {
                return Product::whereUuid($iikoModifier['modifierId'])->first();
            }
        }
        return null;
    }

    /**
     * @param array $except
     * @return Collection
     */
    public static function getProductsForSelect($except = [])
    {
        return Product::whereNotIn('id', $except)->pluck('name', 'id');
    }

    /**
     * @param array $except
     * @return Collection
     */
    public static function getCategoriesForSelect($except = [])
    {
        return Category::whereNotIn('id', $except)->pluck('name', 'id');
    }

    /**
     * @param array $except
     * @return array
     */
    public static function getSharpnessesForSelect()
    {
        return collect(config('shop.sharpnesses'))->map(function ($item, $key) {
            return "<span style=\"color:{$item}\">⬤</span> " . __("shop.sharpnesses.{$key}", [], 'ru');
        });
    }

    /**
     * @param array $except
     * @return array
     */
    public static function getMarksForSelect()
    {
        return collect(config('shop.marks'))->map(function ($item, $key) {
            return __("shop.marks.{$key}", [], 'ru');
        });
    }

    /**
     * @param array $except
     * @return array
     */
    public static function getMeasureUnitsForSelect()
    {
        $measureUnits = [];
        foreach (self::MEASURE_UNITS as $item) {
            $measureUnits[$item] = __("shop.measure_units.{$item}", [''], 'ru');
        }
        return $measureUnits;
    }

    /**
     * @param \Illuminate\Http\UploadedFile $file
     * @return array
     */
    public function saveImage(UploadedFile $file)
    {
        // $ext = $file->guessExtension();
        $fileName = "{$this->id}.png";

        $resizedImage = ImageManagerStatic::make($file->getRealPath())
            ->resize(600, 365)
            ->encode('png');
        $resizedImageWebp1 = ImageManagerStatic::make($file->getRealPath())
            ->resize(235, 143)
            ->encode('webp');
        $resizedImageWebp2 = ImageManagerStatic::make($file->getRealPath())
            ->resize(148, 90)
            ->encode('webp');
        Storage::disk('public')->put("products/{$this->id}.png", $resizedImage);
        Storage::disk('public')->put("products/{$this->id}_medium.webp", $resizedImageWebp1);
        Storage::disk('public')->put("products/{$this->id}_small.webp", $resizedImageWebp2);
        $this->update([
            'image' => Storage::url("products/{$fileName}"),
        ]);
    }

    /**
     * @param \Illuminate\Http\UploadedFile $file
     * @return array
     */
    public function saveMobImage(UploadedFile $file)
    {
        $ext = $file->guessExtension();
        $fileName = "{$this->id}_m.${ext}";

        $resizedImage = ImageManagerStatic::make($file->getRealPath())
            // ->resize(600, 365)
            ->encode($ext);
        Storage::disk('public')->put("products/{$fileName}", $resizedImage);
        $this->update([
            'mob_image' => Storage::url("products/{$fileName}"),
        ]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public static function loadFromIiko()
    {
        $menu = app('iiko')
            ->NomenclatureApi()
            ->getMenu(app('city')->organization_uuid);

        $products = $menu['products'] ?? [];

        // dd($menu, collect($menu['groups'])->pluck('name'),collect($menu['productCategories'])->pluck('name') , collect($products)->pluck('name'), $products[65]);

        $newProducts = [];

        $categories = [];
        $productGroups = [];
        $terminals = [];

        foreach ($products as $product) {
            if (isset($categories[$product['groupId']])) {
                $category = $categories[$product['groupId']];
            } else {
                $category = $categories[$product['groupId']]
                    = Category::whereUuid($product['groupId'])->first();
            }
            if (isset($categories[$product['parentGroup']])) {
                $parentCategory = $categories[$product['parentGroup']];
            } else {
                $parentCategory = $categories[$product['parentGroup']]
                    = Category::whereUuid($product['parentGroup'])->first();
            }
            if (isset($productGroups[$product['productCategoryId']])) {
                $productGroup = $productGroups[$product['productCategoryId']];
            } else {
                $productGroup = $productGroups[$product['productCategoryId']]
                    = ProductGroup::whereUuid($product['productCategoryId'])->first();
            }

            $createdProduct = self::updateOrCreate([
                'uuid' => $product['id'],
            ], [
                'name' => $product['name'],
                'category_id' => $category->id ?? ($parentCategory->id ?? null),
                'product_group_id' => $productGroup->id ?? null,
                'price' => $product['price'],
                // 'amount' => 1,
                // 'weight' => $product['weight'] * 1000, // все ще вагу по товарам тягне з айко. зробіть просив багато разів
                'energy_amount' => $product['energyAmount'],
                'carbohydrate_amount' => $product['carbohydrateAmount'],
                'fat_amount' => $product['fatAmount'],
                'fiber_amount' => $product['fiberAmount'],
                // 'priority' => $product['order'],
                // 'is_active' => $product['isIncludedInMenu'],
            ]);
            if ($product['differentPricesOn'] ?? false) {
                foreach ($product['differentPricesOn'] as $price) {
                    if (isset($terminals[$price['terminalId']])) {
                        $terminal = $terminals[$price['terminalId']];
                    } else {
                        $terminal = $terminals[$price['terminalId']]
                            = Terminal::whereUuid($price['terminalId'])->first();
                    }

                    if ($terminal) {
                        ProductPrice::updateOrCreate([
                            'city_id' => $terminal->city_id,
                            'product_id' => $createdProduct->id,
                        ], [
                            'price' => $price['price'],
                            'iiko_terminal_uuid' => $price['terminalId'],
                        ]);
                    }
                }
            }
            if ($product['images'] && ! $createdProduct->mob_image) {
                $image = collect($product['images'])
                    ->sortBy('uploadDate', SORT_REGULAR, 1)
                    ->first();
                try {
                    $info = pathinfo($image['imageUrl']);
                    $contents = file_get_contents($image['imageUrl']);
                    $file = sys_get_temp_dir() . DIRECTORY_SEPARATOR . $info['basename'];
                    file_put_contents($file, $contents);
                    $uploaded_file = new UploadedFile($file, $info['basename']);
                    $createdProduct->saveMobImage($uploaded_file);
                } catch (\Throwable $th) {
                    logger('error on save product image from iiko', [$th->getMessage()]);
                }
            }
            $newProducts[] = $createdProduct;
        }

        return collect($newProducts);
    }

    public function getSubProductsForWork()
    {
        if ($this->subproducts) {
            return $this->subproducts->count() ? $this->subproducts : collect([$this]);
        }
        return collect([$this]);
    }

    public static function findBowlByIngredients($ingredients)
    {
        $products = self::query();
        foreach ($ingredients as $ingredientId) {
            $products->whereHas('bowlIngredients', function ($q) use ($ingredientId) {
                $q->whereId($ingredientId);
            });
        }
        return $products->first()->append('doubleProduct');
    }
}
