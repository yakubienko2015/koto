<?php

namespace App\Models;

use App\Library\ESputnik;
use Illuminate\Database\Eloquent\Model;

class SmsConfirmation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'phone',
        'code',
        'is_confirmed',
    ];

    public function confirmed()
    {
        return $this->is_confirmed ? true : false;
    }

    public function confirm($code)
    {
        if ($code === $this->code) {
            $this->update(['is_confirmed' => true]);
            return true;
        }
        return false;
    }

    public function sendSms()
    {
        $sms = new \cri2net\sms_fly\SMS_fly(
            config('services.sms_fly.login'),
            config('services.sms_fly.password')
        );
        $sms->alfaname = config('services.sms_fly.alfaname');

        $response = $sms->sendSMS(
            clear_phone_number($this->phone),
            "<#> code {$this->code}"
        );
        logger()->channel('smsStatuses')->info('sms fly response: ', [$response, $this->phone]);
        return $response;
    }
    public function sendAllSms()
    {
        $sms = new \cri2net\sms_fly\SMS_fly(
            config('services.sms_fly.login'),
            config('services.sms_fly.password')
        );
        $sms->alfaname = config('services.sms_fly.alfaname');

        $response = $sms->sendSMS(
            clear_phone_number($this->phone),
            "{$this->code}"
        );
        logger()->channel('smsStatuses')->info('sms fly response: ', [$response, $this->phone]);
        return $response;
    }
}
