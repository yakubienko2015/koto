<?php

namespace App\Models;

use App\Notifications\OpinionNotification;
use Illuminate\Database\Eloquent\Model;

class Opinion extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'slug',
        'order_id',
        'marks',
        'filled_at',
        'comment',
        'updated_at',
    ];

    public $casts = [
        'marks' => 'array',
        'order_id' => 'integer',
        'filled_at' => 'datetime:Y-m-d H:i:s',
    ];

    protected $dates = [
        'filled_at',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }

    /**
     * @return array
     */
    public function getSurveyItems()
    {
        return app('iiko')
            ->DeliverySettingsApi()
            ->getSurveyItems(
                app('city')->organization_uuid, ['orderId' => $this->order->uuid]
            );
    }

    /**
     * @return array
     */
    public function sendToIiko()
    {
        $items = $this->getSurveyItems();
        $iikoOrder = $this->order->getIikoInfo();
        $deliveryId = $iikoOrder['opinion']['deliveryId'];
        $marks = [];
        foreach ($items as $itemKey => $item) {
            $key = config('shop.opinion_marks')[$itemKey] ?? 'Common';
            if (isset($this->marks[$key])) {
                $marks[] = [
                    'surveyItemId' => $item['id'],
                    'mark' => $this->marks[$key] ? 100 : 0,
                ];
            }
        }
        return app('iiko')
            ->OrdersApi()
            ->sendDeliveryOpinion([
                'organization' => app('city')->organization_uuid,
                'deliveryId' => $deliveryId,
                'comment' => $this->comment,
                'marks' => $marks,
            ]);
    }

    /**
     * @return bool if successfuly sended then true else false
     */
    public function sendToCustomer()
    {
        $customer = $this->order->user;
        if ($customer) {
            $notificationsCount = $customer->notifications()
                ->where('type', 'App\Notifications\OpinionNotification')
                ->where('data', 'like', '%"order_id":' . $this->order_id . '%')->count();

            if ($customer->device_token
                    && $this->created_at == $this->updated_at
                    && $notificationsCount === 0) {
                $this->notify();
                $this->update([
                    'updated_at' => now(),
                ]);
                return true;
            }
        }
        return false;
    }

    public function notify()
    {
        return $this->order->user->notify(new OpinionNotification([
            'title' => __('Order opinion'),
            'body' => __('Send order opinion'),
            'link' => route('opinion', $this->slug),
            'order_id' => $this->order_id,
        ]));
    }
}
