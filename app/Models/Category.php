<?php

namespace App\Models;

use App\Library\HasMeta;
use App\Library\HasTranslations;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use Sluggable, HasMeta, HasTranslations;

    /**
     * @var array
     */
    protected $fillable = [
        'name', 'uuid', 'is_active', 'priority', 'description',
    ];

    /**
     * @var array
     */
    public $translatable = ['name', 'description'];

    /**
     * @var array
     */
    public $casts = [
        'is_active' => 'boolean',
        'priority' => 'integer',
    ];

    /**
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug'
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function getMarksAttribute()
    {
        return Mark::whereHas('products', function ($query) {
            $query->whereIn('id', $this->products->pluck('id'));
        })->get();
        return $this->belongsToManyThrought('App\Models\Mark', 'App\Models\Product');
    }

    public function scopeActive($query)
    {
        return $query->whereIsActive(1);
    }

    /**
     * Scope a query to only include OnHeader Pages.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeMenu($query)
    {
        // if (app('city')->slug === 'dnepr') {
        //     $query->where('slug', '<>', 'bouly');
        // }
        return $query->whereIsActive(1)->orderBy('priority', 'desc');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public static function loadFromIiko()
    {
        $iiko = \App::get('iiko');
        $city = \App::get('city');

        $menu = $iiko->NomenclatureApi()->getMenu($city->organization_uuid);

        $groups = $menu['groups'] ?? [];

        $newCategories = [];

        foreach ($groups as $group) {
            $newCategories[] = Category::updateOrCreate([
                'uuid' => $group['id'],
            ], [
                'name' => $group['name'],
                'priority' => $group['order'],
            ]);
        }
        Category::whereNotIn('id', collect($newCategories)->pluck('id'))->update([
            'is_active' => false,
        ]);

        return collect($newCategories);
    }
}
