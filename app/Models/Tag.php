<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function products()
    {
        return $this->belongsToMany('App\Models\Product');
    }

    /**
     * @param array $tags
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public static function findByNames($tags)
    {
        $findedTags = [];
        foreach ($tags ?? [] as $tag) {
            $findedTags[] = self::firstOrCreate(['name' => $tag])->id;
        }
        return $findedTags;
    }
}
