<?php

namespace App\Models;

use App\Library\HasTranslations;
use Illuminate\Database\Eloquent\Model;

class Mark extends Model
{
    use HasTranslations;

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'style',
    ];

    /**
     * @var array
     */
    public $translatable = ['name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany('App\Models\Product');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany('App\Models\Category');
    }
}
