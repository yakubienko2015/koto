<?php

namespace App\Models;

use App\Library\HasTranslations;
use Illuminate\Database\Eloquent\Model;

class BowlIngredient extends Model
{
    use HasTranslations;

    /**
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'step'
    ];

    /**
     * @var array
     */
    public $translatable = ['name', 'description'];

    /**
     * @var array
     */
    public $casts = [
        'step' => 'integer',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany('App\Models\Product');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function getNextIngredients($includeProducts)
    {
        return self::whereStep($this->step + 1)
            ->whereHas('products', function ($query) use ($includeProducts) {
                $query->whereIn('id', $includeProducts);
            })->get();
    }

    public static function getIngredientsTreeInArray()
    {
        return self::whereStep(1)->get()
            ->each(function ($item) {
                $products = $item->products()->pluck('id');
                $item->setAttribute(
                    'next_ingredients',
                    $item->getNextIngredients($products)
                        ->each(function ($subItem) use ($products) {
                            $subProducts = $subItem->products()->pluck('id')
                                ->intersect($products);
                            $subItem->setAttribute(
                                'next_ingredients',
                                $subItem->getNextIngredients($subProducts)
                                    ->toArray()
                            );
                        })->toArray()
                );
            })->toArray();
    }
}
