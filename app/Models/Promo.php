<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Promo extends Model
{
    const PROMO_SUM = 300;

    /**
     * @var array
     */
    protected $fillable = [
        'code',
        'qr_code',
        'product_id',
        'is_active',
        'can_be_single',
        'is_multiuse',
        'valid_from',
        'valid_to',
        'min_basket_sum',
        'discount',
        'iiko_guest_category_uuid',
    ];

    protected $appends = [
        'discount_sum',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

    /**
     * @param [type] $query
     *
     * @return void
     */
    public function scopeActive($query)
    {
        return $query->whereIsActive(true)->orderBy('priority');
    }

    /**
     * @param string $code
     * @param float $orderSum
     *
     * @return Product|null
     */
    public static function getPromoProductByCode($code)
    {
        $_promo = [
            'U100',
            'u100'
        ];
        if (in_array($code, $_promo)) {
            return Product::find(config('shop.order_promo_product_id'));
        }
        return null;
    }

    /**
     * @param string $code
     * @param float $orderSum
     *
     * @return array
     */
    public static function getPromoByCode($code, $orderSum = 0)
    {
        $_promo_product = self::getPromoProductByCode($code);
        if ($_promo_product) {
            if ($orderSum >= self::PROMO_SUM) {
                return [
                    'product' => [
                        'amount' => 1,
                        'product_id' => config('shop.order_promo_product_id'),
                        'sum' => 0,
                        'modifiers' => 0
                    ],
                    'code' => null,
                    'status' => 3,
                    'message' => trans('shop.basket.promo_message_3', [
                        'product' => $_promo_product->name,
                    ]),
                ];
            }
            return [
                'product' => null,
                'code' => null,
                'status' => 2,
                'message' => trans('shop.basket.promo_message_2', [
                    'sum' => self::PROMO_SUM,
                ]),
            ];
        }
        return [
            'product' => null,
            'code' => null,
            'status' => 1,
            'message' => trans('shop.basket.promo_message_1')
        ];
    }

    public static function getDiscountSum($promo = null, $basketSum)
    {
        if (is_null($promo)) { return 0; }

        $promoModel = self::whereCode($promo)
            ->whereIsActive(true)
            ->select('product_id', 'discount')
            ->first();

        return $promoModel->discount_sum ?? 0;
    }

    public function getDiscountSumAttribute()
    {
        return $this->discount > 0
            ? round(($this->product->price ?? 0) * $this->discount / 100, 1)
            : 0;
    }

    public function useInOrder($order)
    {
        if (! $this->is_multiuse) {
            $this->update([
                'is_active' => false,
            ]);
        }
    }

    public function setCategoryToOrderCustomer($iikoOrder)
    {
        if ($this->iiko_guest_category_uuid) {
            try {
                $iikoCustomer = app('iiko')
                    ->CustomersApi()
                    ->getCustomerByPhone($iikoOrder['organization'], $iikoOrder['customer']['phone']);
                app('iiko')->CustomersApi()->addCategory(
                    $iikoOrder['organization'],
                    $iikoCustomer['id'],
                    $this->iiko_guest_category_uuid
                );
            } catch (\Throwable $th) {
                logger('error on add category by promo');
            }
        }
    }
}
