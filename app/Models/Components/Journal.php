<?php

namespace App\Models\Components;

use Illuminate\Database\Eloquent\Model;

class Journal extends Model
{
    /**
     * @var string
     */
    protected $table = 'journal';

    /**
     * @var array
     */
    protected $fillable = [
        'type',
        'message',
    ];
}
