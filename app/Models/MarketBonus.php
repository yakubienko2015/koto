<?php

namespace App\Models;

use Iiko\Biz\Exception\IikoResponseException;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Agent\Agent;

class MarketBonus extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'phone',
        'activated_at',
        'type',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'activated_at',
        'created_at',
    ];

    /**
     * @var array
     */
    public $casts = [
        'activated_at' => 'date:Y-m-d H:i:s',
        'created_at' => 'date:Y-m-d H:i:s',
    ];

    /**
     * @param string $phone
     *
     * @return void
     */
    public function activateByUuid($uuid)
    {
        try {
            // app('iiko')->CustomersApi()->addCategory(
            //     app('city')->organization_uuid,
            //     $uuid,
            //     config('shop.market_bonus_category_uuid')
            // );
            $this->update([
                'activated_at' => now(),
            ]);
        } catch (IikoResponseException $th) {
            logger('iiko error on add category to qr app user', [$th->getMessage()]);
            return false;
        }

        return true;
    }

    /**
     * @param string $phone
     *
     * @return void
     */
    public function activateByPhone($phone)
    {
        try {
            $iikoCustomer = app('iiko')
                ->CustomersApi()
                ->getCustomerByPhone(app('city')->organization_uuid, $this->phone);
        } catch (IikoResponseException $th) {
            return false;
        }

        if (config('shop.market_bonus_category_uuid') && $iikoCustomer) {
            return $this->activateByUuid($iikoCustomer['id']);
        }

        return false;
    }

    /**
     * @param \App\Models\User $user
     *
     * @return void
     */
    public static function activateByUser(User $user, $forceCreate = false)
    {
        $marketBonus = self::wherePhone($user->phone)
            ->whereNull('activated_at')
            ->first();

        if (! $marketBonus && $forceCreate) {
            $agent = new Agent();
            $marketBonus = self::create([
                'phone' => $user->phone,
                'type' => $agent->device() . ' ' . $agent->platform(),
            ]);
        }

        if ($marketBonus && config('shop.market_bonus_category_uuid')) {
            $iikoUser = $user->getIikoCustomerByPhone();
            return $marketBonus->activateByUuid($iikoUser['id'] ?? $user->uuid);
        }

        return false;
    }

    /**
     * @param string $phone
     *
     * @return void
     */
    public static function hasBonus($phone)
    {
        return (bool) self::wherePhone($phone)
            ->whereNotNull('activated_at')
            ->count();
    }
}
