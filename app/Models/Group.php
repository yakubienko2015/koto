<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;
use Telegram;

class Group extends Model
{
    use HasRoles;

    protected $guard_name = 'web';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'city_id',
        'terminal_id',
    ];

    /**
     * @var array
     */
    public $casts = [
        'city_id' => 'integer',
        'terminal_id' => 'integer',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tasks()
    {
        return $this->hasMany('App\Models\Task');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function terminal()
    {
        return $this->belongsTo('App\Models\Terminal');
    }

    public function users()
    {
        return User::role($this->getRoleNames())
            ->when($this->city_id, function ($query) {
                return $query->whereCityId($this->city_id);
            })
            ->when($this->terminal_id, function ($query) {
                return $query->whereHas('terminals', function (Builder $query) {
                    $query->whereId($this->terminal_id);
                });
            })->whereNotNull('telegram_id');
    }

    public function sendMessage($message, $holidayUsers = false)
    {
        $users = $this->users();
        if (! $holidayUsers) {
            $users->whereNotIn('id', UserHoliday::whereDate('date', now())->pluck('user_id'));
        }
        $responses = [];
        foreach ($users->get() as $user) {
            try {
                $responses[] = Telegram::sendMessage([
                    'chat_id' => $user->telegram_id,
                    'text' => $message,
                ]);
            } catch (\Throwable $th) {
                logger('error on send telegram message', [$th->getMessage()]);
                $responses[] = null;
            }
        }
        return collect($responses);
    }
}
