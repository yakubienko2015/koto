<?php

namespace App\Models;

use App\Events\UpdateCourierStatus;
use Illuminate\Database\Eloquent\Model;
use Keygen\Keygen;

class Pin extends Model
{
    public $incrementing = false;

    // Statuses for courier
    const COURIER_STATUS_READY = 'ready';
    const COURIER_STATUS_RETURN = 'return';
    const COURIER_STATUS_ON_WAY = 'on_way';
    const COURIER_STATUS_REST = 'rest';

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'user_id',
        'expired_at',
        'last_used_at',
        'courier_status',
        'courier_must_return_at',
        'rest_time',
    ];

    /**
     * @var array
     */
    public $casts = [
        'user_id' => 'integer',
        'rest_time' => 'integer',
        'last_used_at' => 'datetime:Y-m-d H:i:s',
        'expired_at' => 'datetime:Y-m-d H:i:s',
        'courier_must_return_at' => 'datetime:Y-m-d H:i:s',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'expired_at',
        'last_used_at',
        'courier_must_return_at',
    ];

    protected static function booted()
    {
        static::updated(function ($pin) {
            if ($pin->wasChanged('courier_status')) {
                event(new UpdateCourierStatus($pin));
            }
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public static function generateNumericKey()
    {
        return Keygen::numeric(7)->prefix(mt_rand(1, 9))->generate(true);
    }

    public static function generateID()
    {
        $id = self::generateNumericKey();

        while (Pin::whereId($id)->count() > 0) {
            $id = self::generateNumericKey();
        }

        return $id;
    }

    public function scopeActive($query)
    {
        return $query
            ->where('expired_at', '>', now())
            ->orWhere('expired_at', null);
    }
}
