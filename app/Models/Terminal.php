<?php

namespace App\Models;

use App\Events\UpdateTerminal;
use App\Library\HasTranslations;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role;

class Terminal extends Model
{
    use HasTranslations;

    public const STATUS_LOW = 1;
    public const STATUS_MIDDLE = 2;
    public const STATUS_HIGHT = 3;
    public const STATUS_FULL = 4;

    /**
     * @var array
     */
    protected $fillable = [
        'name', 'city_id', 'uuid', 'address', 'phone', 'status', 'max_cook_load'
    ];

    /**
     * @var array
     */
    public $translatable = ['name', 'address'];

    protected static function booted()
    {
        static::updated(function ($terminal) {
            if ($terminal->wasChanged('status')) {
                event(new UpdateTerminal($terminal));
            }
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function workPlaces()
    {
        return $this->hasMany('App\Models\WorkPlace');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\Models\User');
    }

    public static function getStatusesForSelect()
    {
        return [
            self::STATUS_LOW => 'низкий',
            self::STATUS_MIDDLE => 'средний',
            self::STATUS_HIGHT => 'высокий',
            self::STATUS_FULL => 'перегружен',
        ];
    }

    /**
     * @return array
     */
    public static function getUsersForSelect()
    {
        return User::role(['admin', 'logist'])->get()->map(function ($item) {
            $item->name .= ' ('. $item->getRoleNames()->implode('|') . ')';
            return $item;
        })->pluck('name', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public static function loadFromIiko()
    {
        $iiko = \App::get('iiko');
        $city = \App::get('city');

        $organization = $iiko->OrganizationsApi()->getOrganization($city->organization_uuid);
        $terminals = $iiko->DeliverySettingsApi()->getDeliveryTerminals($city->organization_uuid);

        $terminals = $terminals['deliveryTerminals'];

        $newTerminals = [];

        foreach ($terminals as $terminal) {
            $newTerminals[] = self::updateOrCreate([
                'uuid' => $terminal['deliveryTerminalId'],
            ], [
                'city_id' => $city->id,
                'name' => $terminal['name'] . ' ' . $terminal['deliveryRestaurantName'],
                'address' => $terminal['address'],
                'phone' => $organization['phone'],
            ]);
        }

        return collect($newTerminals);
    }

    public function getLastMonthStats()
    {
        $sumInWork = $this->orders()->whereIn('status', [
            Order::ORDER_STATUS_IN_PROGRESS,
            Order::ORDER_STATUS_READY,
        ])->where('ordered_at', '>', now())
            ->where('ordered_at', '<', now()->addHour())
            ->sum('sum');
        $activeCooks = Timer::where('stopped_at', null)
            ->where('channel', 'like', "%/app/cook/{$this->id}/%")
            ->count();
        $loadPerOneCook = $sumInWork / ($activeCooks ? $activeCooks : 1);

        $recommendStatus = self::STATUS_LOW;
        if ($loadPerOneCook > $this->max_cook_load * 1.5) {
            $recommendStatus = self::STATUS_FULL;
        } elseif ($loadPerOneCook > $this->max_cook_load) {
            $recommendStatus = self::STATUS_HIGHT;
        } elseif ($loadPerOneCook > $this->max_cook_load * 0.5) {
            $recommendStatus = self::STATUS_MIDDLE;
        }

        return [
            'orders_on_work' => $this->orders()->whereIn('status', [
                Order::ORDER_STATUS_IN_PROGRESS,
                Order::ORDER_STATUS_READY,
            ])->where('ordered_at', '>', now())
                ->where('ordered_at', '<', now()->addHour())
                ->count(),
            'sum_on_work' => $sumInWork,
            'couriers_on_work' => Pin::whereNotNull('courier_status')->count(),
            'couriers_free' => Pin::whereCourierStatus(Pin::COURIER_STATUS_READY)->count(),
            'couriers_returning' => Pin::whereCourierStatus(Pin::COURIER_STATUS_RETURN)->count(),
            'recommend_status' => $recommendStatus,
        ];
    }
}
