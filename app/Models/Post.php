<?php

namespace App\Models;

use App\Library\HasMeta;
use App\Library\HasTranslations;
use App\Traits\TimestampSerializable;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Intervention\Image\ImageManagerStatic;

class Post extends Model
{
    use Sluggable, HasMeta, HasTranslations, TimestampSerializable;

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'city_id',
        'slug',
        'description',
        'content',
        'created_at',
        'image',
        'is_active',
        'priority',
    ];

    /**
     * @var array
     */
    public $translatable = ['name', 'content', 'description'];

    /**
     * @var array
     */
    public $casts = [
        'city_id' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'is_active' => 'boolean',
        'priority' => 'integer',
    ];

    /**
     * @var array
     */
    protected $attributes = [
        'is_active' => true,
    ];

    /**
     * @var array
     */
    protected $appends = ['link'];

    /**
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug'
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }

    /**
     *
     */
    public function scopeActive($query)
    {
        return $query->whereIsActive(true)->orderBy('created_at', 'desc')->orderBy('priority');
    }

    /**
     * @param [type] $query
     *
     * @return void
     */
    public function scopeInCity($query)
    {
        return $query->where(function ($query) {
            $query->whereCityId(app('city')->id)->orWhere('city_id', null);
        });
    }

    /**
     * @param [type] $value
     *
     * @return void
     */
    public function getImageAttribute($value)
    {
        $imageUrl = $value ? asset($value) : null;
        return $imageUrl ? ($imageUrl . '?' . $this->updated_at->getTimestamp()) : null;
    }

    /**
     * @param string $value
     *
     * @return string
     */
    public function getWebpImageAttribute($value)
    {
        return str_replace(['.png', '.jpg', '.jpeg'], '.webp', $this->image);
    }

    /**
     * @param \Illuminate\Http\UploadedFile $file
     *
     * @return array
     */
    public function saveImage(UploadedFile $file)
    {
        $ext = $file->guessExtension();
        $fileName = "{$this->id}.${ext}";

        $savedName = $file->storePubliclyAs('public/posts', $fileName);
        $savedName = str_replace('public/', 'storage/', $savedName);
        ImageManagerStatic::make($file->getRealPath())
            ->encode('webp')
            ->save(storage_path("app/public/posts/{$this->id}.webp"));
        $this->update([
            'image' => asset($savedName),
        ]);
    }

    /**
     * Return previous post
     *
     * @return bool
     */
    public function prev()
    {
        $posts = self::orderBy('created_at')->pluck('id')->toArray();
        $key = array_search($this->id, $posts);
        $post = self::select('id', 'name', 'slug')->find($posts[$key - 1] ?? null);
        $this->prev = $post;

        return isset($post);
    }

    /**
     * Return next post
     *
     * @return bool
     */
    public function next()
    {
        $posts = self::orderBy('created_at')->pluck('id')->toArray();
        $key = array_search($this->id, $posts);
        $post = self::select('id', 'name', 'slug')->find($posts[$key + 1] ?? null);
        $this->next = $post;

        return isset($post);
    }

    /**
     * Return interested posts
     *
     * @return array
     */
    public function interested()
    {
        return self::joinMeta()
            ->where('metas.is_active', 1)
            ->orderBy('created_at', 'desc')
            ->whereNotIn('posts.id', [$this->id])
            ->select('posts.*');

        // $posts = self::orderBy('priority')->pluck('id')->toArray();
        // $key = array_search($this->id, $posts);

        // if ($key - $this->interestedCount >= 0) {
        //     $before = array_slice($posts, $key - $this->interestedCount, $this->interestedCount);
        //     $after = [];
        // } else {
        //     $before = array_slice($posts, 0, $key);
        //     $after = array_slice($posts, $key - $this->interestedCount, $this->interestedCount);
        // }

        // $postsIds = array_unique(array_merge($before, $after));
        // $keyy = array_search($this->id, $postsIds);
        // if ($keyy !== false) {
        //     unset($postsIds[$keyy]);
        // }

        // return self::whereIn('id', $postsIds);
    }

    /**
     * @return bool
     */
    public function getLinkAttribute()
    {
        return route('post', [app('city'), $this]);
    }
}
