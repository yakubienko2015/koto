<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class OrderProduct extends Pivot
{
    public $incrementing = true;

    const UPDATED_AT = null;

    /**
     * @var array
     */
    protected $fillable = [
        'order_id',
        'product_id',
        'amount',
        'sum',
        'modifiers',

        'cooked_at',
        'sub_product_id',
    ];

    /**
     * @var array
     */
    public $casts = [
        'order_id' => 'integer',
        'product_id' => 'integer',
        'amount' => 'integer',
        'sum' => 'double',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sub_product()
    {
        return $this->belongsTo('App\Models\Product');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function main()
    {
        return $this->belongsTo('App\Models\OrderProduct', 'order_id', 'order_id')
            ->whereProductId($this->product_id)
            ->whereNull('sub_product_id');
    }

    public function isAllSubReady()
    {
        if (OrderProduct::whereOrderId($this->order_id)
            ->whereProductId($this->product_id)
            ->whereNotNull('sub_product_id')
            ->whereNull('cooked_at')
            ->count()) {
            return false;
        }
        return true;
    }
}
