<?php

namespace App\Models;

use App\Library\TelegramBotHelper;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Storage;
use Telegram;

class TaskUser extends Pivot
{
    /**
     * @var array
     */
    protected $fillable = [
        'task_id',
        'user_id',
        'justification',
        'completed_at',
        'files',
    ];

    /**
     * @var array
     */
    public $casts = [
        'completed_at' => 'datetime:Y-m-d H:i:s',
        'task_id' => 'integer',
        'user_id' => 'integer',
        'files' => 'array',
    ];

    protected $dates = ['created_at', 'completed_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function task()
    {
        return $this->belongsTo('App\Models\Task');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function setCompleted($messageId, $callbackId = null)
    {
        if ($this->completed_at === null) {
            $this->update([
                'completed_at' => now(),
            ]);
            if ($callbackId) {
                Telegram::answerCallbackQuery([
                    'callback_query_id' => $callbackId,
                    'text' => 'успешно выполнено',
                ]);
            }
            Telegram::editMessageText([
                'chat_id' => $this->user->telegram_id,
                'message_id' => $messageId,
            ] + TelegramBotHelper::getTasksMessageForUser($this->user));
        }
    }

    public function getFilesLinks()
    {
        if ($this->files) {
            return implode(', ', $this->files);
        }
        return collect(Storage::files("public/bot/tasks/{$this->id}"))->map(function ($url) {
            return asset(Storage::url($url));
        })->implode(', ');
    }
}
