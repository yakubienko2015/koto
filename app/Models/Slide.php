<?php

namespace App\Models;

use App\Library\HasTranslations;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Intervention\Image\ImageManagerStatic;
use Storage;

class Slide extends Model
{
    use HasTranslations;

    /**
     * @var array
     */
    protected $fillable = [
        'city_id',
        'title',
        'text',
        'button_text',
        'button_link',
        'image',
        'is_active',
        'priority',
    ];

    /**
     * @var array
     */
    public $translatable = ['title', 'text', 'button_text'];

    /**
     * @var array
     */
    public $casts = [
        'is_active' => 'boolean',
    ];

    /**
     * @var array
     */
    protected $attributes = [
        'is_active' => true,
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }

    /**
     * @param [type] $query
     *
     * @return void
     */
    public function scopeActive($query)
    {
        return $query->whereIsActive(true)->orderBy('priority');
    }

    /**
     * @param [type] $query
     *
     * @return void
     */
    public function scopeInCity($query)
    {
        return $query->where(function ($query) {
            $query->whereCityId(app('city')->id)->orWhere('city_id', null);
        });
    }

    /**
     * @param string $value
     *
     * @return string|null
     */
    public function getImageAttribute($value)
    {
        $imageUrl = $value ? asset($value) : null;
        return $imageUrl ? ($imageUrl . '?' . $this->updated_at->getTimestamp()) : null;
    }

    /**
     * @param string $value
     *
     * @return string
     */
    public function getWebpImageAttribute($value)
    {
        return str_replace(['.png', '.jpg', '.jpeg'], '.webp', $this->image);
    }

    /**
     * @param \Illuminate\Http\UploadedFile $file
     * @return array
     */
    public function saveImage(UploadedFile $file)
    {
        $ext = $file->guessExtension();
        $fileName = "{$this->id}.${ext}";

        $resizedImage = ImageManagerStatic::make($file->getRealPath())
            ->resize(720, 440)
            ->encode($ext);
        $resizedImageWebp = ImageManagerStatic::make($file->getRealPath())
            ->resize(720, 440)
            ->encode('webp');
        Storage::disk('public')->put("slides/{$fileName}", $resizedImage);
        Storage::disk('public')->put("slides/{$this->id}.webp", $resizedImageWebp);
        $this->update([
            'image' => Storage::url("slides/{$fileName}"),
        ]);
    }

    /**
     * Return previous post
     *
     * @return bool
     */
    public function prev()
    {
        $posts = self::orderBy('created_at')->pluck('id')->toArray();
        $key = array_search($this->id, $posts);
        $post = self::select('id', 'name', 'slug')->find($posts[$key - 1] ?? null);
        $this->prev = $post;

        return isset($post);
    }
}
