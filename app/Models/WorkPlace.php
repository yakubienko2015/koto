<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorkPlace extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name', 'terminal_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function terminal()
    {
        return $this->belongsTo('App\Models\Terminal');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany('App\Models\Product');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orderWorkPlaces()
    {
        return $this->hasMany('App\Models\OrderWorkPlace');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function productGroups()
    {
        return $this->belongsToMany('App\Models\ProductGroup');
    }
}
