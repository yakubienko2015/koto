<?php

namespace App\Models;

use App\Events\NewOrderWorkPlace;
use App\Events\UpdateOrderWorkPlace;
use App\Traits\TimestampSerializable;
use Illuminate\Database\Eloquent\Relations\Pivot;

class OrderWorkPlace extends Pivot
{
    public $incrementing = true;

    use TimestampSerializable;

    /**
     * @var array
     */
    protected $fillable = [
        'order_id',
        'work_place_id',
        'user_id',
        // 'statuses',
        // 'product_id',
        'completed_at',
        // 'modifiers',
    ];

    protected $dispatchesEvents = [
        'created' => NewOrderWorkPlace::class,
        'updated' => UpdateOrderWorkPlace::class,
    ];

    /**
     * @var array
     */
    public $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'order_id' => 'integer',
        'work_place_id' => 'integer',
        'user_id' => 'integer',
        'statuses' => 'array',
    ];

    protected $dates = ['created_at', 'completed_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function workPlace()
    {
        return $this->belongsTo('App\Models\WorkPlace');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }
}
