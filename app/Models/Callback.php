<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Callback extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'phone', 'name', 'comment', 'status'
    ];

    /**
     * @var array
     */
    public $casts = [
        'status' => 'integer',
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];
}
