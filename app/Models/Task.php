<?php

namespace App\Models;

use Carbon\Carbon;
use Cron\CronExpression;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'group_id',
        'title',
        'description',
        'description_resource',
        'is_required',
        'is_file_required',
        'cron',
        'holiday_users',

        // TODO - remove this fields if no need more
        'every_week',
        'every_day',
        'every_hour',
        'date',
        'time',
    ];

    public $casts = [
        'date' => 'date',
        'time' => 'time',
        'is_required' => 'bool',
        'is_file_required' => 'bool',
        'every_week' => 'bool',
        'every_day' => 'bool',
        'every_hour' => 'bool',
        'date' => 'date:Y-m-d',
        'next_time' => 'date:Y-m-d H:i:s',
        'holiday_users' => 'bool',
    ];

    /**
     * @var array
     */
    protected $appends = [
        'next_time',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo('App\Models\Group');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\Models\User')
            ->using('App\Models\TaskUser')
            ->withPivot([
                'id',
                'completed_at',
                'justification',
                'created_at',
            ]);
    }

    public function getNextTimeAttribute()
    {
        $cron = CronExpression::factory($this->cron);
        return new Carbon($cron->getNextRunDate());
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function share()
    {
        $hasUserTasks = TaskUser::whereDate('created_at', now())
            ->whereNull('completed_at')
            ->where('task_id', $this->id)
            ->pluck('user_id');
        $users = $this->group->users()->whereNotIn('id', $hasUserTasks);

        if (! $this->holiday_users) {
            $users->whereNotIn('id', UserHoliday::whereDate('date', now())
                ->pluck('user_id'));
        }

        $this->users()->attach($users->pluck('id'));
    }

    /**
     * @return \App\Models\Task
     */
    public static function getHolidayTask()
    {
        $task = self::whereCron('* * * * *')->first();
        if (! $task) {
            $task = self::create([
                'title' => 'На выходном',
                'cron' => '* * * * *',
                'group_id' => Group::first()->id,
            ]);
        }
        return $task;
    }
}
