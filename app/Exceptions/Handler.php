<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        // Empty
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    // public function report(Throwable $exception)
    // {
    //     parent::report($exception);
    // }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        if ($request->ajax() || $request->wantsJson()) {
            $response = parent::render($request, $exception);

            $response = [
                'message' => trans('api.exception_error'),
                'code' => $response->getStatusCode(),
                'data' => [],
            ];

            if (config('app.debug')) {
                $response['errors'] = $exception->getMessage();
            }
            if ($exception instanceof ValidationException) {
                $response['errors'] = $exception->validator->errors()->all();
            }

            if (\Lang::has('api.exceptions.' . $response['code'])) {
                $response['message'] = trans('api.exceptions.' . $response['code']);
            }

            return response()->json($response, $response['code']);
        }

        return parent::render($request, $exception);
    }
}
