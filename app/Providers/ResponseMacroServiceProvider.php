<?php

namespace App\Providers;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;

class ResponseMacroServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        // Empty
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('api', function ($code = 200, $message = null, $data = []) {
            $response = ['code' => $code];

            if (! is_null($message)) {
                $response['message'] = $message;
            } elseif (trans('api.exceptions.' . $code)) {
                $response['message'] = trans('api.exceptions.' . $code);
            } else {
                $response['message'] = trans('api.exception_error');
            }
            $response['data'] = $data;

            return Response::make($response, $code);
        });
    }
}
