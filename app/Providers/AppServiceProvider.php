<?php

namespace App\Providers;

use App\Library\CustomConfig;
use App\Models\City;
use App\Models\Order;
use Illuminate\Events\Dispatcher;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Jenssegers\Agent\Agent;
use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;
use LaravelLocalization;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('city', function () {
            if (request()->is('api/*')) {
                return City::whereId(
                    request()->user()->city_id ?? request()->get('city', 1)
                )->firstOr(function () {
                    return City::firstOr(function () {
                        return new City();
                    });
                });
            }
            $cityBySlug = City::whereSlug(request()->city)->first();
            if ($cityBySlug) {
                session(['city' => $cityBySlug->id]);
                return $cityBySlug;
            }
            return City::whereId(session('city', 1))->firstOr(function () {
                return City::firstOr(function () {
                    return new City();
                });
            });
        });

        $this->app->singleton('order', function () {
            $order = new Order();
            if (request()->is('api/*')) {
                $order->loadFromRequest();
            } else {
                $order->loadFromCookies();
            }
            return $order;
        });

        $this->app->singleton('agent', function () {
            return new Agent();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Dispatcher $events)
    {
        CustomConfig::load();
        Schema::defaultStringLength(191);

        $events->listen(BuildingMenu::class, function (BuildingMenu $event) {
            $langs = LaravelLocalization::getSupportedLocales();

            $items = City::all()->map(function (City $city) {
                return [
                    'text' => $city->name,
                    'icon' => '',
                    'url' => 'city/' . $city->id,
                ];
            })->toArray();

            $event->menu->add([
                'text' => app('city')->name,
                'icon' => '',
                'topnav_right' => true,
                'submenu' => $items,
            ]);

            $items = array_map(function ($props, $locale) {
                return [
                    'text' => $props['native'],
                    'icon' => 'flag-icon flag-icon-' . $props['country'],
                    'url' => 'lang/' . $locale,
                ];
            }, $langs, array_keys($langs));

            $event->menu->add([
                'text' => '',
                'icon' => 'flag-icon flag-icon-' . $langs[app()->getLocale()]['country'],
                'topnav_right' => true,
                'submenu' => $items,
            ]);

            // $event->menu->add([
            //     'text' => 'Режим обслуживания',
            //     'url'  => app()->isDownForMaintenance() ? 'admin/artisan?cmd=up' : 'admin/artisan?cmd=down --allow=127.0.0.1 --allow=192.168.0.0/16',
            //     'icon' => app()->isDownForMaintenance() ? 'far fa-fw fa-check-circle' : 'far fa-fw fa-circle',
            //     'icon_color' => app()->isDownForMaintenance() ? 'red' : 'green',
            // ]);
        });
    }
}
