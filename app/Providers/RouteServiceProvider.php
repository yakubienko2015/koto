<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;
use LaravelLocalization;
use Mcamara\LaravelLocalization\Traits\LoadsTranslatedCachedRoutes;

// use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class RouteServiceProvider extends ServiceProvider
{
    use LoadsTranslatedCachedRoutes;

    /**
     * The path to the "home" route for your application.
     *
     * @const string
     */
    public const HOME = '/account';

    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    // public function boot()
    // {
    //     parent::boot();
    // }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        // For change language in admin panel
        Route::middleware('web')
            ->get('/lang/{lang}', 'App\Http\Controllers\Controller@lang');

        // For change city
        Route::middleware('web')
            ->get('/city/{city}', 'App\Http\Controllers\Controller@city')
            ->name('city');

        $this->mapAppRoutes();

        $this->mapWebRoutes();

        $this->mapAdminRoutes();

        try {
            $this->mapCitiesWebRoutes();
        } catch (\Throwable $throw) {
            logger('Error on read db for web routes! ' . $throw->getMessage());
        }
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware([
            'siteBindings',
            'web',
            'localize',
        ])
            ->namespace($this->namespace)
            ->prefix(LaravelLocalization::setLocale())
            ->group(base_path('routes/web.php'));
    }

    /**
     * @return void
     */
    protected function mapCitiesWebRoutes()
    {
        Route::middleware([
            'siteBindings',
            'web',
            'localize',
        ])
            ->namespace($this->namespace)
            ->prefix(LaravelLocalization::setLocale())
            ->group(base_path('routes/cityweb.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::middleware(['api', 'localize'])
            ->prefix('api')
            ->namespace("{$this->namespace}\Api")
            ->group(base_path('routes/api.php'));
    }

    /**
     * Define the "admin" routes for the application.
     *
     * @return void
     */
    protected function mapAdminRoutes()
    {
        Route::prefix('admin')
            ->name('admin.')
            ->middleware([
                'web',
                'auth',
                'can:view-dashboard',
                'localeFromSession',
                'optimizeImages',
            ])
            ->namespace("{$this->namespace}\Admin")
            ->group(base_path('routes/admin.php'));
    }

    /**
     * Define the "app" routes for the application.
     *
     * @return void
     */
    protected function mapAppRoutes()
    {
        Route::prefix('app')
            ->name('app.')
            ->middleware([
                'web',
                \App\Http\Middleware\LogoutUsers::class,
            ])
            ->namespace("{$this->namespace}\App")
            ->group(base_path('routes/app.php'));
    }
}
