<?php

namespace App\Traits;

use App\Models\City;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\Product;
use App\Models\Terminal;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use App\Models\SmsConfirmation;

trait HasOrderIiko
{
    /**
     * @param array $iikoOrder
     * @return array
     */
    public static function convertIikoRequest($iikoOrder)
    {
        return [
            'order' => [
                'fullSum' => $iikoOrder['sum'],
                'date' => $iikoOrder['deliveryDate'],
                'items' => $iikoOrder['items'],
            ],
            'callback' => 0,
            'name' => $iikoOrder['customer']['name'],
            'phone' => $iikoOrder['customer']['phone'],
            'payment' => Order::ORDER_PAYMENT_CASH,
            'address' => $iikoOrder['address'],
            'persons' => $iikoOrder['personsCount'],
            'odd_money' => 0,
            'pre_order' => Carbon::parse($iikoOrder['deliveryDate'])
                ->greaterThan(
                    Carbon::parse($iikoOrder['createdTime'])->addHour()
                ),
            'comment' => $iikoOrder['comment'],
            'source' => 'iiko',
        ];
    }

    /**
     * @param array $items
     * @return collect
     */
    public static function convertIikoItems($items)
    {
        $products = Product::whereIn('uuid', collect($items)->pluck('id'))
            ->get();

        foreach ($products as $product) {
            $item = collect($items)->where('id', $product->uuid)->first();

            $modifier = Product::whereIn('uuid',
                collect($item['modifiers'])->pluck('id')
            )->whereCategoryId(Order::BOWL_MODIFIER_CATEGORY_ID)->first();

            $orderItem = new OrderProduct([
                'amount' => $item['amount'],
                'product_id' => $product->id,
                'sum' => $item['sum'],
                'modifiers' => $modifier->id ?? 0,
            ]);
            $product->pivot = $orderItem;
            // $products->push($product);
        }
        return $products;
    }

    /**
     * @param array $iikoOrder
     * @return void
     */
    public function loadFromIiko($iikoOrder)
    {
        $city = City::whereOrganizationUuid($iikoOrder['organization'])
            ->first();
        $terminal = Terminal::whereUuid(
            $iikoOrder['deliveryTerminal']['deliveryTerminalId']
        )->first();
        if ($terminal) {
           $city = $terminal->city;
        }
        $user = User::whereUuid($iikoOrder['customerId'])->first();

        $this->data = Order::convertIikoRequest($iikoOrder);
        $this->uuid = $iikoOrder['orderId'];
        $this->phone = $iikoOrder['customer']['phone'];
        $this->payment_type = Order::ORDER_PAYMENT_CASH;
        $this->ordered_at = $iikoOrder['deliveryDate'];
        $this->customer_name = $iikoOrder['customer']['name'];
        // $this->status = config('shop.iiko_statuses')[$iikoOrder['status']];
        $this->status = Order::ORDER_STATUS_NEW;
        $this->sum = $iikoOrder['sum'];
        $this->city_id = $city->id ?? null;
        $this->terminal_id = $terminal->id ?? null;
        $this->user_id = $user->id ?? null;
        $this->number = $iikoOrder['number'];
    }

    /**
     * @param array $data
     * @return array
     */
    public static function fixIikoModifiers($data)
    {
        $city = App::get('city');
        $products = Cache::remember('iikoNomenclatures' . $city->id, 86400, function () {
            $iiko = App::get('iiko');
            $city = App::get('city');
            $menu = $iiko->NomenclatureApi()->getMenu($city->organization_uuid);
            return collect($menu['products'] ?? []);
        });

        foreach ($data['order']['items'] ?? [] as $key => $item) {
            $iikoProduct = $products->where('id', $item['id'])->first();
            $modifierProduct = Product::find($item['modifiers'] ?? 0);

            $modifiers = [];
            foreach ($iikoProduct['modifiers'] ?? [] as $modifier) {
                if ($modifier['required']) {
                    $modifiers[] = [
                        'id' => $modifier['modifierId'],
                        'amount' => $modifier['defaultAmount'],
                    ];
                }
            }
            if ($modifierProduct) {
                $modifiers[] = [
                    'id' => $modifierProduct->uuid,
                    'amount' => 1,
                ];
            }
            $data['order']['items'][$key]['modifiers'] = $modifiers;
        }
        return $data;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public static function syncWithIiko()
    {
        $orders = app('city')->orders()
            ->select('id', 'uuid', 'phone', 'status', 'city_id', 'user_id', 'data')
            ->where('created_at', '>=', now()->subdays(8))
            ->where('status', '<>', Order::ORDER_STATUS_CLOSED)
            ->where('status', '<>', Order::ORDER_STATUS_CANCELLED)
            ->whereNotNull('uuid')
            ->orderBy('id', 'desc')
            ->get();

        foreach ($orders as $order) {
            try {
                $iikoOrder = app('iiko')->OrdersApi()->getInfo(app('city')->organization_uuid, $order->uuid, ['request_timeout' => '00:00:10']);
                // dd($iikoOrder);
            } catch (\Throwable $th) {
                logger('iiko error on sync with iiko', [$th->getMessage()]);
                break;
            }
            $order->updateFromIiko($iikoOrder);
        }
    }

    /**
     * @param array $iikoOrder
     * @return void
     */
    public function updateFromIiko($iikoOrder)
    {

        GLOBAL $nestingOrderLevel;
        $nestingOrderLevel = 0;

        $iikoOrderItems = collect($iikoOrder['items']);
        $newItemsUuids = $iikoOrderItems->pluck('id')
            ->diff($this->products->pluck('uuid'));
        if (count($newItemsUuids)) {
            $newItems = $iikoOrderItems->whereIn('id', $newItemsUuids);
            $addItems = Order::convertIikoItems($newItems);
            $this->attachProducts($addItems);
        }
        if (in_array($this->status, [
                Order::ORDER_STATUS_IN_PROGRESS,
                Order::ORDER_STATUS_NEW,
            ])) {
            $removedItemsUuids = $this->products->pluck('uuid')
                ->diff($iikoOrderItems->pluck('id'));
            if (count($removedItemsUuids)) {
                $oldItems = $this->products->whereIn('uuid', $removedItemsUuids);
                $this->products()->detach($oldItems->pluck('id'));
            }
        }
        if (! $this->terminal_id) {
            $terminal = Terminal::whereUuid(
                $iikoOrder['deliveryTerminal']['deliveryTerminalId']
            )->first();
            if ($terminal) {
                $this->terminal_id = $terminal->id;
                $this->city_id = $terminal->city_id;
            }
        }
        if (! $this->user_id) {
            $user = User::whereUuid(
                $iikoOrder['customerId']
            )->first();
            $this->user_id = $user->id ?? null;
        }
        $this->save();
        $newStatus = config('shop.iiko_statuses')[$iikoOrder['status']] ?? 2;

        if ($newStatus != $this->status){
            $message = '';
            if ($this->status == Order::ORDER_STATUS_NEW && $newStatus == Order::ORDER_STATUS_IN_PROGRESS){
                $date = date_create($iikoOrder['deliveryDate']);
                $deliveryDate = date_format($date,'m-d H:i');
                $message = 'Спасибо ' . $iikoOrder['customerName'] . ', заказ принят, будет у вас ' . $deliveryDate;
            }
            if ($newStatus == Order::ORDER_STATUS_ON_WAY) {
                $courier = User::whereUuid(
                            $iikoOrder['courierInfo']['courierId']
                            )->first();
                $deliveryPhone = '';
                if(isset($courier->phone)) {
                    $deliveryPhone = ' ' . str_replace('+38', '', $courier->phone);
                }
                $deliveryName = '';
                if(isset($courier->name)) {
                    $aname = explode(" ", $courier->name);
                    $deliveryName = $aname[0];
                }
                $sum = $iikoOrder['sum'];

                $message = $iikoOrder['customerName'] . ', заказ в пути, курьер ' . $deliveryName . $deliveryPhone . '. На сумму ' . $sum . ' грн.';
            }
            $phone = $iikoOrder['customerPhone'];

            if($message) {
                $sms = new SmsConfirmation(['phone' => $phone, 'code' => $message]);
                try {
                    $sms->sendAllSms();
                } catch (\Throwable $th) {
                    logger('api send sms for customer', [$th->getMessage()]);
                }
            }
        }

        if ($this->status == Order::ORDER_STATUS_READY
            && $newStatus == Order::ORDER_STATUS_IN_PROGRESS) {
            // skip sync status
        } else {
            $this->update([
                'sum' => $iikoOrder['sum'],
                'number' => $iikoOrder['number'],
                'status' => $newStatus,
                'ordered_at' => $iikoOrder['deliveryDate'],
            ]);
        }
    }

    /**
     * @param array $iikoOrder
     * @return void
     */
    public static function createFromIiko($iikoOrder)
    {
        $newOrder = new Order();
        $newOrder->loadFromIiko($iikoOrder);
        $newOrder->save();

        $newOrder->attachProducts(Order::convertIikoItems($iikoOrder['items']));

        $newOrder->update([
            'status' => config('shop.iiko_statuses')[$iikoOrder['status']],
        ]);
    }

    /**
     * @return array
     */
    public function getIikoInfo()
    {
        return app('iiko')->OrdersApi()->getInfo(
            app('city')->organization_uuid,
            $this->uuid
        );
    }
}
