<?php

namespace App\Traits;

use Carbon\Carbon;
use DateTimeInterface;

trait TimestampSerializable
{
    protected function serializeDate(DateTimeInterface $date)
    {
        return Carbon::instance($date)->toDateTimeString();
    }
}
