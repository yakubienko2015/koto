<?php

namespace App\Exports;

use App\Models\TaskUser;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class UserTaskExport implements FromQuery, WithMapping, WithHeadings, ShouldAutoSize
{
    use Exportable;

    private $date = null;

    public function __construct($date = null)
    {
        $this->date = $date;
    }

    /**
    * @return \Illuminate\Support\Query
    */
    public function query()
    {
        $tasks = TaskUser::query()->with('task.group');
        if ($this->date) {
            return $tasks->whereDate('created_at', $this->date);
        }
        return $tasks->whereNotNull('task_id')->whereNotNull('user_id');
    }

    /**
    * @var Invoice $invoice
    */
    public function map($invoice): array
    {
        return [
            $invoice->task->group->name ?? '-',
            $invoice->created_at->format('Y-m-d'),
            $invoice->user->name ?? '-',
            $invoice->user->phone ?? '-',
            $invoice->task->title ?? '-',
            $invoice->completed_at
                ? $invoice->completed_at->format('Y-m-d H:i:s')
                : 'Нет',
            $invoice->getFilesLinks(),
        ];
    }

    public function headings(): array
    {
        return [
            'Группа',
            'Дата',
            'Имя',
            'Телефон',
            'Задания',
            'Выполнено',
            'Файлы',
        ];
    }
}
