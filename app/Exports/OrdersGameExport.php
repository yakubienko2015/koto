<?php

namespace App\Exports;

use App\Models\Order;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class OrdersGameExport implements FromQuery, WithMapping, WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function query()
    {
        return Order::where('data', 'like', '%game_score%');
    }

    /**
    * @var Order $order
    */
    public function map($order): array
    {
        return [
            $order->id,
            $order->created_at->format('Y-m-d H:i:s'),
            $order->ordered_at->format('Y-m-d H:i:s'),
            $order->customer_name,
            (string) $order->phone,
            $order->data['game_score'] ?? 0,
        ];
    }

    public function headings(): array
    {
        return [
            'Номер',
            'Дата создания',
            'Дота заказа',
            'Имя',
            'Телефон',
            'Очки в игре',
        ];
    }
}
