<?php

namespace App\Library;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Meta extends Model
{
    use HasTranslations;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'keywords',
        'robots',
        'metaable_id',
        'metaable_type',

        'content',
        'is_active',
        'city_id',
    ];

    /**
     * @var array
     */
    protected $translatable = [
        'title',
        'description',
        'keywords',

        'content',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\morphTo
     */
    public function metaable()
    {
        return $this->morphTo();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\morphTo
     */
    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }

    /**
     * @return array
     */
    public static function getRobotsForSelect()
    {
        return [
            'index, follow' => 'index, follow',
            'noindex, follow' => 'noindex, follow',
            'index, nofollow' => 'index, nofollow',
            'noindex, nofollow' => 'noindex, nofollow',
        ];
    }
}
