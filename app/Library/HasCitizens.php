<?php

namespace App\Library;

trait HasCitizens
{
    protected function getCitizensAttribute($val)
    {
        $prices = json_decode($val, true);
        if (is_array($prices)) {
            return $prices[app('city')->id] ?? ($prices[1] ?? 0);
        }
        return $val;
    }

    protected function setCitizensAttribute($field, $val)
    {
        $prevValue = json_decode($this->getRawOriginal($field), true);
        if (is_array($prevValue)) {
            $prevValue[app('city')->id] = $val;
        } else {
            $prevValue = [
                '1' => $val,
                app('city')->id => $val,
            ];
        }
        $this->attributes[$field] = $prevValue;
    }
}
