<?php

namespace App\Library;

use App\Models\TaskUser;
use App\Models\User;
use Hash;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Str;
use Telegram;
use Telegram\Bot\Keyboard\Keyboard;

class TelegramBotHelper
{
    /**
     * Undocumented function
     *
     * @return array
     */
    public static function analizMessage($message)
    {
        if (!$message->has('entities') && $message->has('contact')) {
            $contact = $message->getContact();
            $phone = clear_phone_number($contact->getPhoneNumber());
            $user = User::whereTelegramId($contact->getUserId())->first();
            if (!$user) {
                $user = User::wherePhone($phone)->first();

                if ($user) {
                    $user->update(['telegram_id' => $contact->getUserId()]);
                } else {
                    $user = User::create([
                        'name' => $contact->getFirstName(),
                        'phone' => $phone,
                        'password' => Hash::make(Str::random()),
                        'telegram_id' => $contact->getUserId(),
                    ]);
                }
            }
            self::getStartFunctions($user);
        }
        if ($message->has('document')) {
            self::saveFile([
                'file_id' => $message->getDocument()->getFileId(),
                'file_name' => $message->getDocument()->getFileName(),
            ], $message->getFrom());
        }
        if ($message->has('photo')) {
            $photos = $message->getPhoto();
            self::saveFile([
                'file_id' => $photos[count($photos) - 1]['file_id'],
            ], $message->getFrom());
        }
        if ($message->has('video')) {
            self::saveFile([
                'file_id' => $message->getVideo()->getFileId(),
                'file_name' => $message->getDocument()->getFileName(),
            ], $message->getFrom());
        }
        return;
    }

    public static function analizUpdateForQuery($update)
    {
        $query = $update->getCallbackQuery();
        if ($query) {
            $callbackId = $query->getId();
            $userTask = TaskUser::find($query->getData());
            if ($userTask->task->is_file_required) {
                cache(['tg:' . $query->getFrom()->getId() => [
                    'task_user_id' => $userTask->id,
                    'message_id' => $update->getMessage()->getMessageId(),
                ]]);
                Telegram::answerCallbackQuery([
                    'callback_query_id' => $callbackId,
                    'text' => 'Загрузите файл для подтверждения',
                    'show_alert' => true,
                ]);
            } else {
                $userTask->setCompleted(
                    $update->getMessage()->getMessageId(),
                    $callbackId
                );
            }
        }
    }

    public static function saveFile($file, $telegramUser)
    {
        $cachedValue = cache('tg:' . $telegramUser->getId());
        $userTask = TaskUser::find($cachedValue['task_user_id'] ?? null);
        if ($userTask) {
            $telegramFile = Telegram::getFile($file);
            $botToken = Telegram::getBotConfig()['token'];
            $fileName = $telegramFile->getFileUniqueId();

            $contents = file_get_contents("https://api.telegram.org/file/bot{$botToken}/" . $telegramFile->getFilePath());
            $localFile = sys_get_temp_dir() . DIRECTORY_SEPARATOR . $fileName;
            file_put_contents($localFile, $contents);
            $uploadedFile = new UploadedFile($localFile, $fileName);
            $ext = $uploadedFile->guessExtension();
            $fileName = isset($file['file_name'])
                ? $file['file_name']
                : "${fileName}.${ext}";

            if (config('filesystems.disks.google.clientId')) {
                $date = now()->format('Y-m-d');
                $cFileName = "{$userTask->task->group->name}-{$userTask->task->title}-{$date}-{$userTask->user->name}-{$userTask->user->phone}-{$fileName}";
                Storage::cloud()->put($cFileName, $contents);
                $storageUrl = Storage::cloud()->url($cFileName);
            } else {
                Storage::disk('public')->putFileAs("bot/tasks/{$userTask->id}", $uploadedFile, $fileName);
                $storageUrl = asset(Storage::url("bot/tasks/{$userTask->id}/{$fileName}"));
            }

            $userTask->update([
                'files' => array_merge([$storageUrl], $userTask->files ?? []),
            ]);

            $userTask->setCompleted($cachedValue['message_id'] ?? null);
        }
    }

    public static function authUser($update)
    {
        $telegramUser = $update->getMessage()->getFrom();
        if ($update->getCallbackQuery()) {
            $telegramUser = $update->getCallbackQuery()->getFrom();
        }
        $user = User::whereTelegramId($telegramUser->getId())->first();

        if ($user) {
            return $user;
        }

        $btn = Keyboard::button([
            'text' => 'Отправить',
            'request_contact' => true,
        ]);
        $keyboard = Keyboard::make([
            'keyboard' => [[$btn]],
            'resize_keyboard' => true,
            'one_time_keyboard' => true,
        ]);
        Telegram::sendMessage([
            'chat_id' => $telegramUser->getId(),
            'text' => 'Отправьте мне свой номер для авторизации',
            'reply_markup' => $keyboard,
            'entities' => [
                [
                    'type' => 'bot_command',
                ],
            ],
        ]);
        return null;
    }

    public static function getStartFunctions($user)
    {
        if ($user->hasRole('manager')) {
            $keyboard = Keyboard::make([
                'inline_keyboard' => [[
                    Keyboard::inlineButton([
                        'url' => route('admin.groups.index'),
                        'text' => 'Редактировать группы',
                    ])], [
                    Keyboard::inlineButton([
                        'url' => route('admin.tasks.index'),
                        'text' => 'Редактировать задачи',
                    ])], [
                    Keyboard::inlineButton([
                        'url' => route('admin.users.index'),
                        'text' => 'Редактировать пользователей',
                    ])],
                ],
            ]);
        }
        if ($user->hasAnyRole(['manager', 'admin', 'operator', 'courier', 'cook', 'logist'])) {
            Telegram::sendMessage([
                'chat_id' => $user->telegram_id,
                'text' => "/tasks - Список задач\n/stop - Сегодня выходной",
                'reply_markup' => $keyboard ?? null,
            ]);
            return;
        }
        Telegram::sendMessage([
            'chat_id' => $user->telegram_id,
            'text' => 'Вы еще не сотрудник. Ожидайте подтверджения от администратора',
        ]);
    }

    public static function getTasksMessageForUser($user)
    {
        $tasks = '';
        $buttons = [];
        foreach ($user->tasks()->wherePivot('completed_at', null)->wherePivot('created_at', '>=', now()->setHour(1))->get()->sortBy('next_time') as $taskKey => $task) {
            $taskNum = $taskKey + 1;
            $tasks .= "\n{$taskNum}) {$task->title}\n_{$task->description}_";
            if ($task->description_resource) {
                $tasks .= "\n[линк]({$task->description_resource})";
            }
            $buttons[] = [Keyboard::inlineButton([
                'callback_data' => $task->pivot->id,
                'text' => "Выполнить задания {$taskNum}",
            ])];
        }
        return [
            'text' => '*Задачи на ' . now()->format('d.m.Y') . "*\n" . $tasks,
            'parse_mode' => 'Markdown',
            'reply_markup' => Keyboard::make([
                'inline_keyboard' => $buttons,
            ]),
        ];
    }
}
