<?php

namespace App\Library;

use App\Models\City;

trait HasMeta
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\morphOne
     */
    public function meta()
    {
        return $this->morphOne('App\Library\Meta', 'metaable')
            ->whereCityId(app('city')->id);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\morphOne
     */
    public function metas()
    {
        return $this->morphMany('App\Library\Meta', 'metaable');
    }

    /**
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeJoinMeta($query)
    {
        return $query->leftJoin('metas', function ($join) {
            $join->on('metas.metaable_id', '=', $this->getTable() . '.id')
                ->where('metas.city_id', app('city')->id)
                ->where('metas.metaable_type', get_class($this))
                ->limit(1);
        });
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function saveMeta($data)
    {
        $cities = City::pluck('id');
        $metaData = [
            'title' => $data['title'] ?? '',
            'description' => $data['description'] ?? '',
            'keywords' => $data['keywords'] ?? '',
            'robots' => $data['robots'] ?? 'index, follow',
            'content' => $data['content'] ?? '',
            'is_active' => $data['is_active'] ?? true,
        ];
        if ($data['for_all_cities'] ?? false) {
            foreach ($cities as $cityId) {
                Meta::updateOrCreate([
                    'metaable_id' => $this->id,
                    'metaable_type' => get_class($this),
                    'city_id' => $cityId,
                ], $metaData);
            }
        } else {
            Meta::updateOrCreate([
                'metaable_id' => $this->id,
                'metaable_type' => get_class($this),
                'city_id' => app('city')->id,
            ], $metaData);
        }

        return true ;
    }

    /**
     * @return bool
     */
    public function getIsActiveAttribute()
    {
        return $this->meta->is_active ?? false;
    }
}
