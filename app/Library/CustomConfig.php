<?php

namespace App\Library;

use Illuminate\Support\Facades\Storage;

class CustomConfig
{
    const FILE_NAME = 'config';

    /**
     * Undocumented function
     *
     * @return array
     */
    public static function load()
    {
        $driver = config('custom-configs.default');
        $customConfig = [];
        if ($driver === 'path') {
            $customConfig = self::loadFromFile();
        }
        if ($driver === 'storage') {
            $customConfig = self::loadFromStorage();
        }
        config($customConfig);
        return $customConfig;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    private static function loadFromStorage()
    {
        $disk = config('custom-configs.storage');

        if (Storage::disk($disk)->exists(self::FILE_NAME)) {
            return unserialize(Storage::disk($disk)->get(self::FILE_NAME));
        }
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    private static function loadFromFile()
    {
        $path = config('custom-configs.path');
        $fileName = base_path($path . self::FILE_NAME);

        if (file_exists($fileName)) {
            return unserialize(file_get_contents($fileName));
        }
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public static function put($key, $value = null)
    {
        $currentCustomConfig = self::load();
        $currentCustomConfig[$key] = $value;
        config($currentCustomConfig);
        return self::store($currentCustomConfig);
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public static function store($newConfig)
    {
        $oldConfigs = self::load();
        foreach ($newConfig as $newConfigKey => $newConfigValue) {
            $oldConfigs[$newConfigKey] = $newConfigValue;
        }
        $driver = config('custom-configs.default');
        if ($driver === 'path') {
            return self::putToFile($oldConfigs);
        }
        if ($driver === 'storage') {
            return self::putToStorage($oldConfigs);
        }
        return null;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    private static function putToStorage($newConfig)
    {
        $disk = config('custom-configs.storage');
        return Storage::disk($disk)->put(self::FILE_NAME, serialize($newConfig));
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    private static function putToFile($newConfig)
    {
        $path = config('custom-configs.path');
        $fileName = base_path($path . self::FILE_NAME);

        return file_put_contents($fileName, serialize($newConfig));
    }
}
