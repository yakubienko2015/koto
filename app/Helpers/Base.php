<?php

use App\Models\Components\Journal;
use voku\helper\HtmlDomParser;

if (!function_exists('clear_phone_number')) {
    function clear_phone_number($phone)
    {
        $phone = '+' . substr(str_replace(['+', '(', ')', '-', ' '], '', $phone), 0, 12);
        return str_replace(['+0', '+80'], '+380', $phone);
    }
}

if (!function_exists('spy')) {
    function spy($message, $type = 'info')
    {
        Journal::create([
            'type'    => $type,
            'message' => $message
        ]);
    }
}

if (!function_exists('instagram_data')) {
    function instagram_data()
    {
        if (Cache::has('instagram_data')){
            $data = Cache::get('instagram_data');
        } else {
            try {
                $html = HtmlDomParser::file_get_html('https://averin.pro/widget.php?l=kotosushi.com.ua&style=4&width=250&gallery=1&s=180&icc=1&icr=0&t=0&tt=text1&h=1&ttcolor=FFFFFF&th=c3c3c3&bw=f9f9f900&bscolor=666699&bs=99999900&ts=text2&ch=utf8');
                $headerBlock = $html->find('.header')[0] ?? null;
                $imageBlock = $html->find('.data a img')[0] ?? null;
                $image = $imageBlock ? $imageBlock->src : null;
                $subscribers = (int) ($headerBlock ? $headerBlock->plaintext : '0');
                $data = [
                    'subscribers' => $subscribers,
                    'image' => $image,
                ];
            } catch (\Throwable $th) {
                logger('error on load instagram widget', [$th->getMessage()]);
                $data = [
                    'subscribers' => 0,
                    'image' => null,
                ];
            }

            Cache::put('instagram_data', $data, 60 * 60 * 1);
        }
        return $data;
    }
}
