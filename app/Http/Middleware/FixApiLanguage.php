<?php

namespace App\Http\Middleware;

use Closure;
use Mcamara\LaravelLocalization\LanguageNegotiator;

class FixApiLanguage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $negotiator = new LanguageNegotiator(
            config('app.locale'),
            config('laravellocalization.supportedLocales'),
            $request
        );
        app()->setLocale($negotiator->negotiateLanguage());

        return $next($request);
    }
}
