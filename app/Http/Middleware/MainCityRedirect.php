<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Str;

class MainCityRedirect
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (app('city')->id === 1 && Str::endsWith($request->path(), app('city')->slug)) {
            return redirect()->route('index');
        }
        return $next($request);
    }
}
