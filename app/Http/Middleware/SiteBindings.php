<?php

namespace App\Http\Middleware;

use Closure;

class SiteBindings
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Route::bind('page', function ($value) {
        //     return Page::where('slug', $value)->where('city_id', app('city')->id)->firstOrFail();
        // });

        return $next($request);
    }
}
