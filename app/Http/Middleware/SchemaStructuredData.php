<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\View;

class SchemaStructuredData
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        View::share('schemaLocalBusiness', [
            '@context' => 'http://schema.org',
            '@type' => 'LocalBusiness',
            '@id' => 'kotosushi.com.ua',
            'name' => config('app.name', 'Kotosushi'),
            'url' => route('index'),
            'logo' => asset('images/site/logo_lg.png'),
            'image' => asset('images/site/logo_lg.png'),
            'telephone' => config('site.phones')[0] ?? '',
            'priceRange' => '₴₴₴',
            'address' => [
                '@type' => 'PostalAddress',
                'streetAddress' => 'вулиця Московська, 11',
                'addressLocality' => 'Миколаїв',
                'addressRegion' => 'Миколаївська область',
                'postalCode' => '54000',
                'addressCountry' => 'UA',
            ],
        ]);
        View::share('schemaBreadcrumbList', [
            '@context' => 'https://schema.org',
            '@type' => 'BreadcrumbList',
            'itemListElement' => [[
                '@type' => 'ListItem',
                'position' => 1,
                'name' => app('city')->name,
                'item' => app('city')->getIndexUrl(),
            ]],
        ]);
        return $next($request);
    }
}
