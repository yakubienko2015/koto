<?php

namespace App\Http\Requests;

use App\Models\Order;
use App\Models\Product;
use App\Models\Promo;
use App\Rules\CheckAddress;
use App\Rules\InWorkTime;
use App\Rules\IsNotHoliday;
use App\Rules\IsNotOverload;
use App\Rules\MinBasketSum;
use App\Rules\ValidPromo;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class OrderRequest extends FormRequest
{
    protected $minCount = 1;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        config(['iiko-biz.logging' => storage_path() . '/logs/iiko-make-order.log']);
        return true;
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $address = ['city' => app('city')->name] + ($this->address ?? []);
        if ($this->callback) {
            $address['home'] = 1;
        }
        $this->merge([
            'pre_order' => $this->pre_order ? true : false,
            'phone' => clear_phone_number($this->phone),
            'name' => $this->name ?: '-',
            'address' => $address,
            'source' => 'site',
        ]);


        $goodProduct = Product::find(config('shop.good_mood_product_id'));
        if ($goodProduct) {
            app('order')->addLoadedItem($goodProduct, 1);
        }
        if (app('order')->free_sauces) {
            $freeSauseProduct = Product::find(config('shop.free_sauce_product_id'));
            app('order')->addLoadedItem($freeSauseProduct, app('order')->free_sauces);
        }
        if ($this->sauces > 0) {
            $addSauseProduct = Product::find(config('shop.add_sauce_product_id'));
            app('order')->addLoadedItem($addSauseProduct, $this->sauces);
        }

        $items = [];
        foreach (app('order')->products as $product) {
            $items[] = [
                'id' => $product->uuid,
                'code' => $product->id,
                'name' => $product->name,
                'amount' => $product->pivot->amount,
                'sum' => $product->pivot->sum,
                'modifiers' => $product->pivot->modifiers ? $product->pivot->modifiers : null
            ];
        }

        $usedPromo = app('order')->usedPromo;
        $discount = $usedPromo ? $usedPromo->discount_sum : 0;

        $this->merge([
            'order' => [
                'sum' => app('order')->sum + $discount,
                'fullSum' => app('order')->sum,
                'date' => $this->pre_order && $this->date && $this->time
                    ? Carbon::createFromFormat('Y-m-d H:i', $this->date . ' ' . $this->time)
                    // : app('order')->ordered_at,
                    : now()->addSeconds(5),
                'items' => $items,
            ],
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // $byKotcoins = $this->payment === Order::ORDER_PAYMENT_KOTCOINS;
        // Test if user has enought kotcoins
        // if ($byKotcoins && auth()->user()->kotcoins * $kotcoinsPrice >= $this->order['fullSum']) {
        //     unset($payments[Order::ORDER_PAYMENT_KOTCOINS]);
        // }
        $payments = app('city')->getPayments();
        return [
            'phone' => 'required|string|size:13',
            'payment' => [Rule::in($payments)],
            'callback' => 'in:0,1',
            'name' => 'required_if:callback,0',
            'address' => ['array', ! $this->callback ? new CheckAddress() : ''],
            'address.city' => 'string',
            'address.street' => ['required_if:callback,0'],
            'address.home' => 'required_if:callback,0',
            'address.floor' => 'string|nullable',
            'address.doorphone' => 'string|nullable',
            'address.apartment' => 'string|nullable',
            'persons' => 'numeric|nullable',
            'odd_money' => 'numeric|nullable',
            'date' => $this->pre_order ? 'required|date_format:Y-m-d' : '',
            'time' => $this->pre_order ? ('required|date_format:H:i|' . ($this->order['date']->isToday() ? 'after:'.now()->addHours(2)->format('H:i') : '')) : '',
            'order.items' => ['array', 'min:'.$this->minCount],
            'order.date' => ['date', new InWorkTime($this->pre_order), new IsNotHoliday(), new IsNotOverload($this->pre_order)],
            'promo' => $this->promo ? new ValidPromo($this->order['sum'] ?? 0) : '',
            // 'order.sum' => ['numeric', new MinBasketSum($this->promo)],
            'order.sum' => ['numeric', $this->promo ? '' : ('gte:' . config('shop.min_basket_sum'))],
            'order.fullSum' => ['numeric'],
            'pre_order' => '',
            'comment' => '',
            'sauces' => 'numeric|nullable',
            'source' => '',
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $fails = $validator->failed();
        if (isset($fails['order.date']['App\Rules\InWorkTime'])) {
            $workTime = app('city')->getTodayWorkTime();
            return abort(response(['message' => '', 'code' => 422, 'errors' => [[
                'html' => __('Work from :from to :to', [
                    'from' => $workTime['from']->format('H:i'),
                    'to' => $workTime['to']->format('H:i'),
                ]) . '<br><br>' . __('You can do preorder'),
                'imageUrl' => asset('/images/site/sleep.png'),
                'confirmButtonText' => __('Make preorder'),
            ]]], 422));
        }
        if (isset($fails['address']['App\Rules\CheckAddress'])) {
            return abort(response(['message' => '', 'code' => 422, 'errors' => [[
                'html' => __('validation.check_address') . '<br>' . __('validation.check_address_'),
                'imageUrl' => asset('/images/site/location.png'),
                'confirmButtonText' => __('Okay'),
            ]]], 422));
        }
        throw new ValidationException($validator);
    }
}
