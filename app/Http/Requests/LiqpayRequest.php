<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LiqpayRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        config(['iiko-biz.logging' => storage_path() . '/logs/iiko-make-order.log']);
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'signature' => 'required',
            'data'      => 'required',
        ];
    }
}
