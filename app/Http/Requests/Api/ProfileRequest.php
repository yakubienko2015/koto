<?php

namespace App\Http\Requests\Api;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'       => 'required|string|max:255',
            'city_id'    => 'required|integer',
            'birthday'   => 'nullable|date_format:Y-m-d',
            'email'      => ['nullable', 'string', 'email', Rule::unique('users')->ignore($this->user())],
            'middle_name' => 'nullable|string|max:255',
            'sur_name'    => 'nullable|string|max:255',
            'sex'        => ['nullable', Rule::in(User::SEXES)],
        ];
    }
}
