<?php

namespace App\Http\Requests\Api;

use App\Models\City;
use App\Models\MarketBonus;
use App\Models\Order;
use App\Models\Product;
use App\Models\Promo;
use App\Rules\CheckAddress;
use App\Rules\HaveEnoughBonuses;
use App\Rules\InWorkTime;
use App\Rules\IsNotBusy;
use App\Rules\IsNotHoliday;
use App\Rules\IsNotOverload;
use App\Rules\ValidPromo;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        config(['iiko-biz.logging' => storage_path() . '/logs/iiko-make-order-mobile.log']);
        return true;
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $address = ['city' => app('city')->name] + ($this->address ?? []);
        // if ($this->callback) {
        //     $address['home'] = 1;
        // }
        $this->merge([
            'pre_order' => $this->date ? true : false,
            'phone' => clear_phone_number($this->user()->phone),
            'name' => $this->user()->name ?? '-',
            'address' => $address,
            'source' => 'mobile',
        ]);

        // if ($this->request->get('promo')) {
        //     $promo = Promo::whereCode($this->promo)
        //         ->whereIsActive(true)
        //         ->first();
        //     if ($promo) {
        //         config(['shop.min_basket_sum' => $promo->min_basket_sum]);
        //     } else {
        //         throw ValidationException::withMessages(
        //             ['promo' => __('Неправильный промокод')]
        //         );
        //     }
        // }

        if (! $this->user()->getIikoCategories()
                ->contains(config('shop.market_bonus_category_uuid'))) {
            $marketProduct = Product::find(config('shop.market_bonus_product_id'));
            if ($marketProduct) {
                app('order')->addLoadedItem($marketProduct, 1);
            }
        }

        $goodProduct = Product::find(config('shop.good_mood_product_id'));
        if ($goodProduct) {
            app('order')->addLoadedItem($goodProduct, 1);
        }
        if (app('order')->free_sauces) {
            $freeSauseProduct = Product::find(config('shop.free_sauce_product_id'));
            app('order')->addLoadedItem($freeSauseProduct, app('order')->free_sauces);
        }
        if ($this->sauces > 0) {
            $addSauseProduct = Product::find(config('shop.add_sauce_product_id'));
            app('order')->addLoadedItem($addSauseProduct, $this->sauces);
        }

        $items = [];
        foreach (app('order')->products as $product) {
            $items[] = [
                'id' => $product->uuid,
                'code' => $product->id,
                'name' => $product->name,
                'amount' => $product->pivot->amount,
                'sum' => $product->pivot->sum,
                'modifiers' => $product->pivot->modifiers ? $product->pivot->modifiers : null
            ];
        }

        $usedPromo = app('order')->usedPromo;
        $discount = $usedPromo ? $usedPromo->discount_sum : 0;

        $this->merge([
            'order' => [
                'sum' => app('order')->sum + $discount,
                // 'sum' => isset($promo) ? (app('order')->sum + $promo->getDiscountSum()) : app('order')->sum,
                'fullSum' => app('order')->sum,
                'date' => $this->date
                    ? Carbon::parse($this->date)
                    : now()->addSeconds(5),
                'items' => $items,
            ],
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // $byKotcoins = $this->payment === Order::ORDER_PAYMENT_KOTCOINS;
        // Test if user has enought kotcoins
        // if ($byKotcoins && auth()->user()->kotcoins * $kotcoinsPrice >= $this->order['fullSum']) {
        //     unset($payments[Order::ORDER_PAYMENT_KOTCOINS]);
        // }
        // dd(app('city')->getStreets());
        $payments = app('city')->getPayments();
        return [
            'city_id' => [Rule::in(City::pluck('id'))],
            'payment' => [Rule::in($payments)],
            'callback' => 'required|in:0,1',
            'name' => '',
            'phone' => '',
            'address' => ['array', ! $this->callback ? new CheckAddress() : ''],
            'address.city' => 'string',
            'address.street' => ['required', ! $this->callback ? Rule::in(app('city')->getStreets()) : ''],
            'address.home' => 'required',
            'address.floor' => 'string|nullable',
            'address.doorphone' => 'string|nullable',
            'address.apartment' => 'string|nullable',
            'persons' => 'numeric|nullable',
            'odd_money' => 'numeric|nullable',
            'promo' => [$this->promo ? new ValidPromo($this->order['sum'] ?? 0) : ''],
            'order.sum' => ['numeric', $this->promo ? '' : ('gte:' . config('shop.min_basket_sum'))],
            'order.fullSum' => ['numeric'],
            'order.items' => ['array', 'min:1'],
            'order.date' => ['date',
                new InWorkTime($this->pre_order),
                new IsNotHoliday(),
                new IsNotOverload($this->pre_order),
                new IsNotBusy(),
            ],
            'date' => 'nullable|date_format:Y-m-d H:i',
            'pre_order' => '',
            'comment' => '',
            'sauces' => 'nullable|numeric',
            'source' => '',
            'use_bonuses' => [new HaveEnoughBonuses()],
            'liqpay_result_url' => '',
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $fails = $validator->failed();
        if (isset($fails['order.date']['App\Rules\IsNotOverload'])) {
            return abort(response([
                'code' => 490,
                'message' => __('validation.is_overloaded'),
                'data' => [],
            ], 422));
        }
        if (isset($fails['order.date']['App\Rules\IsNotHoliday'])) {
            return abort(response([
                'code' => 491,
                'message' => __('validation.is_not_holiday'),
                'data' => [],
            ], 422));
        }
        if (isset($fails['order.date']['App\Rules\InWorkTime'])) {
            return abort(response([
                'code' => 492,
                'message' => __('validation.in_work_time'),
                'data' => [
                    'recommend_at' => app('city')->getRecommendTime()->format('Y-m-d H:i:s'),
                ],
            ], 422));
        }
        if (isset($fails['use_bonuses']['App\Rules\HaveEnoughBonuses'])) {
            return abort(response([
                'code' => 493,
                'message' => __('validation.have_not_enough_bonuses'),
                'data' => [],
            ], 422));
        }
        if (isset($fails['order.date']['App\Rules\IsNotBusy'])) {
            return abort(response([
                'code' => 494,
                'message' => __('validation.is_overloaded'),
                'data' => [
                    'recommend_at' => app('city')->getRecommendTime()->format('Y-m-d H:i:s'),
                ],
            ], 422));
        }
        throw new ValidationException($validator);
    }
}
