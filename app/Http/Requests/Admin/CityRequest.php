<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Request;

class CityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        Request::flashOnly('_tab');
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slug' => '',
            'name' => ['required', 'string'],
            'name_r' => ['required', 'string'],
            'uuid' => ['nullable', 'uuid'],
            'organization_uuid' => ['nullable', 'uuid'],
            'is_active' => '',
            'work_time' => 'array',
            'payments' => 'nullable|array',
            'configs' => 'nullable|array',
        ];
    }
}
