<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Request;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        Request::flashOnly('_tab');
        return true;
    }

    /**
     * Get data to be validated from the request.
     *
     * @return array
     */
    public function validationData()
    {
        if ($this->password || ! $this->route('user')) {
            $this->merge(['password' => bcrypt($this->password ?? '9nZw0Rq7K2MD')]);
        } else {
            $this->request->remove('password');
        }
        if ($this->is_verified && (! $this->route('user') || ! $this->route('user')->hasVerifiedEmail())) {
            $this->merge(['email_verified_at' => now()]);
        }
        return $this->all();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string'],
            'email' => ['nullable', 'string', 'email', Rule::unique('users')->ignore($this->route('user'))],
            'phone' => ['required', 'string', 'regex:/' . config('auth.phone_pattern') . '/', Rule::unique('users')->ignore($this->route('user'))],
            'password' => ['nullable', 'string'],
            'role' => '',
            'is_active' => '',
            'new_pin' => '',
            'sex' => '',
            'birthday' => ['nullable', 'date'],
            'uuid' => ['nullable', Rule::unique('users')->ignore($this->route('user'))],
            'city_id' => '',
        ];
    }
}
