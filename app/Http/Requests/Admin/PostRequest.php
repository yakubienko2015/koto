<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Request;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        Request::flashOnly('_tab');
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string'],
            'slug' => '',
            'content' => '',
            'description' => '',
            'created_at' => '',
            'image' => ['nullable', 'image'],
            'is_active' => '',
            'meta' => 'array',
            'priority' => ['required', 'numeric'],
        ];
    }
}
