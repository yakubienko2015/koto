<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Request;

class PromoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        Request::flashOnly('_tab');
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required',
            'qr_code' => '',
            'product_id' => '',
            'is_active' => '',
            'can_be_single' => '',
            'valid_from' => '',
            'valid_to' => '',
            'min_basket_sum' => '',
            'is_multiuse' => '',
            'discount' => '',
            'iiko_guest_category_uuid' => '',
        ];
    }
}
