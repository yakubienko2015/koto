<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Request;

class SlideRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        Request::flashOnly('_tab');
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required', 'string'],
            'text' => '',
            'button_text' => '',
            'button_link' => ['required', 'string'],
            'image' => ['nullable', 'image'],
            'is_active' => '',
            'priority' => ['required', 'numeric'],
        ];
    }
}
