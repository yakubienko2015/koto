<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Request;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        Request::flashOnly('_tab');
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'uuid' => '',
            'city_id' => '',
            'terminal_id' => '',
            'user_id' => '',
            'ordered_at' => '',
            'phone' => '',
            'customer_name' => '',
            'sum' => '',
            'data' => '',
            'status' => '',
        ];
    }
}
