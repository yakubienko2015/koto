<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Request;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        Request::flashOnly('_tab');
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string'],
            'uuid' => ['nullable', 'uuid'],
            'category_id' => '',
            'product_group_id' => '',
            'price' => ['required', 'numeric'],
            'old_price' => ['nullable', 'numeric'],
            'improved_price' => ['nullable', 'numeric'],
            'improved_old_price' => ['nullable', 'numeric'],
            'improved_product_id' => '',
            'amount' => ['nullable', 'numeric'],
            'measure_unit' => ['required', 'string'],
            'image' => ['nullable', 'image'],
            'mob_image' => ['nullable', 'image'],
            'mark' => '',
            'sharpness' => '',
            'ingredients' => '',
            'description' => '',
            'weight' => ['required', 'numeric'],
            'energy_amount' => ['nullable', 'numeric'],
            'fiber_amount' => ['nullable', 'numeric'],
            'carbohydrate_amount' => ['nullable', 'numeric'],
            'fat_amount' => ['nullable', 'numeric'],
            'is_active' => '',
            'is_top' => '',
            'is_rec' => '',
            'is_new' => '',
            'priority' => ['required', 'numeric'],
            'meta' => ['array'],
            'tags' => ['nullable', 'array'],
            'marks' => ['nullable', 'array'],
            'work_ingredients' => ['nullable', 'array'],
            'time_for_cook' => ['nullable', 'numeric'],
        ];
    }
}
