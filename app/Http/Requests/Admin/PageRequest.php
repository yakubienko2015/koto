<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Request;

class PageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        Request::flashOnly('_tab');
        return true;
    }

    /**
     * Get data to be validated from the request.
     *
     * @return array
     */
    public function validationData()
    {
        if (is_null($this->slug)) {
            $this->merge(['slug' => '']);
        }
        return $this->all();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string'],
            'slug' => ['string'],
            'content' => '',
            'places' => '',
            'is_active' => '',
            'image' => '',
            'priority' => 'numeric',
            'meta' => 'array',
        ];
    }
}
