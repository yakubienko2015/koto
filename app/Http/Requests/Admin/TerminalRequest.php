<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Request;

class TerminalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        Request::flashOnly('_tab');
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string'],
            'max_cook_load' => ['required', 'numeric'],
            'uuid' => ['nullable', 'uuid'],
            'address' => '',
            'phone' => '',
            'users' => '',
            'status' => '',
            'new_place_name' => 'required_if:new_place,1',
            'work_places.*.name' => ['string', 'required'],
        ];
    }
}
