<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Request;

class SettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        Request::flashOnly('_tab');
        return true;
    }

    /**
     * Get data to be validated from the request.
     *
     * @return array
     */
    public function validationData()
    {
        foreach ($this->all() as $key => $value) {
            if (! is_null($value)) {
                $this->merge([str_replace('$', '.', $key) => $value]);
            }
            $this->request->remove($key);
        }
        if ($this->has('shop.holidays')) {
            $this->merge(['shop.holidays' => array_filter($this->get('shop.holidays'))]);
        }
        return $this->all();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }
}
