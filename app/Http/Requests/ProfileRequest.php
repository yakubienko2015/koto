<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get data to be validated from the request.
     *
     * @return array
     */
    public function validationData()
    {
        // $this->merge([
        //     'phone' => clear_phone_number($this->phone),
        // ]);
        return $this->all();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string'],
            'email' => ['nullable', 'string', 'email', Rule::unique('users')->ignore($this->user())],
            // 'phone' => ['required', 'string', 'regex:/' . config('auth.phone_pattern') . '/', Rule::unique('users')->ignore($this->user())],
            'birthday' => ['nullable', 'date', 'before:-' . config('auth.min_age') . ' years'],
            'password' => ['nullable', 'string', 'min:8', 'confirmed'],
        ];
    }
}
