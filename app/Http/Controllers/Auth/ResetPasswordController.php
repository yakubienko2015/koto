<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use DB;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    public function reset(Request $request)
    {
        $request->merge(['phone' => clear_phone_number($request->phone)]);

        $request->validate([
            'code' => 'required',
            'phone' => ['required', 'string', 'regex:/' . config('auth.phone_pattern') . '/', 'exists:users,phone'],
            'password' => 'required|confirmed|min:8',
        ]);

        $user = User::where('phone', $request->phone)->firstOrFail();

        $results = DB::select('select * from password_resets where email = ? and token = ?', [
            $request->phone, $request->code,
        ]);

        if (count($results)) {
            $this->resetPassword($user, $request->password);
            DB::delete('delete from password_resets where email = ?', [
                $request->phone,
            ]);

            return redirect($this->redirectPath())
                ->with('status', trans('success'));
        } else {
            return redirect()->back()
                ->withInput($request->only(['code', 'phone']))
                ->withErrors(['code' => trans('auth.code.wrong_code')]);
        }
    }
}
