<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\SmsConfirmation;
use App\Models\User;
use DB;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    protected function validateSms(Request $request)
    {
        $request->merge(['phone' => clear_phone_number($request->phone)]);

        $this->validate($request, [
            'phone' => ['required', 'string', 'regex:/' . config('auth.phone_pattern') . '/', 'exists:users,phone']
        ], [
            'required' => trans('auth.code.wrong_phone'),
            'regex' => trans('auth.code.wrong_phone'),
        ]);
    }

    public function sendResetLinkEmail(Request $request)
    {
        $this->validateSms($request);

        $phone = clear_phone_number($request->phone);

        // $user = User::where('phone', $phone)->firstOrFail();

        $code = mt_rand(1000, 9999);
        DB::delete('delete from password_resets where email = ?', [$phone]);
        DB::insert('insert into password_resets (email, token) values (?, ?)', [$phone, $code]);

        if (app()->environment('production')) {
            $sms = new SmsConfirmation(['phone' => $phone, 'code' => $code]);
            try {
                $sms->sendSms();
            } catch (\Throwable $th) {
                logger('forgot password sms error', [$th->getMessage()]);
                throw ValidationException::withMessages(['phone' => __('SMS на этот номер закончились')]);
            }

            return redirect()->route('password.reset', [
                'token' => $phone,
            ]);
        }
        return redirect()->route('password.reset', [
            'token' => $phone,
            'code' => $code,
        ]);
    }
}
