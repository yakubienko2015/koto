<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Iiko\Biz\Exception\IikoResponseException;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $data['phone'] = clear_phone_number($data['phone']);

        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['nullable', 'string', 'email', 'max:255', 'unique:users'],
            'phone' => ['required', 'string', 'regex:/' . config('auth.phone_pattern') . '/', 'max:255', 'unique:users'],
            'birthday' => ['required', 'date', 'before:-' . config('auth.min_age') . ' years'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        $data['phone'] = clear_phone_number($data['phone']);

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'] ?? null,
            'phone' => $data['phone'],
            'birthday' => $data['birthday'],
            'password' => Hash::make($data['password']),
        ])->assignRole('customer');
        try {
            $organization = app('iiko')->OrganizationsApi()->getList()[0];
            $customerId = app('iiko')->CustomersApi()->createOrUpdate($organization['id'], $data);
        } catch (IikoResponseException $exc) {
            logger('Error on register user with iiko', [$exc->getMessage()]);
            $user->delete();
            throw ValidationException::withMessages(['phone' => __('validation.regex', [
                'attribute' => __('validation.attributes.phone'),
            ])]);
        }

        if (is_string($customerId)) {
            $user->update(['uuid' => $customerId]);
        }
        spy('Успешно зарегистрировано нового пользователя: <a href="' . route('admin.users.edit', [$user]) . '">#' . $user->name . '</a>', 'success');

        return $user;
    }
}
