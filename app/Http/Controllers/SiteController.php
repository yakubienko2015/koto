<?php

namespace App\Http\Controllers;

use App\Http\Requests\LiqpayRequest;
use App\Http\Requests\OpinionRequest;
use App\Library\TelegramBotHelper;
use App\Models\Category;
use App\Models\City;
use App\Models\MarketBonus;
use App\Models\Opinion;
use App\Models\Order;
use App\Models\Page;
use App\Models\Post;
use App\Models\Product;
use App\Models\Promo;
use App\Models\Slide;
use App\Models\Task;
use Iiko\Biz\Exception\IikoResponseException;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Spatie\Searchable\ModelSearchAspect;
use Spatie\Searchable\Search;
use Telegram;

class SiteController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($city = '')
    {
        $page = Page::whereSlug('')
            ->inCity()
            ->firstOrNew();

        $schemaBreadcrumbList = View::shared('schemaBreadcrumbList');
        $schemaBreadcrumbList['itemListElement'][] = [
            '@type' => 'ListItem',
            'position' => 2,
            'name' => $page->name,
        ];

        return view('site.pages.main', [
            'page' => $page,
            'metas' => $page->meta,
            'topProducts' => Cache::remember('topProducts', 86400, function () {
                return Product::top()->with('subproducts')->get();
            }),
            'topRamens' => Cache::remember('topRamens', 86400, function () {
                return Product::active()
                    ->activeCategory()
                    ->with('subproducts')
                    ->withCount('orderables')
                    ->orderBy('orderables_count', 'desc')
                    ->limit(3)
                    ->get();
            }),
            'topPosts' => Cache::remember('topPosts' . app('city')->id, 86400, function () {
                return Post::inCity()->active()->limit(3)->get();
            }),
            'slides' => Cache::remember('slides' . app('city')->id, 86400, function () {
                return Slide::active()->inCity()->get();
            }),
            'schemaBreadcrumbList' => $schemaBreadcrumbList,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function category(Request $request, $city, Category $category)
    {
        $products = $category->products()->with('prices', 'category', 'subproducts', 'meta')->active();

        if ($request->marks) {
            $products->whereHas('marks', function ($query) use ($request) {
                $query->whereIn('id', $request->marks ?? []);
            });
        }

        switch ($request->sort) {
            case '2':
                $products = $products->withCount('orderables')
                    ->orderBy('orderables_count', 'desc')
                    ->get();
                break;

            case '3':
                $products = $products->get()->sortBy('price');
                break;

            case '4':
                $products = $products->get()->sortByDesc('price');
                break;

            default:
                if ($request->marks) {
                    $products = $products->orderBy('priority')->get();
                } else {
                    $products = Cache::remember("product_{$city}_{$category->id}", 86400, function () use ($products) {
                        return $products->orderBy('priority')->get();
                    });
                }
                break;
        }

        $schemaBreadcrumbList = View::shared('schemaBreadcrumbList');
        $schemaBreadcrumbList['itemListElement'][] = [
            '@type' => 'ListItem',
            'position' => 2,
            'name' => $category->name,
        ];

        return view('site.pages.category', [
            'category' => $category,
            'products' => $products->where('meta.is_active', 1),
            'metas' => $category->meta,
            'schemaBreadcrumbList' => $schemaBreadcrumbList,
        ]);
    }

    /**
     * Show the page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function page($city, $page)
    {
        $page = Page::whereSlug($page)
            ->inCity()
            ->firstOrNew();

        if (view()->exists("site.pages.{$page->slug}")) {
            $view = "site.pages.{$page->slug}";
        } else {
            $view = 'site.pages.page';
        }

        $schemaBreadcrumbList = View::shared('schemaBreadcrumbList');
        $schemaBreadcrumbList['itemListElement'][] = [
            '@type' => 'ListItem',
            'position' => 2,
            'name' => $page->name,
        ];

        return view($view, [
            'page' => $page,
            'metas' => $page->meta,
            'schemaBreadcrumbList' => $schemaBreadcrumbList,
        ]);
    }

    /**
     * Show the posts.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function posts($city)
    {
        $page = Page::whereSlug('news')->firstOrNew();

        $schemaBreadcrumbList = View::shared('schemaBreadcrumbList');
        $schemaBreadcrumbList['itemListElement'][] = [
            '@type' => 'ListItem',
            'position' => 2,
            'name' => $page->name,
        ];

        return view('site.pages.posts', [
            'posts' => Post::joinMeta()
                ->where('metas.is_active', 1)
                ->select('posts.*')
                ->orderBy('created_at', 'desc')
                ->orderBy('priority')
                ->paginate(5),
            'metas' => $page->meta,
            'page' => $page,
            'schemaBreadcrumbList' => $schemaBreadcrumbList,
        ]);
    }

    /**
     * Show the post.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function post($city, Post $post)
    {
        $page = Page::whereSlug('news')->firstOrNew();

        $schemaBreadcrumbList = View::shared('schemaBreadcrumbList');
        $schemaBreadcrumbList['itemListElement'][] = [
            '@type' => 'ListItem',
            'position' => 2,
            'name' => $page->name,
            'item' => route('posts', [app('city')])
        ];
        $schemaBreadcrumbList['itemListElement'][] = [
            '@type' => 'ListItem',
            'position' => 3,
            'name' => $post->name,
        ];

        return view('site.pages.post', [
            'post' => $post,
            'metas' => $post->meta,
            'schemaBreadcrumbList' => $schemaBreadcrumbList,
        ]);
    }

    /**
     * Show the page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function product($city, Category $category, Product $product)
    {
        if (! $product->is_active) {
            return redirect()->route('category', [app('city'), $category]);
        }
        $schemaBreadcrumbList = View::shared('schemaBreadcrumbList');
        $schemaBreadcrumbList['itemListElement'][] = [
            '@type' => 'ListItem',
            'position' => 2,
            'name' => $category->name,
            'item' => route('category', [app('city'), $category])
        ];
        $schemaBreadcrumbList['itemListElement'][] = [
            '@type' => 'ListItem',
            'position' => 3,
            'name' => $product->name,
        ];
        $schemaProduct = [
            '@context' => 'http://schema.org',
            '@type' => 'Product',
            'name' => $product->name,
            'image' => [asset($product->image),assert($product->mob_image)],
            'description' => $product->meta->description ?? '',
            'sku' => $product->id,
            'mpn' => $product->id,
            'brand' => [
                '@type' => 'Brand',
                'name' => 'Kotosushi',
            ],
            'offers' => [
                '@type' => 'Offer',
                'url' => route('index'),
                'priceCurrency' => 'UAH',
                'price' => $product->price,
                'availability' => 'https://schema.org/InStock',
                'priceValidUntil' => now()->addMonth()->format('Y-m-d'),
            ],
        ];

        return view('site.pages.product', [
            'category' => $category,
            'product' => Product::with('improvedProduct')->find($product->id),
            'metas' => $product->meta ?? $category->meta,
            'schemaBreadcrumbList' => $schemaBreadcrumbList,
            'schemaProduct' => $schemaProduct,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function search($city, Request $request)
    {
        $page = Page::whereSlug('search')
            ->inCity()
            ->firstOrNew();

        if ($request->q) {
            $items = (new Search())
                ->registerModel(Product::class, function(ModelSearchAspect $modelSearchAspect) {
                    $modelSearchAspect
                       ->addSearchableAttribute('name')
                       ->active();
            })->search($request->q);
            $itemsIngredients = (new Search())
                ->registerModel(Product::class, function(ModelSearchAspect $modelSearchAspect) {
                    $modelSearchAspect
                       ->addSearchableAttribute('ingredients')
                       ->active();
            })->search($request->q);
            $products = collect([]);
            foreach ($items->merge($itemsIngredients) as $item) {
                $products[$item->searchable->id] = $item->searchable;
            }
            $products = $products->whereNotNull('category_id');
            $productsInCategory = $products
                ->whereIn('category_id', $request->c ?? 0);
            $productsInElseCategory = $products
                ->whereIn('category_id', Category::active()
                    ->where('id', '<>', $request->c ?? 0)
                    ->pluck('id')
                );
            $items = $productsInCategory->union($productsInElseCategory);
        } else {
            $items = [];
        }
        return view('site.pages.search', [
            'items' => $items,
            'page' => $page,
            'metas' => $page->meta,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function basket($city)
    {
        if (count(app('order')->products) === 0) {
            return redirect(app('city')->getIndexUrl());
        }
        $page = Page::whereSlug('basket')
            ->inCity()
            ->firstOrNew();

        header('Cache-Control: no-cache, no-store, must-revalidate');
        header('Expires: Thu, 1 Jan 1970 00:00:00 GMT');
        header('Pragma: no-cache');
        return view('site.pages.basket', [
            'page' => $page,
            'metas' => $page->meta,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function success($city = '')
    {
        $order = Order::whereId(session('last_order'))->first();
        if ($order) {
            return view('site.pages.success', [
                'order' => $order,
            ]);
        }
        return redirect(app('city')->getIndexUrl());
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function track($city, Order $order)
    {
        if ((auth()->check() && auth()->user()->orders()->exists($order->id))
            || session('last_order') == $order->id) {
            return view('site.pages.track', [
                'page' => Page::whereSlug('track')->firstOrNew(),
                'order' => $order,
                'topPosts' => app('city')
                    ->posts()
                    ->limit(3)
                    ->get(),
            ]);
        }
        return redirect(app('city')->getIndexUrl());
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function play(Request $request, $city, $order)
    {
        // dd($request->ip(), $request->ips(), $request->fingerprint());
        try {
            $orderId = Crypt::decryptString($order);
        } catch (DecryptException $e) {
            $orderId = 0;
        }
        $order = Order::findOrFail($orderId);
        if (!isset($order->data['game_score'])) {
            return view('site.pages.game', [
                'order' => $order,
            ]);
        }
        abort(404);
    }

    /**
     * @param App\Models\Opinion $opinion
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function opinion(Opinion $opinion)
    {
        if ($opinion->filled_at) {abort(404);}

        return view('site.pages.opinion', [
            'opinion' => $opinion,
        ]);
    }

    /**
     * @param App\Models\Opinion $opinion
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function qrOpinion()
    {
        return view('site.pages.qrOpinion');
    }

    /**
     * @param OpinionRequest $request
     * @param App\Models\Opinion $opinion
     *
     * @return Redirect
     */
    public function opinionSave(OpinionRequest $request, Opinion $opinion)
    {
        if ($opinion->filled_at) {abort(404);}

        $opinion->update([
            'comment' => $request->comment,
            'filled_at' => now(),
            'marks' => $request->marks,
        ]);
        spy('Оставлено отзыв: <a href="' . route('admin.opinions.index') . '">#' . $opinion->id . '</a>', 'success');

        try {
            $opinion->sendToIiko();
        } catch (IikoResponseException $exc) {
            logger('Error on sending oninion to iiko', [$exc->getMessage()]);
        }

        return redirect(app('city')->getIndexUrl());
    }

    /**
     * @param Request $request
     * @param string $city
     *
     * @return void
     */
    public function paymentResponse(Request $request, $city, Order $order)
    {
        $request->session()->flash('success', 'Успешно создано');
        $order = $order ?: Order::findOrFail($request->session()->get('last_order', 0));
        if ($order->data['liqpayResultUrl'] ?? false) {
            return redirect()->away($order->data['liqpayResultUrl']);
        }
        if ($order->payment_type === Order::ORDER_PAYMENT_LIQPAY) {
            if (isset($order->data['liqpay']['status'])
                    && $order->data['liqpay']['status'] == 'success') {
                return redirect()->route('track', [app('city')->slug, $request->session()->get('last_order', 0)]);
            }
            return redirect(app('city')->getIndexUrl());
        }
        return redirect()->route('track', [app('city')->slug, $request->session()->get('last_order', 0)]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Support\Responce
     */
    public function liqpayStatus(LiqpayRequest $request)
    {
        $data = json_decode(base64_decode($request->data));
        $order = Order::findOrfail(substr($data->order_id, 5));

        if (($order->data['source'] ?? 'site') === 'mobile') {
            $privateKey = env('LIQPAY_PRIVATE_KEY_M_' . $order->city_id,
                config('services.liqpay.private_key'));
        } else {
            $privateKey = env('LIQPAY_PRIVATE_KEY_' . $order->city_id,
                config('services.liqpay.private_key'));
        }

        $sign = base64_encode(sha1(
            $privateKey .
            $request->data .
            $privateKey,
            1));
        if (config('services.liqpay.allow_without_signature', false) || $sign === $request->signature) {
            $orderData = $order->data;
            $orderData['liqpay'] = $data;
            $order->update(['data' => $orderData]);
            if (config('shop.make_order_with_iiko') && ! $order->uuid && $data->status == 'success') {
                $orderRequest = Order::getOrderRequest($order->data);
                $orderRequest = Order::fixIikoModifiers($orderRequest);
                try {
                    $iikoOrder = app('iiko')->OrdersApi()->addOrder($orderRequest);
                    $order->update([
                        'uuid' => $iikoOrder['orderId'],
                        'sum' => $iikoOrder['sum'],
                    ]);
                    $promo = Promo::whereCode($order->data['promo'] ?? '')
                        ->whereIsActive(true)
                        ->first();
                    if ($promo) {
                        $promo->setCategoryToOrderCustomer($iikoOrder);
                    }
                } catch (IikoResponseException $e) {
                    spy('Ошибка iiko заказа после liqpay: ' . $e->getMessage(), 'warning');
                }
            }
        } else {
            return response([
                'message' => 'Bad signature',
            ], 401);
        }
    }

    /**
     *
     */
    public function telegramWebhook()
    {
        logger('telegram webhook');
        $update = Telegram::commandsHandler(true);

        logger('telegram webhook 2');
        TelegramBotHelper::analizUpdateForQuery($update);
        logger('telegram webhook 3');
        TelegramBotHelper::analizMessage($update->getMessage());
        logger('telegram webhook 4');

        return 'ok';
    }

    /**
     * Undocumented function
     *
     * @param Promo $promo
     *
     * @return void
     */
    public function qrCode(Request $request, Promo $promo)
    {
        if (! $promo->is_active) {
            abort(404);
        }
        return redirect()->route('basket', app('city'))->cookie('promo', $promo->code);
    }

    /**
     * @param Request $request
     * @param string $type
     *
     * @return void
     */
    public function qrStore($type)
    {
        return view('site.pages.marketBonus', [
            'marketType' => $type,
        ]);
    }

    /**
     * @param Request $request
     * @param string $type
     *
     * @return void
     */
    public function qrStoreSave(Request $request, $type)
    {
        $phone = clear_phone_number($request->phone);
        $marketBonus = MarketBonus::firstOrCreate([
            'phone' => $phone,
        ], [
            'type' => $type,
        ]);

        if (is_null($marketBonus->activated_at)) {
            $marketBonus->activateByPhone($phone);
        }
        $link = $type === 'ios'
            ? config('site.store_links.app_store', route('index'))
            : config('site.store_links.google_play', route('index'));

        return redirect($link);
    }

    /**
     * Show the sitemap.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function sitemap($city)
    {
        $page = Page::whereSlug('sitemap')->firstOrNew();

        $schemaBreadcrumbList = View::shared('schemaBreadcrumbList');
        $schemaBreadcrumbList['itemListElement'][] = [
            '@type' => 'ListItem',
            'position' => 2,
            'name' => $page->name,
        ];

        return view('site.pages.sitemap', [
            'posts' => Post::active()->inCity()->get(),
            'pages' => Page::menu()->get(),
            'categories' => Category::menu()->with(['products' => function ($query) {
                $query->active()->orderBy('priority');
            }])->get(),
            'metas' => $page->meta,
            'page' => $page,
            'schemaBreadcrumbList' => $schemaBreadcrumbList,
        ]);
    }

    public function test()
    {
        return 'lol';
        return Task::whereNotNull('cron')
            ->where('cron', '<>', '* * * * *')
            ->get();
    }
}
