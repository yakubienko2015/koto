<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderRequest;
use App\Models\Order;
use Iiko\Biz\Exception\IikoResponseException;
use Illuminate\Http\Request;

class BasketController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function add(Request $request)
    {
        $items = [];
        foreach (app('order')->products as $product) {
            $items["{$product->id}-{$product->pivot->modifiers}"] = $product->pivot->amount;
        }
        foreach ($request->products ?? [] as $product) {
            $items["{$product['id']}-{$product['type']}"] = $product['amount'];
        }
        return back()->cookie('order_items', json_encode($items));
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function remove(Request $request)
    {
        $items = [];
        foreach (app('order')->products as $product) {
            $items["{$product->id}-{$product->pivot->modifiers}"] = $product->pivot->amount;
        }
        foreach ($request->products ?? [] as $product) {
            unset($items["{$product['id']}-{$product['type']}"]);
        }
        return back()->cookie('order_items', json_encode($items));
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function basket(Request $request)
    {
        $order = app('order');
        if ($request->sauces) {
            $order->add_sauce_cost = $request->sauces * $order->sauce_cost;
            $order->sum += $order->add_sauce_cost;
        }

        return response()->json($order);
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function order(OrderRequest $request)
    {
        try {
            $order = Order::makeOrder($request->validated());
        } catch (IikoResponseException $e) {
            spy('Ошибка iiko заказа: ' . $e->getMessage(), 'warning');
            return response()->json(['message' => $e->getMessage()], 401);
        }
        spy('Успешно создано заказ: <a href="' . route('admin.orders.show', $order) . '">#' . $order->id . '</a>', 'success');

        $request->session()->put('last_order', $order->id);
        if ($request->payment === Order::ORDER_PAYMENT_LIQPAY) {
            return response()->json([
                'liqpay_form' => $order->getLiqpayForm(),
                'order_sum' => $order->sum,
            ])->cookie(config('shop.basket_cookie_name', json_encode([])));
        }
        return response()->json([
            'order_sum' => $order->sum,
            'redirect' => route('track', [app('city'), $order]),
        ])->cookie(config('shop.basket_cookie_name', json_encode([])));
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function repeat(Order $order)
    {
        if (auth()->user()->orders()->exists($order->id)) {
            $items = [];
            foreach ($order->products()->withPivot(['modifiers', 'amount'])->get() as $product) {
                $items["{$product->id}-{$product->pivot->modifiers}"] = $product->pivot->amount;
            }
            return redirect('basket')->withInput($order->data)
                ->cookie('order_items', json_encode($items));
        }
        abort(404);
    }
}
