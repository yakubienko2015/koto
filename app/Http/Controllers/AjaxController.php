<?php

namespace App\Http\Controllers;

use App\Http\Requests\CallbackRequest;
use App\Models\Callback;
use App\Models\Order;
use App\Models\Product;
use App\Models\Promo;
use App\Models\User;
use Iiko\Biz\Exception\IikoResponseException;
use Illuminate\Http\Request;
use Throwable;

class AjaxController extends Controller
{
    /**
     * Get cached streets from iiko
     *
     * @return [json]
     */
    public function streets()
    {
        try {
            $streets = app('city')->getStreets();
            return response(json_encode($streets, JSON_UNESCAPED_UNICODE));
        } catch (IikoResponseException $e) {
            return response()->json([]);
        } catch (Throwable $e) {
            return response()->json([]);
        }
    }

    /**
     *
     */
    public function callback(CallbackRequest $request)
    {
        Callback::create($request->validated());

        return back();
    }

    /**
     *
     */
    public function orderTrack(Request $request, $order)
    {
        $order = Order::select('id', 'status', 'ordered_at', 'created_at')
            ->whereId($order)
            ->firstOrFail();
        return response()->json($order->only(['id', 'status', 'time_left']));
    }

    /**
     *
     */
    public function findBowl(Request $request)
    {
        $request->validate([
            'steps' => 'required|array|size:3',
        ]);
        return response()->json(Product::findBowlByIngredients($request->steps));
    }

    /**
     *
     */
    public function validPromo(Request $request)
    {
        $promo = Promo::whereCode($request->promo)
                ->whereIsActive(true)
                ->first();

        $data = $promo ? [
            'product' => null,
            'code' => null,
            'status' => 4,
            'message' => trans('shop.basket.promo_message_4')
        ] : [
            'product' => null,
            'code' => null,
            'status' => 1,
            'message' => trans('shop.basket.promo_message_1')
        ];

        return response($data)->cookie('promo', $request->promo);
    }

    /**
     *
     */
    public function howManyKotcoins(Request $request)
    {
        $request->merge(['phone' => clear_phone_number($request->phone)]);

        $request->validate([
            'phone' => ['required', 'string', 'regex:/' . config('auth.phone_pattern') . '/'],
        ]);
        $coins = User::getIikoBonusesByPhone($request->phone);
        return response()->json(['message' => __('shop.kotcoins.amount', [$coins])]);
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function gameScore(Request $request)
    {
        $order = Order::findOrFail($request->id);

        if (!isset($order->data['game_score'])) {
            try {
                $order->setGameScore($request->score ?? 0);
            } catch (IikoResponseException $e) {
                logger('iiko error on set game score ' . $e->getMessage());
            }
            return response()->json(['success' => true]);
        }
        abort(404);
    }
}
