<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\CallbackOrder;
use App\Models\ESputnik;
use Illuminate\Support\Facades\View;

class CallbackController extends Controller
{
    public $user;

    /**
     * @return void
     */
    public function __construct()
    {
        $this->user = Auth::guard('api')->user();
        if (! $this->user) { return abort(401); }
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     *
     * @return void
     */
    public function send(Request $request)
    {
        $order = CallbackOrder::create(['customer_id' => $this->user->id]);

        return custom_response(200, trans('api.callback_sended'), [
            'order' => $order->only(['id', 'created_at']),
        ]);
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * 
     * @return void
     */
    public function sendMessage(Request $request)
    {
        $request->validate([
            'message' => 'required',
        ]);

        $data = [
            'name' => isset($this->user->email) ? $this->user->email : '',
            'input_message' => $request->message,
        ];

        // Mail::send('email.message', $data, function ($mail) {
        //     $mail->from(config('mail.from.address'), config('mail.from.name'));
        //     $mail->to(env('MAIL_ORDERS_ADDRESS'));
        //     $mail->subject('Новое сообщения!');
        // });

        $mail_view = View::make('email.message', $data);

        $variables = new \stdClass();
        $variables->from = '"Rock&Roll" <mydesign22@yandex.ru>';
        $variables->subject = 'Новое сообщения!';
        $variables->htmlText = $mail_view->render();
        $variables->emails = array(env('MAIL_ORDERS_ADDRESS'));

        $eSputnik = new ESputnik($variables);
        $response_sent_email = $eSputnik->sentEmail();

        return custom_response(200, trans('api.message_sended'));
    }
}
