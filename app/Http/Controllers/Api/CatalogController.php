<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Category;

class CatalogController extends Controller
{
    public function index(Request $request)
    {
        return response()->api(200, null, [
            'categories' => Category::whereIn('id', app('city')->getCategories()->pluck('id'))
                ->with(['products' => function ($query) {
                    return $query->orderBy('priority');
                }, 'products.tags'])->get(),
            'top_products' => Product::top()->get(),
            'top_ramens' => Product::active()
                ->activeCategory()
                ->withCount('orderables')
                ->orderBy('orderables_count', 'desc')
                ->limit(3)
                ->get(),
        ]);
    }

    public function category(Category $category)
    {
        return response()->api(200, null, [
            'products' => $category->products,
            'marks' => Product::getMarksForSelect(),
            'category' => $category,
        ]);
    }

    public function product($productId)
    {
        return response()->api(200, null, [
            'product' => Product::with('improvedProduct')->find($productId)->append('recommends'),
        ]);
    }
}
