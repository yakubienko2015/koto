<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Models\MarketBonus;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Models\User;
use Illuminate\Http\Request;
use Str;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */
    use RegistersUsers;

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'city_id' => ['required', 'integer'],
            'email' => ['nullable', 'string', 'email', 'max:255', 'unique:users'],
            'phone' => ['required', 'string', 'regex:/' . config('auth.phone_pattern') . '/', 'max:255', 'unique:users'],
            'birthday' => ['nullable', 'date', 'before:-' . config('auth.min_age') . ' years'],
            'password' => ['nullable', 'string', 'min:8'],
            'device_token' => 'nullable',
            'device_type' => 'nullable',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        $organization = app('iiko')->OrganizationsApi()->getList()[0];
        $customerId = app('iiko')->CustomersApi()->createOrUpdate($organization['id'], $data);

        return User::create([
            'name' => $data['name'],
            'uuid' => $customerId ?? null,
            'email' => $data['email'] ?? null,
            'phone' => clear_phone_number($data['phone']),
            'city_id' => $data['city_id'] ?? 1,
            'birthday' => $data['birthday'],
            'password' => Hash::make($data['password'] ?? null),
            'device_token' => $data['device_token'] ?? null,
            'ref_code' => Str::random(),
        ])->assignRole('customer');

        // SmsConfirmation::where('phone', $user->phone)->delete();

        // $accessToken = $user->createToken($data['device_type'] ?? 'api');

        // return $user->withAccessToken($accessToken);
        // return $user;
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        MarketBonus::activateByUser($user);
        return (new SmsController())->get($request);
        return response()->api(200, __('auth.registered_complate'), [
            'success' => true,
            'code' => '',
            // 'access_token' => $user->currentAccessToken()->plainTextToken,
            // 'user' => User::whereId($user->id)->select('id', 'email', 'phone', 'city_id', 'device_token', 'referrer_id', 'ref_code')->first()->setAppends([]),
        ]);
    }
}
