<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use App\Models\SmsConfirmation;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function validateSms(Request $request)
    {
        $this->validate($request, [
            'phone' => ['required', 'string', 'regex:/' . config('auth.phone_pattern') . '/']
        ], [
            'required' => trans('auth.code.wrong_phone'),
            'regex' => trans('auth.code.wrong_phone'),
        ]);
    }

    public function sendCodeForResetCustomerPassword(Request $request)
    {
        $this->validateSms($request);

        $phone = clear_phone_number($request->phone);

        $user = User::where('phone', $phone)->first();

        if (! $user) {
            return response()->api(422, 'Пользователя не найдено');
        }

        $code = mt_rand(1000, 9999);
        DB::delete('delete from password_resets where email = ?', [$phone]);
        DB::insert('insert into password_resets (email, token) values (?, ?)', [$phone, $code]);

        if (app()->environment('production')) {
            $sms = new SmsConfirmation(['phone' => $phone, 'code' => $code]);
            try {
                $sms->sendSms();
            } catch (\Throwable $th) {
                logger('api send sms for password reset', [$th->getMessage()]);
                return response()->api(400, trans('SMS на этот номер закончились'), [
                    'phone' => $request->phone,
                ]);
            }

            return response()->api(200, trans('auth.code.sended'), [
                'phone' => $request->phone,
            ]);
        }

        return response()->api(200, trans('auth.code.sended'), [
            'phone' => $request->phone,
            'code' => $code,
        ]);
    }

    public function resetCustomerPassword(Request $request)
    {
        $request->validate([
            'password' => 'required',
            'phone' => 'required',
            'code' => 'required',
        ]);

        $phone = clear_phone_number($request->phone);
        $customer = User::where('phone', $phone)->first();

        if (! $customer) {
            return response()->api(422, 'Пользователя не найдено');
        }

        $results = DB::select('select * from password_resets where email = ? and token = ?', [$phone, $request->code]);

        if (count($results)) {
            $this->resetPassword($customer, $request->password);
            DB::delete('delete from password_resets where email = ?', [$phone]);

            return response()->api(200, null, [
                'user' => $customer,
            ]);
        } else {
            return response()->api(400, trans('auth.code.wrong_code'), [
                'phone' => $phone,
            ]);
        }
    }
}
