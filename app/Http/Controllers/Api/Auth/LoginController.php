<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
     */
    use AuthenticatesUsers;

    protected $user = null;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            'phone' => ['required', 'string', 'regex:/' . config('auth.phone_pattern') . '/'],
            'device_type' => 'nullable',
        ]);
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        $phone = clear_phone_number($request->phone);
        $user = User::where('phone', $phone)->first();

        if (!$user) {
            return false;
        }
        // $accessToken = $user->createToken($request->device_type ?? 'api');

        // $this->user = $user->withAccessToken($accessToken);
        return true;

        // if (! $user || ! Hash::check($request->password, $user->password)) {
        //     throw ValidationException::withMessages([
        //         'phone' => [trans('auth.failed')],
        //     ]);
        // }
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        $this->clearLoginAttempts($request);

        return (new SmsController())->get($request);
        return response()->api(200, trans('auth.logined'), [
            'success' => true,
            'code' => '',
            // 'access_token' => $this->user->currentAccessToken()->plainTextToken,
            // 'user' => User::whereId($this->user->id)->select('id', 'email', 'phone', 'city_id', 'device_token', 'referrer_id', 'ref_code')->first()->setAppends([]),
        ]);
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $request->user()->tokens()->delete();

        return response()->api(200, trans('auth.logouted'));
    }
}
