<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SmsConfirmation;
use App\Models\User;
use App\Rules\SmsConfirm;
use Illuminate\Validation\ValidationException;

class SmsController extends Controller
{
    /**
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function validateSms(Request $request)
    {
        $this->validate($request, [
            'phone' => ['required', 'string', 'regex:/' . config('auth.phone_pattern') . '/']
        ], [
            'required' => trans('auth.code.wrong_phone'),
            'regex' => trans('auth.code.wrong_phone'),
        ]);
    }

    /**
     * method
     *
     * @param  Illuminate\Http\Request $request
     * @return json
     */
    public function get(Request $request)
    {
        $this->validateSms($request);

        $phone = clear_phone_number($request->phone);

        if ($phone === '+380970000000') {
            return response()->api(200, trans('auth.code.sended'), [
                'phone' => $request->phone,
            ]);
        }

        $existCode = SmsConfirmation::where('phone', $phone)->first();

        $send = false;
        $code = mt_rand(1000, 9999);
        if (! $existCode) {
            $sms = SmsConfirmation::create([
                'phone' => $phone,
                'code' => $code,
            ]);
            $send = true;

        } elseif ($existCode->confirmed()) {
            return response()->api(400, trans('auth.code.confirmed'), [
                'phone' => $request->phone,
            ]);

        } else {
            $existCode->update([
                'code' => $code,
            ]);
            $sms = $existCode;
            $send = true;
        }

        if ($send) {
            if (app()->environment('production')) {
                try {
                    $sms->sendSms();
                } catch (\Throwable $th) {
                    logger('api send sms', [$th->getMessage()]);
                    spy('Ошибка отправки sms: ' . $th->getMessage(), 'danger');
                    return response()->api(400, trans('SMS на этот номер закончились'), [
                        'phone' => $request->phone,
                    ]);
                }

                return response()->api(200, trans('auth.code.sended'), [
                    'phone' => $request->phone,
                ]);
            }

            return response()->api(200, trans('auth.code.sended'), [
                'phone' => $request->phone,
                'code' => $code,
            ]);
        } else {
            return response()->api(400, trans('auth.code.fail'), [
                'phone' => $request->phone,
            ]);
        }
    }

    /**
     * method
     *
     * @param  Illuminate\Http\Request $request
     * @return json
     */
    protected function check(Request $request)
    {
        $request->validate([
            'code' => 'required',
            'phone' => [
                'required',
                'string',
                'regex:/' . config('auth.phone_pattern') . '/',
                new SmsConfirm(),
            ],
            'device_type' => 'nullable',
        ]);

        $phone = clear_phone_number($request->phone);
        $user = User::where('phone', $phone)->first();

        if (! $user) {
            return response()->api(400, trans('auth.code.no_phone'), [
                'phone' => $phone,
            ]);
        }
        $user->tokens()->delete();
        $accessToken = $user->createToken($request->device_type ?? 'api');

        $iikoUserInfo = $user->getIikoCustomerByPhone();
        if ($iikoUserInfo && isset($iikoUserInfo['id'])) {
            try {
                $user->update(['uuid' => $iikoUserInfo['id'] ?? null]);
            } catch (\Throwable $th) {
                logger('error on update user uuid');
            }
        }

        $this->user = $user->withAccessToken($accessToken);

        logger()->channel('smsStatuses')->info('sms api checked', [$this->user->phone, $request->code]);
        return response()->api(200, trans('auth.logined'), [
            'access_token' => $this->user->currentAccessToken()->plainTextToken,
            'user' => User::whereId($this->user->id)->select('id', 'email', 'phone', 'city_id', 'device_token', 'referrer_id', 'ref_code')->first()->setAppends([]),
        ]);
        // $sms = SmsConfirmation::where('phone', $phone)->first();
        // if (! $sms) {
        //     return response()->api(400, trans('auth.code.no_phone'), [
        //         'phone' => $phone,
        //     ]);
        // }

        // $status = $sms->confirm($request->code);
        // if ($status) {
        //     return response()->api(200, trans('auth.code.success'), [
        //         'phone' => $phone,
        //     ]);
        // } else {
        //     return response()->api(400, trans('auth.code.wrong_code'), [
        //         'phone' => $phone,
        //         'code' => $request->code,
        //     ]);
        // }
    }
}
