<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\OpinionRequest;
use App\Models\Order;
use Iiko\Biz\Exception\IikoResponseException;

class ServiceRatingController extends Controller
{
    /**
     * @param Request $request
     * @param App\Models\Order $Order
     *
     * @return Redirect
     */
    public function add(OpinionRequest $request, Order $order)
    {
        if ($order->opinion) {
            $opinion = $order->opinion;
            if ($opinion->filled_at) { abort(404); }
            $opinion->update([
                'comment' => 'Моб.:' . $request->comment,
                'filled_at' => now(),
                'marks' => $request->marks,
            ]);
        } else {
            $opinion = $order->opinion()->create([
                'order_id' => $order->id,
                'slug' => substr(md5(uniqid(mt_rand(), true)), 0, 8),
                'comment' => 'Моб.:' . $request->comment,
                'filled_at' => now(),
                'marks' => $request->marks,
            ]);
        }

        spy('Оставлено отзыв: <a href="' . route('admin.opinions.index') . '">#' . $opinion->id . '</a>', 'success');

        try {
            $opinion->sendToIiko();
        } catch (IikoResponseException $exc) {
            logger('Error on sending oninion to iiko', [$exc->getMessage()]);
        }

        return response()->api(200, null, [
            'opinion' => $opinion,
        ]);
    }
}
