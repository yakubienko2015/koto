<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CardController extends Controller
{
    public $user;

    /**
     * @return void
     */
    public function __construct()
    {
        $this->user = Auth::guard('api')->user();
        if (! $this->user) { return abort(401); }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cards = $this->user->cards()->get([
            'id', 'customer_id', 'number', 'valid_month', 'valid_year', 'created_at', 'updated_at',
        ]);

        return custom_response(200, $cards->count() ? null : trans('api.empty'), [
            'cards' => $cards,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'number' => 'required|size:16',
            'valid_month' => 'nullable|numeric|max:12',
            'valid_year' => 'nullable|numeric|min:2000',
        ]);

        $card = $this->user->cards()->create($request->only([
            'number', 'valid_month', 'valid_year',
        ]));

        return custom_response(200, null, [
            'card' => $card->only(['id', 'number', 'valid_month', 'valid_year']),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $card
     * @return \Illuminate\Http\Response
     */
    public function show($card)
    {
        $user_card = $this->user->cards()->select([
            'id', 'customer_id', 'number', 'valid_month', 'valid_year', 'created_at', 'updated_at',
        ])->find($card);

        return custom_response($user_card ? 200 : 404, null, [
            'card' => $user_card,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $card
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $card)
    {
        $this->validate($request, [
            'number' => 'required|size:16',
            'valid_month' => 'nullable|numeric|min:0|max:12',
            'valid_year' => 'nullable|numeric|min:2000|max:3000',
        ]);

        $card = $this->user->cards()->select([
            'id', 'customer_id', 'number', 'valid_month', 'valid_year', 'created_at', 'updated_at',
        ])->find($card);

        if ($card) {
            $card->update($request->only([
                'number', 'valid_month', 'valid_year',
            ]));
        }

        return custom_response($card ? 200 : 404, null, [
            'card' => $card,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $card
     * @return \Illuminate\Http\Response
     */
    public function destroy($card)
    {
        $card = $this->user->cards()->find($card);

        if ($card) {
            $card->delete();

            return custom_response(200);
        } else {
            abort(404);
        }
    }
}
