<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\OrderRequest;
use App\Models\Order;
use App\Models\Post;
use App\Models\Promo;
use DOMDocument;
use Iiko\Biz\Exception\IikoResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Crypt;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class OrderController extends Controller
{
    /**
     * Undocumented function
     *
     * @param Request $request
     *
     * @return void
     */
    public function transactions(Request $request)
    {
        return response()->api(200, null, [
            'orders' => $request->user()->orders()
                ->orderBy('ordered_at', 'desc')
                ->with(['products' => function ($query) {
                    return $query->activeCategory();
                }])->get(),
        ]);
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @param Order $order
     *
     * @return void
     */
    public function transaction(Request $request, $order)
    {
        if ($request->user()->orders()->exists($order)) {
            return response()->api(200, null, [
                'order' => Order::whereId($order)
                    ->with(['products' => function ($query) {
                        return $query->activeCategory();
                    }])->first(),
            ]);
        }
        abort(404);
    }

    /**
     * @param Request $request
     *
     * @return void
     */
    public function track(Request $request)
    {
        $order = $request->user()->orders()->whereNotIn('status', [
            Order::ORDER_STATUS_CANCELLED,
            Order::ORDER_STATUS_CLOSED,
            Order::ORDER_STATUS_DELIVERED,
            Order::ORDER_STATUS_UNCONFIRMED,
        ])->whereNotNull('uuid')
            ->orderBy('ordered_at')->first();
        if ($order) {
            $orderData = $order->data;
            $order->data = null;
            return response()->api(200, null, [
                'order' => $order ? $order->setAppends([]) : null,
                'game_link' => isset($orderData['game_score'])
                    ? null
                    : LaravelLocalization::getLocalizedURL(app()->getLocale(), route('play', [
                        $order->city ?? app('city'),
                        Crypt::encryptString($order->id ?? 0),
                    ])),
                'posts' => Cache::remember('topPosts', 86400, function () {
                    return Post::inCity()->active()->limit(3)->get();
                }),
            ]);
        }
        return response()->api(200, null, [
            'order' => null,
            'game_link' => null,
            'posts' => Cache::remember('topPosts', 86400, function () {
                return Post::inCity()->active()->limit(3)->get();
            }),
        ]);
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @param Order $order
     *
     * @return void
     */
    public function promo(Request $request)
    {
        $request->validate([
            // 'basket_sum' => 'required|numeric',
            'code' => 'required|string',
        ]);
        $promo = Promo::whereCode($request->code)
            ->whereIsActive(true)
            ->with('product')
            ->first();
        return response()->api(200, $promo['message'] ?? null, [
            'promo' => $promo,
            'code' => null,
            'status' => $promo ? 4 : 1,
            'message' => trans('shop.basket.promo_message_'.($promo ? 4 : 1), [
                'product' => $promo->product->name ?? '',
            ]),
        ]);
    }

    /**
     * Undocumented function
     *
     * @param OrderRequest $request
     *
     * @return void
     */
    public function create(OrderRequest $request)
    {
        try {
            $order = Order::makeOrder($request->validated());
        } catch (IikoResponseException $e) {
            spy('Ошибка iiko заказа: ' . $e->getMessage(), 'warning');
            return response()->json(['message' => $e->getMessage()], 401);
        }
        spy('Успешно создано заказ с моб.: <a href="' . route('admin.orders.show', $order) . '">#' . $order->id . '</a>', 'success');
        $liqpayItem = collect($order->data['order']['paymentItems'])
            ->whereIn('paymentType.code', ['CARD', 'FLPC'])
            ->first();

        if ($request->payment === Order::ORDER_PAYMENT_LIQPAY && ($liqpayItem['sum'] ?? 0)) {
            $orderArray = Order::find($order->id)->toArray();
            $orderArray['sum'] = $liqpayItem['sum'];
            $liqpayForm = $order->getLiqpayApiForm($liqpayItem['sum'], route('paymentResponse', [
                'city' => app('city')->slug,
                'order' => $order->id,
            ]));

            try {
                $fromDom = new DOMDocument();
                $fromDom->loadHTML($liqpayForm['form']);

                $formUrl = $fromDom->getElementsByTagName('form')[0]->getAttribute('action');
                $data = $fromDom->getElementsByTagName('input')[0]->getAttribute('value');
                $signature = $fromDom->getElementsByTagName('input')[1]->getAttribute('value');

                $liqpayForm['link'] = $formUrl . '?' . http_build_query(['data' => $data, 'signature' => $signature]);
                $liqpayForm['request'] = [
                    'action' => $formUrl,
                    'method' => 'POST',
                    'data' => $data,
                    'signature' => $signature,
                ];
            } catch (\Throwable $th) {
                logger()->error('error on parse liqpay form', [$th->getMessage()]);
            }

            return response()->api(200, null, [
                'liqpay' => $liqpayForm,
                'order' => $orderArray,
            ]);
        }
        return response()->api(200, null, [
            'order' => Order::find($order->id)->toArray(),
        ]);
    }

    /**
     * Undocumented function
     *
     * @param OrderRequest $request
     *
     * @return void
     */
    public function cancel(Request $request, Order $order)
    {
        try {
            $order->cancelOnIiko();
        } catch (IikoResponseException $e) {
            spy('Ошибка iiko отмени заказа: ' . $e->getMessage(), 'warning');
            return response()->api(200);
        }
        $order->update(['status' => Order::ORDER_STATUS_CANCELLED]);
        spy('Успешно отмененг заказ с моб.: <a href="' . route('admin.orders.show', $order) . '">#' . $order->id . '</a>', 'success');

        return response()->api(200);
    }

    /**
     * Undocumented function
     *
     * @param OrderRequest $request
     *
     * @return void
     */
    public function basket(OrderRequest $request)
    {
        return null;
    }
}
