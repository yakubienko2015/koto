<?php

namespace App\Http\Controllers\Api;

use App\Models\Promocode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;

class PromocodeController extends Controller
{
    public $user;

    /**
     * @return void
     */
    public function __construct()
    {
        $this->user = Auth::guard('api')->user();
        if (! $this->user) { return abort(401); }
    }

    /**
     * @return \App\Models\Promocode
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'code' => 'required',
            'valid_month' => 'numeric|max:12',
            'valid_year' => 'numeric|min:2000',
        ]);

        $promocode = Promocode::create([
            'code' => $request->code,
            'price' => $request->price,
        ]);

        return custom_response(200, null, [
            'promocode' => $promocode,
        ]);
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function activate(Request $request)
    {
        $this->validate($request, [
            'code' => 'required',
            'phone' => 'required',
        ]);

        $promocode = Promocode::where('code', $request->code)->first();

        Validator::make($request->all(), [])->after(function($validator) use ($promocode) {
            if (is_null($promocode)) {
                $validator->errors()->add('promocode', trans('api.promocode_not_found'));
            }
            if (! is_null($promocode) && $promocode->activated) {
                $validator->errors()->add('promocode', trans('api.promocode_activated'));
            }
        })->validate();

        $promocode->update([
            'customer_id' => $this->user->id,
            'activated' => true,
        ]);

        return custom_response(200, null, [
            'promocode' => $promocode,
        ]);
    }
}
