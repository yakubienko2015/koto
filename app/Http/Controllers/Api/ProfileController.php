<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\ProfileRequest;
use App\Models\User;
use Iiko\Biz\Exception\IikoResponseException;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = User::with('referrer')->find(auth()->id());
        $iikoUser = $request->user()->getIikoCustomer();

        $user->middle_name = $iikoUser['middleName'] ?? '';
        $user->sur_name = $iikoUser['surname'] ?? '';
        $user->consent_status = $iikoUser['consentStatus'] ?? '';
        $user->user_data = $iikoUser['userData'] ?? '';

        return response()->api(200, null, [
            'user' => $user,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $favorite_address
     * @return \Illuminate\Http\Response
     */
    public function update(ProfileRequest $request)
    {
        $user = $request->user();
        $user->update($request->validated());
        try {
            $user->saveIikoCustomer([
                'id'         => $user->uuid,
                'phone'      => $user->phone,
                'name'       => $request->name,
                'birthday'   => $request->birthday,
                'email'      => $request->email,
                'middleName' => $request->middle_name,
                'surName'    => $request->sur_name,
                'sex'        => array_search($request->sex, User::SEXES),
            ]);
        } catch (IikoResponseException $e) {
            logger('iiko error on update profile', [$e->getMessage()]);
            // return response()->api(501, null, [
            //     'message' => $e->getMessage(),
            // ]);
        }

        return response()->api(200, null, [
            'user' => $user,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $favorite_address
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $user = $request->user();
        $user->delete();
        try {
            $user->saveIikoCustomer([
                'phone'         => $user->phone,
                'id'            => $user->uuid,
                'consentStatus' => 2,
            ]);
        } catch (IikoResponseException $e) {
            return response()->api(501, null, [
                'message' => $e->getMessage(),
            ]);
        }

        return response()->api(200);
    }
}
