<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Achievement;
use App\Models\City;
use App\Models\LifeDiscount;
use App\Models\Page;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Notifications\DatabaseNotification;

class GeneralInformationController extends Controller
{
    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function bonuses(Request $request)
    {
        return response()->api(200, null, [
            'bonuses' => intval($request->user()->getIikoBonuses()),
            // 'history' => $this->user->getBonusesHistory(),
        ]);
    }

    /**
     * Undocumented function
     *
     * @return \Illuminate\Http\Response
     */
    public function deliverytime()
    {
        $times = $this->user->city
            ->timings()
            ->get(['location', 'time', 'blocked']);
        foreach ($times as $time) {
            $time->label_time = $time->formatTime($time->time);
        }
        return response()->api(200, null, [
            'times' => $times,
        ]);
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function banners()
    {
        $sliders = $this->user->city
            ->sliders()
            ->wherePublished(1)
            ->orderBy('sort')
            ->get(['id', 'image', 'url']);

        $sliders->map(function ($slider) {
            $slider->image = "uploads/slider/{$slider->image}";
            return $slider;
        });

        return response()->api(200, null, [
            'banners' => $sliders,
        ]);
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function pages(Request $request, $pageSlug)
    {
        return response()->api(200, null, [
            'page' => Page::whereCityId(null)
                ->orWhere('city_id', $request
                    ->user()
                    ->city->id)
                ->onApi()
                ->whereSlug($pageSlug)
                ->firstOrFail(),
        ]);
    }

    /**
     * Повертає вулиці
     * ???
     * @return [json]
     */
    public function streets(Request $request)
    {
        if ($request->has('city_id')) {
            $city = City::whereId($request->city_id)->firstOrFail();
        } else {
            $city = app('city');
        }
        return response()->api(200, null, [
            'streets' => $city->getStreets(),
        ]);
    }

    /**
     * Повертає вулиці
     * ???
     * @return [json]
     */
    public function cities(Request $request)
    {
        return response()->api(200, null, [
            'cities' => City::get(),
        ]);
    }

    public function notifications(Request $request)
    {
        return response()->api(200, null, [
            'notifications' => $request->user()->notifications()->whereNull('read_at')->get(),
        ]);
    }

    public function readNotification(Request $request, DatabaseNotification $notification)
    {
        if ($notification->notifiable->id === $request->user()->id) {
            $notification->markAsRead();
            return response()->api(200, null, [
                'notification' => $notification,
            ]);
        }
        return response()->api(403);
    }

    public function removeNotification(Request $request, DatabaseNotification $notification)
    {
        if ($notification->notifiable->id === $request->user()->id
            && $notification->delete()) {
            return response()->api(200);
        }
        return response()->api(403);
    }

    public function setDeviceToken(Request $request)
    {
        $request->validate([
            'device_token' => 'required',
        ]);
        $request->user()->update($request->only(['device_token']));
        return response()->api(200);
    }

    public function info(Request $request)
    {
        return response()->api(200, null, [
            'add_sauce_product' => Product::find(config('shop.add_sauce_product_id')),
        ]);
    }

    public function achievements(Request $request)
    {
        $userCategories = $request->user()->getIikoCategories();

        return response()->api(200, null, [
            'achievements' => Achievement::whereIn('get_category_uuid', $userCategories)
                ->get()->merge(
                    Achievement::active()
                        ->where(function ($query) use ($userCategories) {
                            $query->whereIn('has_category_uuid', $userCategories)
                                ->orWhereNull('has_category_uuid');
                        })->get()
                )->each->append('progress'),
        ]);
    }
}
