<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\BowlIngredient;
use App\Models\Product;

class BowlConstructorController extends Controller
{
    public function getIngredientsTree()
    {
        return response()->api(200, null, BowlIngredient::getIngredientsTreeInArray());
    }

    public function findBowl(Request $request)
    {
        $request->validate([
            'steps' => 'required|array|size:3',
        ]);
        Product::findBowlByIngredients($request->steps);
        return response()->api(200, null, Product::findBowlByIngredients($request->steps));
    }
}
