<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileRequest;
use App\Models\Order;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        if (! config('site.personal_cabinet_enabled')) {
            return redirect('admin');
        }
        return view('site.account.profile', [
            // 'orders' => $request->user()->orders()->orderBy('created_at', 'desc')->paginate(10),
            'bonusTransactions' => $request->user()->transactions,
            'lastOrder' => $request->user()
                ->phoneOrders()
                ->orderBy('created_at', 'desc')
                ->whereStatus(Order::ORDER_STATUS_CLOSED)
                ->first(),
        ]);
    }

    /**
     *
     */
    public function update(ProfileRequest $request)
    {
        $data = $request->validated();
        if ($request->password) {
            $data['password'] = bcrypt($request->password);
        } else {
            unset($data['password']);
        }

        $request->user()->update($data);
        return back();
    }
}
