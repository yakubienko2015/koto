<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Models\Pin;
use App\Models\WorkPlace;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    use AuthenticatesUsers;

    /**
     * @var string
     */
    protected function redirectTo()
    {
        return Auth::guard('app')->user()->getHomePath();
    }

    protected function user()
    {
        $user = Auth::guard('app')->user();
        $user->pin;
        return response()->json($user);
    }

    protected function sendRestTime(Request $request)
    {
        $user = Auth::guard('app')->user();
        $user->pin->update([
            'rest_time' => $user->pin->rest_time + $request->rest_time ?? 0,
        ]);
        return response(null);
    }

    protected function authenticated(Request $request, $user)
    {
        return redirect($user->getHomePath())->with('showWelcome', true);
    }

    protected function loggedOut(Request $request)
    {
        $workPlace = WorkPlace::find($request->work_place);
        if ($workPlace) {
            $workPlace->productGroups()->sync([]);
            $workPlace->products()->sync([]);
        }
        return $request->wantsJson()
            ? new Response('', 204)
            : redirect('app');
    }

    protected $maxAttempts = 3;

    protected function attemptLogin(Request $request)
    {
        $pin = Pin::active()
            ->whereId($request->pin)
            ->first();
        $user = $pin ? $pin->user : null;
        if ($user) {
            Auth::guard('app')->login($user);
            $user->update(['logout' => false]);
            $pin->update(['last_used_at' => now()]);
            return true;
        }
        return false;
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'pin';
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        throw ValidationException::withMessages(
            ['pin' => __('auth.failed_pin')]
        );
    }

    public function showLoginForm()
    {
        return view('app.login');
    }

    protected function guard()
    {
        return Auth::guard('app');
    }

    protected function throttleKey(Request $request)
    {
        return 'app|'.$request->ip();
    }

    protected function validateLogin(Request $request)
    {
        $request->validate([
            'pin' => 'required|string',
        ]);
    }

    public function ping(Request $request)
    {
        $request->user('app')->ping($request->stop);
        return response(true);
    }
}
