<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Models\Terminal;
use Auth;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Undocumented function
     *
     * @param Terminal $terminal
     *
     * @return void
     */
    public function admin(Terminal $terminal)
    {
        return view('app.admin', ['terminal' => $terminal]);
    }

    /**
     * Undocumented function
     *
     * @param Terminal $terminal
     *
     * @return void
     */
    public function stats(Terminal $terminal)
    {
        return view('app.stats', ['terminal' => $terminal]);
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @param Terminal $terminal
     *
     * @return void
     */
    public function changeTerminalStatus(Request $request, Terminal $terminal)
    {
        $terminal->update([
            'status' => $request->status,
            'phone' => $request->user()->phone,
        ]);

        return response()->json([
            'status' => $terminal->status,
            'message' => __('Статус обновлено'),
        ]);
    }

    /**
     * Undocumented function
     *
     * @param Terminal $terminal
     *
     * @return void
     */
    public function getTerminalStats(Terminal $terminal)
    {
        return response()->json($terminal->getLastMonthStats());
    }
}
