<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\ProductGroup;
use App\Models\Terminal;
use App\Models\Timer;
use App\Models\WorkPlace;
use Illuminate\Http\Request;
use Str;

class CookController extends Controller
{
    /**
     * Undocumented function
     *
     * @param Terminal $terminal
     * @param WorkPlace $workPlace
     *
     * @return \Illuminate\Http\Response
     */
    public function cook(Terminal $terminal, WorkPlace $workPlace)
    {
        return view('app.cook', [
            'terminal' => $terminal,
            'workPlace' => $workPlace,
        ]);
    }

    /**
     * Undocumented function
     *
     * @param Terminal $terminal
     *
     * @return \Illuminate\Http\Response
     */
    public function issucook(Request $request, Terminal $terminal)
    {
        // $activeCooks = Timer::where('stopped_at', null)
        //     ->where('channel', 'like', "%/app/cook/{$terminal->id}")
        //     ->where('user_id', '<>', $request->user()->id)
        //     ->count();
        // if ($activeCooks && ! $request->user()->hasRole('manager')) {
        //     return redirect('app/cook');
        // }
        return view('app.issucook', ['terminal' => $terminal]);
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function orderWorkPlaces(Request $request)
    {
        $orderWPs = WorkPlace::findOrFail($request->work_place_id)
            ->orderWorkPlaces()
            ->with('product', 'order')
            ->where('completed_at', null)
            ->leftJoin('orders', 'order_work_place.order_id', '=', 'orders.id')
            ->where('orders.status', Order::ORDER_STATUS_IN_PROGRESS)
            ->orderBy(
                'orders.' . ($request->sort_by ?? 'ordered_at'),
                $request->sort_desc ?? true ? 'desc' : 'asc'
            )
            ->orderBy('id')
            ->select(['order_work_place.*']);

        $paginatedOrderWPs = $orderWPs->paginate($request->size ?? 15);

        return response()->json([
            'items' => $paginatedOrderWPs->items(),
            'per_page' => $paginatedOrderWPs->perPage(),
            'total' => $paginatedOrderWPs->total(),
            'current_page' => $paginatedOrderWPs->currentPage(),
        ]);
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function orderWorkProducts(Request $request)
    {
        $orderPs = OrderProduct::whereCookedAt(null)
            ->leftJoin('products', 'order_product.sub_product_id', '=', 'products.id')
            ->leftJoin('orders', 'order_product.order_id', '=', 'orders.id')
            ->whereIn('products.product_group_id', $request->groups ?? [])
            ->where('orders.status', Order::ORDER_STATUS_IN_PROGRESS)
            ->where('orders.terminal_id', $request->terminal_id)
            ->whereDate('orders.cook_at', '=', now()->format('Y-m-d'))
            ->orderBy('orders.ordered_at', 'asc')
            ->orderBy('id')
            ->orderBy('orders.created_at')
            ->select(['order_product.*'])
            ->with('order', 'sub_product');

            // sub_product.name
            // modifiers
            // id
            // sub_product.time_for_cook
            // sub_product.work_ingredients
            // sub_product.work_ingredients[name weight unit]
            // sub_product.weight
            // sub_product.measure_unit
            // order
            // sub_product.time_for_cook
            // order_id
            // order.is_pre_order
            // order.time_cook_left

            // order.status
            // order.data
            // order.ordered_at
            // order.cook_at
            // order.data

        $paginatedOrderWPs = $orderPs->paginate(99);

        return response()->json([
            'items' => $paginatedOrderWPs->items(),
            'per_page' => $paginatedOrderWPs->perPage(),
            'total' => $paginatedOrderWPs->total(),
            'current_page' => $paginatedOrderWPs->currentPage(),
        ]);
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function cookSelect(Request $request)
    {
        if ($request->has('work_place') && $request->has('product_groups')) {
            // $workPlace = WorkPlace::findOrFail($request->work_place);
            // $workPlace->productGroups()->sync($request->product_groups);
            // $productGroups = ProductGroup::whereIn('id', $request->product_groups);
            // $products = collect([]);
            // foreach ($productGroups->get() as $group) {
            //     $products = $products->merge($group->products);
            // }
            // $workPlace->products()->sync($products->pluck('id'));
            return redirect(Str::of(route('app.cook', [
                'terminal' => $request->terminal,
                'workPlace' => $request->work_place,
                'groups' => $request->product_groups,
            ]))->replaceMatches('/groups%5B\d+%5D/', 'groups[]'))
            ->withCookie(cookie('last_work_place_id', $request->work_place, 100000));
        }
        return view('app.cookSelect', [
            'terminals' => Terminal::all(),
            'workPlaces' => WorkPlace::all(),
            'productGroups' => ProductGroup::all(),
        ]);
    }

    /**
     * Undocumented function
     *
     * @param Order $order
     *
     * @return void
     */
    public function makeOrderReady(Order $order)
    {
        // if ($order->orderWorkPlaces()->where('completed_at', null)->count()) {
        //     return response()->json([
        //         'status' => $order->status,
        //         'message' => __('Невозможно закрыть'),
        //     ]);
        // }
        $order->update(['status' => Order::ORDER_STATUS_READY]);
        return response()->json([
            'status' => $order->status,
        ]);
    }

    /**
     * Undocumented function
     *
     * @param Order $order
     * @param OrderProduct $orderProduct
     *
     * @return void
     */
    public function makeOrderProductReady(Order $order, OrderProduct $orderProduct)
    {
        if ($order->id != $orderProduct->order_id
            || $orderProduct->cooked_at != null) {
            return response()->json([
                'status' => $order->status,
                'message' => __('Невозможно закрыть'),
            ]);
        }
        $orderProduct->update([
            // 'user_id' => auth()->id(),
            'cooked_at' => now(),
        ]);

        if ($orderProduct->isAllSubReady()) {
            $orderProduct->main()->update([
                // 'user_id' => auth()->id(),
                'cooked_at' => now(),
            ]);
        }
        try {
            $order->updateWorkPlaces();
        } catch (\Throwable $th) {
            logger('error on product ready, update work places', [$th->getMessage()]);
        }
        return response()->json([
            'status' => $order->status,
        ]);
    }
}
