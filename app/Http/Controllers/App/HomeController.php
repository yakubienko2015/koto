<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderWorkPlace;
use App\Models\Pin;
use App\Models\Terminal;
use App\Models\User;
use Iiko\Biz\Exception\IikoResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Log;
use Telegram;

class HomeController extends Controller
{
    public function index()
    {
        return redirect(Auth::guard('app')->user()->getHomePath());
    }

    public function operator()
    {
        return view('app.operator');
    }

    public function courier()
    {
        return view('app.courier');
    }

    public function courierCabinet()
    {
        return view('app.courierCabinet');
    }

    public function orders(Request $request)
    {
        $type = $request->type ?? 'operator';
        $terminal = Terminal::find($request->terminal_id);
        $orders = $terminal
            ? $terminal->city->orders()
            : app('city')->orders();
        $append = [];

        if ($type == 'admin') {
            $orders->with('couriers');
        } elseif ($type == 'issucook') {
            $orders->where('status', Order::ORDER_STATUS_IN_PROGRESS)
                ->with(['workPlaces', 'products' => function ($q) {
                    return $q->withPivot(['amount', 'modifiers']);
                }, 'products.subproducts'])
                ->whereDate('orders.ordered_at', '=', now()->format('Y-m-d'));
            // $append = 'work_places';
        } elseif ($type == 'logist') {
            $orders->where('status', Order::ORDER_STATUS_WAITING);
        } elseif ($type == 'courier') {
            $orders = $request->user()
                ->workOrders()
                ->with('terminal')
                ->where('status', Order::ORDER_STATUS_ON_WAY);
        } else {
            $orders->where('status', '>', 0);
        }
        if ($request->phone) {
            $orders->where('phone', 'like', "%{$request->phone}%");
        }
        if ($request->terminal_id) {
            $orders->where('terminal_id', $request->terminal_id);
        }
        if ($request->ordered_at) {
            $orders->whereDate('ordered_at', $request->ordered_at);
        }
        if (in_array($request->status, array_keys(config('shop.order_statuses')))) {
            $orders->where('status', $request->status);
        }
        $orders
            ->orderBy($request->sort_by ?? 'ordered_at', ($request->sort_desc ?? false) ? 'desc' : 'asc')
            ->orderBy('orders.created_at');
        $paginatedOrders = $orders->paginate($request->size ?? 15);

        return response()->json([
            'items' => collect($paginatedOrders->items())->each->append($append),
            'per_page' => $paginatedOrders->perPage(),
            'total' => $paginatedOrders->total(),
            'current_page' => $paginatedOrders->currentPage(),
        ]);
    }

    public function terminals(Request $request)
    {
        $terminals = app('city')->terminals()->where('status', '>', 0)->get();

        return response()->json($terminals);
    }

    public function setOrderDelivered(Request $request)
    {
        $order = Order::findOrFail($request->order_id);
        $courier = $request->user();
        try {
            $order->setDelivered($courier);
        } catch (IikoResponseException $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
            ]);
        }
        if ($courier->workOrders()->whereIn('status', [
            Order::ORDER_STATUS_ON_WAY,
            Order::ORDER_STATUS_READY,
            Order::ORDER_STATUS_WAITING,
        ])->count() === 0) {
            $spendSeconds = $courier->pin->updated_at->diffInSeconds(now());
            $courier->pin->update([
                'courier_status' => Pin::COURIER_STATUS_RETURN,
                'courier_must_return_at' => now()->addSeconds($spendSeconds),
            ]);
        }
        return response()->json([
            'success' => true,
        ]);
    }

    public function view(Order $order)
    {
        // Block for operator
        // $operator = $order->operators()->first();
        // if ($order->status === Order::ORDER_STATUS_NEW || $order->status === Order::ORDER_STATUS_OPERATE) {
        //     if ($operator === null) {
        //         $order->update(['status' => Order::ORDER_STATUS_OPERATE]);
        //         $order->operators()->attach(auth()->id(), ['role' => 'operator']);
        //     } elseif ($operator->id !== auth()->id()) {
        //         return response()->json([
        //             'status' => $order->status,
        //             'message' => __('Оператор :name уже оформляет етот заказ', ['name' => $operator->name]),
        //         ]);
        //     }
        // }
        return response()->json([
            'status' => $order->status,
        ]);
    }

    public function toOrder(Request $request, Order $order)
    {
        $terminal = Terminal::findOrFail($request->terminal_id);

        $operator = $order->operators()->first();
        if ($operator->id !== auth()->id()) {
            return response()->json([
                'status' => $order->status,
                'message' => __('Оператор :name уже оформляет етот заказ', ['name' => $operator->name]),
            ]);
        }

        if ($order->status === Order::ORDER_STATUS_OPERATE) {
            $lastOrder = $terminal->orders()->orderBy('number', 'desc')->first();
            $order->update([
                'status' => Order::ORDER_STATUS_IN_PROGRESS,
                'terminal_id' => $terminal->id,
                'number' => ($lastOrder->number ?? 0) + 1,
            ]);
            return response()->json([
                'status' => $order->status,
                'message' => __('Успешно оформлено'),
            ]);
        }
        return response()->json([
            'status' => $order->status,
            'message' => __('Невозможно оформить'),
        ]);
    }

    public function logoutCourier(Request $request)
    {
        if ($request->user()
            ->workOrders()
            ->where('status', Order::ORDER_STATUS_ON_WAY)->count()) {
            return response([
                'message' => 'Нужно сначала закрыть заказ',
            ], 422);
        }
        $pin = $request->user()->pin;
        $pin->update([
            'courier_status' => null,
        ]);
        Auth::logout();
        return back();
    }

    public function startCourierRest(Request $request)
    {
        $pin = $request->user()->pin;
        $pin->update([
            'courier_status' => Pin::COURIER_STATUS_REST,
        ]);
        return response()->json([
            'status' => $pin->courier_status,
        ]);
    }

    public function stopCourierRest(Request $request)
    {
        $pin = $request->user()->pin;
        $pin->update([
            'courier_status' => Pin::COURIER_STATUS_READY,
        ]);
        return response()->json([
            'status' => $pin->courier_status,
        ]);
    }

    public function sendMessage(Request $request)
    {
        $order = Order::findOrFail($request->order_id);
        $courier = $request->user();
        $terminal = Terminal::findOrFail($order->terminal_id);
        $admin = User::where('phone', '=', $terminal->phone)->first();

        Telegram::sendMessage([
            'chat_id' => $admin->telegram_id,
            'text' => "Курьер " . $courier->name . " задерживается на заказе",
        ]);

        return response()->json([
            'status' => $order->status,
        ]);
    }
}
