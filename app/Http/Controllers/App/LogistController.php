<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Pin;
use Iiko\Biz\Exception\IikoResponseException;
use Illuminate\Http\Request;

class LogistController extends Controller
{
    /**
     * Undocumented function
     *
     * @return void
     */
    public function logist()
    {
        return view('app.logist');
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     *
     * @return void
     */
    public function loginCourier(Request $request)
    {
        $request->validate([
            'pin' => 'required',
        ]);
        $pin = Pin::findOrFail($request->pin);
        if (strlen((string) $pin->user->uuid) < 32 && config('shop.make_order_with_iiko')) {
            return response([
                'message' => 'Этот работник не зарегистрирован в iiko',
            ], 422);
        }
        if ($pin->user->hasRole('courier')) {
            $pin->update([
                'courier_status' => Pin::COURIER_STATUS_READY,
            ]);
            return response()->json([
                'status' => $pin->courier_status,
            ]);
        }
        return response([
            'message' => 'Этот работник не является курьером',
        ], 422);
        dd($pin->user->hasRole('courier'));
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     *
     * @return void
     */
    public function couriers(Request $request)
    {
        $pins = Pin::whereNotNull('courier_status')
            ->orderBy('updated_at', 'asc')
            ->with('user')
            ->get()
            ->groupBy('courier_status');

        return response()->json($pins);
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     *
     * @return void
     */
    public function setCourier(Request $request)
    {
        $request->validate([
            'pin_id' => 'required',
            'order_id' => 'required',
        ]);
        $pin = Pin::findOrFail($request->pin_id);
        $order = Order::findOrFail($request->order_id);
        $secondOrder = Order::find($request->second_order_id);
        // if ($order->couriers()->count() === 0) {
            try {
                $order->assignCourier($pin->user);
                $pin->update([
                    'courier_status' => Pin::COURIER_STATUS_ON_WAY,
                ]);
            } catch (IikoResponseException $e) {
                return response()->json([
                    'success' => false,
                    'message' => $e->getMessage(),
                ]);
            }
            if ($secondOrder) {
                try {
                    $secondOrder->assignCourier($pin->user);
                } catch (IikoResponseException $e) {
                    return response()->json([
                        'success' => false,
                        'message' => $e->getMessage(),
                    ]);
                }
            }
            return response()->json([
                'success' => true,
            ]);
        // }
        // return response()->json([
        //     'success' => false,
        //     'message' => 'Уже есть курьер для этого заказа',
        // ]);
    }
}
