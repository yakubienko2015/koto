<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\RolesDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\RoleRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Session;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use View;

class RoleController extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        View::share('title', 'Роли пользователей');
    }

    /**
     * Display a listing of the resource.
     *
     * @param \App\DataTables\RolesDataTable $dataTable
     * @return \Illuminate\Http\Response
     */
    public function index(RolesDataTable $dataTable)
    {
        return $dataTable->render('components.admin.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.rolesForm', [
            'route' => route('admin.roles.store'),
            'permissions' => User::getPermissionsForSelect(),
            'role' => new Role(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Admin\RoleRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $request)
    {
        $data = $request->validated();

        $role = Role::create($data);

        $role->syncPermissions([$request->permissions]);

        Session::flash('success', __('admin.successStored'));
        return $request->exit
            ? redirect()->route('admin.roles.index')
            : redirect()->route('admin.roles.edit', $role);
    }

    /**
     * @param  \App\Http\Requests\Admin\RoleRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function storePermission(Request $request)
    {
        $request->validate([
            'permission_name' => ['required', 'string', Rule::unique('permissions', 'name')],
        ]);
        Permission::create(['name' => $request->permission_name]);
        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Spatie\Permission\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        return view('admin.rolesForm', [
            'route' => route('admin.roles.update', $role),
            'method' => 'PUT',
            'role' => $role,
            'permissions' => User::getPermissionsForSelect(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Admin\RoleRequest  $request
     * @param  \Spatie\Permission\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(RoleRequest $request, Role $role)
    {
        $role->syncPermissions([$request->permissions]);

        Session::flash('success', __('admin.successUpdated'));
        return $request->exit ? redirect()->route('admin.roles.index') : back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Spatie\Permission\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        $role->delete();

        Session::flash('success', __('admin.successDestroyed'));
        return back();
    }
}
