<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Components\Journal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Storage;
use View;

class DashboardController extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        View::share('title', 'Панель управления');
    }

    /**
     * @return void
     */
    public function dashboard()
    {
        return view('admin.dashboard', [
            'journal' => Journal::latest()->take(30)->get(),
        ]);
    }

    /**
     * @param Request $request
     * @return void
     */
    public function artisan(Request $request)
    {
        $request->validate([
            'cmd' => 'required',
        ]);

        $out = null;
        try {
            if (is_array($request->cmd)) {
                foreach ($request->cmd as $cmd) {
                    $out = $this->runCmd($cmd);
                }
            } else {
                $out = $this->runCmd($request->cmd);
            }
            Session::flash('success', 'Команда выполнена: ' . $out);
        } catch (\Throwable $th) {
            Session::flash('danger', 'Возникла ошибка выполнения скрипта: ' . $th->getMessage());
        }

        return back();
    }

    /**
     * @param Request $request
     * @return void
     */
    public function uploadImage(Request $request)
    {
        $request->validate([
            'upload' => 'required|image',
        ]);

        $savedPath = $request->upload->store('public/content');

        return response()->json([
            'url' => Storage::url($savedPath),
        ]);
    }

    private function runCmd($cmd)
    {
        if (strpos($cmd, 'migrate') != null) {
            return \Artisan::call($cmd, ['--no-interaction' => true, '--force' => true]);
        } else {
            return \Artisan::call($cmd, ['--no-interaction' => true]);
        }
    }
}
