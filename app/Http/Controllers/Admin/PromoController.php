<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\PromosDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\PromoRequest;
use App\Models\Product;
use App\Models\Promo;
use Keygen\Keygen;
use Request;
use Session;
use View;

class PromoController extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        View::share('title', 'Промо');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \App\DataTables\PromosDataTable $dataTable
     * @return mixed
     */
    public function index(PromosDataTable $dataTable)
    {
        return $dataTable->render('components.admin.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.promosForm', [
            'route' => route('admin.promos.store'),
            'products' => Product::getProductsForSelect(),
            'promo' => new Promo(),
            'categories' => collect(app('iiko')
                    ->OrganizationsApi()
                    ->getGuestCategories(app('city')->organization_uuid) ?? []),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Admin\PromoRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PromoRequest $request)
    {
        $data = $request->validated();

        $promo = Promo::create($data);

        Session::flash('success', __('admin.successStored'));
        return $request->exit
            ? redirect()->route('admin.promos.index')
            : redirect()->route('admin.promos.edit', $promo);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function randomQr(Request $request)
    {
        $promo = Promo::create([
            'code' => Keygen::alphanum()->generate('strtoupper'),
            'qr_code' => Keygen::alphanum(6)->generate('strtolower'),
            'product_id' => $request->id ?? null,
            'is_active' => true,
            'can_be_single' => true,
            'is_multiuse' => false,
            'min_basket_sum' => 0,
        ]);

        Session::flash('success', __('admin.successStored'));
        return redirect()->route('admin.promos.edit', $promo);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Promo  $promo
     * @return \Illuminate\Http\Response
     */
    public function edit(Promo $promo)
    {
        return view('admin.promosForm', [
            'route' => route('admin.promos.update', $promo),
            'method' => 'PUT',
            'products' => Product::getProductsForSelect(),
            'promo' => $promo,
            'categories' => collect(app('iiko')
                    ->OrganizationsApi()
                    ->getGuestCategories(app('city')->organization_uuid) ?? []),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Admin\PromoRequest  $request
     * @param  \App\Models\Promo  $promo
     * @return \Illuminate\Http\Response
     */
    public function update(PromoRequest $request, Promo $promo)
    {
        $data = $request->validated();

        $promo->update($data);

        Session::flash('success', __('admin.successUpdated'));
        return $request->exit ? redirect()->route('admin.promos.index') : back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Promo  $promo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Promo $promo)
    {
        $promo->delete();

        Session::flash('success', __('admin.successDestroyed'));
        return back();
    }
}
