<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\SlidesDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\SlideRequest;
use App\Models\City;
use App\Models\Slide;
use Illuminate\Http\Request;
use Session;
use View;

class SlideController extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        View::share('title', 'Слайд-шоу');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \App\DataTables\SlidesDataTable $dataTable
     * @return \Illuminate\Http\Response
     */
    public function index(SlidesDataTable $dataTable)
    {
        return $dataTable->render('components.admin.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.slidesForm', [
            'route' => route('admin.slides.store'),
            'slide' => new Slide(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\SlideRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SlideRequest $request)
    {
        $data = $request->validated();

        $cityId = $request->all_cities ? null : app('city')->id;
        $slide = slide::create($data + ['city_id' => $cityId]);
        if ($request->hasFile('image')) {
            $slide->saveImage($request->file('image'));
        }

        Session::flash('success', __('admin.successStored'));
        return $request->exit
            ? redirect()->route('admin.slides.index')
            : redirect()->route('admin.slides.edit', $slide);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Slide  $slide
     * @return \Illuminate\Http\Response
     */
    public function edit(Slide $slide)
    {
        return view('admin.slidesForm', [
            'route' => route('admin.slides.update', $slide),
            'method' => 'PUT',
            'slide' => $slide,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Slide  $slide
     * @return \Illuminate\Http\Response
     */
    public function update(SlideRequest $request, Slide $slide)
    {
        $data = $request->validated();

        $cityId = $request->all_cities ? null : app('city')->id;
        $slide->update($data + ['city_id' => $cityId]);
        if ($request->hasFile('image')) {
            $slide->saveImage($request->file('image'));
        }

        Session::flash('success', __('admin.successUpdated'));
        return $request->exit ? redirect()->route('admin.slides.index') : back();
    }

    /**
     * Remove the specified resource image.
     *
     * @param  \App\Models\Slide  $slide
     * @return \Illuminate\Http\Response
     */
    public function removeImage(Slide $slide)
    {
        $slide->update(['image' => null]);

        return response()->json([]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Slide  $slide
     * @return \Illuminate\Http\Response
     */
    public function destroy(Slide $slide)
    {
        $slide->delete();

        Session::flash('success', __('admin.successDestroyed'));
        return back();
    }
}
