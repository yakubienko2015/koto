<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\SettingRequest;
use App\Library\CustomConfig;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class SettingsController extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        View::share('title', 'Настройки');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $customerCategories = collect(app('iiko')
                ->OrganizationsApi()
                ->getGuestCategories(app('city')->organization_uuid) ?? []);
        } catch (\Throwable $th) {
            $customerCategories = null;
        }

        return view('admin.settingsForm', [
            'route' => route('admin.settings.update'),
            'method' => 'PUT',
            'customCongif' => CustomConfig::load() ?? [],
            'customerCategories' => $customerCategories,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Admin\SettingRequest  $request
     * @param  \Spatie\Permission\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(SettingRequest $request)
    {
        $data = $request->all();
        CustomConfig::store($data);

        Session::flash('success', __('admin.successUpdated'));
        return $request->exit ? redirect()->route('admin.roles.index') : back();
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function translations()
    {
        return view('admin.settingsTranslations');
    }
}
