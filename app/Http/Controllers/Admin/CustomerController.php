<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\CustomersDataTable;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Http\Requests\Admin\UserRequest;
use App\Models\City;
use Iiko\Biz\Exception\IikoResponseException;
use Illuminate\Support\Facades\Session;
use View;

class CustomerController extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        View::share('title', 'Клиенти');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \App\DataTables\CustomersDataTable $dataTable
     * @return \Illuminate\Http\Response
     */
    public function index(CustomersDataTable $dataTable)
    {
        return $dataTable->render('components.admin.index');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function iiko()
    {
        try {
            User::loadEmployeesPinsFromIiko();
        } catch (IikoResponseException $e) {
            Session::flash('danger', $e->getMessage());
        }
        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.usersForm', [
            'route' => route('admin.users.store'),
            'user' => new User(),
            'roles' => User::getRolesForSelect(),
            'sexes' => User::getSexesForSelect(),
            'cities' => City::pluck('name', 'id'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Admin\UserRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $data = $request->validated();

        $user = User::create($data);

        if ($request->is_verified) {
            $user->markEmailAsVerified();
        }
        $user->syncRoles([$request->role]);
        if ($request->new_pin) {
            $user->generatePin();
        }

        Session::flash('success', __('admin.successStored'));
        return $request->exit
            ? redirect()->route('admin.users.index')
            : redirect()->route('admin.users.edit', $user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $user->pin = $user->pin()->first();
        return view('admin.usersForm', [
            'route' => route('admin.users.update', $user),
            'method' => 'PUT',
            'user' => $user,
            'roles' => User::getRolesForSelect(),
            'sexes' => User::getSexesForSelect(),
            'cities' => City::pluck('name', 'id'),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Admin\UserRequest  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, User $user)
    {
        $data = $request->validated();
        $user->update($data);

        if ($request->is_verified) {
            $user->markEmailAsVerified();
        }
        $user->syncRoles([$request->role]);
        if ($request->new_pin) {
            $user->generatePin();
        }

        Session::flash('success', __('admin.successUpdated'));
        return $request->exit ? redirect()->route('admin.users.index') : back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        Session::flash('success', __('admin.successDestroyed'));
        return back();
    }
}
