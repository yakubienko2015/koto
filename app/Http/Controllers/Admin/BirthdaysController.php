<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\BirthdaysDataTable;
use App\Http\Controllers\Controller;
use View;

class BirthdaysController extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        View::share('title', 'Именинники');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \App\DataTables\BirthdaysDataTable $dataTable
     * @return \Illuminate\Http\Response
     */
    public function index(BirthdaysDataTable $dataTable)
    {
        return $dataTable->render('components.admin.index');
    }
}
