<?php

namespace App\Http\Controllers\Admin;

use App\Console\Commands\ScanIikoOrders;
use App\DataTables\OrdersDataTable;
use App\Exports\OrdersGameExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\OrderRequest;
use App\Models\Order;
use App\Models\Pin;
use App\Models\Promo;
use App\Models\User;
use Iiko\Biz\Exception\IikoResponseException;
use Illuminate\Support\Facades\Artisan;
use Maatwebsite\Excel\Facades\Excel;
use Session;
use View;

class OrderController extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        View::share('title', 'Заказы');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \App\DataTables\OrdersDataTable $dataTable
     * @return \Illuminate\Http\Response
     */
    public function index(OrdersDataTable $dataTable)
    {
        return $dataTable->render('components.admin.index');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function iiko()
    {
        try {
            Session::flash('success', Artisan::call(ScanIikoOrders::class));
        } catch (IikoResponseException $e) {
            Session::flash('danger', $e->getMessage());
        }
        return back();
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function gameStats()
    {
        return Excel::download(new OrdersGameExport, 'order_games.xlsx');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.orderForm', [
            'route' => route('admin.orders.store'),
            'order' => new Order(),
            'terminals' => app('city')->terminals()->pluck('name', 'id'),
            'users' => User::pluck('name', 'id'),
            'paymentTypes' => config('shop.payments'),
            'statuses' => config('shop.order_statuses'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Admin\OrderRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrderRequest $request)
    {
        $data = $request->validated();

        $order = Order::create($data + ['city_id' => app('city')->id]);

        Session::flash('success', __('admin.successStored'));
        return $request->exit
            ? redirect()->route('admin.orders.index')
            : redirect()->route('admin.orders.edit', $order);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        return view('admin.orderView', [
            'route' => route('admin.orders.update', $order),
            'method' => 'PUT',
            'order' => $order,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        return view('admin.orderForm', [
            'route' => route('admin.orders.update', $order),
            'method' => 'PUT',
            'order' => $order,
            'terminals' => app('city')->terminals()->pluck('name', 'id'),
            'users' => User::pluck('name', 'id'),
            'paymentTypes' => config('shop.payments'),
            'statuses' => config('shop.order_statuses'),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Admin\OrderRequest  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(OrderRequest $request, Order $order)
    {
        $data = $request->validated();

        if (! $order->terminal_id && ! isset($data['terminal_id'])) {
            $data['terminal_id'] = app('city')->terminals->first()->id ?? null;
        }
        if ($request->courier_id) {
            $order->adminAssignCourier(User::find($request->courier_id));
        }

        $order->update($data);

        Session::flash('success', __('admin.successUpdated'));
        return $request->exit ? redirect()->route('admin.orders.index') : back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        $order->delete();

        Session::flash('success', __('admin.successDestroyed'));
        return back();
    }

    /**
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function sendToIiko(Order $order)
    {
        if ($order->uuid) {
            Session::flash('success', __('admin.successUpdated'));
            return back();
        }
        $orderRequest = Order::getOrderRequest($order->data);
        $orderRequest = Order::fixIikoModifiers($orderRequest);
        try {
            $iikoOrder = app('iiko')->OrdersApi()->addOrder($orderRequest);
            $order->update([
                'uuid' => $iikoOrder['orderId'],
                'sum' => $iikoOrder['sum'],
            ]);
            $promo = Promo::whereCode($order->data['promo'] ?? '')
                ->whereIsActive(true)
                ->first();
            if ($promo) {
                $promo->setCategoryToOrderCustomer($iikoOrder);
            }
            Session::flash('success', __('admin.successUpdated'));
        } catch (IikoResponseException $e) {
            Session::flash('danger', $e->getMessage());
            spy('Ошибка iiko заказа после sendToIiko: ' . $e->getMessage(), 'warning');
        }

        return back();
    }
}
