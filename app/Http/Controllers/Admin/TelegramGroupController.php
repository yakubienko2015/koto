<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\GroupsDataTable;
use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\Group;
use App\Models\Terminal;
use App\Models\User;
use Illuminate\Http\Request;
use Session;
use View;

class TelegramGroupController extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        View::share('title', 'Групы');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \App\DataTables\GroupsDataTable $dataTable
     * @return mixed
     */
    public function index(GroupsDataTable $dataTable)
    {
        return $dataTable->render('components.admin.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.groupsForm', [
            'route' => route('admin.groups.store'),
            'group' => new Group(),
            'terminals' => Terminal::pluck('name', 'id'),
            'roles' => User::getRolesForSelect()->except('customer'),
            'cities' => City::pluck('name', 'id'),
            'users' => [],
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $group = Group::create($data);
        $group->syncRoles($request->roles);

        Session::flash('success', __('admin.successStored'));
        return $request->exit
            ? redirect()->route('admin.groups.index')
            : redirect()->route('admin.groups.edit', $group);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function edit(Group $group)
    {
        return view('admin.groupsForm', [
            'route' => route('admin.groups.update', $group),
            'method' => 'PUT',
            'group' => $group,
            'terminals' => Terminal::pluck('name', 'id'),
            'roles' => User::getRolesForSelect()->except('customer'),
            'cities' => City::pluck('name', 'id'),
            'users' => $group->users()->pluck('name', 'phone'),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Group $group)
    {
        $data = $request->all();

        $group->update($data);
        $group->syncRoles($request->roles);

        if ($request->send_message && $request->message) {
            $responses = $group->sendMessage($request->message, (bool) $request->holiday_users);
            Session::flash('warning', 'Количество получателей сообщения: ' . count($responses));
        }

        Session::flash('success', __('admin.successUpdated'));
        return $request->exit ? redirect()->route('admin.groups.index') : back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function destroy(Group $group)
    {
        $group->delete();

        Session::flash('success', __('admin.successDestroyed'));
        return back();
    }
}
