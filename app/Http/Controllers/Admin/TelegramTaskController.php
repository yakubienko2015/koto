<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\TasksDataTable;
use App\Exports\UserTaskExport;
use App\Http\Controllers\Controller;
use App\Models\Group;
use App\Models\Task;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Session;
use View;

class TelegramTaskController extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        View::share('title', 'Задачи');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \App\DataTables\TasksDataTable $dataTable
     * @return mixed
     */
    public function index(TasksDataTable $dataTable)
    {
        return $dataTable->render('components.admin.index');
    }

    public function export(Request $request)
    {
        return Excel::download(new UserTaskExport($request->date), 'report.xlsx');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tasksForm', [
            'route' => route('admin.tasks.store'),
            'task' => new Task(),
            'groups' => Group::pluck('name', 'id'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'group_id' => '',
            'title' => 'required',
            'description' => '',
            'description_resource' => '',
            'is_required' => '',
            'is_file_required' => '',
            'cron' => 'required|not_in:* * * * *',
            'holiday_users' => '',
        ]);

        $task = Task::create($data);

        Session::flash('success', __('admin.successStored'));
        return $request->exit
            ? redirect()->route('admin.tasks.index')
            : redirect()->route('admin.tasks.edit', $task);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        if ($task->cron === '* * * * *') {
            return back();
        }
        return view('admin.tasksForm', [
            'route' => route('admin.tasks.update', $task),
            'method' => 'PUT',
            'task' => $task,
            'groups' => Group::pluck('name', 'id'),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        $data = $request->validate([
            'group_id' => '',
            'title' => 'required',
            'description' => '',
            'description_resource' => '',
            'is_required' => '',
            'is_file_required' => '',
            'cron' => 'required|not_in:* * * * *',
            'holiday_users' => '',
        ]);
        $task->update($data);

        Session::flash('success', __('admin.successUpdated'));
        return $request->exit ? redirect()->route('admin.tasks.index') : back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        if ($task->cron === '* * * * *') {
            return back();
        }

        $task->delete();

        Session::flash('success', __('admin.successDestroyed'));
        return back();
    }
}
