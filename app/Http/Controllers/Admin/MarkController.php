<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\MarksDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\MarkRequest;
use App\Models\City;
use App\Models\Mark;
use Session;
use View;

class MarkController extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        View::share('title', 'Метки');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \App\DataTables\MarksDataTable $dataTable
     * @return \Illuminate\Http\Response
     */
    public function index(MarksDataTable $dataTable)
    {
        return $dataTable->render('components.admin.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.marksForm', [
            'route' => route('admin.marks.store'),
            'mark' => new Mark(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MarkRequest $request)
    {
        $data = $request->validated();

        $mark = Mark::create($data);

        Session::flash('success', __('admin.successStored'));
        return $request->exit
            ? redirect()->route('admin.marks.index')
            : redirect()->route('admin.marks.edit', $mark);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Mark  $mark
     * @return \Illuminate\Http\Response
     */
    public function edit(Mark $mark)
    {
        return view('admin.marksForm', [
            'route' => route('admin.marks.update', $mark),
            'method' => 'PUT',
            'mark' => $mark,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Mark  $mark
     * @return \Illuminate\Http\Response
     */
    public function update(MarkRequest $request, Mark $mark)
    {
        $data = $request->validated();

        $mark->update($data);

        Session::flash('success', __('admin.successUpdated'));
        return $request->exit ? redirect()->route('admin.marks.index') : back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Mark  $mark
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mark $mark)
    {
        $mark->delete();

        Session::flash('success', __('admin.successDestroyed'));
        return back();
    }
}
