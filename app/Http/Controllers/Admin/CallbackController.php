<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\CallbacksDataTable;
use App\Http\Controllers\Controller;
use View;

class CallbackController extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        View::share('title', 'Обратные вызовы');
    }

    /**
     * Display a listing of the resource.
     *
     * @param \App\DataTables\CallbacksDataTable $dataTable
     * @return \Illuminate\Http\Response
     */
    public function index(CallbacksDataTable $dataTable)
    {
        return $dataTable->render('components.admin.index');
    }
}
