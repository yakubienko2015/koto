<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\ProductsDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ProductRequest;
use App\Models\BowlIngredient;
use App\Models\Category;
use App\Models\Mark;
use App\Models\Product;
use App\Models\ProductGroup;
use App\Models\Tag;
use Iiko\Biz\Exception\IikoResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Session;
use View;

class ProductController extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        View::share('title', 'Товары');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ProductsDataTable $dataTable)
    {
        return $dataTable->render('components.admin.index');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function iiko()
    {
        try {
            Product::loadFromIiko();
        } catch (IikoResponseException $e) {
            Session::flash('danger', $e->getMessage());
        }
        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.productsForm', [
            'route' => route('admin.products.store'),
            'product' => new Product(),
            'products' => Product::getProductsForSelect(),
            'categories' => Category::pluck('name', 'id'),
            'productGroups' => ProductGroup::pluck('name', 'id'),
            'bowlIngredients' => BowlIngredient::pluck('name', 'id'),
            'sharpnesses' => Product::getSharpnessesForSelect(),
            'marks' => Mark::pluck('name', 'id'),
            'tags' => Tag::pluck('name', 'name'),
            'measureUnits' => Product::getMeasureUnitsForSelect(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Admin\ProductRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $data = $request->validated();

        $product = Product::create($data);
        $product->saveMeta($data['meta'] ?? []);
        $product->tags()->sync(Tag::findByNames($request->tags));
        $product->subproducts()->sync($request->subproducts);
        $product->marks()->sync($request->marks);
        $product->bowlIngredients()->sync($request->bowl_ingredients ?? []);
        if ($request->hasFile('image')) {
            $product->saveImage($request->file('image'));
        }
        if ($request->hasFile('mob_image')) {
            $product->saveMobImage($request->file('mob_image'));
        }

        Session::flash('success', __('admin.successStored'));
        return $request->exit
            ? redirect()->route('admin.products.index')
            : redirect()->route('admin.products.edit', $product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        // fix subproducts priority
        $subProducts = $product->subproducts->pluck('id');
        $sProdusts = Product::getProductsForSelect($subProducts->push($product->id));
        foreach ($product->subproducts->pluck('name', 'id') as $key => $value) {
            $sProdusts[$key] = $value;
        }

        return view('admin.productsForm', [
            'route' => route('admin.products.update', $product),
            'method' => 'PUT',
            'product' => $product,
            'products' => $sProdusts,
            'categories' => Category::pluck('name', 'id'),
            'productGroups' => ProductGroup::pluck('name', 'id'),
            'bowlIngredients' => BowlIngredient::pluck('name', 'id'),
            'sharpnesses' => Product::getSharpnessesForSelect(),
            'marks' => Mark::pluck('name', 'id'),
            'tags' => Tag::pluck('name', 'name'),
            'measureUnits' => Product::getMeasureUnitsForSelect(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Admin\ProductRequest  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, Product $product)
    {
        $data = $request->validated();

        $product->update($data);
        $product->saveMeta($data['meta'] ?? []);
        $product->tags()->sync(Tag::findByNames($request->tags));
        $product->subproducts()->sync($request->subproducts);
        $product->marks()->sync($request->marks);
        $product->bowlIngredients()->sync($request->bowl_ingredients ?? []);
        if ($request->hasFile('image')) {
            $product->saveImage($request->file('image'));
        }
        if ($request->hasFile('mob_image')) {
            $product->saveMobImage($request->file('mob_image'));
        }

        if ($request->new_work_ingredient && $request->work_ingredient) {
            $product->update([
                'work_ingredients' => array_merge($product->work_ingredients ?? [], [$request->work_ingredient]),
            ]);
        }
        if ($request->remove_work_ingredient !== null) {
            $product->update([
                'work_ingredients' => array_diff_key($product->work_ingredients ?? [], [$request->remove_work_ingredient => '']),
            ]);
        }
        if ($request->improved_product_id) {
            $product->improvedProduct()->update([]
            + ($request->improved_price ? [
                'price' => $request->improved_price,
            ] : [])
            + ($request->improved_old_price ? [
                'old_price' => $request->improved_old_price,
            ] : []));
        }

        Session::flash('success', __('admin.successUpdated'));
        return $request->exit ? redirect()->route('admin.products.index') : back();
    }

    /**
     * Remove the specified resource image.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function removeImage(Product $product, Request $request)
    {
        if ($request->is_mob) {
            Storage::disk('public')->delete($product->is_mob);
            $product->update(['mob_image' => null]);
        } else {
            Storage::disk('public')->delete($product->image);
            $product->update(['image' => null]);
        }

        return response()->json([]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();

        Session::flash('success', __('admin.successDestroyed'));
        return back();
    }
}
