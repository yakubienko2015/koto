<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\CitiesDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CityRequest;
use App\Library\CustomConfig;
use Iiko\Biz\Exception\IikoResponseException;
use App\Models\City;
use Session;
use View;

class CityController extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        View::share('title', 'Города');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \App\DataTables\CitiesDataTable $dataTable
     * @return mixed
     */
    public function index(CitiesDataTable $dataTable)
    {
        return $dataTable->render('components.admin.index');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function iiko()
    {
        try {
            City::loadFromIiko();
        } catch (IikoResponseException $e) {
            Session::flash('danger', $e->getMessage());
        }
        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.citiesForm', [
            'route' => route('admin.cities.store'),
            'city' => new City(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Admin\CityRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CityRequest $request)
    {
        $data = $request->validated();

        $city = City::create($data);

        Session::flash('success', __('admin.successStored'));
        return $request->exit
            ? redirect()->route('admin.cities.index')
            : redirect()->route('admin.cities.edit', $city);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\City  $city
     * @return \Illuminate\Http\Response
     */
    public function edit(City $city)
    {
        CustomConfig::load();

        return view('admin.citiesForm', [
            'route' => route('admin.cities.update', $city),
            'method' => 'PUT',
            'city' => $city,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Admin\CityRequest  $request
     * @param  \App\Models\City  $city
     * @return \Illuminate\Http\Response
     */
    public function update(CityRequest $request, City $city)
    {
        $data = $request->validated();
        $city->update($data);

        $customConfigs = CustomConfig::load();
        foreach ($data['configs'] as $configKey => $configValue) {
            $customConfigs["shop.citied.{$city->id}.$configKey"] = $configValue;
        }
        CustomConfig::store($customConfigs);

        Session::flash('success', __('admin.successUpdated'));
        return $request->exit ? redirect()->route('admin.cities.index') : back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\City  $city
     * @return \Illuminate\Http\Response
     */
    public function destroy(City $city)
    {
        $city->delete();

        Session::flash('success', __('admin.successDestroyed'));
        return back();
    }
}
