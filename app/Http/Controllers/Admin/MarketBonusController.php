<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\MarketBonusesDataTable;
use App\Http\Controllers\Controller;
use View;

class MarketBonusController extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        View::share('title', 'QR приложение');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \App\DataTables\MarketBonusesDataTable $dataTable
     * @return \Illuminate\Http\Response
     */
    public function index(MarketBonusesDataTable $dataTable)
    {
        return $dataTable->render('components.admin.index');
    }
}
