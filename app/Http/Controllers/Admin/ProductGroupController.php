<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\ProductGroupsDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ProductGroupRequest;
use App\Models\Product;
use App\Models\ProductGroup;
use Iiko\Biz\Exception\IikoResponseException;
use Illuminate\Http\Request;
use Session;
use View;

class ProductGroupController extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        View::share('title', 'Группы');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ProductGroupsDataTable $dataTable)
    {
        return $dataTable->render('components.admin.index');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function iiko()
    {
        try {
            ProductGroup::loadFromIiko();
        } catch (IikoResponseException $e) {
            Session::flash('danger', $e->getMessage());
        }
        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.productGroupsForm', [
            'route' => route('admin.product-groups.store'),
            'productGroup' => new ProductGroup(),
            'products' => Product::getProductsForSelect(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductGroupRequest $request)
    {
        $data = $request->validated();

        $productGroup = ProductGroup::create($data);
        $productGroup->products()->saveMany(Product::whereIn('id', $request->products)->get());

        Session::flash('success', __('admin.successStored'));
        return $request->exit
            ? redirect()->route('admin.product-groups.index')
            : redirect()->route('admin.product-groups.edit', $productGroup);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProductGroup  $productGroup
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductGroup $productGroup)
    {
        return view('admin.productGroupsForm', [
            'route' => route('admin.product-groups.update', $productGroup),
            'method' => 'PUT',
            'productGroup' => $productGroup,
            'products' => Product::getProductsForSelect(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProductGroup  $productGroup
     * @return \Illuminate\Http\Response
     */
    public function update(ProductGroupRequest $request, ProductGroup $productGroup)
    {
        $data = $request->validated();

        $productGroup->update($data);
        $productGroup->products()->saveMany(Product::whereIn('id', $request->products)->get());

        Session::flash('success', __('admin.successUpdated'));
        return $request->exit ? redirect()->route('admin.product-groups.index') : back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductGroup  $productGroup
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductGroup $productGroup)
    {
        $productGroup->delete();

        Session::flash('success', __('admin.successDestroyed'));
        return back();
    }
}
