<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\CategoriesDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CategoryRequest;
use Iiko\Biz\Exception\IikoResponseException;
use App\Models\Category;
use Session;
use View;

class CategoryController extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        View::share('title', 'Категории');
    }

    /**
     * Display a listing of the resource.
     *
     * @param \App\DataTables\CategoriesDataTable $dataTable
     * @return \Illuminate\Http\Response
     */
    public function index(CategoriesDataTable $dataTable)
    {
        return $dataTable->render('components.admin.index');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function iiko()
    {
        try {
            Category::loadFromIiko();
        } catch (IikoResponseException $e) {
            Session::flash('danger', $e->getMessage());
        }
        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categoriesForm', [
            'route' => route('admin.categories.store'),
            'category' => new Category(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Admin\CategoryRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        $data = $request->validated();

        $category = Category::create($data);
        $category->saveMeta($data['meta'] ?? []);

        Session::flash('success', __('admin.successStored'));
        return $request->exit
            ? redirect()->route('admin.categories.index')
            : redirect()->route('admin.categories.edit', $category);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('admin.categoriesForm', [
            'route' => route('admin.categories.update', $category),
            'method' => 'PUT',
            'category' => $category,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Admin\CategoryRequest  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, Category $category)
    {
        $data = $request->validated();

        $category->update($data);
        $category->saveMeta($data['meta'] ?? []);

        Session::flash('success', __('admin.successUpdated'));
        return $request->exit ? redirect()->route('admin.categories.index') : back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $category->delete();

        Session::flash('success', __('admin.successDestroyed'));
        return back();
    }
}
