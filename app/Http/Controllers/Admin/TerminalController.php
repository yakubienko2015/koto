<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\TerminalsDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\TerminalRequest;
use App\Models\Product;
use App\Models\ProductGroup;
use Iiko\Biz\Exception\IikoResponseException;
use App\Models\Terminal;
use App\Models\WorkPlace;
use Session;
use View;

class TerminalController extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        View::share('title', 'Точки выдачи');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \App\DataTables\TerminalsDataTable $dataTable
     * @return mixed
     */
    public function index(TerminalsDataTable $dataTable)
    {
        return $dataTable->render('components.admin.index');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function iiko()
    {
        try {
            Terminal::loadFromIiko();
        } catch (IikoResponseException $e) {
            Session::flash('danger', $e->getMessage());
        }
        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.terminalsForm', [
            'route' => route('admin.terminals.store'),
            'terminal' => new Terminal(),
            'statuses' => Terminal::getStatusesForSelect(),
            'users' => Terminal::getUsersForSelect(),
            'products' => Product::getProductsForSelect(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Admin\TerminalRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TerminalRequest $request)
    {
        $data = $request->validated();

        $terminal = Terminal::create($data + [
            'city_id' => app('city')->id,
        ]);
        $terminal->users()->sync($request->users ?? []);

        Session::flash('success', __('admin.successStored'));
        return $request->exit
            ? redirect()->route('admin.terminals.index')
            : redirect()->route('admin.terminals.edit', $terminal);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Terminal  $terminal
     * @return \Illuminate\Http\Response
     */
    public function edit(Terminal $terminal)
    {
        return view('admin.terminalsForm', [
            'route' => route('admin.terminals.update', $terminal),
            'method' => 'PUT',
            'terminal' => $terminal,
            'statuses' => Terminal::getStatusesForSelect(),
            'users' => Terminal::getUsersForSelect(),
            'products' => Product::getProductsForSelect(),
            'productGroups' => ProductGroup::pluck('name', 'id'),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Admin\TerminalRequest  $request
     * @param  \App\Models\Terminal  $terminal
     * @return \Illuminate\Http\Response
     */
    public function update(TerminalRequest $request, Terminal $terminal)
    {
        $data = $request->validated();

        $terminal->update($data);
        if ($request->new_work_place) {
            $terminal->workPlaces()->create([
                'name' => $request->new_place_name,
            ]);
        }
        foreach ($request->work_places ?? [] as $workPlaceId => $workPlaceData) {
            $workPlace = WorkPlace::find($workPlaceId);
            $workPlace->update($workPlaceData);
            $workPlace->productGroups()->sync($workPlaceData['product_groups'] ?? []);
            $productGroups = ProductGroup::whereIn('id', $workPlaceData['product_groups'] ?? []);
            $products = collect([]);
            foreach ($productGroups->get() as $group) {
                $products = $products->merge($group->products);
            }
            $workPlace->products()->sync($products->pluck('id'));
        }
        if ($request->remove_place) {
            $terminal->workPlaces()->whereId($request->remove_place)->delete();
        }
        $terminal->users()->sync($request->users ?? []);

        Session::flash('success', __('admin.successUpdated'));
        return $request->exit ? redirect()->route('admin.terminals.index') : back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Terminal  $terminal
     * @return \Illuminate\Http\Response
     */
    public function destroy(Terminal $terminal)
    {
        $terminal->delete();

        Session::flash('success', __('admin.successDestroyed'));
        return back();
    }
}
