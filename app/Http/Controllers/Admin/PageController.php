<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\PagesDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\PageRequest;
use App\Models\Page;
use Session;
use View;

class PageController extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        View::share('title', 'Страницы');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \App\DataTables\PagesDataTable $dataTable
     * @return \Illuminate\Http\Response
     */
    public function index(PagesDataTable $dataTable)
    {
        return $dataTable->render('components.admin.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pagesForm', [
            'route' => route('admin.pages.store'),
            'page' => new Page(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Admin\PageRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PageRequest $request)
    {
        $data = $request->validated();

        $page = Page::create($data);
        $page->saveMeta($data['meta'] ?? []);
        if ($request->hasFile('image')) {
            $page->saveImage($request->file('image'));
        }

        Session::flash('success', __('admin.successStored'));
        return $request->exit
            ? redirect()->route('admin.pages.index')
            : redirect()->route('admin.pages.edit', $page);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        return view('admin.pagesForm', [
            'route' => route('admin.pages.update', $page),
            'method' => 'PUT',
            'page' => $page,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Admin\PageRequest  $request
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(PageRequest $request, Page $page)
    {
        $data = $request->validated();

        $page->update($data);
        $page->saveMeta($data['meta'] ?? []);
        if ($request->hasFile('image')) {
            $page->saveImage($request->file('image'));
        }

        Session::flash('success', __('admin.successUpdated'));
        return $request->exit ? redirect()->route('admin.pages.index') : back();
    }

    /**
     * Remove the specified resource image.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function removeImage(Page $page)
    {
        $page->update(['image' => null]);

        return response()->json([]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        $page->delete();

        Session::flash('success', __('admin.successDestroyed'));
        return back();
    }
}
