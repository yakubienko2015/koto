<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\BowlIngredientsDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\BowlIngredientRequest;
use App\Models\BowlIngredient;
use App\Models\Product;
use Session;
use View;

class BowlIngredientController extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        View::share('title', 'Конструктор боулов');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(BowlIngredientsDataTable $dataTable)
    {
        return $dataTable->render('components.admin.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.bowlIngredientsForm', [
            'route' => route('admin.bowl-ingredients.store'),
            'bowlIngredient' => new BowlIngredient(),
            'products' => Product::whereCategoryId(config('site.bowl_constructors_category_id'))->pluck('name', 'id'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Admin\BowlIngredientRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BowlIngredientRequest $request)
    {
        $data = $request->validated();

        $bowlIngredient = BowlIngredient::create($data);
        $bowlIngredient->products()->sync($request->products);

        Session::flash('success', __('admin.successStored'));
        return $request->exit
            ? redirect()->route('admin.bowl-ingredients.index')
            : redirect()->route('admin.bowl-ingredients.edit', $bowlIngredient);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\bowlIngredient  $bowlIngredient
     * @return \Illuminate\Http\Response
     */
    public function edit(bowlIngredient $bowlIngredient)
    {
        return view('admin.bowlIngredientsForm', [
            'route' => route('admin.bowl-ingredients.update', $bowlIngredient),
            'method' => 'PUT',
            'bowlIngredient' => $bowlIngredient,
            'products' => Product::whereCategoryId(config('site.bowl_constructors_category_id'))->pluck('name', 'id'),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Admin\BowlIngredientRequest  $request
     * @param  \App\Models\bowlIngredient  $bowlIngredient
     * @return \Illuminate\Http\Response
     */
    public function update(BowlIngredientRequest $request, BowlIngredient $bowlIngredient)
    {
        $data = $request->validated();

        $bowlIngredient->update($data);
        $bowlIngredient->products()->sync($request->products);

        Session::flash('success', __('admin.successUpdated'));
        return $request->exit ? redirect()->route('admin.bowl-ingredients.index') : back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BowlIngredient  $bowlIngredient
     * @return \Illuminate\Http\Response
     */
    public function destroy(BowlIngredient $bowlIngredient)
    {
        $bowlIngredient->delete();

        Session::flash('success', __('admin.successDestroyed'));
        return back();
    }
}
