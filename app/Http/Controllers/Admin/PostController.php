<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\PostsDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\PostRequest;
use App\Models\City;
use App\Models\Post;
use Session;
use View;

class PostController extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        View::share('title', 'Блог');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \App\DataTables\PostsDataTable $dataTable
     * @return \Illuminate\Http\Response
     */
    public function index(PostsDataTable $dataTable)
    {
        return $dataTable->render('components.admin.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.postsForm', [
            'route' => route('admin.posts.store'),
            'post' => new Post(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        $data = $request->validated();

        $post = Post::create($data);
        $post->saveMeta($data['meta'] ?? []);
        if ($request->hasFile('image')) {
            $post->saveImage($request->file('image'));
        }

        Session::flash('success', __('admin.successStored'));
        return $request->exit
            ? redirect()->route('admin.posts.index')
            : redirect()->route('admin.posts.edit', $post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        return view('admin.postsForm', [
            'route' => route('admin.posts.update', $post),
            'method' => 'PUT',
            'post' => $post,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, Post $post)
    {
        $data = $request->validated();

        $post->update($data);
        $post->saveMeta($data['meta'] ?? []);
        if ($request->hasFile('image')) {
            $post->saveImage($request->file('image'));
        }

        Session::flash('success', __('admin.successUpdated'));
        return $request->exit ? redirect()->route('admin.posts.index') : back();
    }

    /**
     * Remove the specified resource image.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function removeImage(Post $post)
    {
        $post->update(['image' => null]);

        return response()->json([]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete();

        Session::flash('success', __('admin.successDestroyed'));
        return back();
    }
}
