<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\OpinionsDataTable;
use App\Http\Controllers\Controller;
use View;

class OpinionController extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        View::share('title', 'Отзывы');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \App\DataTables\OpinionsDataTable $dataTable
     * @return \Illuminate\Http\Response
     */
    public function index(OpinionsDataTable $dataTable)
    {
        return $dataTable->render('components.admin.index');
    }
}
