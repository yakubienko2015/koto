<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\AchievementsDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AchievementRequest;
use App\Models\Achievement;
use Session;
use View;

class AchievementController extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        View::share('title', 'Ачивки');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(AchievementsDataTable $dataTable)
    {
        return $dataTable->render('components.admin.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.achievementsForm', [
            'route' => route('admin.achievements.store'),
            'achievement' => new Achievement(),
            'categories' => collect(app('iiko')
                ->OrganizationsApi()
                ->getGuestCategories(app('city')->organization_uuid) ?? [])
                ->where('isActive', true)
                ->pluck('name', 'id'),
            'types' => [
                Achievement::TYPE_LOYALTY => 'Лояльность',
                Achievement::TYPE_FREQUENCY => 'Частота',
                Achievement::TYPE_SUM => 'Сумма',
            ],
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Admin\AchievementRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AchievementRequest $request)
    {
        $data = $request->validated();

        $achievement = Achievement::create($data);
        if ($request->hasFile('image')) {
            $achievement->saveImage($request->file('image'));
        }

        Session::flash('success', __('admin.successStored'));
        return $request->exit
            ? redirect()->route('admin.achievements.index')
            : redirect()->route('admin.achievements.edit', $achievement);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Achievement  $achievement
     * @return \Illuminate\Http\Response
     */
    public function edit(Achievement $achievement)
    {
        return view('admin.achievementsForm', [
            'route' => route('admin.achievements.update', $achievement),
            'method' => 'PUT',
            'achievement' => $achievement,
            'categories' => collect(app('iiko')
                ->OrganizationsApi()
                ->getGuestCategories(app('city')->organization_uuid) ?? [])
                ->where('isActive', true)
                ->pluck('name', 'id'),
            'types' => [
                Achievement::TYPE_LOYALTY => 'Лояльность',
                Achievement::TYPE_FREQUENCY => 'Частота',
                Achievement::TYPE_SUM => 'Сумма',
            ],
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Admin\AchievementRequest  $request
     * @param  \App\Models\Achievement  $achievement
     * @return \Illuminate\Http\Response
     */
    public function update(AchievementRequest $request, Achievement $achievement)
    {
        $data = $request->validated();

        $achievement->update($data);
        if ($request->hasFile('image')) {
            $achievement->saveImage($request->file('image'));
        }

        Session::flash('success', __('admin.successUpdated'));
        return $request->exit ? redirect()->route('admin.achievements.index') : back();
    }

    /**
     * Remove the specified resource image.
     *
     * @param  \App\Models\Achievement  $achievement
     *
     * @return \Illuminate\Http\Response
     */
    public function removeImage(Achievement $achievement)
    {
        $achievement->update(['image' => null]);

        return response()->json([]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Achievement  $achievement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Achievement $achievement)
    {
        $achievement->delete();

        Session::flash('success', __('admin.successDestroyed'));
        return back();
    }
}
