<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Imports\UsersImport;
use App\Models\Category;
use App\Models\Page;
use App\Models\Product;
use App\Models\User;
use App\Notifications\CustomerNotification;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use NotificationChannels\Fcm\Exceptions\CouldNotSendNotification;
use Session;
use View;

class NotificationsController extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        View::share('title', 'Уведомления');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \App\DataTables\OpinionsDataTable $dataTable
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect()->route('admin.notifications.create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.notificationsForm', [
            'route' => route('admin.notifications.store'),
            'users' => User::whereNotNull('device_token')->get()->pluck('phone_name', 'id'),
            'pages' => Page::inCity()->onApi()->pluck('name', 'slug'),
            'categories' => app('city')->getCategories()->pluck('name', 'slug'),
            'products' => Product::active()->pluck('name', 'id'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $users = User::whereIn('id', $request->users ?? []);
        $phones = collect();
        if ($request->hasFile('import_users')) {
            $file = $request->file('import_users');
            $phones = Excel::toCollection(new UsersImport, $file)->first()->map(function ($row) {
                return $row->first();
            });
            $users->orWhereIn('phone', $phones);
        }
        if ($request->exit == 3) {
            Session::flash('success', __('Количество строк в файле: :0', [
                $phones->count(),
            ]));
            $inputs = $request->input();
            $inputs['users'] = $users->pluck('id')->toArray();
            return back()
                ->withInput($inputs);
        }
        $successCount = 0;
        $errorCount = 0;
        $notifyData = [
            'title' => $request->title,
            'body' => $request->body,
            'link' => $request->link ?? '',
            'page' => $request->page_slug,
            'category' => $request->category_slug,
            'product' => $request->product_slug,
        ];
        foreach ($users->get() as $user) {
            try {
                $user->notify(new CustomerNotification($notifyData));
                $successCount++;
            } catch (CouldNotSendNotification $th) {
                $user->update([
                    'device_token' => null,
                ]);
                $errorCount++;
            }
        }
        Session::flash('success', __('Успешно отправлено: :0, проблемно: :1.', [
                $successCount, $errorCount,
            ]));
        return back();
    }
}
