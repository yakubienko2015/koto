<?php

namespace App\Notifications;

use Illuminate\Notifications\Notification;
use NotificationChannels\Fcm\FcmChannel;
use NotificationChannels\Fcm\FcmMessage;
use NotificationChannels\Fcm\Resources\AndroidConfig;
use NotificationChannels\Fcm\Resources\AndroidNotification;
use NotificationChannels\Fcm\Resources\ApnsConfig;
use NotificationChannels\Fcm\Resources\ApnsFcmOptions;
use NotificationChannels\Fcm\Resources\Notification as ResourcesNotification;
use NotificationChannels\Fcm\Resources\WebpushConfig;
use NotificationChannels\Fcm\Resources\WebpushFcmOptions;

class PreOrderNotification extends Notification
{
    private $data = [];

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', FcmChannel::class];
    }

    /**
     * @param [type] $notifiable
     *
     * @return void
     */
    public function toFcm($notifiable)
    {
        $notificationCount = $notifiable->notifications()->whereNull('read_at')->count();
        return FcmMessage::create()
            ->setData([
                'order_id' => (string) $this->data['order_id'],
                'type' => 'App\Notifications\PreOrderNotification',
                'badge' => (string) $notificationCount,
            ])
            ->setNotification(\NotificationChannels\Fcm\Resources\Notification::create()
                ->setTitle($this->data['title'])
                ->setBody($this->data['body'] ?? '')
                // ->setImage(asset('images/logo192.png'))
            )
            ->setWebpush(WebpushConfig::create()
                ->setFcmOptions(WebpushFcmOptions::create()
                    ->setLink($this->data['link'] ?? '')
                )
            )
            ->setAndroid(
                AndroidConfig::create()
                    ->setNotification(AndroidNotification::create()->setSound('default'))
            )
            ->setApns(
                ApnsConfig::create()
                    ->setFcmOptions(ApnsFcmOptions::create()->setAnalyticsLabel('analytics_ios'))
                    ->setPayload(['aps' => ['sound' => 'default', 'badge' => $notificationCount]]));
    }

    /**
    * Get the array representation of the notification.
    *
    * @param  mixed  $notifiable
    * @return array
    */
    public function toArray($notifiable)
    {
        return $this->data;
    }
}
