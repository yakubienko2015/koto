<?php

namespace App\Rules;

use App\Models\SmsConfirmation;
use Illuminate\Contracts\Validation\Rule;

class SmsConfirm implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $phone
     *
     * @return bool
     */
    public function passes($attribute, $phone)
    {
        $sms = SmsConfirmation::where('phone', $phone)
            ->where('code', request()->get('code'))
            ->where('updated_at', '>', now()->subMinutes(20))
            ->first();

        if ($phone === '+380970000000') {
            $sms = SmsConfirmation::where('phone', $phone)
                ->where('code', request()->get('code'))
                ->first();
        }

        return (bool) $sms;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('auth.code.wrong_code');
    }
}
