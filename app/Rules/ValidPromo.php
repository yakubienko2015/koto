<?php

namespace App\Rules;

use App\Models\Promo;
use Illuminate\Contracts\Validation\Rule;

class ValidPromo implements Rule
{
    private $promoModel;
    private $sum;

    public function __construct($sum = null)
    {
        $this->sum = $sum;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $promo
     *
     * @return bool
     */
    public function passes($attribute, $promo)
    {
        $this->promoModel = Promo::whereCode($promo)
            ->whereIsActive(true)
            ->first();
        if ($this->promoModel && $this->promoModel->min_basket_sum > $this->sum) {
            return false;
        }
        return app('order')
            ->products->pluck('id')
            ->contains($this->promoModel->product_id ?? 0);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        if ($this->promoModel) {
            if ($this->promoModel->min_basket_sum > $this->sum) {
                return __('validation.custom.order.promoSum.gte', [
                    'promo' => $this->promoModel->code,
                    'value' => (int) $this->promoModel->min_basket_sum,
                ]);
            }
            return __('validation.invalid_promo_in_basket');
        }
        return __('validation.invalid_promo');
    }
}
