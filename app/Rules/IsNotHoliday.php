<?php

namespace App\Rules;

use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class IsNotHoliday implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $date
     *
     * @return bool
     */
    public function passes($attribute, $date)
    {
        $holidays = config('shop.holidays', []);
        return ! in_array($date->format('Y-m-d'), $holidays);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('validation.is_not_holiday');
    }
}
