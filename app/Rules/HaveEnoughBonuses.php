<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class HaveEnoughBonuses implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($value) {
            return request()->user()->getIikoBonuses() >= 200;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('validation.have_not_enough_bonuses');
    }
}
