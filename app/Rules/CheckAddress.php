<?php

namespace App\Rules;

use Iiko\Biz\Exception\IikoResponseException;
use Illuminate\Contracts\Validation\Rule;

class CheckAddress implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $date
     *
     * @return bool
     */
    public function passes($attribute, $address)
    {
        if (! config('shop.make_order_with_iiko')) {
            return true;
        }
        if (app('city')->getStreets()->search($address['street'] ?? '') == false) {
            return false;
        }
        try {
            $addressResult = app('iiko')->OrdersApi()->checkAddress(
                app('city')->organization_uuid,
                $address
            );
            return $addressResult['addressInZone'] ?? false;
        } catch (IikoResponseException $e) {
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('validation.check_address');
    }
}
