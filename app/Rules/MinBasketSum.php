<?php

namespace App\Rules;

use App\Models\Promo;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class MinBasketSum implements Rule
{
    private $promo;

    public function __construct($promo = null)
    {
        $this->promo = $promo;
        if ($promo) {
            $this->promo = Promo::whereCode($promo)
                ->first();
        }
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $si,
     *
     * @return bool
     */
    public function passes($attribute, $sum)
    {
        return $this->promo
            ? ($sum >= $this->promo->min_basket_sum)
            : ($sum >= config('shop.min_basket_sum'));
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->promo
            ? __('validation.custom.order.promoSum.gte', [
                'promo' => $this->promo->code,
                'value' => (int) $this->promo->min_basket_sum,
            ])
            : __('validation.custom.order.sum.gte', [
                'value' => (int) config('shop.min_basket_sum'),
            ]);
    }
}
