<?php

namespace App\Rules;

use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class IsNotBusy implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $date
     *
     * @return bool
     */
    public function passes($attribute, $date)
    {
        return Order::whereBetween('ordered_at', [
            (new Carbon($date))->subHours(1),
            $date,
        ])->whereIn('status', [
            Order::ORDER_STATUS_IN_PROGRESS,
            Order::ORDER_STATUS_READY,
            Order::ORDER_STATUS_ON_WAY,
            Order::ORDER_STATUS_WAITING,
        ])->sum('sum') < 5000;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('validation.is_overloaded');
    }
}
