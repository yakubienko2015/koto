<?php

namespace App\Rules;

use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class InWorkTime implements Rule
{
    private $isPreOrder;

    public function __construct($isPreOrder = false)
    {
        $this->isPreOrder = $isPreOrder;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $date
     *
     * @return bool
     */
    public function passes($attribute, $date)
    {
        $addMinutes = $this->isPreOrder ? 39 : 0;
        $workTime = app('city')->work_time[$date->dayOfWeekIso - 1];
        $workFrom = Carbon::createFromFormat('H:i', $workTime['from']);
        $workTo = Carbon::createFromFormat('H:i', $workTime['to']);
        return ! $workTime['closed']
            && Carbon::createFromTime($date->hour, $date->minute)->between(
                $workFrom->addMinutes($addMinutes),
                $workTo->addMinutes($addMinutes))
            && ($this->isPreOrder ? $date->greaterThan(now()) : true);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('validation.in_work_time');
    }
}
