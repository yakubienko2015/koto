<?php

namespace App\Rules;

use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class IsNotOverload implements Rule
{
    private $isPreOrder;

    public function __construct($isPreOrder = false)
    {
        $this->isPreOrder = $isPreOrder;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $date
     *
     * @return bool
     */
    public function passes($attribute, $date)
    {
        if (app('city')->getDeliveryTime() == 0
            && $date->format('Y-m-d') == now()->format('Y-m-d')
            && $this->isPreOrder === false) {
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('validation.is_overloaded');
    }
}
