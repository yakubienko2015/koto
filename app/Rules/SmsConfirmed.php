<?php

namespace App\Rules;

use App\Models\SmsConfirmation;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class SmsConfirmed implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $phone
     * 
     * @return bool
     */
    public function passes($attribute, $phone)
    {
        $sms = SmsConfirmation::where('phone', $phone)->first();
        return $sms && $sms->is_confirmed;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('auth.sms.no_confirmed');
    }
}
