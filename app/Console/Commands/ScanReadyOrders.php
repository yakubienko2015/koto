<?php

namespace App\Console\Commands;

use App\Models\Order;
use Illuminate\Console\Command;

class ScanReadyOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shop:scan-ready-orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $orders = Order::whereNotNull('uuid')
            ->whereStatus(Order::ORDER_STATUS_READY);

        foreach ($orders->get() as $order) {
            try {
                $iikoOrder = $order->getIikoInfo();
                $order->update([
                    'status' => config('shop.iiko_statuses')[$iikoOrder['status']],
                ]);
            } catch (\Throwable $thr) {
                logger('Error on update iiko for "ready" order! ' . $thr->getMessage());
            }
        }
    }
}
