<?php

namespace App\Console\Commands;

use App\Library\Meta;
use App\Models\Category;
use App\Models\City;
use App\Models\Page;
use App\Models\Post;
use App\Models\Product;
use Illuminate\Console\Command;

class FixMetas extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix-metas';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        foreach (City::all() as $city) {
            foreach (Product::get() as $product) {
                $meta = Meta::firstOrCreate([
                    'metaable_type' => 'App\Models\Product',
                    'metaable_id' => $product->id,
                    'city_id' => $city->id,
                ]);
                $meta->is_active = $product->is_active;
                $meta->setTranslations('title', ['ru' => null, 'uk' => null]);
                $meta->setTranslations('description', ['ru' => null, 'uk' => null]);
                $meta->setTranslations('content', $product->getTranslations('description'));
                $meta->save();
            }
            foreach (Post::get() as $post) {
                $meta = Meta::firstOrCreate([
                    'metaable_type' => 'App\Models\Post',
                    'metaable_id' => $post->id,
                    'city_id' => $city->id,
                ]);
                $meta->is_active = $post->is_active;
                $meta->setTranslations('title', ['ru' => null, 'uk' => null]);
                $meta->setTranslations('description', ['ru' => null, 'uk' => null]);
                $meta->setTranslations('content', $post->getTranslations('content'));
                $meta->save();
            }
            foreach (Category::get() as $category) {
                $meta = Meta::firstOrCreate([
                    'metaable_type' => 'App\Models\Category',
                    'metaable_id' => $category->id,
                    'city_id' => $city->id,
                ]);
                $meta->is_active = $category->is_active;
                $meta->setTranslations('content', $category->getTranslations('description'));
                $meta->save();
            }
            foreach (Page::get() as $page) {
                $meta = Meta::firstOrCreate([
                    'metaable_type' => 'App\Models\Page',
                    'metaable_id' => $page->id,
                    'city_id' => $city->id,
                ]);
                $meta->is_active = $page->is_active;
                $meta->setTranslations('content', $page->getTranslations('content'));
                $meta->save();
            }
        }
        return 0;
    }
}
