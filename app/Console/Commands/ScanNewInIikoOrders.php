<?php

namespace App\Console\Commands;

use App\Models\Order;
use Iiko\Biz\Exception\IikoResponseException;
use Illuminate\Console\Command;

class ScanNewInIikoOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shop:scan-new-iiko-orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $iikoOrders = app('iiko')
                ->OrdersApi()
                ->getDeliveryOrders(app('city')->organization_uuid, [
                    'dateFrom' => now()->format('Y-m-d'),
                    'dateTo' => now()->format('Y-m-d'),
                ])['deliveryOrders'] ?? [];
        } catch (IikoResponseException $e) {
            logger('iiko error on scan new orders', [$e->getMessage()]);
            $iikoOrders = [];
        }
        $iikoOrder = collect($iikoOrders);

        $siteOrders = Order::whereDate('ordered_at', '=', now())->pluck('uuid');
        $newOrders = $iikoOrder->whereNotIn('orderId', $siteOrders)
            ->whereNotIn('status', ['Новая', 'Отменена']);

        foreach ($newOrders as $order) {
            $newOrder = new Order();
            $newOrder->loadFromIiko($order);
            $newOrder->save();

            $newOrder->attachProducts(Order::convertIikoItems($order['items']));

            $newOrder->update([
                'status' => config('shop.iiko_statuses')[$order['status']],
            ]);
        }
    }
}
