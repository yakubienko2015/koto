<?php

namespace App\Console\Commands;

use App\Models\Pin;
use Illuminate\Console\Command;

class ScanReturnCourier extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'track:couriers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Pin::where('courier_must_return_at', '<=', now())->update([
            'courier_must_return_at' => null,
            'courier_status' => Pin::COURIER_STATUS_READY,
        ]);
        return 0;
    }
}
