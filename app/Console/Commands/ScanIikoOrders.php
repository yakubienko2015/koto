<?php

namespace App\Console\Commands;

use App\Models\Order;
use Iiko\Biz\Exception\IikoResponseException;
use Illuminate\Console\Command;

class ScanIikoOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shop:scan-iiko-orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $iikoOrders = app('iiko')
                ->OrdersApi()
                ->getDeliveryOrders(app('city')->organization_uuid, [
                    'dateFrom' => now()->format('Y-m-d'),
                    'dateTo' => now()->format('Y-m-d'),
                    'request_timeout' => '0:0:30',
                ])['deliveryOrders'] ?? [];
        } catch (IikoResponseException $e) {
            logger()->channel('orderStatuses')->info('ScanIikoOrders:', ['iiko error', $e->getMessage()]);
            return 1001;
        }

        $all = count($iikoOrders);
        $new = 0;
        foreach ($iikoOrders as $iikoOrder) {

            if (microtime(true)-LARAVEL_START > 60) {
                logger()->channel('orderStatuses')->info('ScanIikoOrders:', ['time expired & break for next update']);
                break;
            }

            $siteOrder = Order::whereUuid($iikoOrder['orderId'])->first();
            if ($siteOrder) {
                $siteOrder->updateFromIiko($iikoOrder);
            } else {
                Order::createFromIiko($iikoOrder);
                ++$new;
            }
        }
        logger()->channel('orderStatuses')->info('ScanIikoOrders:', [
            "All: {$all}. New: {$new}",
            array_column($iikoOrders, 'number'),
            microtime(true)-LARAVEL_START
        ]);
        return 0;
    }
}
