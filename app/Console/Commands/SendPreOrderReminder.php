<?php

namespace App\Console\Commands;

use App\Models\Order;
use Illuminate\Console\Command;

class SendPreOrderReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shop:pre-orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $ordersFirst = Order::where('data', 'like', '%"pre_order":true%')
            ->whereStatus(Order::ORDER_STATUS_NEW)
            ->whereBetween('ordered_at', [
                now()->addHours(2)->addMinutes(55),
                now()->addHours(3),
            ])
            ->whereNotNull('number')
            ->get();

        foreach ($ordersFirst as $order) {
            $order->sendPreOrderNotify();
        }

        $ordersSecond = Order::where('data', 'like', '%"pre_order":true%')
            ->whereStatus(Order::ORDER_STATUS_NEW)
            ->whereBetween('ordered_at', [
                now()->addHours(1)->addMinutes(55),
                now()->addHours(2),
            ])
            ->whereNotNull('number')
            ->get();

        foreach ($ordersSecond as $order) {
            $order->sendSecondPreOrderNotify();
        }
        return 0;
    }
}
