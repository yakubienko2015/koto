<?php

namespace App\Console\Commands;

use App\Models\Terminal;
use Illuminate\Console\Command;

class ResetTerminalStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shop:reset-terminal-status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Terminal::where('status', '>', Terminal::STATUS_LOW)
            ->update(['status' => Terminal::STATUS_LOW]);

        return 0;
    }
}
