<?php

namespace App\Console\Commands;

use App\Events\UpdateOrder;
use App\Models\Order;
use Illuminate\Console\Command;

class ScanReadyToCookOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shop:scan-ready-to-cook-orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $orders = Order::where('cook_at', null)
            ->whereDate('ordered_at', '=', now()->format('Y-m-d'))
            ->where('ordered_at', '<', now()->addMinutes(60))
            ->get();

        foreach ($orders as $order) {
            $order->update([
                'cook_at' => now(),
            ]);
            event(new UpdateOrder($order));
        }
        return 0;
    }
}
