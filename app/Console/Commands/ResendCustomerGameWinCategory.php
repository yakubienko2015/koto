<?php

namespace App\Console\Commands;

use App\Models\Order;
use Illuminate\Console\Command;

class ResendCustomerGameWinCategory extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shop:resend-game-win-category';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Order::where('updated_at', '<', now()->subHours(3))
            ->where('updated_at', '>', now()->subDays(2))
            ->where('status', Order::ORDER_STATUS_CLOSED)
            ->where('data', 'like', '%"game_score_sended":false%')
            ->get()->each->sendGameScore();

        return 0;
    }
}
