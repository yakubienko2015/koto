<?php

namespace App\Console\Commands;

use App\Library\TelegramBotHelper;
use App\Models\TaskUser;
use Illuminate\Console\Command;
use Telegram;

class TelegramBotHundler extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'telegram:handle';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Handle long polling updates';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $updates = Telegram::commandsHandler(false, ['timeout' => 30]);
        foreach ($updates as $update) {
            TelegramBotHelper::analizUpdateForQuery($update);
            TelegramBotHelper::analizMessage($update->getMessage());
        }
        $this->info('Updates amount:' . count($updates));
    }
}
