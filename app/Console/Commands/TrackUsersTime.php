<?php

namespace App\Console\Commands;

use App\Models\City;
use App\Models\Timer;
use Illuminate\Console\Command;

class TrackUsersTime extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'track:users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Track spended time by users using pusher';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach (City::pluck('id') as $cityId) {
            Timer::where('stopped_at', null)
                ->where('updated_at', '<', now()->subSeconds(50))
                ->get()
                ->map(function ($timer) {
                    $timer->stop();
                });
        }
    }
}
