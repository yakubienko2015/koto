<?php

namespace App\Console\Commands;

use App\Models\Order;
use Illuminate\Console\Command;

class UpdateWorkPlaces extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shop:update-work-places';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Order::where('status', Order::ORDER_STATUS_IN_PROGRESS)
            ->whereDate('cook_at', '=', now()->format('Y-m-d'))
            ->orderBy('id', 'desc')
            ->get()
            ->each->updateWorkPlaces();
        return 0;
    }
}
