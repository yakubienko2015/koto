<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class SearchBirthdayUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shop:birthdays';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'search for birthdays';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        User::role('customer')
            ->whereDate('next_birthday', now()->addDays(2))
            ->get()->filter(function ($user) {
                return $user->getIikoBonuses(false) >= 400;
            })->each->sendBirthdayNotify();

        return 0;
    }
}
