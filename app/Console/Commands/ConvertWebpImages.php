<?php

namespace App\Console\Commands;

use App\Models\Post;
use App\Models\Product;
use App\Models\Slide;
use Illuminate\Console\Command;
use Intervention\Image\ImageManagerStatic;

class ConvertWebpImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shop:webp-images';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        foreach (Product::all() as $product) {
            try {
                $image = ImageManagerStatic::make(storage_path('app/public/products/' . $product->id . '.png'));
                $image->encode('webp')->resize(235, 143)->save(storage_path('app/public/products/' . $product->id . '_medium.webp'));
                $image->encode('webp')->resize(148, 90)->save(storage_path('app/public/products/' . $product->id . '_small.webp'));
            } catch (\Throwable $th) {
                //throw $th;
            }
        }
        foreach (Slide::all() as $slide) {
            $imagePath = explode('/', $slide->image);
            try {
                ImageManagerStatic::make(
                    storage_path('app/public/slides/' . array_pop($imagePath))
                )->encode('webp')
                ->save(storage_path("app/public/slides/{$slide->id}.webp"));
            } catch (\Throwable $th) {
                //throw $th;
            }
        }
        foreach (Post::all() as $post) {
            $imagePath = explode('/', $post->image);
            try {
                ImageManagerStatic::make(
                    storage_path('app/public/posts/' . array_pop($imagePath))
                )->encode('webp')
                ->save(storage_path("app/public/posts/{$post->id}.webp"));
            } catch (\Throwable $th) {
                //throw $th;
            }
        }
        return 0;
    }
}
