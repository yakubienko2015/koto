<?php

namespace App\Console\Commands;

use App\Models\Opinion;
use App\Models\Order;
use Illuminate\Console\Command;

class SendOpinionLinks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shop:opinions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $reviewTime = config('shop.time_for_opinion', 4);
        foreach (Opinion::whereBetween('created_at', [
                now()->subHours($reviewTime + 24),
                now()->subHours($reviewTime),
            ])
            ->whereNull('filled_at')->get() as $opinion) {
            $opinion->sendToCustomer();
        }
    }
}
