<?php

namespace App\Console\Commands;

use App\Library\TelegramBotHelper;
use App\Models\User;
use Illuminate\Console\Command;
use Telegram;

class SendTaskReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'telegram:task-reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        foreach (User::whereNotNull('telegram_id')->get() as $user) {
            try {
                if ($user->tasks()->wherePivot('completed_at', null)->wherePivot('created_at', '>=', now()->setHour(1))->count()) {
                    Telegram::sendMessage([
                        'chat_id' => $user->telegram_id,
                    ] + TelegramBotHelper::getTasksMessageForUser($user));
                }
            } catch (\Throwable $th) {
                logger('error on send task reminder (telegram)', [
                    $user->telegram_id, $th->getMessage(),
                ]);
            }
        }
    }
}
