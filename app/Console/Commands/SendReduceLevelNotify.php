<?php

namespace App\Console\Commands;

use App\Models\Group;
use App\Models\Terminal;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class SendReduceLevelNotify extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shop:send-reduce-level-notify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $statuses = Terminal::getStatusesForSelect();
        foreach (Terminal::all() as $terminal) {
            $stats = $terminal->getLastMonthStats();
            if ($stats['recommend_status'] < $terminal->status) {
                $operatorGoup = Group::find($terminal->city->config('admins_group_id'));
                $recStatus = Str::upper($statuses[$stats['recommend_status']]);
                $thisStatus = Str::upper($statuses[$terminal->status]);
                if ($operatorGoup) {
                    $operatorGoup->sendMessage("Рекомендуется изменить статус с {$thisStatus} на {$recStatus} в {$terminal->name}");
                }
            }
        }
        return 0;
    }
}
