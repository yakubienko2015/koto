<?php

namespace App\Console\Commands;

use App\Models\Category;
use App\Models\City;
use App\Models\Page;
use App\Models\Post;
use App\Models\Product;
use Illuminate\Console\Command;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\SitemapIndex;
use Spatie\Sitemap\Tags\Sitemap as TagsSitemap;
use Spatie\Sitemap\Tags\Url;

class GenerateSitemap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate the sitemap.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $sitemapIndex = SitemapIndex::create();

        $langs = LaravelLocalization::getSupportedLocales();
        $cities = City::whereIsActive(true)->pluck('id', 'slug');
        $modificationAt = now();

        foreach ($langs as $lang => $langInfo) {
            $langPath = $lang === 'ru' ? '' : "{$lang}/";
            $sitemaps[$lang] = Sitemap::create();
            $sitemaps["{$lang}p"] = Sitemap::create();
            $sitemaps["{$lang}b"] = Sitemap::create();
            $sitemaps["{$lang}c"] = Sitemap::create();

            foreach ($cities as $city => $cityId) {
                foreach (Page::whereIsActive(1)->inCity($cityId)->onSite()->get() as $page) {
                    $cityUrl = ($cityId == 1 && ! $page->slug) ? '' : "{$city}/";
                    $sitemaps[$lang]->add(Url::create("/{$langPath}{$cityUrl}{$page->slug}")
                        ->setPriority($page->slug ? 0.9 : 1)
                        ->setLastModificationDate($modificationAt));
                }
                foreach (Category::menu()->get() as $category) {
                    $sitemaps["{$lang}c"]->add(Url::create("/{$langPath}{$city}/{$category->slug}")
                        ->setPriority(0.8)
                        ->setLastModificationDate($modificationAt));
                }
                foreach (Post::active()->inCity($cityId)->get() as $post) {
                    $sitemaps["{$lang}b"]->add(Url::create("/{$langPath}{$city}/news/{$post->slug}")
                        ->setPriority(0.7)
                        ->setLastModificationDate($modificationAt));
                }
                foreach (Product::active()->with('category')->activeCategory()->get() as $product) {
                    $sitemaps["{$lang}p"]->add(Url::create("/{$langPath}{$city}/{$product->category->slug}/{$product->slug}")
                        ->setPriority(0.6)
                        ->setLastModificationDate($modificationAt));
                }
            }

            $sitemaps[$lang]->writeToFile(public_path("sitemap-{$lang}.xml"));
            $sitemaps["{$lang}p"]->writeToFile(public_path("sitemap-{$lang}-product.xml"));
            $sitemaps["{$lang}b"]->writeToFile(public_path("sitemap-{$lang}-blog.xml"));
            $sitemaps["{$lang}c"]->writeToFile(public_path("sitemap-{$lang}-category.xml"));
            $sitemapIndex->add(
                TagsSitemap::create("/sitemap-{$lang}.xml")
                    ->setLastModificationDate($modificationAt)
            )->add(
                TagsSitemap::create("/sitemap-{$lang}-product.xml")
                    ->setLastModificationDate($modificationAt)
            )->add(
                TagsSitemap::create("/sitemap-{$lang}-blog.xml")
                    ->setLastModificationDate($modificationAt)
            )->add(
                TagsSitemap::create("/sitemap-{$lang}-category.xml")
                    ->setLastModificationDate($modificationAt)
            );
        }

        $sitemapIndex->writeToFile(public_path('sitemap.xml'));

        return 0;
    }
}
