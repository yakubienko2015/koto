<?php

namespace App\Console;

use App\Console\Commands\GenerateSitemap;
use App\Console\Commands\ResendCustomerGameWinCategory;
use App\Console\Commands\ResetTerminalStatus;
use App\Console\Commands\ScanIikoOrders;
use App\Console\Commands\UpdateEmployeesData;
use App\Console\Commands\SendOpinionLinks;
use App\Console\Commands\TrackUsersTime;
use App\Console\Commands\ScanReadyToCookOrders;
use App\Console\Commands\ScanReturnCourier;
use App\Console\Commands\SearchBirthdayUsers;
use App\Console\Commands\SendPreOrderReminder;
use App\Console\Commands\SendReduceLevelNotify;
use App\Console\Commands\SendTaskReminder;
use App\Console\Commands\UpdateNextBirthdays;
use App\Console\Commands\UpdateWorkPlaces;
use App\Models\Pin;
use App\Models\Task;
use App\Models\User;
use DB;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Notifications\DatabaseNotification;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        try {
            DB::connection()->getPdo();
        } catch (\Throwable $throw) {
            logger()->error('Error on read db for cron! ' . $throw->getMessage());
            die('Error on read db for cron!');
        }
        $schedule->command(TrackUsersTime::class)->everyMinute();
        if (config('shop.make_order_with_iiko')) {
            $schedule->command(ScanIikoOrders::class)->everyMinute();
        }
        $schedule->command(UpdateEmployeesData::class)->hourly();

        $workTime = app('city')->getTodayWorkTime();
        $schedule->command(SendOpinionLinks::class)
            ->everyMinute()
            ->between($workTime['from'], $workTime['to']);
        $schedule->command(ScanReadyToCookOrders::class)
            ->everyMinute()
            ->between($workTime['from'], $workTime['to']);
        $schedule->command(SendPreOrderReminder::class)
            ->everyMinute()
            ->between($workTime['from']->subHours(3), $workTime['to']);
        $schedule->command(SendReduceLevelNotify::class)
            ->everyTenMinutes()
            ->between($workTime['from'], $workTime['to']);

        $schedule->command('telescope:prune')->daily();
        $schedule->call(function () {
            Pin::where('rest_time', '>', 0)->update(['rest_time' => 0]);
            Pin::whereNotNull('courier_status')->update(['courier_status' => null]);
            DB::table('product_group_work_place')->truncate();
            User::role(['courier', 'cook', 'admin', 'logist'])->update(['logout' => true]);
            DatabaseNotification::whereType('App\Notifications\WinGameNotification')
                ->where('created_at', '<', now()->subDays(14))
                ->delete();
        })->daily();

        foreach (Task::whereNotNull('cron')->where('cron', '<>', '* * * * *')->get() as $task) {
            $schedule->call(function () use ($task) {
                $task->share();
            })->cron($task->cron);
        }
        $schedule->command(SendTaskReminder::class)
            ->everyThirtyMinutes()
            ->between($workTime['from'], $workTime['to']);
        $schedule->command(ScanReturnCourier::class)->everyMinute();
        $schedule->command(UpdateWorkPlaces::class)->everyMinute();
        $schedule->command(GenerateSitemap::class)->daily();
        $schedule->command(ResetTerminalStatus::class)->daily();
        $schedule->command(UpdateNextBirthdays::class)->dailyAt('09:00');
        $schedule->command(SearchBirthdayUsers::class)->dailyAt('09:01');
        $schedule->command(ResendCustomerGameWinCategory::class)->everyThreeHours();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
