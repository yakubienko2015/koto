<?php

namespace App\Bot\Commands;

use App\Library\TelegramBotHelper;
use Telegram\Bot\Commands\Command;

class StartCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'start';

    /**
     * @var string Command Description
     */
    protected $description = 'Начать использовать бота';

    /**
     * {@inheritdoc}
     */
    public function handle()
    {
        $message = $this->getUpdate()->getMessage();
        $fromUser = $message->getFrom();
        $this->replyWithMessage([
            'text' => "Привет я АСТРА (Алгоритм, Созданный в Телеграмм Ради Автоматизации)

Моей главной задачей является сделать твою работу комфортнее.
Пока ты в кото я буду:
1 Присылать тебе уведомления о собраниях и других событиях в жизни компании
2 Помогу тебе не забыть выполнить задания руководителя.
3 Помогу тебе подтвердить качественное выполнение задания при помощи фотоотчета.

Можешь актуализировать список задач при помощи команды /tasks
и если ты на выходном то в любой момент выключи при помощи команды /stop

Поделись со мной своим номером для начала работы.

Твоя Астра."]);

        // $user = TelegramBotHelper::authUser($this->getUpdate());

        // if ($user) {
        //     TelegramBotHelper::getStartFunctions($user);
        // }
    }
}
