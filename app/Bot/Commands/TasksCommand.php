<?php

namespace App\Bot\Commands;

use App\Library\TelegramBotHelper;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Keyboard\Keyboard;

class TasksCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'tasks';

    /**
     * @var string Command Description
     */
    protected $description = 'Список заданий';

    /**
     * {@inheritdoc}
     */
    public function handle()
    {
        $user = TelegramBotHelper::authUser($this->getUpdate());

        if (! $user) {
            return;
        }

        if ($user->hasAnyRole(['manager', 'admin', 'operator', 'courier', 'cook', 'logist'])) {
            $this->replyWithMessage(TelegramBotHelper::getTasksMessageForUser($user));
            return;
        }

        TelegramBotHelper::getStartFunctions($user);
    }
}
