<?php

namespace App\Bot\Commands;

use App\Library\TelegramBotHelper;
use App\Models\Task;
use App\Models\TaskUser;
use App\Models\UserHoliday;
use Telegram\Bot\Commands\Command;

class StopCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'stop';

    /**
     * @var string Command Description
     */
    protected $description = 'На выходном';

    /**
     * {@inheritdoc}
     */
    public function handle()
    {
        $user = TelegramBotHelper::authUser($this->getUpdate());
        if ($user) {
            UserHoliday::firstOrCreate([
                'user_id' => $user->id,
                'date' => now()->format('Y-m-d'),
            ], []);
            $user->tasks()->wherePivot('completed_at', null)->where('holiday_users', false)->detach();

            $holidayTask = Task::getHolidayTask();
            if ($holidayTask) {
                TaskUser::create([
                    'user_id' => $user->id,
                    'task_id' => $holidayTask->id,
                    'completed_at' => now(),
                ]);
            }
            $this->replyWithMessage(['text' => 'Хорошего отдыха']);
        }
    }
}
