<?php

namespace App\Events;

use App\Models\OrderWorkPlace;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UpdateOrderWorkPlace implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $orderWorkPlace;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(OrderWorkPlace $orderWorkPlace)
    {
        $this->orderWorkPlace = $orderWorkPlace;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PresenceChannel("app-orders.{$this->orderWorkPlace->order->city_id}");
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {
        return ['orderWorkPlace' => [
            'id' => $this->orderWorkPlace->id,
            'status' => $this->orderWorkPlace->status ?? null,
        ]];
    }
}
