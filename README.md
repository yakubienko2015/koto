# Mekler

## About

Документація по API на [postman](https://documenter.getpostman.com/view/6709765/SzmiYGwy)

## Authentication

Сторінка авторизації: [/login](/login)

1. **Manager (Супер адмін)**
    - Логін: manager@gmail.com
    - Пароль: secret
2. **Admin (Адмін)**
    - Логін: admin@gmail.com
    - Пароль: secret

Сторінка авторизації в модулі (оператор, курєр, повар): [/app/login](/app/login)

1. **Попередньо потрібно згенерувати пін-код в адмінці для конкретного працівника**

## Install local

1. _ $ `composer install`
2. _ # `chmod -R 777 storage/ bootstrap/cache`
3. Створити тадлицю в БД
4. [Заповнити .env](##Configuration)
5. _ $ `php artisan migrate --seed`
6. _ $ `php artisan optimize:clear`
7. _ $ `php artisan route:trans:clear`
8. _ $ `npm install`
9. _ $ `npm run dev`
10. Налаштувати крон:  
`* * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1`

## Install prod

1. _ $ `composer install --optimize-autoloader --no-dev`
2. _ # `chmod -R 777 storage/ bootstrap/cache`
3. Створити тадлицю в БД
4. [Заповнити .env](##Configuration)
5. _ $ `php artisan migrate --seed`
6. _ $ `php artisan optimize`
7. _ $ `php artisan route:trans:cache`
8. _ $ `npm install`
9. _ $ `npm run production`
10. Налаштувати крон:  
`* * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1`

## Update

1. _ $ `composer update`
2. _ # `chmod -R 777 storage/ bootstrap/cache`
3. _ $ `php artisan migrate`
4. _ $ `php artisan optimize`
5. _ $ `php artisan route:trans:cache`

## Configuration

---
Поміняти середовище: (local, production):  
`APP_ENV=local`

Включити чи виключити відладку: (true, false):  
`APP_DEBUG=true`

Замінити адресу:  
`APP_URL=https://kotosushi.com`

Потрібно для слідкування за замовлення в реальному режимі:  
`BROADCAST_DRIVER=pusher`

Налаштувати доступа до БД:  
`DB_DATABASE=koto`  
`DB_USERNAME=root`  
`DB_PASSWORD=`

Налаштувати пушер. Для тестування можна залишити ці:  
`PUSHER_APP_ID=995108`  
`PUSHER_APP_KEY=66159fcaca6f2db25132`  
`PUSHER_APP_SECRET=542698245855cdc4b196`  
`PUSHER_APP_CLUSTER=eu`

Налаштувати iiko. Можна залишити щоб використати демо версію:  
`IIKO_BIZ_USER_ID=demoDelivery`  
`IIKO_BIZ_USER_SECRET=PI1yFaKFCGvvJKi`

false: щоб замовлення не надсилались в iiko:  
`SHOP_MAKE_ORDER_WITH_IIKO=true`

## License
