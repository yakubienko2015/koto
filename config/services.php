<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'liqpay' => [
        'public_key' => env('LIQPAY_PUBLIC_KEY', 'i99999999999'),
        'private_key' => env('LIQPAY_PRIVATE_KEY', 'qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq'),
        'allow_without_signature' => true, // DANGER - must be false for secure
    ],

    'liqpays' => [
        1 => [
            's' => [
                'public_key' => env('LIQPAY_PUBLIC_KEY_1'),
                'private_key' => env('LIQPAY_PRIVATE_KEY_1'),
            ],
            'm' => [
                'public_key' => env('LIQPAY_PUBLIC_KEY_M_1'),
                'private_key' => env('LIQPAY_PRIVATE_KEY_M_1'),
            ],
        ],
        2 => [
            's' => [
                'public_key' => env('LIQPAY_PUBLIC_KEY_2'),
                'private_key' => env('LIQPAY_PRIVATE_KEY_2'),
            ],
            'm' => [
                'public_key' => env('LIQPAY_PUBLIC_KEY_M_2'),
                'private_key' => env('LIQPAY_PRIVATE_KEY_M_2'),
            ],
        ],
        3 => [
            's' => [
                'public_key' => env('LIQPAY_PUBLIC_KEY_3'),
                'private_key' => env('LIQPAY_PRIVATE_KEY_3'),
            ],
            'm' => [
                'public_key' => env('LIQPAY_PUBLIC_KEY_M_3'),
                'private_key' => env('LIQPAY_PRIVATE_KEY_M_3'),
            ],
        ],
        4 => [
            's' => [
                'public_key' => env('LIQPAY_PUBLIC_KEY_4'),
                'private_key' => env('LIQPAY_PRIVATE_KEY_4'),
            ],
            'm' => [
                'public_key' => env('LIQPAY_PUBLIC_KEY_M_4'),
                'private_key' => env('LIQPAY_PRIVATE_KEY_M_4'),
            ],
        ],
    ],

    'sms_fly' => [
        'login' => env('SMS_FLY_LOGIN'),
        'password' => env('SMS_FLY_PASSWORD'),
        'alfaname' => env('SMS_FLY_ALFANAME'),
    ],

];
