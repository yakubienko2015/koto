<?php

return [
    /*
     * Default table attributes when generating the table.
     */
    'table' => [
        'class'    => 'table table-bordered',
        'id'       => 'data-table',
    ],

    /*
     * Default parameters.
     */
    'parameters' => [
        'language' => [
            'url'  => '//cdn.datatables.net/plug-ins/1.10.20/i18n/Russian.json',
        ],
        'dom'          => '<"row"<"col-sm-12 col-md-7"B><"col-sm-12 col-md-5"f>>rt<"row"<"col-sm-12 col-md-7"i><"col-sm-12 col-md-5"p>>',
        // 'buttons'      => [
        //     [
        //         'extend' => 'create',
        //         'text' => '<i class="fa fa-plus"></i> Добавить',
        //     ],'create', 'export', 'reset'],
        'scrollX' => false,
        'autoWidth' => false,
        'orderBy' => 0,
    ],

    /*
     * Default condition to determine if a parameter is a callback or not.
     * Callbacks needs to start by those terms or they will be casted to string.
     */
    'callback' => ['$', '$.', 'function'],

    /*
     * Html builder script template.
     */
    'script' => 'datatables::script',

    /*
     * Html builder script template for DataTables Editor integration.
     */
    'editor' => 'datatables::editor',
];
