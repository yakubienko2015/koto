<?php

return [
    'social_links' => [
        'facebook' => '#',
        'instagram' => '#',
        'youtube' => '#',
    ],
    'store_links' => [
        'app_store' => '#',
        'google_play' => '#',
    ],
    'phones' => [
        '+38 099 999 99 99',
        '+38 099 999 99 99',
        '+38 099 999 99 99',
    ],
    'copyright' => 'Ⓒ 2020 КотоСуши',
    'jivochat' => env('JIVOCHAT_ENABLED', false),

    'personal_cabinet_enabled' => env('PERSONAL_CABINET_ENABLED', true),
];
