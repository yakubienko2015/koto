<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    | Here you can change the default title of your admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#61-title
    |
    */

    'title' => 'Адмінка',
    'title_prefix' => '',
    'title_postfix' => ' | Kotosushi',

    /*
    |--------------------------------------------------------------------------
    | Favicon
    |--------------------------------------------------------------------------
    |
    | Here you can activate the favicon.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#62-favicon
    |
    */

    'use_ico_only' => true,
    'use_full_favicon' => false,

    /*
    |--------------------------------------------------------------------------
    | Logo
    |--------------------------------------------------------------------------
    |
    | Here you can change the logo of your admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#63-logo
    |
    */

    'logo' => '<b>Kotosushi</b>',
    'logo_img' => 'images/oleus/oleus.png',
    'logo_img_class' => 'brand-image elevation-3',
    'logo_img_xl' => null,
    'logo_img_xl_class' => 'brand-image-xs',
    'logo_img_alt' => 'Kotosushi',

    /*
    |--------------------------------------------------------------------------
    | User Menu
    |--------------------------------------------------------------------------
    |
    | Here you can activate and change the user menu.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#64-user-menu
    |
    */

    'usermenu_enabled' => true,
    'usermenu_header' => false,
    'usermenu_header_class' => 'bg-primary',
    'usermenu_image' => false,
    'usermenu_desc' => false,
    'usermenu_profile_url' => false,

    /*
    |--------------------------------------------------------------------------
    | Layout
    |--------------------------------------------------------------------------
    |
    | Here we change the layout of your admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#65-layout
    |
    */

    'layout_topnav' => null,
    'layout_boxed' => null,
    'layout_fixed_sidebar' => null,
    'layout_fixed_navbar' => null,
    'layout_fixed_footer' => null,

    /*
    |--------------------------------------------------------------------------
    | Authentication Views Classes
    |--------------------------------------------------------------------------
    |
    | Here you can change the look and behavior of the authentication views.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#661-authentication-views-classes
    |
    */

    'classes_auth_card' => 'card-outline card-primary',
    'classes_auth_header' => '',
    'classes_auth_body' => '',
    'classes_auth_footer' => '',
    'classes_auth_icon' => '',
    'classes_auth_btn' => 'btn-flat btn-primary',

    /*
    |--------------------------------------------------------------------------
    | Admin Panel Classes
    |--------------------------------------------------------------------------
    |
    | Here you can change the look and behavior of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#662-admin-panel-classes
    |
    */

    'classes_body' => '',
    'classes_brand' => '',
    'classes_brand_text' => '',
    'classes_content_wrapper' => '',
    'classes_content_header' => 'container-fluid',
    'classes_content' => 'container-fluid',
    'classes_sidebar' => 'sidebar-dark-primary elevation-4',
    'classes_sidebar_nav' => '',
    'classes_topnav' => 'navbar-white navbar-light',
    'classes_topnav_nav' => 'navbar-expand-md',
    'classes_topnav_container' => 'container',

    /*
    |--------------------------------------------------------------------------
    | Sidebar
    |--------------------------------------------------------------------------
    |
    | Here we can modify the sidebar of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#67-sidebar
    |
    */

    'sidebar_mini' => true,
    'sidebar_collapse' => false,
    'sidebar_collapse_auto_size' => false,
    'sidebar_collapse_remember' => true,
    'sidebar_collapse_remember_no_transition' => true,
    'sidebar_scrollbar_theme' => 'os-theme-light',
    'sidebar_scrollbar_auto_hide' => 'l',
    'sidebar_nav_accordion' => true,
    'sidebar_nav_animation_speed' => 300,

    /*
    |--------------------------------------------------------------------------
    | Control Sidebar (Right Sidebar)
    |--------------------------------------------------------------------------
    |
    | Here we can modify the right sidebar aka control sidebar of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#68-control-sidebar-right-sidebar
    |
    */

    'right_sidebar' => false,
    'right_sidebar_icon' => 'fas fa-cogs',
    'right_sidebar_theme' => 'dark',
    'right_sidebar_slide' => true,
    'right_sidebar_push' => true,
    'right_sidebar_scrollbar_theme' => 'os-theme-light',
    'right_sidebar_scrollbar_auto_hide' => 'l',

    /*
    |--------------------------------------------------------------------------
    | URLs
    |--------------------------------------------------------------------------
    |
    | Here we can modify the url settings of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#69-urls
    |
    */

    'use_route_url' => true,

    'dashboard_url' => 'admin.dashboard',

    'logout_url' => 'logout',

    'login_url' => 'login',

    'register_url' => 'register',

    'password_reset_url' => 'password.request',

    'password_email_url' => 'password.email',

    'profile_url' => 'account',

    /*
    |--------------------------------------------------------------------------
    | Laravel Mix
    |--------------------------------------------------------------------------
    |
    | Here we can enable the Laravel Mix option for the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#610-laravel-mix
    |
    */

    'enabled_laravel_mix' => true,
    'laravel_mix_css_path' => 'css/admin.css',
    'laravel_mix_js_path' => 'js/admin.js',

    /*
    |--------------------------------------------------------------------------
    | Menu Items
    |--------------------------------------------------------------------------
    |
    | Here we can modify the sidebar/top navigation of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#611-menu
    |
    */

    'menu' => [
        [
            'text' => 'Панель управления',
            'url'  => 'admin',
            'icon' => 'fas fa-home',
        ],
        [
            'text' => '',
            'url'  => '/',
            'topnav' => true,
            'icon' => 'fas fa-external-link-alt',
            'target' => '_blank',
        ],
        [
            'text' => '',
            'topnav_right' => true,
            'icon' => 'fas fa-terminal',
            'can' => 'run-commands',
            'submenu' => [
                [
                    'text' => 'Закешировать даные',
                    'url'  => 'admin/artisan?cmd[]=optimize&cmd[]=route:trans:cache',
                    'icon' => '',
                ],
                [
                    'text' => 'Очистить кеш',
                    'url'  => 'admin/artisan?cmd=optimize:clear',
                    'icon' => '',
                ],
                [
                    'text' => 'Сгенерировать sitemap',
                    'url'  => 'admin/artisan?cmd=sitemap:generate',
                    'icon' => '',
                ],
            ],
        ],
        [
            'text' => 'Пользователи',
            'can'  => 'view-users',
            'icon' => 'fas fa-fw fa-users',
            'submenu' => [
                [
                    'text' => 'Работники',
                    'url'  => 'admin/users',
                ],
                [
                    'text' => 'Клиенти',
                    'url'  => 'admin/customers',
                ],
                [
                    'text' => 'Именинники',
                    'url'  => 'admin/birthdays',
                ],
            ],
        ],
        [
            'text' => 'Роли пользователей',
            'url'  => 'admin/roles',
            'can'  => 'view-roles',
            'icon' => 'fas fa-fw fa-user-lock',
        ],
        [
            'text' => 'Города',
            'url'  => 'admin/cities',
            'can'  => 'view-cities',
            'icon' => 'fas fa-fw fa-city',
        ],
        [
            'text' => 'Точки продаж',
            'url'  => 'admin/terminals',
            'can'  => 'view-terminals',
            'icon' => 'fas fa-fw fa-store',
        ],
        [
            'text' => 'Категории',
            'url'  => 'admin/categories',
            'can'  => 'view-categories',
            'icon' => 'fas fa-fw fa-sitemap',
        ],
        [
            'text' => 'Группы',
            'url'  => 'admin/product-groups',
            'can'  => 'view-product-groups',
            'icon' => 'fas fa-fw fa-sitemap',
        ],
        [
            'text' => 'Товары',
            'url'  => 'admin/products',
            'can'  => 'view-products',
            'icon' => 'fas fa-fw fa-boxes',
        ],
        [
            'text' => 'Заказы',
            'url'  => 'admin/orders',
            'can'  => 'view-orders',
            'icon' => 'fas fa-fw fa-clipboard-list',
        ],
        [
            'text' => 'Страницы',
            'url'  => 'admin/pages',
            'can'  => 'view-pages',
            'icon' => 'fas fa-fw fa-window-restore',
        ],
        [
            'text' => 'Блог',
            'url'  => 'admin/posts',
            'can'  => 'view-posts',
            'icon' => 'fas fa-fw fa-blog',
        ],
        [
            'text' => 'Отзывы',
            'url'  => 'admin/opinions',
            'can'  => 'view-opinions',
            'icon' => 'fas fa-fw fa-copy',
        ],
        [
            'text' => 'Конструктор боулов',
            'url'  => 'admin/bowl-ingredients',
            'can'  => 'view-bowl-ingredients',
            'icon' => 'fas fa-fw fa-copy',
        ],
        [
            'text' => 'Обратные вызовы',
            'url'  => 'admin/callbacks',
            'can'  => 'view-callbacks',
            'icon' => 'fas fa-fw fa-phone',
        ],
        [
            'text' => 'Слайд-шоу',
            'url'  => 'admin/slides',
            'can'  => 'view-slides',
            'icon' => 'fab fa-fw fa-slideshare',
        ],
        [
            'text' => 'Промо',
            'url'  => 'admin/promos',
            'can'  => 'view-promos',
            'icon' => 'fas fa-fw fa-ticket-alt',
        ],
        [
            'text' => 'Ачивки',
            'url'  => 'admin/achievements',
            'can'  => 'view-achievements',
            'icon' => 'fas fa-fw fa-boxes',
        ],
        [
            'text' => 'Телеграм бот',
            'icon' => 'fab fa-fw fa-telegram-plane',
            'can'  => 'view-telegram',
            'submenu' => [
                [
                    'text' => 'Групы',
                    'url'  => 'admin/telegram/groups',
                ],
                [
                    'text' => 'Задачи',
                    'url'  => 'admin/telegram/tasks',
                ],
            ],
        ],
        [
            'text'        => 'Уведомления',
            'url'         => 'admin/notifications/create',
            'icon'        => 'fas fa-fw fa-sms',
            'can'         => 'manage-notifications',
        ],
        [
            'text' => 'Метки',
            'url'  => 'admin/marks',
            'can'  => 'view-marks',
            'icon' => 'fas fa-fw fa-marker',
        ],
        [
            'text'        => 'Настройки',
            'url'         => 'admin/settings',
            'icon'        => 'fas fa-fw fa-cogs',
            'can'         => 'view-settings',
        ],
        [
            'text'        => 'QR приложение',
            'url'         => 'admin/market-bonuses',
            'icon'        => 'fas fa-fw fa-mobile-alt',
            'can'         => 'view-market-bonuses',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Menu Filters
    |--------------------------------------------------------------------------
    |
    | Here we can modify the menu filters of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#612-menu-filters
    |
    */

    'filters' => [
        JeroenNoten\LaravelAdminLte\Menu\Filters\HrefFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SearchFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ActiveFilter::class,
        // JeroenNoten\LaravelAdminLte\Menu\Filters\SubmenuFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ClassesFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\GateFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\LangFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\DataFilter::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Plugins Initialization
    |--------------------------------------------------------------------------
    |
    | Here we can modify the plugins used inside the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#613-plugins
    |
    */

    'plugins' => [
        [
            'name' => 'Datatables',
            'active' => true,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
                ],
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css',
                ],
            ],
        ],
        [
            'name' => 'Select2',
            'active' => true,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css',
                ],
            ],
        ],
        [
            'name' => 'Chartjs',
            'active' => true,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.bundle.min.js',
                ],
            ],
        ],
        [
            'name' => 'Sweetalert2',
            'active' => true,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdn.jsdelivr.net/npm/sweetalert2@8',
                ],
            ],
        ],
        [
            'name' => 'Pace',
            'active' => true,
            'files' => [
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/blue/pace-theme-center-radar.min.css',
                ],
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js',
                ],
            ],
        ],
    ],
];
