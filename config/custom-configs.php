<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Filename
    |--------------------------------------------------------------------------
    |
    | The default place for saving
    |
    | Supported Drivers: "path", "storage"
    |
    */

    'default' => 'path',

    'storage' => 'local',

    'path' => 'storage/app/',
];
