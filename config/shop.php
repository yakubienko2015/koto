<?php

use App\Models\Order;
use App\Models\Terminal;

return [
    'sharpnesses' => [
        'weak' => '#00ff00',
        'medium' => '#ffff00',
        'hot' => '#ff0000',
    ],
    'marks' => [
        'vegan' => '#00ff00',
        'philadelphia' => '#ffff00',
        'salmon' => '#ffff00',
        'meat' => '#ffff00',
        'maki_rols' => '#ffff00',
    ],
    'work_days' => [
        'Понедельник',
        'Вторник',
        'Среда',
        'Четверг',
        'Пятница',
        'Суббота',
        'Воскресенье',
    ],
    'default_work_times' => [
        ['from' => '09:00', 'to' => '22:00', 'closed' => false],
        ['from' => '09:00', 'to' => '22:00', 'closed' => false],
        ['from' => '09:00', 'to' => '22:00', 'closed' => false],
        ['from' => '09:00', 'to' => '22:00', 'closed' => false],
        ['from' => '09:00', 'to' => '22:00', 'closed' => false],
        ['from' => '09:00', 'to' => '22:00', 'closed' => true],
        ['from' => '09:00', 'to' => '22:00', 'closed' => true],
    ],
    'payments' => [
        Order::ORDER_PAYMENT_LIQPAY => 'На сайте (LiqPay)',
        Order::ORDER_PAYMENT_CASH => 'Наличными',
        // Order::ORDER_PAYMENT_KOTCOINS => 'Коткоины',
    ],
    'order_statuses' => [
        // Order::ORDER_STATUS_NO_EXISTS => 'Не существует',
        Order::ORDER_STATUS_NEW => 'Новая',
        Order::ORDER_STATUS_OPERATE => 'Оформляется',
        Order::ORDER_STATUS_IN_PROGRESS => 'На кухне',
        Order::ORDER_STATUS_READY => 'Готово',
        Order::ORDER_STATUS_WAITING => 'Ждет отправки',
        Order::ORDER_STATUS_ON_WAY => 'В пути',
        Order::ORDER_STATUS_CANCELLED => 'Отменена',
        Order::ORDER_STATUS_DELIVERED => 'Доставлена',
        Order::ORDER_STATUS_UNCONFIRMED => 'Не подтверждена',
        Order::ORDER_STATUS_CLOSED => 'Закрыта',
    ],
    'order_status_variants' => [
        // Order::ORDER_STATUS_NO_EXISTS => 'Не существует',
        Order::ORDER_STATUS_NEW => 'pre-warning',
        Order::ORDER_STATUS_OPERATE => 'warning',
        Order::ORDER_STATUS_IN_PROGRESS => 'pre-success',
        Order::ORDER_STATUS_READY => 'pre-success',
        Order::ORDER_STATUS_WAITING => 'pre-success',
        Order::ORDER_STATUS_ON_WAY => 'success',
        Order::ORDER_STATUS_CANCELLED => 'danger',
        Order::ORDER_STATUS_DELIVERED => 'info',
        Order::ORDER_STATUS_UNCONFIRMED => 'purple',
        Order::ORDER_STATUS_CLOSED => 'danger',
    ],
    'iiko_statuses' => [
        // Статус доставки
        'Новая'         => Order::ORDER_STATUS_NEW,
        'Ждет отправки'     => Order::ORDER_STATUS_WAITING,
        'В пути'      => Order::ORDER_STATUS_ON_WAY,
        'Закрыта'      => Order::ORDER_STATUS_CLOSED,
        'Отменена'   => Order::ORDER_STATUS_CANCELLED,
        'Доставлена'   => Order::ORDER_STATUS_DELIVERED,
        'Не подтверждена' => Order::ORDER_STATUS_UNCONFIRMED,
        'Готовится' => Order::ORDER_STATUS_IN_PROGRESS,
        'Готово' => Order::ORDER_STATUS_READY,

        // Статус заказа
        // 'NEW' => 1, Новая
        // 'AWAITING_DELIVERY' => 1, Ждет отправки
        // 'ON_THE_WAY' => 1, В пути
        // 'CLOSED' => 2, Закрыта
        // 'CANCELLED' => 3, Отменена
        // 'DELIVERED' => 2, Доставлена
        // 'NOT_CONFIRMED' => 3, Не подтверждена
        // 'IN_PROGRESS' => 1, Готовится
        // 'READY' => 1, Готово
    ],
    'terminal_statuses' => [
        Terminal::STATUS_LOW => 'низкий уровень',
        Terminal::STATUS_MIDDLE => 'средний',
        Terminal::STATUS_HIGHT => 'высокий',
        Terminal::STATUS_FULL => 'перегрузка',
    ],
    'terminal_status_variants' => [
        Terminal::STATUS_LOW => 'success',
        Terminal::STATUS_MIDDLE => 'warning',
        Terminal::STATUS_HIGHT => 'danger',
        Terminal::STATUS_FULL => 'dark',
    ],
    'min_basket_sum' => 400,
    'min_basket_payment_sum' => 800,

    'max_load' => 2000,

    /**
     * Time in hours for send customer message for get opinion
     */
    'time_for_opinion' => 4,

    /**
     * Questions for opinion
     */
    'opinion_marks' => [
        'Price',            // Цены           // iiko - Цена (Common)
        'DeliveryFood',     // Вкус блюд      // iiko - Вкус еды
        'Common',           // Ассортимент    // iiko - Ассортимент (DeliveryOperator)
        'Size',             // Размер порций  // iiko - Размер порций (Common)
        'DeliveryOperator', // Персонал       // iiko - Персонал
        'Courier',          // Время доставки // iiko - Время доставки (Common)
    ],

    'make_order_with_iiko' => env('SHOP_MAKE_ORDER_WITH_IIKO', true),

    'kotcoin_rates' => [
        [0,0,0,0,0,0,0,0,0,0,1.2,1.2,1,1,1,1,0.8,0.8,0.8,0.8,0.8,0.8,0,0],
        [0,0,0,0,0,0,0,0,0,0,1.2,1.2,1,1,1,1,0.8,0.8,0.8,0.8,0.8,0.8,0,0],
        [0,0,0,0,0,0,0,0,0,0,1.2,1.2,1,1,1,1,0.8,0.8,0.8,0.8,0.8,0.8,0,0],
        [0,0,0,0,0,0,0,0,0,0,1.2,1.2,1,1,1,1,0.8,0.8,0.8,0.8,0.8,0.8,0,0],
        [0,0,0,0,0,0,0,0,0,0,1.2,1.2,1,1,1,1,0.8,0.8,0.8,0.8,0.8,0.8,0,0],
        [0,0,0,0,0,0,0,0,0,0,1.2,1.2,1,1,1,1,0.8,0.8,0.8,0.8,0.8,0.8,0,0],
        [0,0,0,0,0,0,0,0,0,0,1.2,1.2,1,1,1,1,0.8,0.8,0.8,0.8,0.8,0.8,0,0],
    ],

    /**
     * Default operator price per minute in grn
     */
    'default_operator_price' => 1,

    /**
     * Pre order and birthdays notification
     */
    'operators_group_id' => null,

    /**
     * @const array
     */
    'page_places' => [
        'header'  => 'Главное меню',
        'footer'  => 'Меню в футере (Чтиво)',
        'foot_client'  => 'Меню в футере (Клиентам)',
        'foot_geo'  => 'Меню в футере (География)',
        'site'  => 'На сайте',
        'api'  => 'Для мобильной версии',
    ],

    'bowls_category_id' => 3,
    'bowl_constructors_category_id' => 7,

    'free_sauce_product_id' => null,
    'add_sauce_product_id' => null,
    'rolls_category_id' => 1,
    'presents_category_id' => 12,

    'good_mood_product_id' => null,
    'win_cutomer_category_uuid' => null,
    'market_bonus_category_uuid' => null,

    'order_promo_product_id' => 132,

    'basket_cookie_name' => 'kotosushi_basket',

    'iiko_roles' => [
        'МЕН' => 'manager',
        'MN0' => 'sys_admin',
        'АДМ' => 'admin',
        'ADM' => 'manager',
        'ОПЕ' => 'operator',
        'ОП' => 'operator',
        'КЛИ' => 'customer',
        'КУР' => 'courier',
        'Кур_А' => 'courier',
        'БОУ' => 'cook',
        'СУШ' => 'cook',
        'ПОМ' => 'cook',
        'ШЕФ' => 'cook',
        'ЛОГ' => 'logist',
        'МАР' => 'marketer',
        'DW1' => 'courier',
        'АДМ Д' => 'admin',
    ],

    'free_sauce_coff' => 0.55,

    'citied' => [
        '*' => [
            /**
             * Reduce level notification
             */
            'admins_group_id' => null,
        ],
    ],
];
