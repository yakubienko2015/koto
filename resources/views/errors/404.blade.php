@extends(request()->is('app/*') ? 'errors::minimal' : 'site.app')

@section('title', __('Page Not Found'))
@section('code', '404')
@section('message', __('Page Not Found'))

@section('content')
<section>
    <div class="page" style="text-align:center">
        <img src="{{ asset('images/site/404.png') }}" alt="404">
    </div>
</section>
@endsection
