<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="manifest" href="/web-manifest.json">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link rel="preload" href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600;700;800;900&display=swap" as="style">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600;700;800;900&display=swap">

    <!-- Styles -->
    <link rel="preload" href="{{ mix('css/site.css') }}" as="style">
    <link href="{{ mix('css/site.css') }}" rel="stylesheet">
    @stack('styles')

    <!-- SEO -->
    @isset($metas)
    <title>@yield('title', $metas->title ? $metas->title : config('app.name', 'Laravel'))</title>
    <meta name="description" content="@yield('meta.description', $metas->description ?? '')" >
    <meta name="keywords" content="@yield('meta.keywords', $metas->keywords ?? '')" >
    <meta name="robots" content="@yield('meta.robots', $metas->robots ?? 'noindex, nofollow')" />
    @else
    <title>@yield('title', config('app.name', 'Laravel'))</title>
    <meta name="description" content="@yield('meta.description', config('app.meta.description'))" >
    <meta name="keywords" content="@yield('meta.keywords', config('app.meta.keywords'))" >
    <meta name="robots" content="@yield('meta.robots', config('app.meta.keywords', 'noindex, nofollow'))" />
    @endisset
    @yield('meta')
    @if (! ((isset($metas) && ($metas->robots ?? 'noindex, nofollow') == 'noindex, nofollow') || request()->has('page') || request()->has('sort') || request()->has('marks')))
    <link rel="canonical" href="{{ url()->current() }}" />
    @endif
    @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
    <link rel="alternate" hreflang="{{ $localeCode }}" href="{{ $localeCode == 'ru' ? LaravelLocalization::getNonLocalizedURL() : LaravelLocalization::getLocalizedURL($localeCode) }}" />
    @endforeach
    <meta property="og:locale" content="{{ app()->getLocale() }}">
    <meta property="og:type" content="website">
    <meta property="og:title" content="@yield('title', $metas->title ?? config('app.name', 'Laravel'))">
    <meta property="og:description" content="@yield('meta.description', $metas->description ?? config('app.meta.description'))">
    <meta property="og:url" content="{{ request()->url() }}">
    <meta property="og:site_name" content="{{ config('app.name', 'Laravel') }}">
    <meta property="og:image" content="{{ asset('images/site/og-image.jpg') }}">
    <meta property="og:image:secure_url" content="{{ asset('images/site/og-image.jpg') }}">
    <meta property="og:image:width" content="1280px">
    <meta property="og:image:height" content="1280px">
    <meta name="google-site-verification" content="b-brJpsg7DFRvo1E8-x8Tm7XTtTNhnH1EJB0-njk8nY" />

    @isset($schemaLocalBusiness)
    <script type="application/ld+json">
        @json($schemaLocalBusiness ?? null, JSON_UNESCAPED_UNICODE)
    </script>
    @endisset
    @isset($schemaBreadcrumbList)
    <script type="application/ld+json">
        @json($schemaBreadcrumbList ?? null, JSON_UNESCAPED_UNICODE)
    </script>
    @endisset
    @isset($schemaProduct)
    <script type="application/ld+json">
        @json($schemaProduct ?? null, JSON_UNESCAPED_UNICODE)
    </script>
    @endisset
    @if (config('site.jivochat'))
        <script src="//code.jivosite.com/widget/azYK79HeO6" async></script>
    @endif
</head>
<body>
    <div
        id="app"
        data-work-time=@json(app('city')->getTodayWorkTimeJs())
        data-city-name="{{ app('city')->slug }}"
        data-need-subscribe="{{ auth()->check() ? ! auth()->user()->device_token : false }}"
        data-presents-category-id="{{ config('shop.presents_category_id') }}"
        data-analytics-products-exclude='[129,131,138,144]'
        class="container{{ request()->is('*/play/*') ? ' game-page' : '' }}">
        @if (! Agent::isMobile())
        <div class="left-side">
            @include('site.partials.sidebar')
        </div>
        @endif
        <!-- /.left-side -->
        <div class="right-side os-flex-1">
            @include('site.partials.header')
            <main class="main">
                @yield('content')
            </main>
            @include('site.partials.footer')
        </div>
        <!-- /.right-side -->
    </div>

    <!-- Modals -->
    {{-- @include('site.modals.location') --}}
    @include('site.modals.workTime')
    @include('site.modals.callback')

    <!-- Scripts -->
    <script src="{{ mix('js/site.js') }}" defer></script>
    @stack('scripts')
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-172841759-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-172841759-1');
    </script>
    @if (\Session::has('scriptCode'))
        {!! \Session::get('scriptCode') !!}
    @endif
    <!-- Hotjar Tracking Code for https://kotosushi.com.ua/ -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:2036694,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
    <!-- BEGIN PLERDY CODE -->
    <script type="text/javascript" defer>
        var _protocol = (("https:" == document.location.protocol) ? " https://" : " http://");
        var _site_hash_code = "44e3cdfce6f0da2e083cbec8c3383906";
        var _suid = 8392;
        </script>
    <script type="text/javascript" defer src="https://a.plerdy.com/public/js/click/main.js"></script>
    <!-- END PLERDY CODE -->
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1521792691339957');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=1521792691339957&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->
</body>
</html>
