<div class="catalog-item os-flex os-flex-between">
    <div class="left-item os-flex-1">
        <div class="os-flex os-flex-between">
            <div class="name os-flex-1">
                <a href="{{ route('product', [app('city'), $product->category ?? '', $product]) }}">
                    {{ $product->name }}
                </a>
            </div>
            <div class="price">{{ __('shop.price', [$product->price]) }}</div>
        </div>
        <product-ramen-item
            :product='@json($product)'
        ></product-ramen-item>
        <div class="consist">
            {{ $product->full_ingredients }}
        </div>
    </div>
    <div class="right-item os-text-center">
        <div class="media os-flex os-flex-middle os-flex-center">
            <a href="{{ route('product', [app('city'), $product->category ?? '', $product]) }}">
                <picture>
                    <source srcset="{{ $product->webp_image }}" type="image/webp">
                    <img loading="lazy" src="{{ $product->image }}" alt="{{ $product->name }}">
                </picture>
            </a>
        </div>
        <button class="link-bay" type="submit" form="ramen-product-{{ $product->id }}">
            {{ __('shop.to_basket') }}
        </button>
    </div>
</div>
