@php $mobileFlex = $mobileFlex ?? false; $mediaImg = $mediaImg ?? false @endphp
@if ($post)
<div onclick="window.location = '{{ route('post', [app('city'), $post]) }}'">
    <div class="item os-flex @if($mobileFlex) os-flex-column @endif">
        <div class="media{{ $mobileFlex || $mediaImg ? '' : '-img' }} os-background-cover">
            <picture>
                <source srcset="{{ $post->webp_image }}" type="image/webp">
                <img loading="lazy" src="{{ $post->image }}" alt="slide">
            </picture>
        </div>
        <div class="teaser os-flex-1 os-flex os-flex-column os-flex-between">
            <div>
                @if($mobileFlex)
                <div class="title">
                    {{ $post->name }}
                </div>
                @else
                <div class="os-flex">
                    <div class="title os-flex-1">
                        {{ $post->name }}
                    </div>
                    <div class="date">
                        {{ $post->created_at->format('d.m.Y') }}
                    </div>
                </div>
                @endif
                <div class="sub-title">{!! $post->description !!}</div>
            </div>
            <div>
                <a href="{{ route('post', [app('city'), $post]) }}" class="os-link-detail">
                    {{ __('Details') }}
                </a>
            </div>
        </div>
    </div>
</div>
@endif
