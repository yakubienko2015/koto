<div class="footer-contacts">
    <div class="grid-footer">
        <div>
            <a href="{{ app('city')->getIndexUrl() }}">
                <img loading="lazy" src="{{ asset('images/site/cat.png') }}" alt="logo-koto" class="logo-koto">
            </a>
        </div>
        <div class="os-flex os-flex-column social-block">
            @if (config('site.social_links.facebook'))
                <a href="{{ config('site.social_links.facebook') }}" target="_blank">Facebook</a>
            @endif
            @if (config('site.social_links.instagram'))
                <a href="{{ config('site.social_links.instagram') }}" target="_blank">Instagram</a>
            @endif
            @if (config('site.social_links.youtube'))
                <a href="{{ config('site.social_links.youtube') }}" target="_blank">YouTube</a>
            @endif
        </div>
    </div>
    <div class=" os-text-center">
        @foreach (array_filter(config('site.phones')) as $phone)
            <a class="item-tel" href="tel:{{ clear_phone_number($phone) }}">
                {{ $phone }}
            </a>
        @endforeach
            <img loading="lazy" width="100px" src="https://picua.org/images/2018/11/11/61921fd3b6ccd7d3fb4d67918133d44a.png" alt="liqpay">
    </div>
    <div class="copyright os-text-center">
        {{ config('site.copyright') }}
    </div>
</div>
