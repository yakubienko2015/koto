<div>
    <div class="catalog-item bowles-item os-flex os-flex-between">
        <div class="left-item os-flex-1">
            <div class="os-flex os-flex-between">
                <div class="name os-flex-1">
                    <a href="#">
                        {{ __('Bowl constructor') }}
                    </a>
                </div>
            </div>
            <div class="params">
                <div>{{ __('from') }} {{ __('shop.weight', [300]) }}</div>
            </div>
            <div class="price">{{ __('shop.price', [79]) }}</div>
            <button class="link-bay os-btn-bowles" type="button">
                {{ __('Collect') }}
            </button>
        </div>
        <div class="right-item os-text-center">
            <div class="media os-flex os-flex-middle os-flex-center">
                <a href="#">
                    <img loading="lazy" src="{{ asset('images/site/prod-01.png') }}" alt="bowl">
                </a>
            </div>
            <button class="link-bay os-btn-bowles mb" type="button">
                {{ __('Collect') }}
            </button>
        </div>
    </div>
</div>
