<section class="left-top">
    <div class="left-header">
        @include('site.partials.logo')
    </div>
    {{-- @if (\App\Models\Page::whereSlug('bonus')->active()->count()) --}}
    <nav class="left-menu">
        @include('site.partials.categories', ['classPrefix' => 'left-', 'blockedImg' => true])
        {{-- <a href="{{ route('page', [app('city'), 'bonus']) }}" class="left-menu-item{{ request()->is('/*/bonus') ? ' active' : '' }}">
            <div class="left-menu-img">
                <img src="{{ asset("images/site/present.svg") }}" alt="bonus" class="left-menu-image">
            </div>
            <div class="left-menu-text">{{ \App\Models\Page::whereSlug('bonus')->firstOrNew()->name }}</div>
        </a> --}}
    </nav>
    {{-- @endif --}}
</section>
<!-- /.left-top -->
<section class="left-bottom">
    <span class="left-bottom-text">{{ __('Download our app') }}</span>
    <button class="app-link">
        <a href="{{ config('site.store_links.app_store') }}">
            <img loading="lazy" src="{{ asset('images/site/app_store.png') }}" alt="AppStore" class="app-image">
        </a>
    </button>
    <button class="app-link">
        <a href="{{ config('site.store_links.google_play') }}">
            <img loading="lazy" src="{{ asset('images/site/google_play.svg') }}" alt="Google Play" class="app-image">
        </a>
    </button>
    <div class="social">
        @if (config('site.social_links.facebook'))
            <a href="{{ config('site.social_links.facebook') }}" target="_blank" class="social-link">
                <img src="{{ asset('images/site/fb.svg') }}" alt="facebook">
                <img src="{{ asset('images/site/fb-hover.svg') }}" alt="facebook" class="img-hover">
            </a>
        @endif
        @if (config('site.social_links.instagram'))
            <a href="{{ config('site.social_links.instagram') }}" target="_blank" class="social-link">
                <img src="{{ asset('images/site/inst.svg') }}" alt="instagram">
                <img src="{{ asset('images/site/inst-hover.svg') }}" alt="instagram" class="img-hover">
            </a>
        @endif
        @if (config('site.social_links.youtube'))
            <a href="{{ config('site.social_links.youtube') }}" target="_blank" class="social-link">
                <img src="{{ asset('images/site/youtube.svg') }}" alt="youtube">
                <img src="{{ asset('images/site/youtube-hover.svg') }}" alt="youtube" class="img-hover">
            </a>
        @endif
    </div>
    <!-- /.social -->
    {{-- <div class="inst-text">Instagram {{ number_format((instagram_data()['subscribers'] ?? 2222) / 1000, 1) }} {{ app()->getLocale() == 'uk' ? 'тис' : 'тыс'}}</div> --}}
</section>

<div class="rolls">
    {{-- <img loading="lazy" src="{{ instagram_data()['image'] ?? '/images/site/rolls.jpg' }}" alt="rolls" class="rools-image"> --}}
    <div class="fb-page" data-href="https://www.facebook.com/39minsushi" data-tabs="timeline" data-width="" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
        <blockquote cite="https://www.facebook.com/39minsushi" class="fb-xfbml-parse-ignore">
            <a href="https://www.facebook.com/39minsushi">Koto Sushi</a>
        </blockquote>
    </div>
</div>
<div class="subscribe-text">{!! __('Subscribe to our socials!' ) !!}</div>


<div class="subscribe-text">
    {!! __('г. Николаев ул. Соборная 14. 54001' ) !!}
    <br>
    Email: <a href="mailto:Kotonoodles@gmail.com">Kotonoodles@gmail.com</a>
</div>
{{-- <div class="subscribe-text"></div> --}}

<a href="https://site-devel.com/" class="oleus" rel="nofollow" target="_blank" title="">
    <img loading="lazy" src="{{ asset('images/site/oleus.png') }}" alt="OLEUS - cоздание сайтов для бизнеса">
</a>

@push('scripts')
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v8.0&appId=2327761534105492&autoLogAppEvents=1" nonce="DjznkhwL"></script>
@endpush
