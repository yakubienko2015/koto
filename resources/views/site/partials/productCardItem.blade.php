<div>
    <div class="card-item-last os-flex os-flex-column">
        <div class="media os-text-center">
            <a href="{{ route('product', [app('city'), $product->category ?? '', $product]) }}">
                <picture>
                    <source srcset="{{ $product->webp_image_medium }}" type="image/webp">
                    <img loading="lazy" src="{{ $product->image }}" alt="{{ $product->name }}">
                </picture>
            </a>
        </div>
        <div class="card-bottom">
            <div class="os-flex os-flex-middle">
                <div class="title os-flex-1">
                    <span onclick="window.location.href=this.getAttribute('href')" style="cursor: pointer;" href="{{ route('product', [app('city'), $product->category ?? '', $product]) }}">
                        {{ $product->name }}
                    </span>
                </div>
                <div class="price os-position-relative">{{ __('shop.price', [$product->price]) }}</div>
            </div>
            <div class="consist">
                {{ $product->full_ingredients }}
            </div>
            <product-card-item :product='@json($product)'></product-card-item>
        </div>
    </div>
</div>
