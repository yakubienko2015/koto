<div class="card-order">
    <div class="card-order-content">
        <div class="title-order os-text-uppercase">{{ __('shop.my_order') }}</div>
        <div class="card-sm-items">
            <mini-basket-items></mini-basket-items>
        </div>
        <div class="total-sm os-flex os-flex-between">
            <div>{{ __('shop.pay_amount') }}:</div>
            <mini-basket-sum></mini-basket-sum>
        </div>
        <div class="description os-text-center">
            <a class="os-link-order" href="{{ route('basket', app('city')) }}">
                {{ __('shop.make_order') }}
            </a>
        </div>
    </div>
</div>
