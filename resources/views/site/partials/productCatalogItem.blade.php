<div>
    <div class="catalog-item os-flex os-flex-between{{ $product->is_top ? ' mark-hit' : '' }}{{ $product->is_new ? ' mark-new' : '' }}{{ $product->is_rec ? ' mark-recommend' : '' }}">
        @if ($product->is_top)
        <div class="mark icon-hit"></div>
        @endif
        @if ($product->is_new)
        <div class="mark icon-new"></div>
        @endif
        @if ($product->is_rec)
        <div class="mark icon-recommend"></div>
        @endif
        <div class="left-item os-flex-1">
            <div class="os-flex os-flex-between">
                <div class="name os-flex-1">
                    <span onclick="window.location.href=this.getAttribute('href')" style="cursor: pointer;" href="{{ route('product', [app('city'), $product->category ?? '', $product]) }}">
                        {{ $product->name }}
                    </span>
                </div>
                <div class="price">{{ __('shop.price', [$product->price]) }}</div>
            </div>
            <product-catalog-item
                :product='@json($product)'
                hide-ingredients="{{ isset($hideIngredients) }}"
            ></product-catalog-item>
        </div>
        <div class="right-item os-text-center">
            <div class="media os-flex os-flex-middle os-flex-center">
                <a href="{{ route('product', [app('city'), $product->category ?? '', $product]) }}">
                    <picture>
                        <source srcset="{{ $product->webp_image }}" type="image/webp">
                        <img loading="lazy" src="{{ $product->image }}" alt="{{ $product->name }}">
                    </picture>
                </a>
            </div>
            <button class="link-bay" type="submit" form="cat-product-{{ $product->id }}">
                {{ __('shop.to_basket') }}
            </button>
        </div>
    </div>
</div>
