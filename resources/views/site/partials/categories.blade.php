@foreach (app('city')->getCategories() as $category)
    <a href="{{ route('category', [app('city'), $category]) }}" class="{{ $classPrefix ?? '' }}menu-item{{ (request()->category->id ?? 0) === $category->id ? ' active' : '' }}">
        @isset($blockedImg)
            <div class="left-menu-img">
                <img loading="lazy" src="{{ asset("images/site/categories/{$category->slug}.svg") }}" alt="{{ $category->name }}" class="{{ $classPrefix ?? '' }}menu-image">
            </div>
        @else
            <img loading="lazy" src="{{ asset("images/site/categories/{$category->slug}.svg") }}" alt="{{ $category->name }}" class="{{ $classPrefix ?? '' }}menu-image">
        @endisset
        <div class="{{ $classPrefix ?? '' }}menu-text">{{ $category->name }}</div>
    </a>
@endforeach
