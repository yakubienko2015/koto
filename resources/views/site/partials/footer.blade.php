<footer class="footer">
    @if (! Agent::isMobile())
    <div class="cities-block">
        <span class="text-footer">{{ __('Geography') }}</span>
        <nav class="cities-list">
            @foreach (App\Models\City::pluck('name', 'slug') as $id => $city)
                <a href="{{ route('cityIndex', $id) }}" class="city-link">
                    <span class="link-footer">{{ $city }}</span>
                </a>
            @endforeach
            @foreach (app('city')->getPagesOnFooterGeo() as $page)
                <a href="{{ route('page', [app('city'), $page]) }}" class="city-link">
                    <span class="link-footer">{{ $page->name }}</span>
                </a>
            @endforeach
        </nav>
    </div>

    <div class="footer-menu">
        <span class="text-footer">{{ __('Menu') }}</span>
        @foreach (app('city')->getCategories()->chunk(3) as $categoryChank)
        <nav class="footer-menu-list">
            @foreach ($categoryChank as $category)
                <a href="{{ route('category', [app('city'), $category]) }}" class="footer-menu-link">
                    <span class="link-footer">{{ $category->name }}</span>
                </a>
            @endforeach
        </nav>
        @endforeach
    </div>

    <div class="read">
        <span class="text-footer">{{ __('Read') }}</span>
        @foreach (app('city')->getPagesOnFooter()->chunk(3) as $pageChank)
        <nav class="read-list">
            @foreach ($pageChank as $page)
                <a href="{{ route('page', [app('city'), $page]) }}" class="read-link">
                    <span class="link-footer">{{ $page->name }}</span>
                </a>
            @endforeach
        </nav>
        @endforeach
    </div>

    <div class="customers">
        <span class="text-footer">{{ __('Customers') }}</span>
        @foreach (app('city')->getPagesOnFooterClient()->chunk(3) as $pageChank)
        <nav class="customers-list">
            @foreach ($pageChank as $page)
                <a href="{{ route('page', [app('city'), $page]) }}" class="customers-link">
                    <span class="link-footer">{{ $page->name }}</span>
                </a>
            @endforeach
        </nav>
        @endforeach
    </div>
    @endif

    @include('site.partials.footerContacts')
</footer>
<!-- /.footer -->
