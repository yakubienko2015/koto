<div class="card-account">
    <div class="card-account-content os-text-center">
        <div class="title-account os-text-uppercase">
            {{ __('Personal cabinet') }}
        </div>
        <div class="sub-title-account">
            {{ __('Bonuses and history') }}
        </div>
        @guest
        <login-form
            login-url="{{ route('login') }}"
            redirect-url="{{ route('account') }}"
        ></login-form>
        <form>
            <div>
                <a class="link-form link-registration" href="#">
                  {{ __('Register') }}
                </a>
            </div>
            <div>
                <a class="link-form" href="{{ route('password.request') }}">
                    {{ __('Forgot Your Password?') }}
                </a>
            </div>
        </form>
        @else
        <form>
            <div class="description">
                <a class="os-link-order" href="{{ route('account') }}">
                    {{ __('Profile') }}
                </a>
            </div>

            <div>
                <a class="link-form" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>
            </div>
        </form>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
        @endguest
    </div>
</div>
