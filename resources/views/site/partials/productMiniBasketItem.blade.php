<div class="card-sm os-flex os-flex-middle">
    <div class="media os-flex os-flex-middle os-flex-center">
        <img loading="lazy" src="{{ $product->image }}" alt="{{ $product->name }}">
    </div>
    <product-mini-basket-item
        :product='@json($product)'
    ></product-mini-basket-item>
</div>
