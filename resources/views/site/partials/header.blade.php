<header class="@if(request()->path() == '/') @elseif(request()->path() != app('city')->slug) not-front @else  @endif">
    <div class="os-position-relative os-flex os-flex-middle header">
        <button class="menu-button">
            <picture>
                <source srcset="{{ asset('images/site/menu_mobile.webp') }}" type="image/webp"><source>
                <img src="{{ asset('images/site/menu_mobile.png') }}" alt="Menu" class="mobile-icon">
            </picture>
        </button> <!-- /.menu-button -->
        <div class="mobile-menu">
            <div class="mobile-menu-content os-position-relative">
                <div class="os-flex os-flex-column os-flex-between os-height-1-1">
                    <div>
                        <div class="top-mobile-menu">
                            {{ __('Navigation') }}
                        </div>
                        <button class="menu-button-close">
                            <img src="{{ asset('images/site/close.svg') }}" alt="close">
                        </button>
                        <nav>
                            <ul class="mobile-menu-items">
                                <li>
                                    <a href="{{ app('city')->getIndexUrl() }}" class="mobile-menu-item">
                                        {{ __('Main page') }}
                                    </a>
                                </li>
                                <li class="parent-item">
                                    <a href="#" class="mobile-menu-item">
                                        {{ __('Menu') }}
                                    </a>
                                    <ul class="sub-menu">
                                        @foreach (app('city')->getCategories() as $category)
                                            <li>
                                                <a href="{{ route('category', [app('city'), $category]) }}" class="mobile-menu-item">
                                                    {{ $category->name }}
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                                @foreach (app('city')->getPagesOnHeader() as $page)
                                    <li>
                                        <a href="{{ route('page', [app('city'), $page]) }}" class="mobile-menu-item">
                                            {{ $page->name }}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                            <div class="lang-mobile">
                                @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                    <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ $localeCode == 'ru' ? LaravelLocalization::getNonLocalizedURL() : LaravelLocalization::getLocalizedURL($localeCode) }}" class="link-ru{{ app()->getLocale() === $localeCode ? ' active-lang' : '' }}">
                                        {{ Str::upper($localeCode === 'uk' ? 'ua' : $localeCode) }}
                                    </a>
                                @endforeach
                            </div>
                        </nav>
                    </div>
                    <div class="footer">
                        @include('site.partials.footerContacts')
                    </div>
                </div>
            </div>
            <!-- /.left-menu -->
        </div>
        <!-- /.mobile-menu -->
        @if(request()->path() == '/') @elseif(request()->path() != app('city')->slug)
        @if (config('site.jivochat'))
        <button class="mobile-connect-button">
            <img src="{{ asset('images/site/mobile.svg') }}" alt="Связаться" class="mobile-icon">
        </button>
        @else
        <a class="mobile-connect-button" href="javascript:void(0)" onclick="jivo_api.open(); return false;">
            <img src="{{ asset('images/site/mobile.svg') }}" alt="Связаться" class="mobile-icon">
        </a>
        @endif
        @endif

        @php $selectCities = app('city')->whereIsActive(true)->where('id', '<>', app('city')->id)->get(); @endphp

        <div class="location location-header">
            <a href="#" class="link-location select select-icon os-flex os-flex-middle">
                {{ app('city')->name }}
                @if ($selectCities->count())
                    <span class="myarrow"></span>
                @endif
            </a>
            @if ($selectCities->count())
            <div class="location-content">
                <ul>
                    @foreach($selectCities as $city)
                    <li>
                        <a href="{{ route('cityIndex', $city) }}">
                            {{ $city->name }}
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>
            @endif
        </div>
        @if (config('site.jivochat'))
        <a class="connect-button" href="javascript:void(0)" onclick="jivo_api.open(); return false;">
            <img src="{{ asset('images/site/phone.svg') }}" alt="Connect" class="connect-button-icon">
            <span class="connect-button-text">{{ __('Connect') }}</span>
        </a>
        @else
        <button class="connect-button">
            <img src="{{ asset('images/site/phone.svg') }}" alt="Connect" class="connect-button-icon">
            <span class="connect-button-text">{{ __('Connect') }}</span>
        </button>
        @endif
        <!-- Header menu -->
        @if (! Agent::isMobile())
        <nav class="menu-list">
            @foreach (app('city')->getPagesOnHeader() as $page)
                <a href="{{ route('page', [app('city'), $page]) }}" class="menu-link">
                    {{ $page->name }}
                </a>
            @endforeach
        </nav>
        @endif
        <div class="header-right-side">
            <!-- Search input -->
            <form id="search-form" action="{{ route('search', app('city')) }}" class="box-search-input" onsubmit="return !! document.getElementById('search-field').value">
                <input type="search" id="search-field" class="search-input" name="q" value="">
                <input type="hidden" name="c" value="{{ request('c') ?? request()->category->id ?? request()->product->category->id ?? ' ' }}">
                <button type="submit" class="search-button">
                    <img src="{{ asset('images/site/search.png') }}" alt="search-icon">
                </button>
                <button class="search-button-close">
                    <img src="{{ asset('images/site/close.svg') }}" alt="">
                </button>
            </form>
            <button type="submit" class="search-btn">
                <label for="search-field" style="margin:0">
                    <img src="{{ asset('images/site/search.png') }}" alt="search-icon">
                </label>
            </button>
            <button class="order-button">
                <img src="{{ asset('images/site/order.png') }}" alt="Order">
                <goods-counter />
            </button>
            @if (config('site.personal_cabinet_enabled'))
            @auth
                <button class="login-button active">
                    <img src="{{ asset('images/site/login-active.svg') }}" class="login-btn-active" alt="Login">
                </button>
            @else
                <button class="login-button">
                    <img src="{{ asset('images/site/login.svg') }}" alt="Login">
                </button>
            @endauth
            @endif
            <div class="lang-pc">
            @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ $localeCode == 'ru' ? LaravelLocalization::getNonLocalizedURL() : LaravelLocalization::getLocalizedURL($localeCode) }}" class="link-ru{{ app()->getLocale() === $localeCode ? ' active-lang' : '' }}">
                    {{ Str::upper($localeCode === 'uk' ? 'ua' : $localeCode) }}
                </a>
            @endforeach
            </div>
        </div>
        <!-- /.header-right-side -->

        @include('site.partials.modalBasket')
        @if (config('site.personal_cabinet_enabled'))
        @include('site.partials.modalLogin')
        @guest
            @include('site.partials.modalRegister')
        @endguest
        @endif
    </div>
</header>
