@if (app('city')->getDeliveryTime() > 0)
<a href="{{ app('city')->getIndexUrl() }}" class="logo">
    <img loading="lazy" src="{{ asset('images/site/cat.png') }}" alt="cat" class="logo-image">
</a>
<div class="clock">
    <div class="timing">{{ app('city')->getDeliveryTimeString() }}</div>
    <div class="minute">{{ __('minutes') }}</div>
</div>
@else
<a href="{{ app('city')->getIndexUrl() }}" class="logo">
    <img loading="lazy" src="{{ asset('images/site/logo_koto.svg') }}" alt="cat" class="logo-image">
</a>
@endif

