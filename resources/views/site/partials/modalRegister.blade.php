<div class="card-registration">
    <div class="card-account-content os-text-center">
        <div class="title-account os-text-uppercase">
            {{ __('Register') }}
        </div>
        <register-form
            register-url="{{ route('register') }}"
            redirect-url="{{ route('account') }}"
        ></register-form>
        <form>
            <div>
                <a class="link-form login-button" href="#">
                    {{ __('Login') }}
                </a>
            </div>
        </form>
    </div>
</div>
