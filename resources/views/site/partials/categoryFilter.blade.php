<form action="{{ route('category', [app('city'), $category]) }}" onchange="this.submit()" class="os-filter-sort">
    <div>
        <label class="{{ request('sort') == '1' ? 'active' : '' }}">
            <input name="sort" type="radio" value="1"{{ request('sort') == '1' ? ' checked' : '' }}>
            {{ __('shop.sort_by.1') }}
        </label>
        <label class="{{ request('sort') == '2' ? 'active' : '' }}">
            <input name="sort" type="radio" value="2"{{ request('sort') == '2' ? ' checked' : '' }}>
            {{ __('shop.sort_by.2') }}
        </label>
        <label class="{{ request('sort') == '3' ? 'active' : '' }}">
            <input name="sort" type="radio" value="3"{{ request('sort') == '3' ? ' checked' : '' }}>
            {{ __('shop.sort_by.3') }}
        </label>
        <label class="{{ request('sort') == '4' ? 'active' : '' }}">
            <input name="sort" type="radio" value="4"{{ request('sort') == '4' ? ' checked' : '' }}>
            {{ __('shop.sort_by.4') }}
        </label>
    </div>
    <div class="filter-consist">
        @foreach ($category->marks->whereNotNull('style')->pluck('name', 'id') as $key => $item)
            <label>
                <input class="os-checkbox" type="checkbox" onchange="$(this).parent().siblings().find('input').prop('checked', false)" name="marks[]" value="{{ $key }}"{{ in_array($key, request('marks') ?? []) ? ' checked' : '' }}>
                <span>
                    {{ $item }}
                </span>
            </label>
        @endforeach
        @foreach ($category->marks->where('style', null)->pluck('name', 'id') as $key => $item)
            <label>
                <input class="os-checkbox" type="checkbox" onchange="$(this).parent().siblings().find('input').prop('checked', false)" name="marks[]" value="{{ $key }}"{{ in_array($key, request('marks') ?? []) ? ' checked' : '' }}>
                <span>
                    {{ $item }}
                </span>
            </label>
        @endforeach
    </div>
</form>
