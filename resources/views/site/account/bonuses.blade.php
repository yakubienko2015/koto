<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Дата</th>
            <th>Сумма</th>
            <th>Заказ</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($bonusTransactions as $transaction)
        <tr>
            <td>{{ $transaction->created_at }}</td>
            <td>{{ $transaction->sum }}</td>
            <td><a href="#">({{ $transaction->order_id }})</a> {{ __('shop.price', [$transaction->sum]) }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
