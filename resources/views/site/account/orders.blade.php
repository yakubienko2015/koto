<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th style="width: 10px">#</th>
            <th>Дата</th>
            <th>Блюда</th>
            <th>Сумма</th>
            <th>Начисленные бонусы</th>
            <th>Статус</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($orders as $order)
        <tr>
            <td>{{ $order->id }}</td>
            <td>{{ $order->ordered_at }}</td>
            <td>{{ $order->dishes }}</td>
            <td>{{ __('shop.price', [$order->sum]) }}</td>
            <td>{{ $order->bonuses }}</td>
            <td>{{ __('shop.order_statuses.' . $order->status) }}</td>
            <td><a class="btn btn-success" href="{{ route('ajax.basket.repeat', $order) }}">Повторить</a></td>
        </tr>
        @endforeach
    </tbody>
</table>
{{ $orders->links() }}
