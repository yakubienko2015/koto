





<form action="{{ route('account') }}" method="post">
    @csrf

    <div class="form-group row">
        <label for="name" class="col-sm-2 col-form-label">{{ __('Name') }}</label>
        <div class="col-sm-10">
            <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror"
                   value="{{ old('name', auth()->user()->name) }}"
                   placeholder="{{ __('Name') }}" required autocomplete="name">
        </div>
        @error('name')
            <div class="invalid-feedback">
                <strong>{{ $message }}</strong>
            </div>
        @enderror
    </div>

    <div class="form-group row">
        <label for="phone" class="col-sm-2 col-form-label">{{ __('Phone') }}</label>
        <div class="col-sm-10">
            <input type="tel" name="phone" id="phone" class="form-control @error('phone') is-invalid @enderror"
                   value="{{ old('phone', auth()->user()->phone) }}" pattern="{{ config('auth.phone_pattern') }}"
                   placeholder="{{ __('Phone') }}" required autocomplete="phone">
        </div>
        @error('phone')
            <div class="invalid-feedback">
                <strong>{{ $message }}</strong>
            </div>
        @enderror
    </div>

    <div class="form-group row">
        <label for="phone" class="col-sm-2 col-form-label">{{ __('E-Mail Address') }}</label>
        <div class="col-sm-10">
            <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email', auth()->user()->email) }}"
               placeholder="{{ __('E-Mail Address') }}" required autocomplete="email">
        </div>
        @error('email')
            <div class="invalid-feedback">
                <strong>{{ $message }}</strong>
            </div>
        @enderror
    </div>

    <div class="form-group row">
        <label for="phone" class="col-sm-2 col-form-label">{{ __('Birthday') }}</label>
        <div class="col-sm-10">
            <input type="date" name="birthday" class="form-control @error('birthday') is-invalid @enderror" value="{{ old('birthday', auth()->user()->birthday ?  auth()->user()->birthday->format('Y-m-d') : null) }}"
               placeholder="{{ __('Birthday') }}" required autocomplete="birthday">
        </div>


        @error('birthday')
            <div class="invalid-feedback">
                <strong>{{ $message }}</strong>
            </div>
        @enderror
    </div>

    <div class="form-group row">
        <label for="password" class="col-sm-2 col-form-label">{{ __('Password') }}</label>
        <div class="col-sm-10">
            <input type="password" class="form-control @error('password') is-invalid @enderror"
               placeholder="{{ __('Password') }}" required autocomplete="new-password">
        </div>

        @error('password')
            <div class="invalid-feedback">
                <strong>{{ $message }}</strong>
            </div>
        @enderror
    </div>

    <div class="form-group row">
        <div class="offset-sm-2 col-sm-10">
            <button type="submit" class="btn btn-danger">Зберегти</button>
        </div>
    </div>
</form>
