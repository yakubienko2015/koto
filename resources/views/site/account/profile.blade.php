@extends('site.app')

@section('content')

<section>
    <div class="os-container">
        <div class="search-result">
            {{ __('Bonuses and history') }}
        </div>

        <div class="account-catcoin">
            <div class="os-child-width-1-2 os-grid">
                <div class="os-flex">
                    <div class="catuser os-flex os-flex-middle os-flex-center">
                        <img loading="lazy" src="{{ asset('images/site/catcoin.svg') }}" alt="kotcoin">
                    </div>
                    <div>
                        <div class="title">
                            {{ __('On your wallet') }}:
                        </div>
                        <div class="os-flex os-flex-wrap os-flex-bottom">
                            <div class="count-catcoin">
                                <div>
                                    {{ auth()->user()->kotcoins }}
                                </div>
                                <div>
                                    {{ __('kotcoins') }}
                                </div>
                            </div>
                            <div class="description">
                                <p>
                                    {{ __('Download our app') }}
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
                <div>

                    <div class="title">
                        {{ __('Change profile data') }}:
                    </div>
                    <form action="{{ route('updateAccount') }}" method="post">
                        @csrf
                        @method('PUT')

                        <div>
                            <input type="text" name="name" class="os-input" value="{{ old('name', auth()->user()->name) }}" placeholder="{{ __('Name') }}">
                            @error('name')
                                <strong>{{ $message }}</strong>
                            @enderror
                            <input type="phone" disabled class="os-input" value="{{ old('phone', auth()->user()->phone) }}" pattern="{{ config('auth.phone_pattern') }}" placeholder="{{ __('Phone') }}">
                            @error('phone')
                                <strong>{{ $message }}</strong>
                            @enderror
                        </div>
                        <div>
                            <input type="email" name="email" class="os-input" value="{{ old('email', auth()->user()->email) }}" placeholder="{{ __('E-Mail Address') }}">
                            @error('email')
                                <strong>{{ $message }}</strong>
                            @enderror
                            <input type="date" readonly class="os-input" value="{{ old('birthday', auth()->user()->birthday ?  auth()->user()->birthday->format('Y-m-d') : null) }}" placeholder="{{ __('Birthday') }}">
                            @error('birthday')
                                <strong>{{ $message }}</strong>
                            @enderror
                        </div>
                        <div>
                            <input type="password" name="password" class="os-input" placeholder="{{ __('Password') }}">
                            @error('password')
                                <strong>{{ $message }}</strong>
                            @enderror
                            <input type="password" name="password_confirmation" class="os-input" placeholder="{{ __('Confirm Password') }}">
                            <button class="btn-link">{{ __('Save') }}</button>
                        </div>
                    </form>

                </div>
            </div>

            @isset($lastOrder)
            <div class="last-see">
                {{ __('We saw at') }}: {{ $lastOrder->created_at->format('d.m.Y') }}
            </div>
            @endisset
        </div>

        <div class="catalog-items">
            @isset($lastOrder)
            <span class="title-02">{{ __('At that day you order') }}:</span>
            <div class="os-grid">
                @foreach ($lastOrder->products()->active()->activeCategory()->get() as $product)
                    @include('site.partials.productCatalogItem', $product)
                @endforeach
            </div>
            @endisset

            <div class="os-description">
                {!! $page->meta->content ?? '' !!}
            </div>
        </div>
    </div>
</section>

@endsection
