<div class="os-modal os-modal-statistic open" style="display: block">
    <div class="modal-content os-text-center os-position-relative">
        <button class="modal-icon-close">
            <img src="{{ asset('images/site/close.svg') }}" alt="close">
        </button>
        <div class="description-form">
            {{ __('Help make our service better') }}
        </div>

        <form class="card-body" action="{{ route('opinion.save', $opinion) }}" method="POST">
            @csrf

            @foreach (config('shop.opinion_marks') as $mark)
            <div class="item-static os-flex os-flex-middle os-flex-between">
                <label class="static-down">
                    <input type="radio" class="os-input" name="marks[{{ $mark }}]" value="0" required>
                    <span class="icon"></span>
                </label>
                <div>
                    {{ __($mark ?? '') }}
                </div>
                <label class="static-up">
                    <input type="radio" class="os-input" name="marks[{{ $mark }}]" value="1" required>
                    <span class="icon"></span>
                </label>
            </div>
            @endforeach

            <div class="description">
                <button type="submit" class="os-link-order">
                    {{ __('Send') }}
                </button>
            </div>
        </form>
    </div>
</div>
