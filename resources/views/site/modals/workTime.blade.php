<div class="os-modal os-modal-work-time">
    <div class="modal-content os-text-center os-position-relative">
        <div class="logo-type">
            <img loading="lazy" src="{{ asset('images/site/sleep.png') }}" alt="">
        </div>

        @php $workTime = app('city')->getTodayWorkTime(); @endphp
        <p>
            @if ($workTime['closed'])
                {{ __('Today is close') }}
            @else
                {{ __('Work from :from to :to', ['from' => $workTime['from']->format('H:i'), 'to' => $workTime['to']->format('H:i')]) }}
            @endif
        </p>

        <p>
            {{ __('You can do preorder') }}
        </p>
        <div class="description os-text-center">
            <a class="os-link-order modal-close" href="#">
                {{ __('Make preorder') }}
            </a>
        </div>
        <div>
            <button class="link-modal modal-close">
                {{ __('Skip') }}
            </button>
        </div>
        <button class="modal-icon-close modal-close">
            <img src="{{ asset('images/site/close.svg') }}" alt="">
        </button>
    </div>
</div>
