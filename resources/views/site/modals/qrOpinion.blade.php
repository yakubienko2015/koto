<div class="os-modal os-modal-statistic open" style="display: block">
    <div class="modal-content os-text-center os-position-relative">
        <button class="modal-icon-close">
            <img src="{{ asset('images/site/close.svg') }}" alt="close">
        </button>
        <div class="description-form">
            {{ __('Help make our service better 2') }}
        </div>

        <form class="card-body" action="#" id="service-better">
            <div class="item-static os-flex os-flex-middle os-flex-between">
                <label class="static-down">
                    <input type="radio" class="os-input" name="mark" id="service-better-false" value="0" required
                        data-title="{{ __('Service bad title') }}"
                        data-html="{{ __('Service bad text') }}"
                        data-confirm="{{ __('Service bad button') }}"
                        data-href="{{ app('city')->config('service_bad', 'https://docs.google.com/forms/d/1ZdMmXXhGRDtBxKg59WrQYKhUVqCMalI3C4jB50BDiig') }}">
                    <span class="icon"></span>
                    {{ __('Service bad') }}
                </label>
                <div>
                    &nbsp;
                </div>
                <label class="static-up">
                    <input type="radio" class="os-input" name="mark" id="service-better-true" value="1" required
                        data-title="{{ __('Service good title') }}"
                        data-html="{{ __('Service good text') }}"
                        data-confirm="{{ __('Service good button') }}"
                        data-href="{{ app('city')->config('service_good', 'https://g.page/kotosushi-com-ua/review?gm') }}">
                    <span class="icon"></span>
                    {{ __('Service good') }}
                </label>
            </div>

            <div class="description">
                <button type="submit" class="os-link-order">
                    {{ __('Send') }}
                </button>
            </div>
        </form>
    </div>
</div>
