<div class="os-modal os-modal-delivery">
    <div class="modal-content os-text-center os-position-relative">
        <div class="logo-type">
            <img loading="lazy" src="{{ asset('images/site/logo_lg.svg')}}" alt="logo">
        </div>
        <h3>
            {{ __('Hellp!') }}
        </h3>
        <p>
            Доставить за 39 минут<br>
            по этому адресу?
        </p>
        <div class="delivery-select">
            <select class="select">
                <option selected>Днепр</option>
                <option>Харьков</option>
                <option>Киев</option>
                <option>Хмельницкий</option>
            </select>
            <span class="myarrow"></span>
        </div>
        <div class="delivery-select">
            <select class="select">
                <option selected>Улица</option>
                <option>Улица</option>
                <option>Улица</option>
                <option>Улица</option>
            </select>
            <span class="myarrow"></span>
        </div>
        <div class="os-flex os-flex-between">
            <div class="label-text">
                <label>Дом</label>
                <div class="os-form-controls">
                    <input type="text" class="os-input" placeholder="">
                </div>
            </div>
            <div class="label-text">
                <label>Квартира</label>
                <div class="os-form-controls">
                    <input type="text" class="os-input" placeholder="">
                </div>
            </div>
        </div>
        <div class="description os-text-center">
            <button class="os-link-order" type="submit" form="basket-form" name="callback" value="0">
                {{ __('Verify') }}
            </button>
        </div>
        <div>
            <button class="link-modal modal-close">
                {{ __('Skip') }}
            </button>
        </div>
        <button class="modal-icon-close">
            <img src="{{ asset('images/site/close.svg')}}" alt="close">
        </button>
    </div>
</div>
