<div class="os-modal os-modal-location">
    <div class="modal-content os-text-center os-position-relative">
        <div class="logo-type">
            <img loading="lazy" src="{{ asset('images/site/logo_lg.png') }}" alt="">
        </div>
        <h3>
            {{ __('Hellp!') }}
        </h3>
        <p>
            {!! __('Select city') !!}
        </p>
        <div class="location location-header">
            <select class="select" id="welcome-select-city" name="select_city">
                @foreach(app('city')->all() as $city)
                    <option value="{{ route('cityIndex', $city) }}">
                        {{ $city->name }}
                    </option>
                @endforeach
            </select>
            <span class="myarrow"></span>
        </div>
        <div class="description os-text-center">
            <a class="os-link-order" href="#" id="welcome-submit-city">
                {{ __('Verify') }}
            </a>
        </div>
        <div>
            <button class="link-modal modal-close">
                {{ __('Skip') }}
            </button>
        </div>
        <button class="modal-icon-close">
            <img src="{{ asset('images/site/close.svg') }}" alt="">
        </button>
    </div>
</div>
