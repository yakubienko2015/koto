<div class="os-modal os-modal-callback">
    <div class="modal-content os-text-center os-position-relative">
        <button class="modal-icon-close">
            <img src="{{ asset('images/site/close.svg')}}" alt="close">
        </button>
        <button class="menu-button-close">
            <img src="{{ asset('images/site/close.svg')}}" alt="close">
        </button>
        <div class="top-modal-form">
            {{ __('Connect manager') }}
        </div>
        <div class="callback-pc">
            <div class="title-form os-text-uppercase">
                {{ __('Send message for connect') }}
            </div>
            <form action="{{ route('ajax.callback') }}" method="POST">
                @csrf

                <input name="phone" type="tel" class="os-input" placeholder="{{ __('Enter phone') }}" required>
                <input name="name" type="text" class="os-input" placeholder="{{ __('Enter name') }}" required>
                {{-- <div class="location location-header">
                    <select class="select" onchange="location = this.value;">
                        @foreach(app('city')->all() as $city)
                            <option value="{{ route('cityIndex', $city) }}"{{ app('city')->id === $city->id ? ' selected' : '' }}>{{ $city->name }}</option>
                        @endforeach
                    </select>
                    <span class="myarrow"></span>
                </div> --}}
                <textarea name="comment" class="os-textarea" placeholder="{{ __('Enter comment') }}" rows="6"></textarea>
                <div class="description">
                    <button class="os-link-order">
                        {{ __('Send') }}
                    </button>
                </div>
            </form>
        </div>
        <div class="callback-mobile">
            <div class="title-form os-text-uppercase">
                {!! __('Choose callback way') !!}
            </div>

            <div class="box-icon-manager">
                <a href="https://telegram.me/Kotosushi_bot">
                    <img loading="lazy" src="{{ asset('images/site/icon-call-01.png')}}" alt="telegram">
                    <span>Телеграм</span>
                </a>
                @if (config('site.jivochat'))
                    <a href="javascript:jivo_api.open()">
                        <img loading="lazy" src="{{ asset('images/site/icon-call-02.png')}}" alt="chat">
                        <span>Чат на сайте</span>
                    </a>
                @endif
                <a href="viber://pa?chatURI=kotobot">
                    <img loading="lazy" src="{{ asset('images/site/icon-call-03.png')}}" alt="viber">
                    <span>Вайбер</span>
                </a>
                <a href="tel:+380986689999">
                    <img loading="lazy" src="{{ asset('images/site/icon-call-04.png')}}" alt="phone">
                    <span>По телефону</span>
                </a>
            </div>
        </div>
    </div>
</div>
