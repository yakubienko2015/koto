<div class="os-modal os-modal-bowles">
    <div class="modal-content os-text-center os-position-relative">
        <button class="modal-icon-close">
            <img src="{{ asset('images/site/close.svg')}}" alt="close">
        </button>
        <div class="top-modal-form">
            {{ __('shop.bowl_constructor.title') }}
        </div>
        <div class="title-form os-text-uppercase">
            {{ __('shop.bowl_constructor.description') }}
        </div>

        <bowl-constructor
            :ingredients='@json(\App\Models\BowlIngredient::getIngredientsTreeInArray())'
        />
    </div>
</div>

<div class="mobile-bowl">
    <div class="mobile-menu-content os-position-relative">
        <div class="top-mobile-menu">
            {{ __('shop.bowl_constructor.title') }}
        </div>
        <button class="menu-button-close">
            <img src="{{ asset('images/site/close.svg')}}" alt="close">
        </button>
        <div class="bowl-content-mb os-text-center">
            <div class="title-form os-text-uppercase">
                {{ __('shop.bowl_constructor.description') }}
            </div>

            <bowl-constructor
                :ingredients='@json(\App\Models\BowlIngredient::getIngredientsTreeInArray())'
            />
        </div>
    </div>
</div>
