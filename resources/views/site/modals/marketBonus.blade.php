<div class="os-modal os-modal-market open" style="display: block">
    <div class="modal-content os-text-center os-position-relative">
        <h3>
            {{ __('We take you bonuses') }}
        </h3>
        <p>
            {{ __('Pass your phone') }}
        </p>
        <form class="card-body" action="{{ route('qrStoreSave', $marketType) }}" method="POST">
            @csrf

            <input type="tel" maxlength="50" class="os-input" style="margin-bottom: 0;" placeholder="{{ __('shop.forms.phone') }}" required autocomplete="phone" name="phone">

            <div class="description">
                <button type="submit" class="os-link-order">
                    {{ __('OK') }}
                </button>
            </div>
            <div class="description-form" style="margin-top: 0;">
                {{ __('You get bonuses after first order') }}
            </div>
        </form>
    </div>
</div>
