@extends('site.app')

@section('content')

<article>
    <div class="page">
        <h1 class="title-page">
            {{ $page->name }}
        </h1>

        <div class="description-page">
            <nav>
                <ul>
                    @foreach ($pages as $page)
                    <li>
                        @if ($page->slug === '' && app('city')->id == 1)
                        <a href="{{ route('index') }}">{{ $page->name }}</a>
                        @else
                        <a href="{{ route('page', [app('city'), $page]) }}">{{ $page->name }}</a>
                        @endif
                        @if ($page->slug === 'news')
                        <ul>
                            @foreach ($posts as $post)
                            <li>
                                <a href="{{ route('post', [app('city'), $post]) }}">{{ $post->name }}</a>
                            </li>
                            @endforeach
                        </ul>
                        @endif
                    </li>
                    @endforeach
                    @foreach ($categories as $category)
                    <li>
                        <a href="{{ route('category', [app('city'), $category]) }}">{{ $category->name }}</a>
                        <ul>
                            @foreach ($category->products as $product)
                            <li>
                                <a href="{{ route('product', [app('city'), $category, $product]) }}">{{ $product->name }}</a>
                            </li>
                            @endforeach
                        </ul>
                    </li>
                    @endforeach
                </ul>
            </nav>
        </div>
    </div>
</article>

@endsection
