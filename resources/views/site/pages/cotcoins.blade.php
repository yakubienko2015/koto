@extends('site.app')

@section('content')

<article>
    <div class="page page-catcoin">
        <div class="os-grid">
            <div class="os-width-2-3">

                <h1 class="title-page">
                    {{ $page->name }}
                </h1>

                <div class="description ck-content">
                    {!! $page->meta->content ?? '' !!}
                </div>

                <how-many-kotcoins
                    source-url="{{ route('ajax.howManyKotcoins') }}"
                ></how-many-kotcoins>

                <div class="os-text-center">
                    <a class="link-page" href="{{ route('category', [app('city'), \App\Models\Category::menu()->first()]) }}">
                        {{ __('Go to catalog') }}
                    </a>
                </div>
            </div>

            <div class="os-width-1-3 page-other-news">
                <div class="front-news">
                    @foreach (\App\Models\Post::active()->inCity()->limit(3)->get() as $post)
                        @include('site.partials.postCardItem', ['post' => $post])
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</article>

@endsection
