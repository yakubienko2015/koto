@extends('site.app')

@section('content')

<section>
    <div class="os-container">
        <div class="search-result">
            {{ __('Search results') }}: <span>{{ request('q') }}</span>
        </div>
        <div class="catalog-items">
            <div class="os-grid">
                @foreach ($items as $item)
                    @include('site.partials.productCatalogItem', ['product' => $item])
                @endforeach
            </div>

            {{-- <div class="os-text-center">
                <button class="link-page">
                    Показать все результаты
                </button>
            </div> --}}

            <div class="os-description ck-content">
                {!! $page->meta->content ?? '' !!}
            </div>
        </div>
    </div>
</section>

@endsection
