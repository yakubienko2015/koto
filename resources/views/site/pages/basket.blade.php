@extends('site.app')

@section('content')

<h1 class="order-title">{{ $page->name }}</h1>
<section class="order-content">
    <div class="payment" id="basket-form">
        <main-basket
            submit-url="{{ route('ajax.basket.order') }}"
            :payments='@json(app('city')->payments)'
            user-name="{{ auth()->user()->name ?? '' }}"
            user-phone="{{ auth()->user()->phone ?? '' }}"
            :cities='@json(app('city')->whereIsActive(true)->pluck('name', 'id'))'
            :city='@json(app('city'))'
            start-date="{{ now()->addMinutes(122)->format('Y-m-d') }}"
            start-time="{{ now()->addMinutes(122)->format('H:i') }}"
            :min-basket-sum="{{ config('shop.min_basket_sum') }}"
            :min-basket-payment-sum="{{ config('shop.min_basket_payment_sum') }}"
            :kotcoin-rate="{{ 1 }}"
            start-promo="{{ request()->cookie('promo') ?? '' }}"
        />

        <div class="download-app">
            <span class="download-app-text">{!! __('shop.basket.download_app') !!}</span>
            <button class="app-link">
                <a href="{{ config('site.store_links.app_store') }}">
                    <img loading="lazy" src="{{ asset('images/site/app_store.svg') }}" alt="AppStore" class="app-image">
                </a>
            </button>
            <button class="app-link">
                <a href="{{ config('site.store_links.google_play') }}">
                    <img loading="lazy" src="{{ asset('images/site/google_play.svg') }}" alt="Google Play" class="app-image">
                </a>
            </button>
        </div>
    </div>
    <basket-items></basket-items>
</section>
<div class="sauce-description ck-content">{!! $page->meta->content ?? '' !!}</div>

@if (Agent::isMobile())
    <section class="left-bottom store-links os-text-center">
        <div class="left-bottom-text">{{ __('Download our app') }}</div>
        <button class="app-link">
            <a href="{{ config('site.store_links.app_store') }}">
                <img loading="lazy" src="{{ asset('images/site/app_store.png') }}" alt="AppStore" class="app-image">
            </a>
        </button>
        <button class="app-link">
            <a href="{{ config('site.store_links.google_play') }}">
                <img loading="lazy" src="{{ asset('images/site/google_play.svg') }}" alt="Google Play" class="app-image">
            </a>
        </button>
    </section>
@endif

@if (app('order')->recommends()->count())
<div class="recommend-product">
    <span class="os-text-uppercase title-02">
        {{ __('Worth to try') }}
    </span>
    <div class="catalog-items">
        <div class="os-child-width-1-4 os-grid">
            @foreach (app('order')->recommends()->get() as $recommendProduct)
                @include('site.partials.productCatalogItem', ['product' => $recommendProduct])
            @endforeach
        </div>
    </div>
</div>
@endif

@include('site.modals.delivery')

@endsection

@if (app('city')->getDeliveryTime() == 59)
@push('scripts')
    <script>
        if (! sessionStorage.getItem('overloadedConfirmed')) {
            setTimeout(function () {
                Swal.fire({
                    title: '{{ __('Attend') }}',
                    html: '<p>{{ __('We overloaded') }}</p><p><small>{{ __('We overloaded small') }}</small></p>',
                    confirmButtonText: '{{ __('shop.push_confirm') }}',
                    customClass: {
                        title: 'swal2-title color',
                    }
                }).then((result) => {
                    if (result.isConfirmed) {
                        sessionStorage.setItem('overloadedConfirmed', true)
                    }
                })
            }, 1000)
        }
    </script>
@endpush
@endif

@if (app('city')->getDeliveryTime() == 0)
@push('scripts')
    <script>
        if (! sessionStorage.getItem('overloadedConfirmed')) {
            setTimeout(function () {
                Swal.fire({
                    title: '{{ __('shop.push_title') }}',
                    text: '{{ __('validation.is_overloaded') }}',
                    confirmButtonText: '{{ __('shop.push_confirm') }}'
                }).then((result) => {
                    if (result.isConfirmed) {
                        sessionStorage.setItem('overloadedConfirmed', true)
                    }
                })
            }, 1000)
        }
    </script>
@endpush
@endif
