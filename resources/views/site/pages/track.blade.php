@extends('site.app')

@section('content')

<article>
    <div class="page page-food-meter">
        <div class="os-grid">
            <div class="os-width-2-3">

                <h1 class="title-page">
                    {{ __('Your order in work') }}
                </h1>

                <div class="countdown-title os-text-center">
                    {{ __('Delivery countdown') }}:
                </div>

                <shop-tracker
                    :order='@json($order)'
                ></shop-tracker>

                <div class="social-block os-text-center">
                    @if (config('site.social_links.facebook'))
                        <a href="{{ config('site.social_links.facebook') }}" target="_blank">Facebook</a>
                    @endif
                    @if (config('site.social_links.instagram'))
                        <a href="{{ config('site.social_links.instagram') }}" target="_blank">Instagram</a>
                    @endif
                    @if (config('site.social_links.youtube'))
                        <a href="{{ config('site.social_links.youtube') }}" target="_blank">YouTube</a>
                    @endif
                </div>

                @if (! isset($order->data['game_score']))
                    <div class="description os-text-center">
                        <a class="link-green" class="" href="{{ route('play', [app('city'), Crypt::encryptString($order->id)]) }}">
                            {{ __('Play gift game') }}
                        </a>
                    </div>
                @endif

                <div class="description ck-content">
                    {!! $page->meta->content ?? '' !!}
                </div>
            </div>

            <div class="os-width-1-3 page-other-news">
                <div class="front-news">
                    <span class="title-02">
                        {{ __('Last news') }}:
                    </span>

                    @foreach ($topPosts as $post)
                        @include('site.partials.postCardItem', ['post' => $post, 'mediaImg' => true])
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</article>

{{-- {{ route('track', session('last_order', 0)) }} --}}

@endsection
