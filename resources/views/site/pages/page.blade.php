@extends('site.app')

@section('content')

<article>
    <div class="page">
        <h1 class="title-page">
            {{ $page->name }}
        </h1>

        <div class="description-page ck-content">
            {!! $page->meta->content ?? '' !!}
        </div>
    </div>
</article>

@endsection
