@extends('site.app')

@section('meta')
    @if ($posts->currentPage() != 1)
        @section('title', $metas->title . ' | ' . $posts->currentPage())
        @section('meta.description', $metas->description . ' | ' . $posts->currentPage())
        @section('meta.robots', '')
        <link rel="prev" href="{{ $posts->currentPage() == 2 ? route('posts', app('city')) : $posts->previousPageUrl() }}">
    @endif
    @if ($posts->currentPage() != $posts->lastPage())
        <link rel="next" href="{{ $posts->nextPageUrl() }}">
    @endif
@stop

@section('content')

<article>
    <div class="front-news page-news items-news">
        <h1 class="title-page">
            {{ $page->name }}
        </h1>
        <div class="os-grid os-child-width-1-2">
            @foreach ($posts as $post)
                @include('site.partials.postCardItem', ['post' => $post])
            @endforeach
        </div>

        <div class="pagination-mobile">
            {{ $posts->onEachSide(1)->links('site.partials.pagination') }}
        </div>

        <div class="pagination-pc">
            {{ $posts->onEachSide(1)->links('site.partials.pagination') }}
        </div>
    </div>
    <div class="description-page ck-content">
        {!! $page->meta->content ?? '' !!}
    </div>
</article>

@endsection
