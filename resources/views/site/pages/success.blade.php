@extends('site.app')

@section('content')

<section>
    <div class="page">
        <h1 class="title-page os-text-center">
            {{ __('Thanks page') }}
        </h1>

        <div class="description os-text-center">
            <a href="{{ route('track', [app('city'), session('last_order', 0)]) }}" class="btn btn-success">{{ __('Go to tracker') }}</a>
        </div>
    </div>
</section>

@endsection
