@extends('site.app')

@section('content')

<article>
    <div class="page">
        <span class="os-text-uppercase">
            {{ __('Top products') }}
        </span>
        <div class="slider slider-hit">
            @foreach (App\Models\Product::top()->with('subproducts')->get() as $product)
                @include('site.partials.productCardItem', ['product' => $product, 'hideIngredients' => true])
            @endforeach
        </div>

        <h1 class="title-page">
            {{ $page->name }}
        </h1>

        <div class="description-page ck-content">
            {!! $page->meta->content ?? '' !!}
        </div>
    </div>
</article>

@endsection
