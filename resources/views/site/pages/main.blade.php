@extends('site.app')

@section('content')

<section>
    <div class="main-block os-flex">
        <div class="front-slider">
                <div class="slider slider-main">
                    @foreach ($slides as $slide)
                    <div>
                        <div class="image os-flex">
                            <div class="bestsellers">
                                <div class="seo-block">
                                    <span class="title-01">
                                        {{ $slide->title }}
                                    </span>
                                    <div class="description">
                                        <p>
                                            {{ $slide->text }}
                                        </p>
                                        <a class="os-link-order" href="{{ $slide->button_link }}">
                                            {{ $slide->button_text }}
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <a href="{{ $slide->button_link }}">
                                <picture>
                                    <source srcset="{{ $slide->webp_image }}" type="image/webp">
                                    <img loading="lazy" src="{{ $slide->image }}" alt="slide">
                                </picture>
                            </a>
                        </div>
                    </div>
                    @endforeach
                </div>
        </div>
    </div>
</section>

<section>
    <div class="cards-last">
        <span class="os-text-uppercase title-02">
            {{ __('Top products') }}
        </span>
        <div class="slider slider-hit">
            @foreach ($topProducts as $product)
                @include('site.partials.productCardItem', ['product' => $product, 'hideIngredients' => true])
            @endforeach
        </div>
    </div>
</section>

<nav class="category-menu-mobile os-text-center front-category-menu">
    @include('site.partials.categories')
    {{-- @if (\App\Models\Page::whereSlug('bonus')->active()->count())
    <a href="{{ route('page', [app('city'), 'bonus']) }}" class="menu-item{{ request()->is('/*/bonus') ? ' active' : '' }}">
        <img src="{{ asset("images/site/present.svg") }}" alt="bonus" class="left-menu-image">
        <div class="menu-text">{{ \App\Models\Page::whereSlug('bonus')->firstOrNew()->name }}</div>
    </a>
    @endif --}}
</nav>

<section>
    <div class="advantages">
        <div class="description ck-content">
            <h1>{{ __('Main page title') }}</h1>
            {!! $page->meta->content ?? '' !!}
        </div>
        <div class="os-child-width-1-4 os-grid">
            <div>
                <div class="os-flex item">
                    <div>
                        <img loading="lazy" src="{{ asset('images/site/delivery.png') }}" alt="image">
                    </div>
                    <div class="os-flex-1">
                        <div class="title os-text-uppercase">
                            {{ __('Main box 1 title') }}
                        </div>
                        <div class="sub-title">
                            {{ __('Main box 1 description') }}
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="os-flex item">
                    <div>
                        <img loading="lazy" src="{{ asset('images/site/cat.png') }}" alt="image">
                    </div>
                    <div class="os-flex-1">
                        <div class="title os-text-uppercase">
                            {{ __('Main box 2 title') }}
                        </div>
                        <div class="sub-title">
                            {{ __('Main box 2 description') }}
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="os-flex item">
                    <div>
                        <img loading="lazy" src="{{ asset('images/site/tasty.png') }}" alt="image">
                    </div>
                    <div class="os-flex-1">
                        <div class="title os-text-uppercase">
                            {{ __('Main box 3 title') }}
                        </div>
                        <div class="sub-title">
                            {{ __('Main box 3 description') }}
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="os-flex item">
                    <div>
                        <img loading="lazy" src="{{ asset('images/site/cat_up.png') }}" alt="image">
                    </div>
                    <div class="os-flex-1">
                        <div class="title os-text-uppercase">
                            {{ __('Main box 4 title') }}
                        </div>
                        <div class="sub-title">
                            {{ __('Main box 4 description') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<article>
    @if (! Agent::isMobile())
    <div class="front-news">
        <div class="os-child-width-1-2 os-grid">
            @include('site.partials.postCardItem', ['post' => $topPosts->get(0), 'mediaImg' => true])
            <div>
                @include('site.partials.postCardItem', ['post' => $topPosts->get(1), 'mediaImg' => true])
                @include('site.partials.postCardItem', ['post' => $topPosts->get(2), 'mediaImg' => true])
            </div>
        </div>
    </div>
    @endif
    <div class="front-news-mobile">
        <span class="title-02 os-text-uppercase">{{ __('Stocks and kotcoins') }}</span>
        <div class="slider slider-news">
            @foreach ($topPosts as $post)
                @include('site.partials.postCardItem', ['post' => $post, 'mobileFlex' => true])
            @endforeach
        </div>
    </div>
</article>

@if (Agent::isMobile())
<section class="left-bottom store-links">
    <div class="left-bottom-text">{{ __('Download our app') }}</div>
    <button class="app-link">
        <a href="{{ config('site.store_links.app_store') }}">
            <img loading="lazy" src="{{ asset('images/site/app_store.png') }}" alt="AppStore" class="app-image">
        </a>
    </button>
    <button class="app-link">
        <a href="{{ config('site.store_links.google_play') }}">
            <img loading="lazy" src="{{ asset('images/site/google_play.svg') }}" alt="Google Play" class="app-image">
        </a>
    </button>
</section>
@endif

<section>
    <div class="front-product-top">
        <span class="title-02 os-text-uppercase">{{ __('Top ramens') }}</span>

        @foreach ($topRamens as $product)
            @include('site.partials.productRamenItem', ['product' => $product])
        @endforeach
    </div>
</section>

@endsection
