@extends('site.app')

@if (! $product->meta->title)
@section('title', __('shop.metas_template_product.title', ['name' => $product->name, 'category' => $product->category->name, 'city' => app('city')->name_r]))
@endif
@if (! $product->meta->description)
@section('meta.description', __('shop.metas_template_product.description', ['name' => $product->name, 'category' => $product->category->name, 'city' => app('city')->name_r, 'price' => $product->price, 'phone' => config('site.phones')[0] ?? '']))
@endif

@section('content')

<section>
    <div class="os-container">
        <div>
            <ul class="os-breadcrumb os-flex os-flex-wrap">
                <li><a href="{{ app('city')->getIndexUrl() }}">{{ __('Main page') }}</a></li>
                <li>
                    <a href="{{ route('category', [app('city'), $product->category]) }}">
                        {{ $product->category->name }}
                    </a>
                </li>
                <li>
                    <span>
                        {{ $product->name }}
                    </span>
                </li>
            </ul>
        </div>

        <div class="card-product">
            <div class="os-flex">
                <div class="card-left os-flex-1 os-flex">
                    <div class="card-media">
                        <img src="{{ $product->image }}" alt="{{ $product->name }}">
                    </div>
                    <div class="card-content">
                        <form class="card-content-box os-flex os-flex-column os-flex-between" action="{{ route('ajax.basket.add') }}" method="POST">
                            @csrf
                            @method('PUT')
                            <input type="hidden" name="products[0][id]" value="{{ $product->id }}">

                            <div>
                                <div class="os-flex os-flex-middle os-flex-between">
                                    <h1 class="os-text-uppercase">{{ $product->name }}</h1>
                                    <div class="amount-mobile">
                                        {{ __('shop.amount', [$product->amount]) }}
                                        |
                                        {{ __("shop.measure_units.{$product->measure_unit}", [$product->weight]) }}
                                    </div>
                                </div>
                                @foreach ($product->marks->where('style') as $mark)
                                    <div class="mark {{ $mark->style }}">
                                        {{ $mark->name }}
                                    </div>
                                @endforeach
                                <div class="amount">
                                    {{ __('shop.amount', [$product->amount]) }}
                                    |
                                    {{ __("shop.measure_units.{$product->measure_unit}", [$product->weight]) }}
                                </div>
                                <div class="params">
                                    <div class="param os-flex">
                                        <div>{{ __('shop.consist') }}:</div>
                                        <div class="os-flex-1">
                                            {{ $product->ingredients }}
                                            @foreach ($product->subproducts as $subproduct)
                                                {{ $loop->index ? ', ' : '' }}<u title="{{ $subproduct->ingredients }}">
                                                    {{ $subproduct->name }}
                                                </u>
                                            @endforeach
                                        </div>
                                    </div>
                                    @if ($product->energy_amount > 0)
                                    <div class="param os-flex">
                                        <div>{{ __('shop.energies') }}:</div>
                                        <div class="os-flex-1">
                                            @if ($product->fiber_amount > 0 && $product->fat_amount > 0 && $product->carbohydrate_amount > 0)
                                                {{ __('shop.energy_detail', [round($product->energy_amount), round($product->fiber_amount), round($product->fat_amount), round($product->carbohydrate_amount)]) }}
                                            @else
                                                {{ __('shop.energy', [round($product->energy_amount)]) }}
                                            @endif
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>

                            <product-item
                                :product='@json($product)'
                                :improved-product='@json($product->improvedProduct()->with('subproducts')->first())'
                            />
                        </form>
                    </div>
                </div>
            </div>
        </div>

        @if ($product->recommends()->count())
        <span class="title-02">
            {{ __('Worth to try') }}:
        </span>
        <div class="catalog-items">
            <div class="os-grid">
                @foreach ($product->recommends()->get() as $recommendProduct)
                    @include('site.partials.productCatalogItem', ['product' => $recommendProduct])
                @endforeach
            </div>
        </div>
        @endif
        <div class="os-description ck-content">
            {!! $product->meta->content ?? '' !!}
        </div>
    </div>
</section>

@endsection
