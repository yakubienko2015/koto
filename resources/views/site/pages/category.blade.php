@extends('site.app')

@if (request()->has('marks') || request()->has('sort'))
@section('meta.robots', 'noindex, nofollow')
@endif

@section('content')

<section>
    <div class="os-container">
        <ul class="os-breadcrumb os-flex">
            <li><a href="{{ app('city')->getIndexUrl() }}">{{ __('Main page') }}</a></li>
            <li><span>{{ $category->name }}</span></li>
        </ul>
        <nav class="category-menu-mobile os-text-center nav-category">
            @include('site.partials.categories')
            {{-- <a href="{{ route('page', [app('city'), 'bonus']) }}" class="menu-item{{ request()->is('/*/bonus') ? ' active' : '' }}">
                <img src="{{ asset("images/site/present.svg") }}" alt="bonus" class="left-menu-image">
                <div class="menu-text">{{ \App\Models\Page::whereSlug('bonus')->firstOrNew()->name }}</div>
            </a> --}}
        </nav>
        <h1 class="title-page">{{ $category->name }}</h1>
        <div class="catalog-sort">
            <button class="os-btn-sort">
                <span class="os-flex-inline os-flex-middle arrow-controls">
                    <img src="{{ asset('images/site/controls.svg') }}" alt="controls">
                    {{ __('Filter') }}
                </span>
                <img src="{{ asset('images/site/arrow-color.svg') }}" alt="arrow">
            </button>

            @include('site.partials.categoryFilter')
        </div>
        {{--@if ($category->id === (int) config('site.bowls_category_id'))--}}
            {{--<button class="os-btn-bowles mb">--}}
                {{--<span class="os-flex-inline os-flex-middle arrow-controls">--}}
                    {{--<img src="{{ asset('images/site/bowls_constructor.svg') }}" alt="bowls_constructor">--}}
                    {{--{{ __('shop.bowl_constructor.title') }}--}}
                {{--</span>--}}
                {{--<img src="{{ asset('images/site/arrow-color.svg') }}" alt="arrow">--}}
            {{--</button>--}}
        {{--@endif--}}
        <div class="catalog-items">
            <div class="os-grid">
                @if ($category->id === (int) config('site.bowls_category_id'))
                    @include('site.partials.bowlCatalogItem')
                @endif
                @foreach ($products as $product)
                    @include('site.partials.productCatalogItem', ['product' => $product])
                @endforeach
            </div>
            <div class="os-description ck-content">
                {!! $category->meta->content ?? '' !!}
            </div>
        </div>
    </div>
</section>

<div class="mobile-filter">
    <div class="mobile-menu-content os-position-relative">
        <div class="top-mobile-menu">
            {{ __('Filter') }}
        </div>
        <button class="menu-button-close">
            <img src="{{ asset('images/site/close.svg') }}" alt="close">
        </button>

        @include('site.partials.categoryFilter')
    </div>
</div>

@if ($category->id === (int) config('site.bowls_category_id'))
    @include('site.modals.bowlConstructor')
@endif

@endsection
