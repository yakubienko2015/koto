@extends('site.app')

@if (! ($post->meta->title ?? ''))
@section('title', __('shop.metas_template_post.title', ['name' => $post->name, 'city' => app('city')->name]))
@endif
@if (! ($post->meta->description ?? ''))
@section('meta.description', __('shop.metas_template_post.description', ['name' => $post->name, 'city' => app('city')->name, 'phone' => config('site.phones')[0] ?? '']))
@endif

@section('content')

<article>
    <div class="front-news page-news">
        <div class="os-grid">
            <div class="os-width-2-3 page-last-news">
                <div class="item">
                    <div class="os-flex">
                        <h1 class="title os-flex-1">
                            {{ $post->name }}
                        </h1>
                        <div class="date">
                            {{ $post->created_at->format('d.m.Y') }}
                        </div>
                    </div>
                    <div class="media os-background-cover" style="background-image: url({{ $post->image }})"></div>
                    <div class="teaser os-flex-1 os-flex os-flex-column os-flex-between">
                        <div>
                            <div class="sub-title ck-content">
                                {!! $post->meta->content ?? '' !!}
                            </div>
                        </div>
                        <div>
                        </div>
                    </div>
                </div>

                <div class="os-flex os-flex-between box-news-pagination">
                    @if($post->prev())
                        <a href="{{ route('post', [app('city'), $post->prev->slug]) }}">{{ __('Prev') }}</a>
                    @endif
                    @if($post->next())
                        <a href="{{ route('post', [app('city'), $post->next->slug]) }}">{{ __('Next') }}</a>
                    @endif
                </div>

                <div class="box-more-news os-text-center">
                    <a href="{{ route('posts', app('city')) }}">
                        {{ __('To all news') }}
                    </a>
                </div>
            </div>

            <div class="os-width-1-3 page-other-news">
                @foreach ($post->interested()->limit(3)->get() as $post)
                    @include('site.partials.postCardItem', ['post' => $post])
                @endforeach
            </div>
        </div>
    </div>
</article>

@endsection
