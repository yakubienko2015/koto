@extends('site.app')

@section('content')

<section class="game-body">
    <div class="modal-info">
        <div class="info-content">
            {!! __('Rotate for play') !!}
        </div>
        <div class="game-img">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
    <div class="game-page">
        <koto-game
            redirect-url="{{ route('track', [app('city')->slug, $order]) }}"
            order-id="{{ $order->id }}"
        ></koto-game>
    </div>
</section>

@endsection
