@extends('adminlte::auth.passwords.confirm')

@section('title', __('Confirm Password'))

@section('body')
    <div class="lockscreen-wrapper">
        <div class="lockscreen-logo">
            <a href="/">{!! config('adminlte.logo', '<b>Admin</b>LTE') !!}</a>
        </div>

        <div class="lockscreen-name">{{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email }}}</div>

        <div class="lockscreen-item">
            @if(config('adminlte.usermenu_image'))
            <div class="lockscreen-image">
                <img src="{{ Auth::user()->adminlte_image() }}" alt="{{ Auth::user()->name }}">
            </div>
            @endif

            <form method="POST" action="{{ route('password.confirm') }}" class="lockscreen-credentials @if(!config('adminlte.usermenu_image'))ml-0 @endif">
                @csrf

                <div class="input-group">
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="{{ __('Password') }}" autofocus>

                    <div class="input-group-append">
                        <button type="submit" class="btn"><i class="fas fa-arrow-right text-muted"></i></button>
                    </div>
                </div>
            </form>
        </div>
        @error('password')
            <div class="lockscreen-subitem text-center" role="alert">
                <b class="text-danger">{{ $message }}</b>
            </div>
        @enderror
        <div class="help-block text-center">
            {{ __('Please confirm your password before continuing.') }}
        </div>
        <div class="text-center">
            <a href="{{ route('password.request') }}">
                {{ __('Forgot Your Password?') }}
            </a>
        </div>
    </div>
@stop
