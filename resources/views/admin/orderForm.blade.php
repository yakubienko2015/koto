@extends('components.admin.form')

@section('form')
    <x-admin.tabs>
        <x-admin.tab title="ОСНОВНЫЕ ПАРАМЕТРЫ" active id="general">
            <x-admin.inputs.text name="uuid" label="Iiko uuid" :value="old('uuid', $order->uuid)" />
            <x-admin.inputs.select
                name="terminal_id"
                label="Точка доставки"
                placeholder="-- Выбрать --"
                :options="$terminals"
                :value="old('terminal_id', $order->terminal_id)"
            />
            <x-admin.inputs.select
                name="user_id"
                label="Пользователь"
                placeholder="-- Выбрать --"
                :options="$users"
                :value="old('user_id', $order->user_id)"
            />
            <x-admin.inputs.text type="datetime" name="ordered_at" label="Дата доставки" :value="old('ordered_at', $order->ordered_at)" required />
            <x-admin.inputs.text name="phone" label="Телефон пользователя" :value="old('phone', $order->phone)" required />
            <x-admin.inputs.text name="customer_name" label="Имя пользователя" :value="old('customer_name', $order->customer_name)" required />
            <x-admin.inputs.select
                name="payment_type"
                label="Способ оплаты"
                :options="$paymentTypes"
                :value="old('payment_type', $order->payment_type)"
                required
            />
            <x-admin.inputs.text name="sum" label="Сумма" :value="old('sum', $order->sum)" required />
            <x-admin.inputs.select
                name="status"
                label="Статус"
                :options="$statuses"
                :value="old('status', $order->status)"
                required
            />
        </x-admin.tab>
    </x-admin.tabs>
@stop
