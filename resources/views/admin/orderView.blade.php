@extends('components.admin.view')

@section('content')
<div class="pb-1">
    <div class="invoice p-3 mb-3">
        <div class="row">
            <div class="col-12">
                <h4>
                    <i class="fas fa-globe"></i> {{ config('app.name') }}
                    <small class="float-right">Дата создания: {{ $order->created_at }}</small>
                </h4>
            </div>
        </div>
        <!-- info row -->
        <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
                <b>Заказ №{{ $order->id }}</b> #{{ $order->number ?? '-' }}<br>
                <b>Iiko ID:</b> {{ $order->uuid }}<br>
                <b>Дата:</b> {{ $order->ordered_at->format('Y-m-d') }}<br>
                <b>Время:</b> {{ $order->ordered_at->format('H:i:s') }}<br>
                <b>Кол-во персон:</b> {{ $order->data['persons'] ?? '-' }}<br>
                <b>Промокод:</b> {{ $order->data['promo'] ?? '-' }}<br>
                <b>Создано в:</b> {{ $order->data['source'] ?? '-' }}<br>
                @isset($order->data['game_score'])
                    <b>Очки в игре:</b> {{ $order->data['game_score'] }}
                    @isset($order->data['game_score_sended'])
                        ({{ $order->data['game_score_sended'] ? 'Успешно' : 'Не успешно' }})
                    @endisset
                    <br>
                @endisset
            </div>
            <div class="col-sm-4 invoice-col">
                <b>Пользователь</b><br>
                <b>Имя:</b>
                <a href="{{ route('admin.users.edit', $order->user ?? 0) }}" target="_blank">{{ $order->customer_name }}</a>
                <br>
                <b>Телефон:</b> {{ $order->phone }}<br>
                <b>Почта:</b> {{ $order->user->email ?? '' }}
            </div>
            <div class="col-sm-4 invoice-col">
                <b>Оплата и доставка</b><br>
                <b>Передзвонить:</b> {{ ($order->data['callback'] ?? true) ? 'Да' : 'Нет' }}<br>
                <b>Предзаказ:</b> {{ ($order->data['pre_order'] ?? true) ? 'Да' : 'Нет' }}<br>
                <b>Способ оплаты:</b> {{ __("shop.payments.{$order->payment_type}") }}<br>
                @if ($order->payment_type == App\Models\Order::ORDER_PAYMENT_LIQPAY)
                <b>Статус оплаты:</b> (#{{ $order->data['liqpay']['payment_id'] ?? '-' }}) {{ $order->data['liqpay']['status'] ?? '-' }}<br>
                @endif
                <b>Адрес доставки:</b> {{ $order->address }}<br>
                <b>Терминал доставки:</b> {{ $order->terminal->name ?? '-' }}<br>
                <b>Курєр:</b> {{ $order->couriers()->pluck('name')->implode(', ') }}
            </div>
        </div>

        <div class="row">
            <div class="col-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Товар</th>
                            <th width="120">Кол-во</th>
                            <th width="120">Сумма</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($order->products()->withPivot(['amount', 'sum', 'modifiers'])->get() as $product)
                        <tr>
                            <td><b>{{ $product->name }} {{ $product->pivot->modifiers ? '(улучшенный)' : '' }}</b><br>
                                {{ $product->subProducts->pluck('name')->implode(' | ') }}
                            </td>
                            <td>{{ __('shop.amount', [$product->pivot->amount]) }}</td>
                            <td>{{ __('shop.price', [$product->pivot->sum]) }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="1"></td>
                            <td><b>Итого:</b></td>
                            <td>{{ __('shop.price', [$order->sum]) }}</td>
                        </tr>
                        @isset($order->data['persons'])
                        <tr>
                            <td colspan="1"></td>
                            <td><b>Сдача с:</b></td>
                            <td>{{ __('shop.price', [$order->data['odd_money'] ?? '-']) }}</td>
                        </tr>
                        @endisset
                    </tfoot>
                </table>
            </div>
        </div>

        <div class="row no-print">
            <form action="{{ route('admin.orders.update', $order) }}" class="py-3" method="POST">
                @csrf
                @method('PUT')

                @foreach (config('shop.order_statuses') as $statusId => $statusName)
                    @if ($statusId == App\Models\Order::ORDER_STATUS_ON_WAY)
                        <span class="collapse show on-way">
                            <button class="btn btn-success m-1" @if($order->status == $statusId) disabled @endif type="button" data-toggle="collapse" data-target=".on-way" aria-expanded="false" aria-controls="onWay">
                                {{ $statusName }}
                            </button>
                        </span>
                        <span class="collapse on-way">
                            <div class="input-group">
                                <select class="custom-select" name="courier_id">
                                    <option disabled selected>Курєр</option>
                                    @foreach (App\Models\User::join('pins', 'users.id', '=', 'pins.user_id')->whereNotNull('courier_status')->get() as $courier)
                                        <option value="{{ $courier->user_id }}">{{ $courier->name }}</option>
                                    @endforeach
                                </select>
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-success" value="{{ $statusId }}" name="status" @if($order->status == $statusId) disabled @endif>{{ 'В путь' }}</button>
                                </div>
                            </div>
                        </span>
                    @else
                        <button type="submit" class="btn btn-success m-1" value="{{ $statusId }}" name="status" @if($order->status == $statusId) disabled @endif>{{ $statusName }}</button>
                    @endif
                @endforeach
            </form>
            <div class="col-12">
                @if (! $order->uuid)
                <form action="{{ route('admin.orders.sendToIiko', $order) }}" class="form-inline ml-2 float-right" method="POST">
                    @csrf
                    <button type="submit" class="btn btn-warning">
                        Отправить в iiko
                    </button>
                </form>
                @endif
                <a href="{{ route('admin.orders.edit', $order) }}" class="btn btn-success float-right">
                    Редактировать
                </a>
            </div>
        </div>
    </div>
</div>
@stop
