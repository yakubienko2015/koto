@extends('components.admin.form')

@section('form')
    <x-admin.tabs>
        <x-admin.tab title="ОСНОВНЫЕ ПАРАМЕТРЫ" active id="general">
            <x-admin.inputs.text
                name="name"
                label="Название"
                :value="old('name', $group->name)"
                required
            />
            <x-admin.inputs.select
                name="city_id"
                label="Город"
                :options="$cities"
                :value="old('city_id', $group->city_id)"
                placeholder="--Все города--"
            />
            <x-admin.inputs.select
                name="terminal_id"
                label="Точка"
                :options="$terminals"
                :value="old('terminal_id', $group->terminal_id)"
                placeholder="--Все точки--"
            />
            <x-admin.inputs.select
                name="roles[]"
                id="roles"
                label="Роли"
                :options="$roles"
                :values="old('roles', $group->getRoleNames())"
                multiple
            />
            <p>
                @foreach ($users as $userPhone => $userName)
                    <span class="badge badge-info">{{ "$userName ($userPhone)" }}</span>
                @endforeach
            </p>
        </x-admin.tab>
    </x-admin.tabs>

    @if ($group->id)
    <div class="row py-0">
        <div class="col-12">
            <x-admin.addModal
                id="sendMessage"
                name="send_message"
                title="Сделать оповещения в группе"
                buttonTitle="Сделать оповещения в группе"
                cancelTitle="Отмена"
            >
                <x-admin.inputs.textarea
                    name="message"
                    label="Сообщения:"
                />
                <x-admin.inputs.checkbox
                    name="holiday_users"
                    label="Показывать тем кто на выходном"
                />
            </x-admin.addModal>
        </div>
    </div>
    @endif
@stop

@push('scripts')
    <script>
        $("select").select2({
            width: '100%',
        });
    </script>
@endpush
