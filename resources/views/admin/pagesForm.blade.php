@extends('components.admin.form', ['hasFile' => true])

@section('form')
    <x-admin.tabs>
        <x-admin.tab title="ОСНОВНЫЕ ПАРАМЕТРЫ" active id="general">
            <div class="row">
                <div class="col-6">
                    <input
                        class="file fileupload"
                        data-browse-on-zone-click="true"
                        @if($page->image)data-initial-preview="<img src='{{ $page->image }}' class='file-preview-image'>"@endif
                        type="file"
                        name="image"
                        data-show-upload="false"
                        data-show-browse="false"
                        data-show-caption="false"
                        data-show-remove="false"
                        data-delete-extra-data='{"_token":"{{ csrf_token() }}"}'
                        data-delete-url="{{ route('admin.pages.removeImage', $page->id ?? '0') }}"
                        data-drop-zone-title="Перетащите изображения сюда &hellip;"
                        data-drop-zone-click-title="(Или щёлкните, чтобы выбрать)"
                    />
                </div>
                <div class="col-6">
                    <x-admin.inputs.text name="name" :label="'Название ('.app()->getLocale().')'" :value="old('name', $page->name)" required />
                    <x-admin.inputs.text name="slug" label="URL" :value="old('slug', $page->slug)" />
                    <x-admin.inputs.checkboxes
                        name="places"
                        label="Места размещения"
                        :values="old('places', $page->places)"
                        :options="config('shop.page_places')"
                    />
                    <x-admin.inputs.text
                        type="number"
                        name="priority"
                        label="Порядок"
                        :value="old('priority', $page->priority ?? 0)"
                        required
                    />
                </div>
            </div>
        </x-admin.tab>
        <x-admin.tab title="META ({{ $page->meta->city->name ?? '-' }})" id="seo">
            <x-admin.metas :meta="(object) old('meta', $page->meta)" />
        </x-admin.tab>
    </x-admin.tabs>
@stop
