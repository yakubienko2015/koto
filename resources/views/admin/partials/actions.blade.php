@php $modelName = Str::of($model->getTable())->replace('_', '-'); @endphp

<div class="btn-group">
    @if(Route::has("admin.{$modelName}.show") && auth()->user()->can("view-{$modelName}"))
    <a href="{{ route("admin.{$modelName}.show", $model) }}" class="btn btn-sm btn-success"
            aria-label="view" title="View">
        <i class="fa fa-eye"></i>
    </a>
    @endif

    @if(Route::has("admin.{$modelName}.edit") && auth()->user()->can("manage-{$modelName}"))
    <a href="{{ route("admin.{$modelName}.edit", $model) }}" class="btn btn-sm btn-primary"
            aria-label="edit" title="Редактировать">
        <i class="fa fa-edit"></i>
    </a>
    @endif

    @if(Route::has("admin.{$modelName}.destroy") && auth()->user()->can("manage-{$modelName}"))
    <form action="{{ route("admin.{$modelName}.destroy", $model) }}" method="POST"
        onsubmit="return confirm('Действительно удалить?');">
        @csrf
        @method('DELETE')
        <button type="submit" class="btn btn-sm btn-danger btn-flat" aria-label="delete" title="Удалить">
            <i class="fa fa-trash"></i>
        </button>
    </form>
    @endif
</div>
