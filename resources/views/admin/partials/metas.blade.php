<div class="row">
    <div class="col-6">
        <div class="form-group">
            <label for="meta-title-{{ $locale }}">Meta title ({{ $locale }})</label>
            <input type="text" class="form-control" id="meta-title-{{ $locale }}" name="meta_title[{{ $locale }}]"
                value="{{ old('meta_title.'.$locale, $meta->getTranslation('title', $locale)) }}">
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <label for="meta-keywords-{{ $locale }}">Meta keywords ({{ $locale }})</label>
            <input type="text" class="form-control" id="meta-key-{{ $locale }}" name="meta_keywords[{{ $locale }}]"
                value="{{ old('meta_keywords.'.$locale, $meta->getTranslation('keywords', $locale)) }}">
        </div>
    </div>
</div>

<div class="form-group">
    <label for="meta-description-{{ $locale }}">Meta description ({{ $locale }})</label>
    <textarea class="form-control" id="meta-description-{{ $locale }}" name="meta_description[{{ $locale }}]"
        rows="2">{!! old('meta_description.'.$locale, $meta->getTranslation('description', $locale)) !!}</textarea>
</div>
