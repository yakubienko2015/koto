@php $modelName = Str::kebab($model->getTable()); @endphp

@if(Route::has("admin.{$modelName}.create") && auth()->user()->can("manage-{$modelName}"))
<a href="{{ route("admin.{$modelName}.create") }}" class="btn btn-primary float-right"
        aria-label="create" title="Create">
    Добавить
</a>
@endif
