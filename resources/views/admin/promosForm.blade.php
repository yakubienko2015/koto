@extends('components.admin.form')

@section('form')
    <x-admin.tabs>
        <x-admin.tab title="ОСНОВНЫЕ ПАРАМЕТРЫ" active id="general">
            <div class="row">
                <div class="col-sm-12">
                    <x-admin.inputs.text
                        name="code"
                        label="Код"
                        helpText="Зачення повинне відповідати номеру купону в iiko, якщо надається знижка на активний товар. Якщо подарунок, тоді не створюється купон в iiko."
                        :value="old('code', $promo->code)"
                    />
                </div>
                <div class="col-sm-6">
                    <x-admin.inputs.text name="qr_code" label="QR Код" :value="old('qr_code', $promo->qr_code)" />
                </div>
                <div class="col-sm-6">
                    <x-admin.inputs.text
                        type="number"
                        append="грн"
                        min="0"
                        name="min_basket_sum"
                        label="Минимальная сума заказа"
                        :value="(int) old('min_basket_sum', $promo->min_basket_sum)"
                    />
                </div>
                <div class="col-sm-6">
                    <x-admin.inputs.select
                        id="product"
                        name="product_id"
                        label="Подарок"
                        :options="$products"
                        :value="old('product_id', $promo->product_id)"
                        helpText="Товар повинен бути неактивний, якщо він подарунковий"
                    />
                </div>
                <div class="col-sm-6">
                    <x-admin.inputs.text
                        append="%"
                        min="0"
                        max="100"
                        step="0.01"
                        type="number"
                        name="discount"
                        label="Скидка"
                        helpText="Знижка на одиницю товара"
                        :value="old('discount', $promo->discount ?? 100)"
                    />
                </div>
                <div class="col-sm-6">
                    <x-admin.inputs.select
                        name="iiko_guest_category_uuid"
                        label="Категория для гостя"
                        placeholder="-- Выбрать --"
                        :options="$categories->pluck('name', 'id')"
                        :value="old('iiko_guest_category_uuid', $promo->iiko_guest_category_uuid)"
                    />
                </div>
            </div>
            <x-admin.inputs.checkbox
                name="is_active"
                label="Активен"
                :checked="(bool) old('is_active', $promo->is_active)"
            />
            {{-- <x-admin.inputs.checkbox
                name="can_be_single"
                label="Можно один подарок для заказа"
                :checked="(bool) old('can_be_single', $promo->can_be_single)"
            /> --}}
            <x-admin.inputs.checkbox
                name="is_multiuse"
                label="Можно использовать неколько раз"
                :checked="(bool) old('is_multiuse', $promo->is_multiuse)"
            />
        </x-admin.tab>
    </x-admin.tabs>
@stop
