@extends('components.admin.form')

@section('form')
    <x-admin.tabs>
        <x-admin.tab title="ОСНОВНЫЕ ПАРАМЕТРЫ" active id="general">
            <x-admin.inputs.text name="name" :label="'Название ('.app()->getLocale().')'" :value="old('name', $category->name)" required />
            <x-admin.inputs.text name="slug" label="URL" :value="old('slug', $category->slug)" helpText="Генерируется автоматически из названия" />
            <x-admin.inputs.text name="uuid" label="Iiko uuid" :value="old('uuid', $category->uuid)" />
            <x-admin.inputs.text type="number" name="priority" label="Порядок" :value="old('priority', $category->priority)" />
        </x-admin.tab>
        <x-admin.tab title="META ({{ $category->meta->city->name ?? '-' }})" id="seo">
            <x-admin.metas :meta="(object) old('meta', $category->meta)" />
        </x-admin.tab>
    </x-admin.tabs>
@stop
