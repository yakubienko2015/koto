@extends('components.admin.form', ['hasFile' => true])

@section('form')
    <x-admin.tabs>
        <x-admin.tab title="ОСНОВНЫЕ ПАРАМЕТРЫ" active id="general">
            <div class="row">
                <div class="col-6">
                    <input
                        class="file fileupload"
                        data-browse-on-zone-click="true"
                        @if($post->image)data-initial-preview="<img src='{{ $post->image }}' class='file-preview-image'>"@endif
                        type="file"
                        name="image"
                        data-show-upload="false"
                        data-show-browse="false"
                        data-show-caption="false"
                        data-show-remove="false"
                        data-delete-extra-data='{"_token":"{{ csrf_token() }}"}'
                        data-delete-url="{{ route('admin.posts.removeImage', $post->id ?? '0') }}"
                        data-drop-zone-title="Перетащите изображения сюда &hellip;"
                        data-drop-zone-click-title="(Или щёлкните, чтобы выбрать)"
                    />
                </div>
                <div class="col-6">
                    <x-admin.inputs.text name="name" :label="'Название ('.app()->getLocale().')'" :value="old('name', $post->name)" required />
                    <x-admin.inputs.text name="slug" label="URL" :value="old('slug', $post->slug)" helpText="Генерируется автоматически из названия" />
                    <x-admin.inputs.text
                        type="date"
                        name="created_at"
                        label="Дата"
                        :value="old('created_at', $post->created_at ? $post->created_at->format('Y-m-d') : now()->format('Y-m-d'))"
                    />
                    <x-admin.inputs.text
                        type="number"
                        name="priority"
                        label="Порядок"
                        :value="old('priority', $post->priority ?? 0)"
                        required
                    />
                </div>
            </div>

            <x-admin.inputs.textarea
                name="description"
                :label="'Вступительный текст ('.app()->getLocale().')'"
                :value="$post->description ?? ''"
            />
        </x-admin.tab>
        <x-admin.tab title="META ({{ $post->meta->city->name ?? '-' }})" id="seo">
            <x-admin.metas :meta="(object) old('meta', $post->meta)" />
        </x-admin.tab>
    </x-admin.tabs>
@stop
