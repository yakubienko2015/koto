@extends('components.admin.form')

@section('form')
    <x-admin.tabs>
        <x-admin.tab title="ОСНОВНЫЕ ПАРАМЕТРЫ" active id="general">
            <div class="row">
                <div class="col-sm-6">
                    <x-admin.inputs.text name="name" :label="'Название ('.app()->getLocale().')'" :value="old('name', $city->name)" />
                </div>
                <div class="col-sm-6">
                    <x-admin.inputs.text name="name_r" :label="'Название для мета ('.app()->getLocale().')'" :value="old('name_r', $city->name_r)" />
                </div>
                <div class="col-sm-6">
                    <x-admin.inputs.text name="slug" label="URL" :value="old('slug', $city->slug)" helpText="Генерируется автоматически из названия" />
                </div>
                <div class="col-sm-6">
                    <x-admin.inputs.text name="uuid" label="Iiko uuid города" :value="old('uuid', $city->uuid)" />
                </div>
                <div class="col-sm-6">
                    <x-admin.inputs.text name="organization_uuid" label="Iiko uuid организации" :value="old('organization_uuid', $city->organization_uuid)" />
                </div>
            </div>
            <x-admin.inputs.checkbox
                name="is_active"
                label="Заблокирован"
                :checked="!old('is_active', $city->is_active)"
                :value="false"
            />
        </x-admin.tab>

        <x-admin.tab title="ВРЯМЯ РАБОТЫ" id="worktime">
            <div class="row">
                @foreach (config('shop.work_days') as $key => $item)
                    <div class="col-sm-4">
                        <x-admin.inputs.text
                            prepend="От"
                            type="time"
                            :name="'work_time[' . $key . '][from]'"
                            :label="$item"
                            :value="old('work_time.' . $key . '.from', $city->work_time[$key]['from'] ?? '00:00')" />
                    </div>
                    <div class="col-sm-4">
                        <x-admin.inputs.text
                            prepend="До"
                            type="time"
                            :name="'work_time[' . $key . '][to]'"
                            label="_"
                            :value="old('work_time.' . $key . '.to', $city->work_time[$key]['to'] ?? '00:00')" />
                    </div>
                    <div class="col-sm-4">
                        <br>
                        <x-admin.inputs.checkbox
                            :name="'work_time[' . $key . '][closed]'"
                            label="Выходной"
                            :checked="old('work_time.' . $key . '.closed', !!($city->work_time[$key]['closed'] ?? 0))" />
                    </div>
                    <hr class="w-100 m-0">
                @endforeach
            </div>
        </x-admin.tab>

        <x-admin.tab title="Способы оплаты" id="payments">
            <x-admin.inputs.checkboxes
                name="payments"
                :options="config('shop.payments')"
                :values="old('payments', $city->payments)"
            />
        </x-admin.tab>

        @if ($city->id)
        <x-admin.tab title="Настройки" id="settings">
            <x-admin.inputs.select
                name="configs[admins_group_id]"
                label="Группа администраторов для уменшения нагрузки"
                placeholder="-- Выбрать --"
                :options="\App\Models\Group::pluck('name', 'id')"
                :value="old('configs.admins_group_id', $city->config('admins_group_id'))"
            />
            <x-admin.inputs.text
                name="configs[service_bad]"
                label="Понравился ли вам наш сервис? Так себе"
                :value="old('configs.service_bad', $city->config('service_bad'))"
            />
            <x-admin.inputs.text
                name="configs[service_good]"
                label="Понравился ли вам наш сервис? Отлично"
                :value="old('configs.service_good', $city->config('service_good'))"
            />
        </x-admin.tab>
        @endif
    </x-admin.tabs>
@stop
