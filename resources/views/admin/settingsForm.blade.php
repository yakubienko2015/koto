@extends('components.admin.form')

@section('form')
    <x-admin.tabs>
        <x-admin.tab title="ОБЩИЕ НАСТРОЙКИ" active id="general">
            <x-admin.inputs.text
                name="shop$min_basket_sum"
                label="Сумма минимального заказа"
                :value="old('shop$min_basket_sum', $customCongif['shop.min_basket_sum'] ?? '')"
                :placeholder="config('shop.min_basket_sum')"
                append="грн"
            />
            <x-admin.inputs.text
                name="shop$min_basket_payment_sum"
                label="Сумма минимального заказа для оплаты"
                :value="old('shop$min_basket_payment_sum', $customCongif['shop.min_basket_payment_sum'] ?? '')"
                :placeholder="config('shop.min_basket_payment_sum')"
                append="грн"
            />
            <x-admin.inputs.text
                name="shop$max_load"
                label="Сумма нагрузки на смену"
                :value="old('shop$max_load', $customCongif['shop.max_load'] ?? '')"
                :placeholder="config('shop.max_load')"
                append="грн"
            />
            <x-admin.inputs.text
                type="number"
                name="shop$time_for_opinion"
                label="Укажите время, через сколько пользователь должен получить форму для отзывов"
                :value="old('shop$time_for_opinion', $customCongif['shop.time_for_opinion'] ?? '')"
                :placeholder="config('shop.time_for_opinion')"
                append="часов"
            />
            <x-admin.inputs.text
                type="number"
                name="shop$default_operator_price"
                label="Укажите стоимость минуты для оператора"
                :value="old('shop$default_operator_price', $customCongif['shop.default_operator_price'] ?? '')"
                :placeholder="config('shop.default_operator_price')"
                append="грн"
                step="0.01"
            />
            <hr>
            <div class="row">
                <div class="col-sm-4">
                    <x-admin.inputs.text
                        name="site$social_links$facebook"
                        label="Ссылка на facebook"
                        :value="old('site$social_links$facebook', $customCongif['site.social_links.facebook'] ?? '')"
                        :placeholder="config('site.social_links.facebook')"
                        helpText="Если нужно убрать, введите 0."
                    />
                </div>
                <div class="col-sm-4">
                    <x-admin.inputs.text
                        name="site$social_links$instagram"
                        label="Ссылка на instagram"
                        :value="old('site$social_links$instagram', $customCongif['site.social_links.instagram'] ?? '')"
                        :placeholder="config('site.social_links.instagram')"
                        helpText="Если нужно убрать, введите 0."
                    />
                </div>
                <div class="col-sm-4">
                    <x-admin.inputs.text
                        name="site$social_links$youtube"
                        label="Ссылка на youtube"
                        :value="old('site$social_links$youtube', $customCongif['site.social_links.youtube'] ?? '')"
                        :placeholder="config('site.social_links.youtube')"
                        helpText="Если нужно убрать, введите 0."
                    />
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <x-admin.inputs.text
                        name="site$store_links$app_store"
                        label="Ссылка на App Store"
                        :value="old('site$store_links$app_store', $customCongif['site.store_links.app_store'] ?? '')"
                        :placeholder="config('site.store_links.app_store')"
                    />
                </div>
                <div class="col-sm-6">
                    <x-admin.inputs.text
                        name="site$store_links$google_play"
                        label="Ссылка на Google Play"
                        :value="old('site$store_links$google_play', $customCongif['site.store_links.google_play'] ?? '')"
                        :placeholder="config('site.store_links.google_play')"
                    />
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <x-admin.inputs.text
                        name="site$phones$0"
                        label="Телефон 1"
                        :value="old('site$phones$0', $customCongif['site.phones.0'] ?? '')"
                        :placeholder="config('site.phones.0')"
                        helpText="Если нужно убрать, введите 0."
                    />
                </div>
                <div class="col-sm-4">
                    <x-admin.inputs.text
                        name="site$phones$1"
                        label="Телефон 2"
                        :value="old('site$phones$1', $customCongif['site.phones.1'] ?? '')"
                        :placeholder="config('site.phones.1')"
                        helpText="Если нужно убрать, введите 0."
                    />
                </div>
                <div class="col-sm-4">
                    <x-admin.inputs.text
                        name="site$phones$2"
                        label="Телефон 3"
                        :value="old('site$phones$2', $customCongif['site.phones.2'] ?? '')"
                        :placeholder="config('site.phones.2')"
                        helpText="Если нужно убрать, введите 0."
                    />
                </div>
            </div>
            <x-admin.inputs.text
                name="site$copyright"
                label="Copyright"
                :value="old('site$copyright', $customCongif['site.copyright'] ?? '')"
                :placeholder="config('site.copyright')"
            />
            <x-admin.inputs.checkbox
                name="shop$make_order_with_iiko"
                label="Синхронизация с iiko"
                :checked="(bool) old('shop$make_order_with_iiko', $customCongif['shop.make_order_with_iiko'] ?? config('shop.make_order_with_iiko'))"
                :value="true"
            />
        </x-admin.tab>

        {{-- <x-admin.tab title="Бонусы (коткоины)" id="kotcoins">
            <caption>Курс коткоина</caption>
            <div class="table-responsive">
                <table class="table table-bordered table-sm">
                    <thead>
                        <tr>
                            <th>Время</th>
                            @for ($i = 0; $i < 24; $i++)
                            <th>{{ $i }}</th>
                            @endfor
                        </tr>
                    </thead>
                    <tbody>
                        @foreach (config('shop.work_days') as $key => $item)
                        <tr>
                            <td>{{ $item }}</td>
                            @for ($i = 0; $i < 24; $i++)
                            <th>
                                <input type="number"
                                    name="{{ 'shop$kotcoin_rates$' . $key . '$' . $i }}"
                                    value="{{ old('shop$kotcoin_rates$'.$key.'$'.$i, $customCongif['shop.kotcoin_rates.'.$key.'.'.$i] ?? '') }}"
                                    placeholder="{{ config('shop.kotcoin_rates.'.$key.'.'.$i) }}"
                                    style="width:50px"
                                    step="0.01"
                                    min="0"
                                />
                            </th>
                            @endfor
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </x-admin.tab> --}}

        <x-admin.tab title="Вспомогательные" id="bowls">
            <x-admin.inputs.select
                name="site$bowls_category_id"
                label="Категория для отображения конструктора боулов"
                placeholder="-- Выбрать --"
                :options="\App\Models\Category::pluck('name', 'id')"
                :value="old('site$bowls_category_id', $customCongif['site.bowls_category_id'] ?? '')"
            />
            <x-admin.inputs.select
                name="site$bowl_constructors_category_id"
                label="Категория c конструкторами боулов"
                placeholder="-- Выбрать --"
                :options="\App\Models\Category::pluck('name', 'id')"
                :value="old('site$bowl_constructors_category_id', $customCongif['site.bowl_constructors_category_id'] ?? '')"
            />
            <x-admin.inputs.select
                name="shop$add_sauce_product_id"
                label="СИВ набор платный"
                placeholder="-- Выбрать --"
                :options="\App\Models\Product::pluck('name', 'id')"
                :value="old('shop$add_sauce_product_id', $customCongif['shop.add_sauce_product_id'] ?? '')"
            />
            <x-admin.inputs.select
                name="shop$free_sauce_product_id"
                label="СИВ набор бесплатный"
                placeholder="-- Выбрать --"
                :options="\App\Models\Product::pluck('name', 'id')"
                :value="old('shop$free_sauce_product_id', $customCongif['shop.free_sauce_product_id'] ?? '')"
            />
            <x-admin.inputs.select
                name="shop$rolls_category_id"
                label="Категория с роллами (для расчета бесплатных СИВ наборов)"
                placeholder="-- Выбрать --"
                :options="\App\Models\Category::pluck('name', 'id')"
                :value="old('shop$rolls_category_id', $customCongif['shop.rolls_category_id'] ?? '')"
            />
            <x-admin.inputs.select
                id="rolls_group_ids"
                name="shop$rolls_group_ids[]"
                multiple
                label="Группи с роллами (для расчета бесплатных СИВ наборов)"
                placeholder="-- Выбрать --"
                :options="\App\Models\ProductGroup::pluck('name', 'id')"
                :values="old('shop$rolls_group_ids', $customCongif['shop.rolls_group_ids'] ?? [])"
            />
            <x-admin.inputs.select
                name="shop$good_mood_product_id"
                label="Товар для хорошего настроения"
                placeholder="-- Выбрать --"
                :options="\App\Models\Product::pluck('name', 'id')"
                :value="old('shop$good_mood_product_id', $customCongif['shop.good_mood_product_id'] ?? '')"
            />
            <x-admin.inputs.select
                name="shop$order_promo_product_id"
                label="Промо товар"
                placeholder="-- Выбрать --"
                :options="\App\Models\Product::pluck('name', 'id')"
                :value="old('shop$order_promo_product_id', $customCongif['shop.order_promo_product_id'] ?? '')"
            />
            <x-admin.inputs.select
                name="shop$market_bonus_product_id"
                label="Товар вместо QR кода в приложение"
                placeholder="-- Выбрать --"
                :options="\App\Models\Product::pluck('name', 'id')"
                :value="old('shop$market_bonus_product_id', $customCongif['shop.market_bonus_product_id'] ?? '')"
            />
            @if ($customerCategories)
            <x-admin.inputs.select
                name="shop$win_cutomer_category_uuid"
                label="Категория для гостя при выигрыше в игру"
                placeholder="-- Выбрать --"
                :options="$customerCategories->pluck('name', 'id')"
                :value="old('shop$win_cutomer_category_uuid', $customCongif['shop.win_cutomer_category_uuid'] ?? '')"
            />
            <x-admin.inputs.select
                name="shop$market_bonus_category_uuid"
                label="Категория для гостя, который переходит по QR коде в приложение"
                placeholder="-- Выбрать --"
                :options="$customerCategories->pluck('name', 'id')"
                :value="old('shop$market_bonus_category_uuid', $customCongif['shop.market_bonus_category_uuid'] ?? '')"
            />
            @endif
            <x-admin.inputs.select
                name="shop$presents_category_id"
                label="Категория с подарками"
                placeholder="-- Выбрать --"
                :options="\App\Models\Category::pluck('name', 'id')"
                :value="old('shop$presents_category_id', $customCongif['shop.presents_category_id'] ?? '')"
            />
            <x-admin.inputs.select
                name="shop$operators_group_id"
                label="Группа операторов для подтверждения заказа и дней рождения"
                placeholder="-- Выбрать --"
                :options="\App\Models\Group::pluck('name', 'id')"
                :value="old('shop$operators_group_id', $customCongif['shop.operators_group_id'] ?? '')"
            />
        </x-admin.tab>

        <x-admin.tab title="Выходные" id="holidays">
            @foreach (old('shop$holidays', $customCongif['shop.holidays'] ?? []) as $holiday)
                <x-admin.inputs.text
                    name="shop$holidays[]"
                    type="date"
                    :value="$holiday"
                />
            @endforeach
            <x-admin.inputs.text
                name="shop$holidays[]"
                type="date"
            />
        </x-admin.tab>
    </x-admin.tabs>
@stop

@push('scripts')
    <script>
        $("#rolls_group_ids").select2({
            width: '100%',
        });
    </script>
@endpush
