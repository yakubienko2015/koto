@extends('components.admin.form')

@section('form')
    <x-admin.tabs>
        <x-admin.tab title="ОСНОВНЫЕ ПАРАМЕТРЫ" active id="general">
            <x-admin.inputs.text
                name="name"
                :label="'Название ('.app()->getLocale().')'"
                :value="old('name', $terminal->name)"
                required
            />
            <x-admin.inputs.text
                name="uuid"
                label="Iiko uuid"
                :value="old('uuid', $terminal->uuid)"
            />
            <x-admin.inputs.text
                name="address"
                :label="'Адрес ('.app()->getLocale().')'"
                :value="old('address', $terminal->address)"
            />
            <x-admin.inputs.text
                name="phone"
                label="Телефон"
                :value="old('phone', $terminal->phone)"
            />
            <x-admin.inputs.text
                name="max_cook_load"
                type="number"
                min="0"
                append="грн/час"
                label="Максомальная нагрузка на повара"
                :value="old('max_cook_load', $terminal->max_cook_load)"
            />
            <x-admin.inputs.select
                name="status"
                label="Статус"
                :options="$statuses"
                :value="old('status', $terminal->status)"
            />
            <x-admin.inputs.select
                name="users[]"
                label="Пользователи"
                :options="$users"
                :values="old('users', $terminal->users()->pluck('id'))"
                multiple
            />
        </x-admin.tab>
        @if ($terminal->id)
        <x-admin.tab title="РАБОЧИЕ МЕСТА" id="places">
            <table class="table">
                <thead>
                    <tr>
                        <th style="width: 150px">Название</th>
                        <th>Группи</th>
                        <th style="width: 40px">Удалить</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($terminal->workPlaces as $workPlace)
                        <tr>
                            <td>
                                <x-admin.inputs.text
                                    name="work_places[{{ $workPlace->id }}][name]"
                                    :value="old('work_places.'.$workPlace->id.'.name', $workPlace->name)"
                                />
                            </td>
                            <td>
                                <x-admin.inputs.select
                                    name="work_places[{{ $workPlace->id }}][product_groups][]"
                                    :options="$productGroups"
                                    multiple
                                    class="select2"
                                    :values="old('work_places.'.$workPlace->id.'.product_groups', $workPlace->productGroups()->pluck('id'))"
                                />
                            </td>
                            <td>
                                <button class="btn btn-danger float-right" name="remove_place" value="{{ $workPlace->id }}">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <x-admin.addModal
                id="WorkPlace"
                name="new_work_place"
                title="Новый стол"
                buttonTitle="Додать стол"
                cancelTitle="Отмена"
            >
                <x-admin.inputs.text
                    name="new_place_name"
                    title="Название стола:"
                />
            </x-admin.addModal>
        </x-admin.tab>
        @endif
    </x-admin.tabs>
@stop

@push('scripts')
    <script>
        $(".select2").select2({
            width: '100%',
        });
    </script>
@endpush
