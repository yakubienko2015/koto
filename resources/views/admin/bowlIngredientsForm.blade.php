@extends('components.admin.form')

@section('form')
    <x-admin.tabs>
        <x-admin.tab title="ОСНОВНЫЕ ПАРАМЕТРЫ" active id="general">
            <x-admin.inputs.text name="name" :label="'Название ('.app()->getLocale().')'" :value="old('name', $bowlIngredient->name)" required />
            <x-admin.inputs.text type="number" step="1" name="step" label="Шаг" :value="old('step', $bowlIngredient->step)" required />
            <x-admin.inputs.textarea name="description" :label="'Описание ('.app()->getLocale().')'" :value="old('description', $bowlIngredient->description)" />
            <x-admin.inputs.select
                name="products[]"
                id="products"
                label="Боулы"
                multiple
                :options="$products"
                :values="old('products', $bowlIngredient->products()->pluck('id', 'name'))"
            />
        </x-admin.tab>
    </x-admin.tabs>
@stop

@push('scripts')
    <script>
        $("#products").select2({
            width: '100%',
        });
    </script>
@endpush
