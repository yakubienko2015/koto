@extends('adminlte::page')

@section('title', $title ?? '')

@section('adminlte_css_pre')
    <!-- flag-icon-css -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.3.0/css/flag-icon.min.css">
@endsection

@section('content')
    <style>
        .content-header {
            padding: 0;
        }
        .content-wrapper > .content, .container-fluid, html, .wrapper, body {
            padding: 0;
            height: 100% !important;
        }
        .content-wrapper {
            height: calc(100% - 57px);
        }
    </style>
    <iframe src="{{ route('languages.index') }}" frameborder="0" width="100%" height="100%"></iframe>
@stop
