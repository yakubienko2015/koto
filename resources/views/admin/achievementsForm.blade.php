@extends('components.admin.form', ['hasFile' => true])

@section('form')
    <x-admin.tabs>
        <x-admin.tab title="ОСНОВНЫЕ ПАРАМЕТРЫ" active id="general">
            <div class="row">
                <div class="col-6">
                    <label>Изображения (квадратное) (любой формат изображения)</label>
                    <input
                        class="file fileupload"
                        data-browse-on-zone-click="true"
                        @if($achievement->image)data-initial-preview="<img src='{{ $achievement->image }}' class='file-preview-image'>"@endif
                        type="file"
                        name="image"
                        data-show-upload="false"
                        data-show-browse="false"
                        data-show-caption="false"
                        data-show-remove="false"
                        data-delete-extra-data='{"_token":"{{ csrf_token() }}","is_mob":"true"}'
                        data-delete-url="{{ route('admin.achievements.removeImage', $achievement->id ?? '0') }}"
                        data-drop-zone-title="Перетащите изображения сюда &hellip;"
                        data-drop-zone-click-title="(Или щёлкните, чтобы выбрать)"
                    />
                </div>
                <div class="col-6">
                    <x-admin.inputs.text
                        name="name"
                        :label="'Название ('.app()->getLocale().')'"
                        :value="old('name', $achievement->name)"
                        required
                    />
                    <x-admin.inputs.textarea
                        name="description"
                        :label="'Условия выполнения ('.app()->getLocale().')'"
                        :value="old('description', $achievement->description)"
                        required
                    />
                    <x-admin.inputs.text
                        type="number"
                        name="bonuses"
                        label="Кол-во бонусов"
                        :value="old('bonuses', $achievement->bonuses)"
                    />
                    <x-admin.inputs.checkbox
                        name="is_active"
                        label="Видимость клиентами"
                        :checked="(bool) old('is_active', $achievement->is_active)"
                    />
                </div>
                <div class="col-6">
                    <x-admin.inputs.select
                        name="get_category_uuid"
                        label="Категория гостей прохождения"
                        required
                        :options="$categories"
                        :value="old('get_category_uuid', $achievement->get_category_uuid)"
                    />
                </div>
                <div class="col-6">
                    <x-admin.inputs.select
                        name="has_category_uuid"
                        label="Категория гостей для ачивки"
                        placeholder="-- Выбрать --"
                        :options="$categories"
                        :value="old('has_category_uuid', $achievement->has_category_uuid)"
                    />
                </div>
                <div class="col-12">
                    <x-admin.inputs.select
                        name="type"
                        label="Тип"
                        id="type"
                        required
                        :options="$types"
                        :value="old('type', $achievement->type)"
                    />
                    <span class="collapse{{ $achievement->type === 'frequency' ? ' show' : '' }}" id="typeFrequency">
                        <x-admin.inputs.text
                            type="number"
                            name="frequency"
                            label="кол-во чеков"
                            :value="old('frequency', $achievement->frequency ?? 0)"
                        />
                    </span>
                    <span class="collapse{{ $achievement->type === 'sum' ? ' show' : '' }}" id="typeSum">
                        <x-admin.inputs.text
                            type="number"
                            name="sum"
                            label="сумма"
                            :value="old('sum', $achievement->sum ?? 0)"
                        />
                    </span>
                </div>
                <div class="col-6">
                    <x-admin.inputs.text
                        type="date"
                        name="active_from"
                        label="Дата начала"
                        :value="old('active_from', $achievement->active_from ? $achievement->active_from->format('Y-m-d') : '')"
                    />
                </div>
                <div class="col-6">
                    <x-admin.inputs.text
                        type="date"
                        name="active_to"
                        label="Дата окончания"
                        :value="old('active_to', $achievement->active_to ? $achievement->active_to->format('Y-m-d') : '')"
                    />
                </div>
            </div>
        </x-admin.tab>
    </x-admin.tabs>
@stop

@push('scripts')
    <script>
        $("#type").on("change", function (element) {
            switch (element.target.value) {
                case 'loyalty':
                    $("#typeFrequency").collapse('hide');
                    $("#typeSum").collapse('hide');
                    break;
                case 'frequency':
                    $("#typeFrequency").collapse('show');
                    $("#typeSum").collapse('hide');
                    break;
                case 'sum':
                    $("#typeFrequency").collapse('hide');
                    $("#typeSum").collapse('show');
                    break;
            }
        });
    </script>
@endpush
