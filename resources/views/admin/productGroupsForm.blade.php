@extends('components.admin.form')

@section('form')
    <x-admin.tabs>
        <x-admin.tab title="ОСНОВНЫЕ ПАРАМЕТРЫ" active id="general">
            <x-admin.inputs.text name="name" label="Название" :value="old('name', $productGroup->name)" required />
            <x-admin.inputs.text name="uuid" label="Iiko uuid" :value="old('uuid', $productGroup->uuid)" />
            <x-admin.inputs.checkbox
                name="is_active"
                label="Заблокирован"
                :checked="!old('is_active', $productGroup->is_active)"
                :value="false"
            />
            <x-admin.inputs.select
                id="products"
                name="products[]"
                label="Товары"
                :options="$products"
                multiple
                :values="old('products', $productGroup->products()->pluck('id'))"
            />
        </x-admin.tab>
    </x-admin.tabs>
@stop

@push('scripts')
    <script>
        $("#products").select2({
            width: '100%',
        });
    </script>
@endpush
