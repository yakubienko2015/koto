@extends('components.admin.form', ['hasFile' => true])

@section('form')
    <h5 class="text-right">
        <a href="https://console.firebase.google.com/project/kotosushi-delivery/notification/compose">Создать уведомления в Firebase</a>
    </h5>
    <x-admin.inputs.text name="title" label="Заголовок" required :value="old('title')" />
    <x-admin.inputs.textarea name="body" label="Текст" required :value="old('body')" />
    <x-admin.inputs.select
        id="users"
        name="users[]"
        label="Пользователи"
        :options="$users"
        multiple
        required
        :values="old('users')"
    />
    <div class="form-group">
        <label for="importUsers">Пользователи из файла</label>
        <div class="input-group">
            <div class="custom-file">
                <input type="file" class="custom-file-input" id="importUsers" name="import_users" accept=".xlsx, .xls">
                <label class="custom-file-label" for="importUsers" data-browse="Выбрать">xlsx файл</label>
            </div>
            <div class="input-group-append">
                <button class="btn btn-primary" type="submit" name="exit" value="3">Загрузить</button>
            </div>
        </div>
        <small>после загрузки, клиенты добавятся в поле "Пользователи"</small>
    </div>
    <x-admin.inputs.text name="link" label="Url" :value="old('link')" />
    <div class="row">
        <div class="col">
            <x-admin.inputs.select
                id="category_slug"
                name="category_slug"
                label="Категория для приложения"
                :options="$categories"
                :value="old('category_slug')"
                placeholder="-- не выбрано --"
            />
        </div>
        <div class="col">
            <x-admin.inputs.select
                id="page_slug"
                name="page_slug"
                label="Страница для приложения"
                :options="$pages"
                :value="old('page_slug')"
                placeholder="-- не выбрано --"
            />
        </div>
        <div class="col">
            <x-admin.inputs.select
                id="product_slug"
                name="product_slug"
                label="Товар для приложения"
                :options="$products"
                :value="old('product_slug')"
                placeholder="-- не выбрано --"
            />
        </div>
    </div>
@stop

@push('scripts')
    <script>
        $("#users").select2({
            width: '100%',
        });
        $('#category_slug').on('change', () => {
            $('#page_slug, #product_slug').val('');
        });
        $('#page_slug').on('change', () => {
            $('#category_slug, #product_slug').val('');
        });
        $('#product_slug').on('change', () => {
            $('#page_slug, #category_slug').val('');
        });
    </script>
@endpush
