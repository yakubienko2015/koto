@extends('components.admin.form', ['hasFile' => true])

@section('form')
    <x-admin.tabs>
        <x-admin.tab title="ОСНОВНЫЕ ПАРАМЕТРЫ" active id="general">
            <div class="row">
                <div class="col-6">
                    <input
                        class="file fileupload"
                        data-browse-on-zone-click="true"
                        @if($slide->image)data-initial-preview="<img src='{{ $slide->image }}' class='file-preview-image'>"@endif
                        type="file"
                        name="image"
                        data-show-upload="false"
                        data-show-browse="false"
                        data-show-caption="false"
                        data-show-remove="false"
                        data-delete-extra-data='{"_token":"{{ csrf_token() }}"}'
                        data-delete-url="{{ route('admin.slides.removeImage', $slide->id ?? '0') }}"
                        data-drop-zone-title="Перетащите изображения сюда &hellip;"
                        data-drop-zone-click-title="(Или щёлкните, чтобы выбрать)"
                    />
                </div>
                <div class="col-6">
                    <x-admin.inputs.text name="title" :label="'Заголовок ('.app()->getLocale().')'" :value="old('title', $slide->title)" required />
                    <x-admin.inputs.checkbox
                        name="is_active"
                        label="Доступно"
                        :checked="(bool) old('is_active', $slide->is_active)"
                    />
                    <x-admin.inputs.checkbox
                        name="all_cities"
                        label="Для всех городов"
                        :checked="old('all_cities', !$slide->city_id)"
                    />
                    <x-admin.inputs.text
                        type="number"
                        name="priority"
                        label="Порядок"
                        :value="old('priority', $slide->priority ?? 0)"
                        required
                    />
                </div>
                <div class="col-6">
                    <x-admin.inputs.text name="button_text" :label="'Текст на кнопке ('.app()->getLocale().')'" :value="old('button_text', $slide->button_text)" required />
                </div>
                <div class="col-6">
                    <x-admin.inputs.text name="button_link" label="Ссылкой на кнопке" :value="old('button_link', $slide->button_link)" required />
                </div>
                <div class="col-12">
                    <x-admin.inputs.textarea
                        name="text"
                        :label="'Текст ('.app()->getLocale().')'"
                        :value="$slide->text ?? ''"
                    />
                </div>
            </div>
        </x-admin.tab>
    </x-admin.tabs>
@stop
