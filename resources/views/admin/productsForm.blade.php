@extends('components.admin.form', ['hasFile' => true])

@section('form')
    <x-admin.tabs>
        <x-admin.tab title="ОСНОВНЫЕ ПАРАМЕТРЫ" active id="general">
            <div class="row">
                <div class="col-6">
                    <label>Изображения (автоматически измениться в 600x365) (любой формат изображения)</label>
                    <input
                        class="file fileupload"
                        data-browse-on-zone-click="true"
                        @if($product->image)data-initial-preview="<img src='{{ $product->image }}' class='file-preview-image'>"@endif
                        type="file"
                        name="image"
                        data-show-upload="false"
                        data-show-browse="false"
                        data-show-caption="false"
                        data-show-remove="false"
                        data-delete-extra-data='{"_token":"{{ csrf_token() }}"}'
                        data-delete-url="{{ route('admin.products.removeImage', $product->id ?? '0') }}"
                        data-drop-zone-title="Перетащите изображения сюда &hellip;"
                        data-drop-zone-click-title="(Или щёлкните, чтобы выбрать)"
                    />
                    <label>Изображения (квадратное) (любой формат изображения)</label>
                    <input
                        class="file fileupload"
                        data-browse-on-zone-click="true"
                        @if($product->mob_image)data-initial-preview="<img src='{{ $product->mob_image }}' class='file-preview-image'>"@endif
                        type="file"
                        name="mob_image"
                        data-show-upload="false"
                        data-show-browse="false"
                        data-show-caption="false"
                        data-show-remove="false"
                        data-delete-extra-data='{"_token":"{{ csrf_token() }}","is_mob":"true"}'
                        data-delete-url="{{ route('admin.products.removeImage', $product->id ?? '0') }}"
                        data-drop-zone-title="Перетащите изображения сюда &hellip;"
                        data-drop-zone-click-title="(Или щёлкните, чтобы выбрать)"
                    />
                </div>
                <div class="col-6">
                    <x-admin.inputs.text name="name" :label="'Название ('.app()->getLocale().')'" :value="old('name', $product->name)" required />
                    <x-admin.inputs.text
                        name="slug"
                        label="URL"
                        :value="old('slug', $product->slug)"
                        helpText="Генерируется автоматически из названия"
                    />
                    <x-admin.inputs.select
                        name="category_id"
                        label="Категория"
                        placeholder="-- Выбрать --"
                        :options="$categories"
                        :value="old('category_id', $product->category_id)"
                    />
                    <x-admin.inputs.select
                        name="product_group_id"
                        label="Группа"
                        placeholder="-- Выбрать --"
                        :options="$productGroups"
                        :value="old('product_group_id', $product->product_group_id)"
                    />
                    <x-admin.inputs.textarea name="ingredients" :label="'Ингредиенты ('.app()->getLocale().')'" :value="old('ingredients', $product->ingredients)" />
                </div>

                <div class="col-4">
                    <x-admin.inputs.text
                        type="number"
                        name="weight"
                        label="Вес"
                        :value="old('weight', $product->weight ?? 0)"
                        required>
                        <x-slot name="appendHtml">
                            <x-admin.inputs.select
                                name="measure_unit"
                                :value="old('measure_unit', $product->measure_unit)"
                                :options="$measureUnits"
                                required />
                        </x-slot>
                    </x-admin.inputs.text>
                </div>
                <div class="col-4">
                    <x-admin.inputs.text type="number" name="amount" label="Кол-во" :value="old('amount', $product->amount)" />
                </div>
                <div class="col-4">
                    <x-admin.inputs.text step="0.01" type="number" name="energy_amount" label="Энергетическая ценность на 100 г" :value="old('energy_amount', $product->energy_amount)" append="Ккал" />
                </div>
                <div class="col-4">
                    <x-admin.inputs.text step="0.01" type="number" name="carbohydrate_amount" label="Количество углеводов на 100 г" :value="old('carbohydrate_amount', $product->carbohydrate_amount)" />
                </div>
                <div class="col-4">
                    <x-admin.inputs.text step="0.01" type="number" name="fat_amount" label="Количество жиров на 100 г" :value="old('fat_amount', $product->fat_amount)" />
                </div>
                <div class="col-4">
                    <x-admin.inputs.text step="0.01" type="number" name="fiber_amount" label="Количество белков на 100 г" :value="old('fiber_amount', $product->fiber_amount)" />
                </div>
                <div class="col-6">
                    <x-admin.inputs.select
                        id="marks"
                        name="marks[]"
                        multiple
                        label="Метки"
                        :options="$marks"
                        :values="old('marks', $product->marks()->pluck('id', 'name'))"
                    />
                </div>
                <div class="col-6">
                    <x-admin.inputs.text
                        type="number"
                        name="priority"
                        label="Порядок"
                        :value="old('priority', $product->priority ?? 0)"
                        required
                    />
                </div>
                <div class="col-12">
                    <x-admin.inputs.checkbox
                        name="is_top"
                        label="Хит продажа (+рамка)"
                        :checked="(bool) old('is_top', $product->is_top)"
                    />
                </div>
                <div class="col-12">
                    <x-admin.inputs.checkbox
                        name="is_new"
                        label="Новый (+рамка)"
                        :checked="(bool) old('is_new', $product->is_new)"
                    />
                </div>
                <div class="col-12">
                    <x-admin.inputs.checkbox
                        name="is_rec"
                        label="Рекомендован (+рамка)"
                        :checked="(bool) old('is_rec', $product->is_rec)"
                    />
                </div>
            </div>
        </x-admin.tab>

        <x-admin.tab title="ДОПОЛНИТЕЛЬНЫЕ ПАРАМЕТРЫ" id="additional">
            <div class="row">
                <div class="col-6">
                    <x-admin.inputs.text name="uuid" label="Идентификатор IIKO" :value="old('uuid', $product->uuid)" />
                </div>
                <div class="col-6">
                    <x-admin.inputs.select
                        id="select-tags"
                        name="tags[]"
                        label="Теги"
                        :options="$tags"
                        multiple
                        :values="old('tags', $product->tags->pluck('name'))"
                        helpText='Используются для блоков "Стоит попробовать"'
                    />
                </div>
                <div class="col-12">
                    <x-admin.inputs.select
                        id="sub-products"
                        name="subproducts[]"
                        label="Под товары"
                        :options="$products"
                        multiple
                        :values="old('subproducts', $product->subproducts()->pluck('id'))"
                        helpText="Если товар состоит из существующих товаров"
                    />
                </div>
                @if (config('shop.bowls_category_id') == $product->category_id)
                <div class="col-12">
                    <x-admin.inputs.select
                        name="bowl_ingredients[]"
                        id="bowl-ingredients"
                        label="Ингредиенты для конструктора боулов"
                        multiple
                        :options="$bowlIngredients"
                        :values="old('bowl_ingredients', $product->bowlIngredients()->pluck('id', 'name'))"
                        :helpText="'Создать ингредиент можно <a href='.route('admin.bowl-ingredients.index').'>здесь</a>'"
                    />
                </div>
                @endif
                <div class="col-sm-6">
                    <x-admin.inputs.text type="number" append="мин" step="0.1" name="time_for_cook" label="Время приготовления для повара" :value="old('time_for_cook', $product->time_for_cook)" />
                </div>
            </div>
            <label>Ингредиенты для повара</label>
            <table class="table">
                <thead>
                    <tr>
                        <th>Название</th>
                        <th>Вага</th>
                        <th style="width: 40px">Удалить</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($product->work_ingredients ?? [] as $key => $ingredient)
                        <tr>
                            <td>
                                <x-admin.inputs.text
                                    title="Название"
                                    name="work_ingredients[{{ $key }}][name]"
                                    :value="old('work_ingredient.'.$key.'.name', $ingredient['name'])"
                                />
                            </td>
                            <td>
                                <x-admin.inputs.text
                                    type="number"
                                    name="work_ingredients[{{ $key }}][weight]"
                                    :value="old('work_ingredient.'.$key.'.weight', $ingredient['weight'])"
                                    step="0.01"
                                >
                                    <x-slot name="appendHtml">
                                        <x-admin.inputs.select
                                            name="work_ingredients[{{ $key }}][unit]"
                                            :options="['г'=>'г', 'мл'=>'мл', 'шт'=>'шт']"
                                            :value="old('work_ingredient.'.$key.'.unit', $ingredient['unit'] ?? 'г')"
                                        />
                                    </x-slot>
                                </x-admin.inputs.text>
                            </td>
                            <td>
                                <button class="btn btn-danger float-right" name="remove_work_ingredient" value="{{ $key }}">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <x-admin.addModal
                id="WorkIngredient"
                name="new_work_ingredient"
                title="Новый ингредиент"
                buttonTitle="Додать ингредиент"
                cancelTitle="Отмена"
            >
                <x-admin.inputs.text
                    name="work_ingredient[name]"
                    label="Название"
                />
                <x-admin.inputs.text
                    name="work_ingredient[weight]"
                    label="Вага"
                    type="number"
                    step="0.01"
                >
                    <x-slot name="appendHtml">
                        <x-admin.inputs.select
                            name="work_ingredient[unit]"
                            :options="['г'=>'г', 'мл'=>'мл', 'шт'=>'шт']"
                            value="г"
                        />
                    </x-slot>
                </x-admin.inputs.text>
            </x-admin.addModal>
        </x-admin.tab>

        <x-admin.tab title="ЦЕНЫ" id="prices">
            <div class="row">
                <div class="col-md-6">
                    <x-admin.inputs.text
                        type="number"
                        name="price"
                        :label="'Цена стандарт ('.app('city')->name.')'"
                        step="0.01"
                        append="грн"
                        required
                        :value="old('price', $product->price ?? 0)"
                    />
                </div>
                <div class="col-md-6">
                    <x-admin.inputs.text
                        type="number"
                        name="old_price"
                        :label="'Старая цена стандарт ('.app('city')->name.')'"
                        step="0.01"
                        append="грн"
                        :value="old('old_price', $product->old_price)"
                    />
                </div>
                <div class="col-12">
                    <x-admin.inputs.select
                        name="improved_product_id"
                        label="Улучшенный товар"
                        placeholder="-- Выбрать --"
                        :options="$products"
                        :value="old('improved_product_id', $product->improved_product_id)"
                        :helpText="$product->improved_product_id
                            ? '<a href='.route('admin.products.edit',$product->improved_product_id).'>Редактировать товар</a>'
                            : ''"
                    />
                </div>
                <div class="col-md-6">
                    <x-admin.inputs.text
                        type="number"
                        name="improved_price"
                        :label="'Цена улучшенного ('.app('city')->name.')'"
                        step="0.01"
                        append="грн"
                        :value="old('improved_price', $product->improved_price)"
                    />
                </div>
                <div class="col-md-6">
                    <x-admin.inputs.text
                        type="number"
                        name="improved_old_price"
                        :label="'Старая цена улучшенного ('.app('city')->name.')'"
                        step="0.01"
                        append="грн"
                        :value="old('improved_old_price', $product->improved_old_price)"
                    />
                </div>
            </div>
        </x-admin.tab>

        <x-admin.tab title="META ({{ $product->meta->city->name ?? '-' }})" id="seo">
            <x-admin.metas :meta="(object) old('meta', $product->meta)" />
        </x-admin.tab>
    </x-admin.tabs>
@stop

@push('scripts')
    <script>
        $("#select-tags").select2({
            tags: true,
            width: '100%',
        });
        $("#sub-products, #bowl-ingredients, #marks").select2({
            width: '100%',
        });
        $("#sub-products").on("select2:select", function (evt) {
            var element = evt.params.data.element;
            var $element = $(element);

            $element.detach();
            $(this).append($element);
            $(this).trigger("change");
        });
    </script>
@endpush
