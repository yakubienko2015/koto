@extends('components.admin.form')

@section('form')
    <x-admin.tabs>
        <x-admin.tab title="ОСНОВНЫЕ ПАРАМЕТРЫ" active id="general">
            <div class="row">
                <div class="col-6">
                    <x-admin.inputs.text
                        name="title"
                        label="Название"
                        :value="old('title', $task->title)"
                        required
                    />
                </div>
                <div class="col-6">
                    <x-admin.inputs.select
                        name="group_id"
                        label="Группа"
                        :options="$groups"
                        :value="old('group_id', $task->group_id)"
                        required
                    />
                </div>
                <div class="col-12">
                    <x-admin.inputs.text
                        name="description"
                        label="Описания"
                        :value="old('description', $task->description)"
                        required
                    />
                </div>
                <div class="col-12">
                    <x-admin.inputs.text
                        name="description_resource"
                        label="Ссылка на видео или другую инструкцию"
                        :value="old('description_resource', $task->description_resource)"
                    />
                </div>
                <div class="col-12">
                    <x-admin.inputs.checkbox
                        name="is_required"
                        label="Обязательно сделать"
                        :checked="old('is_required', $task->is_required)"
                    />
                    <x-admin.inputs.checkbox
                        name="is_file_required"
                        label="Контроль файлом"
                        :checked="old('is_file_required', $task->is_file_required)"
                    />
                    <x-admin.inputs.checkbox
                        name="holiday_users"
                        label="Показывать тем кто на выходном"
                        :checked="old('holiday_users', $task->holiday_users)"
                    />
                </div>
                <div class="col-12">
                    <x-admin.inputs.text
                        name="cron"
                        id="cron"
                        label="Планирование события"
                        :value="old('cron', $task->cron)"
                        readonly
                    />
                </div>
            </div>
        </x-admin.tab>
    </x-admin.tabs>
@stop

@push('scripts')
    <script src="{{ asset('\vendor\jqcron\jqCron.js') }}"></script>
    <script src="{{ asset('\vendor\jqcron\jqCron.ru.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('\vendor\jqcron\jqCron.css') }}">
    <script>
        $("#cron").jqCron({
            enabled_minute: true,
            multiple_dom: true,
            multiple_month: true,
            // multiple_mins: true,
            multiple_dow: true,
            multiple_time_hours: true,
            // multiple_time_minutes: true,
            default_period: 'week',
            default_value: '0 9 * * 1-5',
            numeric_zero_pad: true,
            no_reset_button: false,
            lang: 'ru'
        });
    </script>
@endpush
