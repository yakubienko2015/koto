@extends('components.admin.form')

@section('form')
    <x-admin.tabs>
        <x-admin.tab title="ОСНОВНЫЕ ПАРАМЕТРЫ" active id="general">
            <x-admin.inputs.text name="name" label="Машинное имя" required :value="old('name', $role->name)" :disabled="$role->id" />
            <x-admin.inputs.text title="Название" :value="__('admin.roles.' . $role->name)" disabled />
        </x-admin.tab>
        <x-admin.tab title="ДОСТУПНЫЕ ПРАВА" id="roles">
            <x-admin.inputs.checkboxes name="permissions" label="Права" :values="$role->getPermissionNames()" :options="$permissions" />
        </x-admin.tab>
        <x-admin.tab title="СОЗДАНИЯ ПРАВА" id="permission">
            <x-admin.inputs.text
                name="permission_name"
                label="Додать право"
                helpText="поле предназначено для разработчика"
                required
                form="new-permission-form"
                style="max-width:300px"
                placeholder="Например: manage-users">
                <x-slot name="appendHtml">
                    <button class="btn btn-success" type="submit" form="new-permission-form">
                        +
                    </button>
                </x-slot>
            </x-admin.inputs.text>
        </x-admin.tab>
    </x-admin.tabs>
@stop

@section('after_form')
    <form action="{{ route('admin.permissions.store') }}" id="new-permission-form" method="POST" hidden style="display:none">
        @csrf
    </form>
@stop
