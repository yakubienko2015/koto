@extends('components.admin.index')

@section('content')
    <div class="row">
        @isset($journal)
            <div class="card w-100">
                <div class="card-header">
                    <h3 class="card-title">Журнал событий</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body p-0">
                    <table class="table table-sm">
                        <thead>
                            <tr>
                                <th style="width: 10px">№</th>
                                <th style="width: 150px">Время</th>
                                <th>Сообщения</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($journal as $message)
                            <tr class="table-{{ $message->type }}">
                                <td>{{ $message->id }}</td>
                                <td>{{ $message->created_at }}</td>
                                <td>{!! $message->message !!}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
        @endisset
    </div>
@stop
