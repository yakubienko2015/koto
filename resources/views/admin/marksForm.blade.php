@extends('components.admin.form')

@section('form')
    <x-admin.tabs>
        <x-admin.tab title="ОСНОВНЫЕ ПАРАМЕТРЫ" active id="general">
            <x-admin.inputs.text name="name" :label="'Название ('.app()->getLocale().')'" :value="old('name', $mark->name)" />
            <x-admin.inputs.text name="style" label="Стиль для отображения" :value="old('style', $mark->style)" />
        </x-admin.tab>
    </x-admin.tabs>
@stop
