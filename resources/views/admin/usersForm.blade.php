@extends('components.admin.form')

@section('form')
    <x-admin.tabs>
        <x-admin.tab title="ОСНОВНЫЕ ПАРАМЕТРЫ" active id="general">
            <div class="row">
                <div class="col">
                    <x-admin.inputs.text name="name" label="Имя" :value="old('name', $user->name)" require />
                </div>
                <div class="col">
                    <x-admin.inputs.text name="phone" label="Телефон" :value="old('phone', $user->phone)" require />
                </div>
                <div class="col">
                    <x-admin.inputs.text name="uuid" label="Iiko uuid" :value="old('uuid', $user->uuid)" />
                </div>
            </div>
            <x-admin.inputs.text name="email" label="E-почта" :value="old('email', $user->email)" type="email" require />
            <div class="row">
                <div class="col">
                    <x-admin.inputs.select
                        name="sex"
                        label="Пол"
                        :options="$sexes"
                        :value="old('sex', $user->sex)"
                    />
                </div>
                <div class="col">
                    <x-admin.inputs.select
                        name="city_id"
                        label="Город"
                        :options="$cities"
                        :value="old('city_id', $user->city_id)"
                        placeholder="--Без города--"
                    />
                </div>
                <div class="col">
                    <x-admin.inputs.text
                        type="date"
                        name="birthday"
                        label="Дата рождения"
                        :value="old('birthday', $user->birthday ? $user->birthday->format('Y-m-d') : null)"
                    />
                </div>
            </div>
        </x-admin.tab>
        <x-admin.tab title="ДОСТУП" id="access">
            <x-admin.inputs.select
                name="role"
                label="Роль пользователя"
                :options="$roles"
                :value="old('role', $user->getRoleNames()->first())"
            />

            <div class="row">
                <div class="col">
                    <x-admin.inputs.text name="password" label="Пароль"
                        placeholder="Новый пароль" helpText="Для входа в админку сайта и ЛК пользователя" />
                </div>
                <div class="col">
                    <x-admin.inputs.text label="PiN" disabled :value="$user->getActivePin()->id ?? ''" helpText="Для входа в модуль"/>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <x-admin.inputs.checkbox
                        name="is_verified"
                        label="Активирован"
                        helpText="Активация аккаунта после подтвержения email."
                        :checked="old('is_verified', $user->email_verified_at ? true : false)"
                    />
                </div>
                <div class="col">
                    <x-admin.inputs.checkbox
                        name="is_active"
                        label="Заблокирован"
                        helpText="Заблокирует доступ к аккаунту пользователя. Аккаунт при этом не удаляется."
                        :checked="!old('is_active', $user->is_active)"
                        :value="false"
                    />
                </div>
                <div class="col">
                    <x-admin.inputs.checkbox
                        name="new_pin"
                        label="Новый PiN"
                        helpText="Сгенерировать новый PiN для входа в модуль"
                    />
                </div>
            </div>
        </x-admin.tab>
        <x-admin.tab title="Выходные" id="holidays">
            @foreach ($user->holidays ?? [] as $holiday)
                <x-admin.inputs.text
                    name="holidays[{{ $holiday->id }}]"
                    type="date"
                    :value="$holiday->date->format('Y-m-d')"
                />
            @endforeach
            {{-- <x-admin.inputs.text
                name="new_holiday"
                type="date"
            /> --}}
        </x-admin.tab>
    </x-admin.tabs>
@stop
