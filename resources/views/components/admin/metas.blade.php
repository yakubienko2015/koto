@props(['meta' => new App\Library\Meta()])

<x-admin.inputs.text
    name="meta[title]"
    :label="'Title ('.app()->getLocale().')'"
    :value="$meta->title ?? ''"
/>
<x-admin.inputs.textarea
    name="meta[description]"
    :label="'Descrption ('.app()->getLocale().')'"
    :value="$meta->description ?? ''"
/>
<x-admin.inputs.textarea
    name="meta[keywords]"
    :label="'Keywords ('.app()->getLocale().')'"
    :value="$meta->keywords ?? ''"
/>
<x-admin.inputs.select
    name="meta[robots]"
    label="Robots"
    :options="\App\Library\Meta::getRobotsForSelect()"
    :value="$meta->robots ?? ''"
/>
<x-admin.inputs.textarea
    name="meta[content]"
    class="ckeditor"
    :label="'Контент ('.app()->getLocale().')'"
    :value="$meta->content ?? ''"
/>
<x-admin.inputs.checkbox
    name="meta[is_active]"
    label="Доступен"
    :checked="(bool) old('meta.is_active', $meta->is_active ?? true)"
/>
<x-admin.inputs.checkbox
    name="meta[for_all_cities]"
    label="Сохранить для всех городов"
/>
