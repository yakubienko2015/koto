@props(['label', 'id' => uniqid(), 'required' => false, 'helpText', 'options' => [], 'value' => '', 'values' => [], 'placeholder'])

<div class="form-group">
    @isset($label)<label for="{{ $id }}">{{ $label }} @if($required)<span class="text-danger">*</span>@endif</label>@endisset
    <select id="{{ $id }}" {{ $attributes->merge(['class' => 'form-control custom-select']) }}>
        @isset($placeholder)<option value="" @if($required)disabled @endif>{{ $placeholder }}</option>@endisset
        @foreach ($options as $key => $item)
            <option {{ $key == $value || in_array($key, is_array($values) ? $values : $values->toArray()) ? 'selected' : '' }} value="{{ $key }}">{{ $item }}</option>
        @endforeach
    </select>
    @isset($helpText)<small class="form-text text-muted">{!! $helpText !!}</small>@endisset
</div>
