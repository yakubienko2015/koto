@props(['type' => 'text', 'label', 'id' => uniqid(), 'required' => false, 'helpText', 'prepend' => '', 'append' => '', 'prependHtml' => '', 'appendHtml' => ''])

<div class="form-group">
    @isset($label)
        <label for="{{ $id }}">{{ $label }} @if($required)<span class="text-danger">*</span>@endif</label>
    @endisset
    @if($prepend || $append || $prependHtml || $appendHtml)<div class="input-group">@endif
    @if($prependHtml)
        <div class="input-group-prepend">
            {!! $prependHtml !!}
        </div>
    @endif
    @if($prepend)
        <div class="input-group-prepend">
            <span class="input-group-text">{{ $prepend }}</span>
        </div>
    @endif
    <input type="{{ $type }}" id="{{ $id }}" class="form-control" {{ $attributes }} @if($required) required @endif>
    @if($append)
        <div class="input-group-append">
            <span class="input-group-text">{{ $append }}</span>
        </div>
    @endif
    @if($appendHtml)
        <div class="input-group-append">
            {!! $appendHtml !!}
        </div>
    @endif
    @if($prepend || $append || $prependHtml || $appendHtml)</div>@endif
    @isset($helpText)<small class="form-text text-muted">{!! $helpText !!}</small>@endisset
</div>
