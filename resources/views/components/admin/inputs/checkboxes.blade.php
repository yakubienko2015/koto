@props(['label', 'id' => uniqid(), 'helpText', 'values' => [], 'name' => '', 'require' => false, 'options' => []])

<div class="form-group">
    <input class="d-none" type="hidden" value="" name="{{ $name }}">
    @isset($label)
        <label>{{ $label }} @if($require)<span class="text-danger">*</span>@endif</label>
    @endisset
    @isset($helpText)<small class="form-text text-muted">{{ $helpText }}</small>@endisset
    @foreach ($options as $key => $item)
    <div class="custom-control custom-checkbox">
        <input class="custom-control-input" type="checkbox"{{ in_array($key, is_array($values) ? $values : $values->toArray()) ? ' checked' : '' }} id="{{ $id }}{{ $key }}" value="{{ $key }}" name="{{ $name }}[]">
        <label for="{{ $id }}{{ $key }}" class="custom-control-label">{{ $item }}</label>
    </div>
    @endforeach
</div>
