@props(['type' => 'text', 'label', 'id' => uniqid(), 'required' => false, 'helpText', 'value' => ''])

<div class="form-group">
    @isset($label)
        <label for="{{ $id }}">{{ $label }} @if($required)<span class="text-danger">*</span>@endif</label>
    @endisset
    <textarea id="{{ $id }}" {{ $attributes->merge(['class' => 'form-control']) }}@if($required) required @endif>{{ $value }}</textarea>
    @isset($helpText)<small class="form-text text-muted">{{ $helpText }}</small>@endisset
</div>
