@props(['label', 'id' => uniqid(), 'helpText', 'value' => '', 'name' => '', 'require' => false, 'options' => []])

<div class="form-group">
    @isset($label)
        <label>{{ $label }} @if($require)<span class="text-danger">*</span>@endif</label>
    @endisset
    @isset($helpText)<small class="form-text text-muted">{{ $helpText }}</small>@endisset
    @foreach ($options as $key => $item)
    <div class="custom-control custom-radio">
        <input class="custom-control-input" type="radio"{{ $key == $value ? ' checked' : '' }} id="{{ $id }}{{ $key }}" value="{{ $key }}" name="{{ $name }}">
        <label for="{{ $id }}{{ $key }}" class="custom-control-label">{!! $item !!}</label>
    </div>
    @endforeach
</div>
