@props(['label', 'id' => uniqid(), 'require' => false, 'helpText', 'value' => true, 'name' => ''])

<div class="form-group">
    <div class="custom-control custom-checkbox">
        <input class="d-none" type="hidden" value="{{ (integer)!$value }}" name="{{ $name }}">
        <input class="custom-control-input" type="checkbox" id="{{ $id }}" value="{{ (integer)$value }}" name="{{ $name }}" {{ $attributes }}>
        @isset($label)
            <label for="{{ $id }}" class="custom-control-label">
                {{ $label }} @if($require)<span class="text-danger">*</span>@endif
            </label>
        @endisset
    </div>
    @isset($helpText)<small class="form-text text-muted">{{ $helpText }}</small>@endisset
</div>
