@extends('adminlte::page')

@section('title', $title ?? '')

@section('adminlte_css_pre')
    <!-- flag-icon-css -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.3.0/css/flag-icon.min.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content_header')
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1>{{ $title ?? '' }}</h1>
        </div>
        <div class="col-sm-6">
            <div class="float-sm-right">
                {{ Breadcrumbs::render() }}
            </div>
        </div>
    </div>
    @include('components.admin.alerts')
@stop

@section('adminlte_js')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script src="{{ asset('vendor/fileinput-ru.js') }}"></script>
    @stack('scripts')
@endsection
