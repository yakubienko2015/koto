@props(['active' => false, 'title', 'id' => uniqid()])

@php
    $old = old('_tab');
    $req = request()->get('_tab');
    $active = $old ? $old == $id : ($req ? $req == $id : $active);
@endphp

@push('pills')
    <li class="nav-item">
        <a class="nav-link{{ $active ? ' active' : '' }}" href="#{{ $id }}" data-toggle="tab"
            onclick="$('#tab-{{ $id }}').prop('checked', true)">{{ $title }}</a>
        <input id="tab-{{ $id }}"{{ $active ? ' checked' : '' }} type="radio" name="_tab" class="d-none" value="{{ $id }}">
    </li>
@endpush

<div class="tab-pane{{ $active ? ' active' : '' }}" id="{{ $id }}">
    {!! $slot !!}
</div>
