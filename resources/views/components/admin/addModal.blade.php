@props(['id' => uniqid(), 'name', 'buttonTitle' => 'Add', 'title' => 'New', 'cancelTitle' => 'Cancel'])

<button type="button" class="btn btn-info float-right" data-toggle="modal" data-target="#add{{ $id }}">
    <i class="fas fa-plus"></i> {{ $buttonTitle }}
</button>
<div class="modal fade" id="add{{ $id }}" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ $title }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {!! $slot !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ $cancelTitle }}</button>
                <button class="btn btn-primary" name="{{ $name }}" value="1">{{ $buttonTitle }}</button>
            </div>
        </div>
    </div>
</div>
