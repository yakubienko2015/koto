@extends('app.app')

@section('header')
    <h5 class="nav-item ml-auto mt-2">Модуль логиста</h5>
@endsection

@section('content')
    {{-- @include('app.navbar') --}}

    <logist-module
        :statuses='@json(config('shop.order_statuses'))'
        :status-variants='@json(config('shop.order_status_variants'))'
        :terminal-variants='@json(config('shop.terminal_status_variants'))'
        city-id={{ app('city')->id }}
    ></logist-module>
@endsection
