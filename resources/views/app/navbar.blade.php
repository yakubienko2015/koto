<nav class="navbar bg-light shadow-sm mb-3">
    <a class="navbar-brand p-0" href="#">
        <img loading="lazy" src="{{ asset('/images/logo.jpg') }}" height="39" class="d-inline-block align-top" alt="Logo">
    </a>

    @yield('header')

    @if(! ($hideCitySelect ?? false))
    <span class="nav-item ml-auto dropdown">
        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">
            {{ app('city')->name }}
        </a>
        <div class="dropdown-menu dropdown-menu-right">
            @foreach(app('city')->all() as $city)
                <a href="{{ route('cityIndex', $city) }}" class="dropdown-item{{ app('city')->id === $city->id ? ' active' : '' }}">
                    {{ $city->name }}
                </a>
            @endforeach
        </div>
    </span>
    @endif

    <span class="nav-link pl-2">
        {{ Auth::guard('app')->user()->name }}
        <a href="{{ route('app.logout') }}" class="pl-2" onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
            <i class="fas fa-power-off"></i>
        </a>
        <form id="logout-form" action="{{ route('app.logout') }}" method="POST" style="display: none;">
            @csrf
            <input type="hidden" name="work_place" value="{{ request()->workPlace ? request()->workPlace->id : '' }}">
        </form>
    </span>
</nav>
