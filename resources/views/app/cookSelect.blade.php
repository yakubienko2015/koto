@extends('app.app')

@section('header')
    <div class="nav-item ml-auto text-center">
        <h5 class="mb-0">Модуль повара</h5>
    </div>
@endsection

@section('content')
    @include('app.navbar')

    <div class="container">
        <div class="row align-items-center">
            @foreach ($terminals as $terminal)
                <div class="col text-center">
                    <h5>
                        <a href="{{ route('app.issucook', $terminal) }}">{{ $terminal->name }}</a>
                    </h5>
                    <form>
                        <input type="hidden" name="terminal" value="{{ $terminal->id }}">
                        <div class="form-group">
                            <label for="work-place{{ $terminal->id }}">Стол</label>
                            <select class="form-control" id="work-place{{ $terminal->id }}" name="work_place">
                                @foreach ($workPlaces->where('terminal_id', $terminal->id) as $workPlace)
                                    @php
                                        $usersIds = \App\Models\Timer::activeCookOnTerminalWorkPlace($terminal, $workPlace)->pluck('user_id');
                                    @endphp
                                    <option value="{{ $workPlace->id }}" {{ request()->cookie('last_work_place_id') == $workPlace->id ? 'selected' : '' }}>{{ $workPlace->name }}{{ count($usersIds) ? '(занято)' : '' }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="groups{{ $terminal->id }}">Группы</label>
                            <select multiple class="custom-select custom-select-lg select2groups" id="groups{{ $terminal->id }}" name="product_groups[]">
                                @foreach ($productGroups as $group)
                                    @php
                                        $usersIds = \App\Models\Timer::activeCookOnTerminalProductGroup($terminal, $group)->pluck('user_id');
                                    @endphp
                                    <option value="{{ $group->id }}">{{ $group->name }} {{ count($usersIds) ? '(занято)' : '' }}</option>
                                @endforeach
                            </select>
                        </div>
                        <button class="btn btn-success" type="submit">Перейти</button>
                    </form>
                </div>
            @endforeach
        </div>
    </div>
@endsection
