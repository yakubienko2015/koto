@extends('app.app')

@section('title', 'Login')

@section('content')
<div class="container h-100">

    <div class="row align-items-center h-100">
        <div class="col">
            <form action="{{ route('app.login') }}" class="login-form jumbotron bg-white mx-auto" method="POST">
                @csrf
                <div class="lockscreen-logo">
                    <a href="{{ app('city')->getIndexUrl() }}"><img src="{{ asset('images/logo.jpg') }}" alt="logo" class="logo img-fluid mx-auto d-block"></a>
                </div>
                <div class="input-group input-group-lg">
                    <div class="input-group-prepend">
                        <label class="input-group-text" for="pin">
                            <b>PiN</b>
                        </label>
                    </div>

                    <input id="pin" inputmode="numeric" type="password" name="pin" class="form-control bg-light @error('pin') is-invalid @enderror" readonly required autocomplete="off" autofocus onfocus="this.removeAttribute('readonly');">

                    <div class="input-group-append">
                        <button type="button" class="btn btn-secondary p-2" id="show-password" onclick="document.getElementById('pin').type = document.getElementById('pin').type == 'password' ? 'text' : 'password'"><i class="fas fa-eye"></i></button>
                        <button type="reset" class="btn btn-danger"><i class="fas fa-times"></i></button>
                        <button type="submit" class="btn btn-info"><i class="fas fa-arrow-right"></i></button>
                    </div>
                    @error('pin')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
            </form>
        </div>
    </div>
</div>
@stop
