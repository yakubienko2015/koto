@extends('app.app')

@section('header')
    <a class="nav-link" href="{{ route('app.admin', $terminal) }}">Админ</a>
    <h5 class="nav-item ml-auto mt-2">Модуль статистики</h5>
@endsection

@section('content')
    @include('app.navbar')

    <operator-module
        :statuses='@json(config('shop.order_statuses'))'
        :status-variants='@json(config('shop.order_status_variants'))'
        :terminal-variants='@json(config('shop.terminal_status_variants'))'
        city-id={{ app('city')->id }}
    ></operator-module>

    @if (Session::get('showWelcome'))
    <welcome-modal
        :comments='@json(auth('app')->user()->getWelcomeComments())'
        header="{{ 'Привет ' . auth('app')->user()->name }}"
    ></welcome-modal>
    @endif
@endsection
