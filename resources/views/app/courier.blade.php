@extends('app.app')

@section('header')
    <a class="nav-link btn btn-success" href="{{ route('app.courierCabinet') }}">В кабинет</a>
    <h5 class="nav-item mx-auto mt-2">Модуль курьера</h5>
@endsection

@section('content')
    {{-- @include('app.navbar', ['hideCitySelect' => true]) --}}

    <courier-module
        :statuses='@json(config('shop.order_statuses'))'
        :status-variants='@json(config('shop.order_status_variants'))'
        city-id={{ app('city')->id }}
    ></courier-module>

    @if (Session::get('showWelcome'))
    <welcome-modal
        :comments='@json(auth('app')->user()->getWelcomeComments())'
        header="{{ 'Привет ' . auth('app')->user()->name }}"
    ></welcome-modal>
    @endif
@endsection
