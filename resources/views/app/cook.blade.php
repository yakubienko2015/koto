@extends('app.app')

@section('header')
    <div class="nav-item mx-auto text-center">
        <h5 class="mb-0">Модуль повара</h5>
        <small>{{ $terminal->name }} - {{ $workPlace->name }}</small>
    </div>
@endsection

@section('content')
    {{-- @include('app.navbar', ['hideCitySelect' => true]) --}}

    <cook-module
        :statuses='@json(config('shop.order_statuses'))'
        :status-variants='@json(config('shop.order_status_variants'))'
        :terminal-variants='@json(config('shop.terminal_status_variants'))'
        city-id={{ app('city')->id }}
        :terminal='@json($terminal)'
        :work-place='@json($workPlace)'
        :time-for-rest='45'
        :groups='@json(request()->get('groups') ?? [])'
    ></cook-module>

    @if (Session::get('showWelcome'))
    <welcome-modal
        :comments='@json(auth('app')->user()->getWelcomeComments())'
        header="{{ 'Привет ' . auth('app')->user()->name }}"
    ></welcome-modal>
    @endif
@endsection
