@extends('app.app')

@section('header')
    <div class="nav-item ml-auto text-center">
        <h5 class="mb-0">Модуль повара выдачи</h5>
        <small>{{ $terminal->name }}</small>
    </div>
@endsection

@section('content')
    {{-- @include('app.navbar') --}}

    <issucook-module
        :statuses='@json(config('shop.order_statuses'))'
        :status-variants='@json(config('shop.order_status_variants'))'
        :terminal-variants='@json(config('shop.terminal_status_variants'))'
        city-id={{ app('city')->id }}
        :terminal='@json($terminal)'
        :work-places='@json($terminal->workPlaces)'
        :time-for-rest='45'
    ></issucook-module>

    @if (Session::get('showWelcome'))
    <welcome-modal
        :comments='@json(auth('app')->user()->getWelcomeComments())'
        header="{{ 'Привет ' . auth('app')->user()->name }}"
    ></welcome-modal>
    @endif
@endsection
