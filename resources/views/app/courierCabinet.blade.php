@extends('app.app')

@section('header')
    <h5 class="nav-item mx-auto mt-2">Модуль курьера</h5>
@endsection

@section('content')
    {{-- @include('app.navbar', ['hideCitySelect' => true]) --}}

    <div style="max-width: 450px" class="h5 mx-auto">
        <p class="p-3 w-50">
            <a class="nav-link btn btn-lg btn-pre-warning " href="{{ route('app.courier') }}">Назад</a>
        </p>

        <courier-cabinet-module
            :time-for-rest="45"
            city-id={{ app('city')->id }}
            :can-rest='@json(auth()->user()->workOrders()->whereStatus(\App\Models\Order::ORDER_STATUS_ON_WAY)->count() < 1)'
        ></courier-cabinet-module>

        <p class="p-3">
            всего доставок: {{ auth()->user()->workOrders()->whereDate('ordered_at', now()->format('Y-m-d'))->count() }}<br>
            закрыто доставок: {{ auth()->user()->workOrders()->whereDate('ordered_at', now()->format('Y-m-d'))->whereStatus(\App\Models\Order::ORDER_STATUS_CLOSED)->count() }}<br>
            {{-- не сделано доставок: {{ 0 }}<br> --}}
            <br>
            {{-- накоплено наличными: {{ auth()->user()->workOrders()->where('status', '<>', \App\Models\Order::ORDER_STATUS_CLOSED)->sum('sum') }} грн за заказы<br> --}}
        </p>
    </div>

    <table class="table table-bordered mx-auto p-3" style="max-width: 450px">
        @foreach (auth()->user()->workOrders()->whereDate('ordered_at', now())->orderBy('ordered_at', 'desc')->get() as $order)
            <tr>
                <td>
                    ул. {{ $order->data['address']['street'] }}
                    д. {{ $order->data['address']['home'] }}
                    кв. {{ $order->data['address']['apartment'] }}<br>
                    клиент {{ $order->customer_name }}<br>
                    сумма {{ $order->sum }} грн. {{ $order->payment_type == 'liqpay' ? 'САЙТ' : 'НАЛ' }}
                </td>
                <td class="text-right">
                    {{ $order->ordered_at->format('H:i') }}<br>
                    №{{ $order->number }}<br>
                    {{ $order->phone }}
                </td>
            </tr>
        @endforeach
    </table>

    <div class="text-center">
        <a class="btn btn-success btn-lg mx-auto my-3" href="https://docs.google.com/document/d/1uLEe_i3sk8TE3SonJIPPR_mfpu960xLiOufy1dp9_m0/edit?usp=sharing">правила работы</a>
    </div>

    <form class="text-center w-100 d-block pb-3" action="{{ route('app.logoutCourier') }}" method="POST">
        @csrf
        @method('PUT')

        <button type="submit" class="btn btn-pre-warning btn-lg">закончить смену</button>
    </form>
@endsection
