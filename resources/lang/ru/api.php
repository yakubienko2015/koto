<?php

return array (
  'callback_sended' => 'Калбек отправлен.',
  'empty' => 'Пусто',
  'error' => 'Ошибка',
  'exception_error' => 'Похоже вы нашли какую-то внутреннюю ошибку на сайте. Расскажите нам о ней. 
Заказ можно оформить по телефону:
0986689999',
  'exceptions' => 
  array (
    200 => 'Успешно',
    400 => 'Ошибка',
    401 => 'Неразрешено',
    404 => 'Не найдено',
    422 => 'Ошибка валидации',
    501 => 'Ошибка в iiko',
  ),
  'favorite_addresses_limit' => 'Можно только 5 адресов!',
  'message_sended' => 'Сообщения отправлено',
  'not_found' => 'Не найдено',
  'order' => 
  array (
    'product_not_found' => 'Товар ":item" не найдено.',
  ),
  'promocode_activated' => 'Промокод уже активирован!',
  'promocode_not_found' => 'Промокод не найден!',
  'success' => 'Успешно',
);
