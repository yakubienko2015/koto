<?php

return array (
  'accepted' => 'Вы должны принять :attribute.',
  'active_url' => 'Поле :attribute содержит недействительный URL.',
  'after' => 'В поле :attribute должна быть дата после :date.',
  'after_or_equal' => 'В поле :attribute должна быть дата после или равняться :date.',
  'alpha' => 'Поле :attribute может содержать только буквы.',
  'alpha_dash' => 'Поле :attribute может содержать только буквы, цифры, дефис и нижнее подчеркивание.',
  'alpha_num' => 'Поле :attribute может содержать только буквы и цифры.',
  'array' => 'Поле :attribute должно быть массивом.',
  'attributes' =>
  array (
    'address.street' => 'Улица',
    'address' => 'Адрес',
    'age' => 'Возраст',
    'available' => 'Доступно',
    'city' => 'Город',
    'content' => 'Контент',
    'country' => 'Страна',
    'date' => 'Дата',
    'day' => 'День',
    'description' => 'Описание',
    'email' => 'E-Mail адрес',
    'excerpt' => 'Выдержка',
    'first_name' => 'Имя',
    'gender' => 'Пол',
    'hour' => 'Час',
    'last_name' => 'Фамилия',
    'minute' => 'Минута',
    'mobile' => 'Моб. номер',
    'month' => 'Месяц',
    'name' => 'Имя',
    'password' => 'Пароль',
    'password_confirmation' => 'Подтверждение пароля',
    'phone' => 'Телефон',
    'second' => 'Секунда',
    'sex' => 'Пол',
    'size' => 'Размер',
    'time' => 'Время',
    'title' => 'Наименование',
    'username' => 'Номер телефона',
    'year' => 'Год',
  ),
  'before' => 'В поле :attribute должна быть дата до :date.',
  'before_or_equal' => 'В поле :attribute должна быть дата до или равняться :date.',
  'between' =>
  array (
    'array' => 'Количество элементов в поле :attribute должно быть между :min и :max.',
    'file' => 'Размер файла в поле :attribute должен быть между :min и :max Килобайт(а).',
    'numeric' => 'Поле :attribute должно быть между :min и :max.',
    'string' => 'Количество символов в поле :attribute должно быть между :min и :max.',
  ),
  'boolean' => 'Поле :attribute должно иметь значение логического типа.',
  'check_address' => 'Ваш адрес, находится за зоной доставки,',
  'check_address_' => 'обратитесь в кол центр.',
  'confirmed' => 'Поле :attribute не совпадает с подтверждением.',
  'custom' =>
  array (
    'address' =>
    array (
      'home' =>
      array (
        'required' => 'Поле "Дом" обязательно для заполнения',
        'required_if' => 'Поле "Дом" обязательно для заполнения',
      ),
      'street' =>
      array (
        'required' => 'Поле "Улица" обязательно для заполнения',
        'required_if' => 'Поле "Улица" обязательно для заполнения',
      ),
    ),
    'birthday' =>
    array (
      'before' => 'Вы должны быть старше 16 лет.',
    ),
    'order' =>
    array (
      'fullSum' =>
      array (
        'gte' => 'Минимальная сумма заказа :value грн.',
      ),
      'promoSum' =>
      array (
        'gte' => 'Вы используете промокод :promo. Минимальная сумма для подарка :value грн.',
      ),
      'sum' =>
      array (
        'gte' => 'Минимальная сумма заказа :value грн.',
      ),
    ),
    'time' =>
    array (
      'after' => 'Предзаказ можно оформить за минимум два часа до времени доставки.',
    ),
  ),
  'date' => 'Поле :attribute не является датой.',
  'date_equals' => 'Поле :attribute должно быть датой равной :date.',
  'date_format' => 'Поле :attribute не соответствует формату :format.',
  'different' => 'Поля :attribute и :other должны различаться.',
  'digits' => 'Длина цифрового поля :attribute должна быть :digits.',
  'digits_between' => 'Длина цифрового поля :attribute должна быть между :min и :max.',
  'dimensions' => 'Поле :attribute имеет недопустимые размеры изображения.',
  'distinct' => 'Поле :attribute содержит повторяющееся значение.',
  'email' => 'Поле :attribute должно быть действительным электронным адресом.',
  'ends_with' => 'Поле :attribute должно заканчиваться одним из следующих значений: :values',
  'exists' => 'Выбранное значение для :attribute некорректно.',
  'file' => 'Поле :attribute должно быть файлом.',
  'filled' => 'Поле :attribute обязательно для заполнения.',
  'gt' =>
  array (
    'array' => 'Количество элементов в поле :attribute должно быть больше :value.',
    'file' => 'Размер файла в поле :attribute должен быть больше :value Килобайт(а).',
    'numeric' => 'Поле :attribute должно быть больше :value.',
    'string' => 'Количество символов в поле :attribute должно быть больше :value.',
  ),
  'gte' =>
  array (
    'array' => 'Количество элементов в поле :attribute должно быть больше или равно :value.',
    'file' => 'Размер файла в поле :attribute должен быть больше или равен :value Килобайт(а).',
    'numeric' => 'Поле :attribute должно быть больше или равно :value.',
    'string' => 'Количество символов в поле :attribute должно быть больше или равно :value.',
  ),
  'have_not_enough_bonuses' => 'Недостаточно бонусов',
  'image' => 'Поле :attribute должно быть изображением.',
  'in' => 'Выбранное значение для :attribute ошибочно.',
  'in_array' => 'Поле :attribute не существует в :other.',
  'in_work_time' => 'Время на которое ты хочешь оформить заказ, уже кто-то занял. Выбери другое.',
  'integer' => 'Поле :attribute должно быть целым числом.',
  'invalid_promo' => 'Неправильный промокод.',
  'invalid_promo_in_basket' => 'Для этого промокода нет соответствующего товара в корзине.',
  'ip' => 'Поле :attribute должно быть действительным IP-адресом.',
  'ipv4' => 'Поле :attribute должно быть действительным IPv4-адресом.',
  'ipv6' => 'Поле :attribute должно быть действительным IPv6-адресом.',
  'is_not_holiday' => 'Основа нашей кухни это чистота, поэтому сегодня мы проводим генеральную уборку и тренинг для персонала. Спасибо тебе:)',
  'is_overloaded' => 'Мы перегружены, но ты можешь оформить предзаказ:)',
  'json' => 'Поле :attribute должно быть JSON строкой.',
  'lt' =>
  array (
    'array' => 'Количество элементов в поле :attribute должно быть меньше :value.',
    'file' => 'Размер файла в поле :attribute должен быть меньше :value Килобайт(а).',
    'numeric' => 'Поле :attribute должно быть меньше :value.',
    'string' => 'Количество символов в поле :attribute должно быть меньше :value.',
  ),
  'lte' =>
  array (
    'array' => 'Количество элементов в поле :attribute должно быть меньше или равно :value.',
    'file' => 'Размер файла в поле :attribute должен быть меньше или равен :value Килобайт(а).',
    'numeric' => 'Поле :attribute должно быть меньше или равно :value.',
    'string' => 'Количество символов в поле :attribute должно быть меньше или равно :value.',
  ),
  'max' =>
  array (
    'array' => 'Количество элементов в поле :attribute не может превышать :max.',
    'file' => 'Размер файла в поле :attribute не может быть более :max Килобайт(а).',
    'numeric' => 'Поле :attribute не может быть более :max.',
    'string' => 'Количество символов в поле :attribute не может превышать :max.',
  ),
  'mimes' => 'Поле :attribute должно быть файлом одного из следующих типов: :values.',
  'mimetypes' => 'Поле :attribute должно быть файлом одного из следующих типов: :values.',
  'min' =>
  array (
    'array' => 'Количество элементов в поле :attribute должно быть не менее :min.',
    'file' => 'Размер файла в поле :attribute должен быть не менее :min Килобайт(а).',
    'numeric' => 'Поле :attribute должно быть не менее :min.',
    'string' => 'Количество символов в поле :attribute должно быть не менее :min.',
  ),
  'not_in' => 'Выбранное значение для :attribute ошибочно.',
  'not_regex' => 'Выбранный формат для :attribute ошибочный.',
  'numeric' => 'Поле :attribute должно быть числом.',
  'password' => 'Неверный пароль.',
  'present' => 'Поле :attribute должно присутствовать.',
  'regex' => 'Поле :attribute имеет ошибочный формат.',
  'required' => 'Поле :attribute обязательно для заполнения.',
  'required_if' => 'Поле :attribute обязательно для заполнения, когда :other равно :value.',
  'required_unless' => 'Поле :attribute обязательно для заполнения, когда :other не равно :values.',
  'required_with' => 'Поле :attribute обязательно для заполнения, когда :values указано.',
  'required_with_all' => 'Поле :attribute обязательно для заполнения, когда :values указано.',
  'required_without' => 'Поле :attribute обязательно для заполнения, когда :values не указано.',
  'required_without_all' => 'Поле :attribute обязательно для заполнения, когда ни одно из :values не указано.',
  'same' => 'Значения полей :attribute и :other должны совпадать.',
  'size' =>
  array (
    'array' => 'Количество элементов в поле :attribute должно быть равным :size.',
    'file' => 'Размер файла в поле :attribute должен быть равен :size Килобайт(а).',
    'numeric' => 'Поле :attribute должно быть равным :size.',
    'string' => 'Количество символов в поле :attribute должно быть равным :size.',
  ),
  'starts_with' => 'Поле :attribute должно начинаться из одного из следующих значений: :values',
  'string' => 'Поле :attribute должно быть строкой.',
  'timezone' => 'Поле :attribute должно быть действительным часовым поясом.',
  'unique' => 'Такое значение поля :attribute уже существует.',
  'uploaded' => 'Загрузка поля :attribute не удалась.',
  'url' => 'Поле :attribute имеет ошибочный формат.',
  'uuid' => 'Поле :attribute должно быть корректным UUID.',
);
