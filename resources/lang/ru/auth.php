<?php

return array (
  'code' => 
  array (
    'confirmed' => 'Номер уже подтвержден',
    'fail' => 'Ошибка отправки',
    'no_phone' => 'Номер не найден',
    'sended' => 'Код отправлен',
    'success' => 'Номер подтвержден успешно',
    'wrong_code' => 'Неверный код',
    'wrong_phone' => 'Неправильный телефон',
  ),
  'failed' => 'Не правильно введены данные',
  'failed_pin' => 'PiN не найдено',
  'logined' => 'Успешно',
  'logouted' => 'Успешно',
  'registered_complate' => 'Успешно',
  'sms' => 
  array (
    'no_confirmed' => 'Код не подтверждено',
  ),
  'throttle' => 'Слишком много попыток входа. Пожалуйста, попробуйте еще раз через :seconds секунд.',
  'unauthorized' => 'Неразрешено',
);
